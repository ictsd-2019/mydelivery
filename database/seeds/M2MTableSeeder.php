<?php

use Illuminate\Database\Seeder;

class M2MTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m2ms')->insert(['name' => 'DCP MIM']);
        DB::table('m2ms')->insert(['name' => 'DCP Non MIM']);
        DB::table('m2ms')->insert(['name' => 'Non DCP MIM']);
        DB::table('m2ms')->insert(['name' => 'Non DCP Non MIM']);
        DB::table('m2ms')->insert(['name' => 'Non DCP']);
        DB::table('m2ms')->insert(['name' => 'DCP']);
        DB::table('m2ms')->insert(['name' => 'MIM NON DCP']);
    }
}
