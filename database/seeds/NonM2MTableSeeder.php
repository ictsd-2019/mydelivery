<?php

use Illuminate\Database\Seeder;

class NonM2MTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('non_m2ms')->insert(['name' => 'Combo']);
        DB::table('non_m2ms')->insert(['name' => 'Multi']);
        DB::table('non_m2ms')->insert(['name' => 'Reguler']);
        DB::table('non_m2ms')->insert(['name' => 'Replacement HLR']);
        DB::table('non_m2ms')->insert(['name' => 'Mikro']);
        DB::table('non_m2ms')->insert(['name' => 'Makro']);
    }
}
