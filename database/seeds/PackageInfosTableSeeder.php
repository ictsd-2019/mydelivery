<?php

use Illuminate\Database\Seeder;

class PackageInfosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('package_infos')->insert(['package' => 'Business Call Center 50K', 'price'=> '45,455']);
		DB::table('package_infos')->insert(['package' => 'Business Prime 100K', 'price'=> '90,909']);
		DB::table('package_infos')->insert(['package' => 'Business Pro Call Center 250K', 'price'=> '227,273']);
		DB::table('package_infos')->insert(['package' => 'Business Pro Call Center 500K', 'price'=> '454,545']);
		DB::table('package_infos')->insert(['package' => 'CUG UKM - Mandiri 25K', 'price'=> '22,727']);
		DB::table('package_infos')->insert(['package' => 'CUG UKM - Mandiri 45K', 'price'=> '40,909']);
		DB::table('package_infos')->insert(['package' => 'CUG UKM - Mandiri 95K', 'price'=> '86,364']);
		DB::table('package_infos')->insert(['package' => 'CUG UKM Mandiri 25K', 'price'=> '22,727']);
		DB::table('package_infos')->insert(['package' => 'CUG UKM Mandiri 45K', 'price'=> '40,909']);
		DB::table('package_infos')->insert(['package' => 'CUG UKM Mandiri 95K', 'price'=> '86,364']);
		DB::table('package_infos')->insert(['package' => 'Grand Data 20GB', 'price'=> '160']);
		DB::table('package_infos')->insert(['package' => 'Indosat Business 150K Call Center', 'price'=> '136,364']);
		DB::table('package_infos')->insert(['package' => 'Indosat Business M2M 25K Syresnet', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'Indosat Business M2M EDC 20K', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'Indosat Business M2M EDC GPRS II', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'Indosat Business M2M EDC USSD', 'price'=> '13']);
		DB::table('package_infos')->insert(['package' => 'Indosat Business M2M FM 20K', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'Indosat Business M2M FM 20K Generic', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'Indosat Business M2M FM 25K', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'Indosat Business M2M FM 25K Generic', 'price'=> '22,727']);
		DB::table('package_infos')->insert(['package' => 'Indosat Business M2M FM SMS 20K Generic', 'price'=> '20']);
		DB::table('package_infos')->insert(['package' => 'Indosat Business M2M FM SMS 25K Generic', 'price'=> '25']);
		DB::table('package_infos')->insert(['package' => 'Indosat Business M2M ST 10K', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'Indosat Business M2M ST 15K', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'Indosat Business M2M VTS 25K', 'price'=> '25']);
		DB::table('package_infos')->insert(['package' => 'Indosat Business PRO', 'price'=> '90,909']);
		DB::table('package_infos')->insert(['package' => 'Indosat Business PRO Bundling', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'Indosat Business VPN 200K Primacom', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'Indosat Mobile - CUG 50K NCS', 'price'=> '45,455']);
		DB::table('package_infos')->insert(['package' => 'Indosat Mobile Bulanan - Business', 'price'=> '45,455']);
		DB::table('package_infos')->insert(['package' => 'Indosat Mobile CUG 100K NCS', 'price'=> '90,909']);
		DB::table('package_infos')->insert(['package' => 'Indosat Mobile CUG 50K NCS', 'price'=> '45,455']);
		DB::table('package_infos')->insert(['package' => 'Indosat Mobile CUG 75K NCS', 'price'=> '68,182']);
		DB::table('package_infos')->insert(['package' => 'Indosat Mobile Free Abonemen - Business', 'price'=> '22,727']);
		DB::table('package_infos')->insert(['package' => 'Indosat Mobile Pro Bulanan24', 'price'=> '90,909']);
		DB::table('package_infos')->insert(['package' => 'Indosat Mobile VPN 25K EDC CIMB Niaga', 'price'=> '22,727']);
		DB::table('package_infos')->insert(['package' => 'Indosat Mobile VPN GPRS Listrindo', 'price'=> '22,727']);
		DB::table('package_infos')->insert(['package' => 'Indosat Pro Smart 119', 'price'=> '108,182']);
		DB::table('package_infos')->insert(['package' => 'Indosat Pro Smart 119', 'price'=> '108,182']);
		DB::table('package_infos')->insert(['package' => 'Indosat Pro Smart 149 Lite', 'price'=> '135,455']);
		DB::table('package_infos')->insert(['package' => 'Indosat Pro Smart 175', 'price'=> '159,091']);
		DB::table('package_infos')->insert(['package' => 'Indosat Pro Smart 25', 'price'=> '22,727']);
		DB::table('package_infos')->insert(['package' => 'Indosat Pro Smart 49 Lite', 'price'=> '44,545']);
		DB::table('package_infos')->insert(['package' => 'Indosat Pro Smart 49K Lite', 'price'=> '44,545']);
		DB::table('package_infos')->insert(['package' => 'Indosat Pro Smart 65', 'price'=> '59,091']);
		DB::table('package_infos')->insert(['package' => 'Indosat Pro Smart 65K', 'price'=> '59,091']);
		DB::table('package_infos')->insert(['package' => 'Indosat Pro Smart 79 Lite', 'price'=> '71,818']);
		DB::table('package_infos')->insert(['package' => 'Indosat Pro Smart 99', 'price'=> '90']);
		DB::table('package_infos')->insert(['package' => 'Indosat Pro Smart 99 Lite', 'price'=> '90']);
		DB::table('package_infos')->insert(['package' => 'Indosat Pro Talk 119', 'price'=> '108,182']);
		DB::table('package_infos')->insert(['package' => 'Indosat Pro Talk 149 Lite', 'price'=> '135,455']);
		DB::table('package_infos')->insert(['package' => 'Indosat Pro Talk 175', 'price'=> '159,091']);
		DB::table('package_infos')->insert(['package' => 'Indosat Pro Talk 49 Lite', 'price'=> '44,545']);
		DB::table('package_infos')->insert(['package' => 'Indosat Pro Talk 65', 'price'=> '59,091']);
		DB::table('package_infos')->insert(['package' => 'Indosat Pro Talk 79 Lite', 'price'=> '71,818']);
		DB::table('package_infos')->insert(['package' => 'Indosat Pro Talk 99', 'price'=> '90']);
		DB::table('package_infos')->insert(['package' => 'Indosat Pro Talk 99 Lite', 'price'=> '90']);
		DB::table('package_infos')->insert(['package' => 'INDOSATBUSINESS VehicleTelematics Bundle', 'price'=> '160']);
		DB::table('package_infos')->insert(['package' => 'M2M AMR CSD', 'price'=> '22,727']);
		DB::table('package_infos')->insert(['package' => 'M2M Freedom Generic Paket A', 'price'=> '25']);
		DB::table('package_infos')->insert(['package' => 'M2M Freedom Generic Paket B', 'price'=> '49']);
		DB::table('package_infos')->insert(['package' => 'M2M Freedom Generic Paket C', 'price'=> '90']);
		DB::table('package_infos')->insert(['package' => 'M2M Freedom Generic Paket D', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'M2M Freedom Generic Paket E', 'price'=> '299']);
		DB::table('package_infos')->insert(['package' => 'M2M VPN Quota Generic Data 1 GB', 'price'=> '100']);
		DB::table('package_infos')->insert(['package' => 'M2M VPN Quota Generic Data 10 GB', 'price'=> '500']);
		DB::table('package_infos')->insert(['package' => 'M2M VPN Quota Generic Data 2 GB', 'price'=> '150']);
		DB::table('package_infos')->insert(['package' => 'M2M VPN Quota Generic Data 3 GB', 'price'=> '200']);
		DB::table('package_infos')->insert(['package' => 'M2M VPN Quota Generic Data 5 GB', 'price'=> '300']);
		DB::table('package_infos')->insert(['package' => 'M2M VPN Quota Generic Data 8 GB', 'price'=> '450']);
		DB::table('package_infos')->insert(['package' => 'M2M VPN Unlimited Generic Data 2 GB', 'price'=> '200']);
		DB::table('package_infos')->insert(['package' => 'M2M VPN Unlimited Generic Data 3 GB', 'price'=> '250']);
		DB::table('package_infos')->insert(['package' => 'M2M VPN Unlimited Generic Data 5 GB', 'price'=> '350']);
		DB::table('package_infos')->insert(['package' => 'Matrix BIZ Lite', 'price'=> '22,727']);
		DB::table('package_infos')->insert(['package' => 'Matrix BIZ Plus', 'price'=> '45,455']);
		DB::table('package_infos')->insert(['package' => 'Matrix BIZ Plus', 'price'=> '45,455']);
		DB::table('package_infos')->insert(['package' => 'Matrix BIZ Premium', 'price'=> '90']);
		DB::table('package_infos')->insert(['package' => 'Matrix Business Lite', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'Matrix Business Plus', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'Matrix Business Premium', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'Matrix Super Combo', 'price'=> '45,455']);
		DB::table('package_infos')->insert(['package' => 'Matrix Super Gadget', 'price'=> '90,909']);
		DB::table('package_infos')->insert(['package' => 'MTX GPRS EDC Bank Mandiri', 'price'=> '25']);
		DB::table('package_infos')->insert(['package' => 'MTX GPRS EDC BCA (25K)', 'price'=> '25']);
		DB::table('package_infos')->insert(['package' => 'MTX GPRS EDC BNI', 'price'=> '22,727']);
		DB::table('package_infos')->insert(['package' => 'MTX M2M EDC 15K BPRKS', 'price'=> '15']);
		DB::table('package_infos')->insert(['package' => 'MTX M2M EDC 20K', 'price'=> '20']);
		DB::table('package_infos')->insert(['package' => 'MTX M2M EDC 20K Bank Mayora', 'price'=> '20']);
		DB::table('package_infos')->insert(['package' => 'Pro Freedom Basic', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'Pro Freedom Bronze', 'price'=> '44,545']);
		DB::table('package_infos')->insert(['package' => 'PRO Freedom Bundling (Gold12)', 'price'=> '144,545']);
		DB::table('package_infos')->insert(['package' => 'PRO Freedom Bundling (Gold24)', 'price'=> '90']);
		DB::table('package_infos')->insert(['package' => 'PRO Freedom Bundling (Infinite12)', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'PRO Freedom Bundling (Infinite24)', 'price'=> '181,818']);
		DB::table('package_infos')->insert(['package' => 'PRO Freedom Bundling (Platinum12)', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'PRO Freedom Bundling (Platinum24)', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'PRO Freedom Bundling (Signature12)', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'PRO Freedom Bundling (Signature24)', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'Pro Freedom Gold', 'price'=> '180,909']);
		DB::table('package_infos')->insert(['package' => 'Pro Freedom Platinum', 'price'=> '271,818']);
		DB::table('package_infos')->insert(['package' => 'Pro Freedom Silver', 'price'=> '90']);
		DB::table('package_infos')->insert(['package' => 'Pro Freedom Voice', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'PRO Mobile Solution Gold 12', 'price'=> '353,637']);
		DB::table('package_infos')->insert(['package' => 'PRO Mobile Solution Gold 24', 'price'=> '244,546']);
		DB::table('package_infos')->insert(['package' => 'PRO Mobile Solution Infinite 12', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'PRO Mobile Solution Infinite 24', 'price'=> '272,728']);
		DB::table('package_infos')->insert(['package' => 'PRO Mobile Solution Platinum 12', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'PRO Mobile Solution Platinum 24', 'price'=> '181,819']);
		DB::table('package_infos')->insert(['package' => 'PRO Mobile Solution Signature 12', 'price'=> '227,274']);
		DB::table('package_infos')->insert(['package' => 'PRO Mobile Solution Signature 24', 'price'=> '227,274']);
		DB::table('package_infos')->insert(['package' => 'Business JNE 50K', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'Paket SIM Dependent 170 Mb per Month', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'M2M Managed Connectivity', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'M2M Managed Connectivity ( PLN AMR ICON )', 'price'=> '22,5']);
		DB::table('package_infos')->insert(['package' => 'M2M Managed Connectivity ( PT. Solusi Media Semesta )', 'price'=> '15']);
		DB::table('package_infos')->insert(['package' => 'M2M Managed Connectivity ( PT. GCI Indonesia - ZTE Indonesia )', 'price'=> '50']);
		DB::table('package_infos')->insert(['package' => 'M2M Managed Connectivity ( PT Huawei Tech Investmnet )', 'price'=> '55']);
		DB::table('package_infos')->insert(['package' => 'M2M Managed Connectivity ( PT. PLN AREA SURABAYA SELATAN )', 'price'=> '25']);
		DB::table('package_infos')->insert(['package' => 'M2M Managed Connectivity ( PT SAYAP MAS UTAMA )', 'price'=> '15']);
		DB::table('package_infos')->insert(['package' => 'M2M Managed Connectivity ( Mitra Transaksi Indonesia )', 'price'=> '21']);
		DB::table('package_infos')->insert(['package' => 'M2M Managed Connectivity ( PT. Andalas Mitra Primaindo )', 'price'=> '15']);
		DB::table('package_infos')->insert(['package' => 'M2M Managed Connectivity ( PLN (Persero) APD Bali )', 'price'=> '20']);
		DB::table('package_infos')->insert(['package' => 'M2M Managed Connectivity ( PT CIPTA SRIGATI LESTARI )', 'price'=> '15']);
		DB::table('package_infos')->insert(['package' => 'M2M Managed Connectivity ( PT DASSAGA KOMUNIKASI )', 'price'=> '10']);
		DB::table('package_infos')->insert(['package' => 'M2M Managed Connectivity ( PT TORNADO TEKNOLOGI VENTURA )', 'price'=> '10']);
		DB::table('package_infos')->insert(['package' => 'M2M Managed Connectivity ( PT Solusi Integrasi Transportasi Utama (SITU) )', 'price'=> '25']);
		DB::table('package_infos')->insert(['package' => 'Paket SIM Dependent 170 Mb per Month', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'Pro Freedom Biz 25K', 'price'=> '22,727']);
		DB::table('package_infos')->insert(['package' => 'Pro Freedom Biz Bronze 45K', 'price'=> '40,909']);
		DB::table('package_infos')->insert(['package' => 'Pro Freedom Biz Silver 90K', 'price'=> '81,818']);
		DB::table('package_infos')->insert(['package' => 'Indosat Business M2M EDC GPRS', 'price'=> '0']);
		DB::table('package_infos')->insert(['package' => 'M2M VPN Quota Generic Data 5 GB Rp.113.000 Abo', 'price'=> '102,727']);

    }
}
