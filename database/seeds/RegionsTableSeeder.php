<?php

use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('regions')->insert(['name' => 'Media Energy', 'category' => 'offline']);
        DB::table('regions')->insert(['name' => 'Government And Public Sector', 'category' => 'offline']);
        DB::table('regions')->insert(['name' => 'Supplychain and Manufacture', 'category' => 'offline']);
        DB::table('regions')->insert(['name' => 'New Service Account', 'category' => 'offline']);
        DB::table('regions')->insert(['name' => 'Financial Institution', 'category' => 'offline']);
        DB::table('regions')->insert(['name' => 'Enterprise Campaign & Distribution', 'category' => 'offline']);
        DB::table('regions')->insert(['name' => 'Jakarta', 'category' => 'offline']);
        DB::table('regions')->insert(['name' => 'Sumatera', 'category' => 'offline']);
        DB::table('regions')->insert(['name' => 'EJBN & Kalisulamapa', 'category' => 'offline']);
        DB::table('regions')->insert(['name' => 'Indirect Channel', 'category' => 'offline']);
        DB::table('regions')->insert(['name' => 'West And Central Java', 'category' => 'offline']);

    }
}
