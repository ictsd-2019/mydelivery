<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(['name' => 'Administrator', 'guard_name' => 'Administrator', 'landing_page' => '/profile', 'remarks' => 'Super user for Web Admin']);
        DB::table('roles')->insert(['name' => 'Supervisor', 'guard_name' => 'Supervisor', 'landing_page' => '/profile', 'remarks' => 'Admin to supervise and oversee user activity']);
        DB::table('roles')->insert(['name' => 'Management', 'guard_name' => 'Management', 'landing_page' => '/profile', 'remarks' => 'Management to oversee business process']);
        DB::table('roles')->insert(['name' => 'Disabled', 'guard_name' => 'Disabled', 'landing_page' => '/suspended', 'remarks' => 'Suspend user']);
        DB::table('roles')->insert(['name' => 'Activation Manager', 'guard_name' => 'Activation Manager', 'landing_page' => '/activation/manage', 'remarks' => 'Manage and oversee business process of ICT SD']);
        DB::table('roles')->insert(['name' => 'Activation Report', 'guard_name' => 'Activation Report', 'landing_page' => '/activation/manage', 'remarks' => 'Manage and execute activation and delivery']);
        DB::table('roles')->insert(['name' => 'Sales Manager', 'guard_name' => 'Sales Manager', 'landing_page' => '/profile', 'remarks' => 'Manage and oversee business process of Sales']);
        DB::table('roles')->insert(['name' => 'Sales Account', 'guard_name' => 'Sales Account', 'landing_page' => '/order/manage', 'remarks' => 'Manage and generate order']);
        DB::table('roles')->insert(['name' => 'Sales Admin', 'guard_name' => 'Sales Admin', 'landing_page' => '/order/manage', 'remarks' => 'Manage and generate order']);
        DB::table('roles')->insert(['name' => 'RM Manager', 'guard_name' => 'RM Manager', 'landing_page' => '/preorder/manage', 'remarks' => 'Manage and oversee business process of RM']);
        DB::table('roles')->insert(['name' => 'RM Officer', 'guard_name' => 'RM Officer', 'landing_page' => '/preorder/manage', 'remarks' => 'Manage and coordinate SIM/Device supply']);
        DB::table('roles')->insert(['name' => 'Product Manager', 'guard_name' => 'Product Manager', 'landing_page' => '/product/manage', 'remarks' => 'Manage and oversee business process of product']);
        DB::table('roles')->insert(['name' => 'Product Officer', 'guard_name' => 'Product Officer', 'landing_page' => '/product/manage', 'remarks' => 'Manage and create product catalog']);
        DB::table('roles')->insert(['name' => 'Product Distributor', 'guard_name' => 'Product Distributor', 'landing_page' => '/web', 'remarks' => 'Manage and Update stock status']);
        DB::table('roles')->insert(['name' => 'Verification Manager', 'guard_name' => 'Verification Manager', 'landing_page' => '/verification/manage', 'remarks' => 'Manage and oversee business process of RCA']);
        DB::table('roles')->insert(['name' => 'Verification Officer', 'guard_name' => 'Verification Officer', 'landing_page' => '/verification/manage', 'remarks' => 'Manage and approve verification process']);
        DB::table('roles')->insert(['name' => 'Courier Manager', 'guard_name' => 'Courier Manager', 'landing_page' => '/courier/manage', 'remarks' => 'Manage and oversee business process of Courier']);
        DB::table('roles')->insert(['name' => 'Courier Agent', 'guard_name' => 'Courier Agent', 'landing_page' => '/courier/manage', 'remarks' => 'Manage and deliver delivery request']);
        DB::table('roles')->insert(['name' => 'Trainee', 'guard_name' => 'Trainee', 'landing_page' => '/web/user_assessment/profile', 'remarks' => 'For Training and Demo Purpose']);
    }
}
