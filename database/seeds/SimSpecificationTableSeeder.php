<?php

use Illuminate\Database\Seeder;

class SimSpecificationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sim_specifications')->insert(['name' => '2G']);
        DB::table('sim_specifications')->insert(['name' => '3G']);
        DB::table('sim_specifications')->insert(['name' => '4G']);
    }
}
