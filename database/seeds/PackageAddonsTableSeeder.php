<?php

use Illuminate\Database\Seeder;

class PackageAddonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('package_addons')->insert(['addon' => 'Call Offnet 130k ( 600 menit )', 'price'=> '118,182']);
		DB::table('package_addons')->insert(['addon' => 'Call Offnet 40k ( 150 menit )', 'price'=> '36,364']);
		DB::table('package_addons')->insert(['addon' => 'Call Offnet 80k ( 325 menit )', 'price'=> '72,727']);
		DB::table('package_addons')->insert(['addon' => 'Call Onnet 10k ( 250 menit )', 'price'=> '9,091']);
		DB::table('package_addons')->insert(['addon' => 'Call Onnet 20k ( 1000 menit )', 'price'=> '18,182']);
		DB::table('package_addons')->insert(['addon' => 'Call Onnet 40k ( 5000 menit )', 'price'=> '36,364']);
		DB::table('package_addons')->insert(['addon' => 'Data 10GB', 'price'=> '118,182']);
		DB::table('package_addons')->insert(['addon' => 'Data 2GB', 'price'=> '36,364']);
		DB::table('package_addons')->insert(['addon' => 'Data 6GB', 'price'=> '72,727']);
		DB::table('package_addons')->insert(['addon' => 'Detail Billing', 'price'=> '9,091']);
		DB::table('package_addons')->insert(['addon' => 'DUO', 'price'=> '22,727']);
		DB::table('package_addons')->insert(['addon' => 'IBD Quota 17.5GB ( 259K )', 'price'=> '235,455']);
		DB::table('package_addons')->insert(['addon' => 'IBD Quota 1GB ( 29K )', 'price'=> '26,364']);
		DB::table('package_addons')->insert(['addon' => 'IBD Quota 25GB ( 399K )', 'price'=> '362,727']);
		DB::table('package_addons')->insert(['addon' => 'IBD Quota 2GB ( 49K )', 'price'=> '44,545']);
		DB::table('package_addons')->insert(['addon' => 'IBD Quota 5GB ( 99K )', 'price'=> '90']);
		DB::table('package_addons')->insert(['addon' => 'IBD Quota 8.5GB ( 159K )', 'price'=> '144,545']);
		DB::table('package_addons')->insert(['addon' => 'IBD Unlimited 17GB ( 289K )', 'price'=> '262,727']);
		DB::table('package_addons')->insert(['addon' => 'IBD Unlimited 1GB ( 59K )', 'price'=> '53,636']);
		DB::table('package_addons')->insert(['addon' => 'IBD Unlimited 25GB ( 429K )', 'price'=> '390']);
		DB::table('package_addons')->insert(['addon' => 'IBD Unlimited 2GB ( 79K )', 'price'=> '71,818']);
		DB::table('package_addons')->insert(['addon' => 'IBD Unlimited 5GB ( 129K )', 'price'=> '117,273']);
		DB::table('package_addons')->insert(['addon' => 'IBD Unlimited 8.5GB ( 189K )', 'price'=> '171,818']);
		DB::table('package_addons')->insert(['addon' => 'Old IBD Quota 1GB ( 25K )', 'price'=> '22,727']);
		DB::table('package_addons')->insert(['addon' => 'Old IBD Quota 2.5GB ( 49K )', 'price'=> '44,545']);
		DB::table('package_addons')->insert(['addon' => 'Old IBD Quota 5.5GB ( 99K )', 'price'=> '90']);
		DB::table('package_addons')->insert(['addon' => 'Old IBD Quota 8.5GB ( 149K )', 'price'=> '135,455']);
		DB::table('package_addons')->insert(['addon' => 'Old IBD Unlimited 1.5GB ( 49K )', 'price'=> '44,545']);
		DB::table('package_addons')->insert(['addon' => 'Old IBD Unlimited 3.5GB ( 99K )', 'price'=> '90']);
		DB::table('package_addons')->insert(['addon' => 'Old IBD Unlimited 5.5GB ( 149K )', 'price'=> '135,455']);
		DB::table('package_addons')->insert(['addon' => 'SMS 10K', 'price'=> '9,091']);
		DB::table('package_addons')->insert(['addon' => 'SMS 20K', 'price'=> '18,182']);
		DB::table('package_addons')->insert(['addon' => 'UC BASIC', 'price'=> '72,727']);
		DB::table('package_addons')->insert(['addon' => 'UC SILVER', 'price'=> '145,455']);
		DB::table('package_addons')->insert(['addon' => 'IDD + IR / IDD ONLY / IR ONLY', 'price'=> '0']);
		DB::table('package_addons')->insert(['addon' => 'SWEETENER', 'price'=> '0']);
		DB::table('package_addons')->insert(['addon' => 'APN PUBLIC / APN PRIVATE', 'price'=> '0']);
		DB::table('package_addons')->insert(['addon' => 'FREE GPRS', 'price'=> '0']);
		DB::table('package_addons')->insert(['addon' => 'PERSONAL BALANCE', 'price'=> '0']);
		DB::table('package_addons')->insert(['addon' => 'DELETED / BLOCK ADD ON', 'price'=> '0']);
		DB::table('package_addons')->insert(['addon' => 'Rp.17.500 Abo', 'price'=> '15,909']);
		DB::table('package_addons')->insert(['addon' => 'Rp.100.000 Abo Data 1 Gb', 'price'=> '90,909']);
		DB::table('package_addons')->insert(['addon' => 'X', 'price'=> '0']);
		DB::table('package_addons')->insert(['addon' => 'Rp.75.000 Abo Data 3 Gb', 'price'=> '68,182']);
		DB::table('package_addons')->insert(['addon' => 'Rp.15.000 Abo', 'price'=> '13,636']);
		DB::table('package_addons')->insert(['addon' => 'OTHER', 'price'=> '0']);
		DB::table('package_addons')->insert(['addon' => 'OTC', 'price'=> '0']);
		DB::table('package_addons')->insert(['addon' => 'MRC', 'price'=> '0']);
		DB::table('package_addons')->insert(['addon' => 'Rp.200.000 Abo Data 2 Gb Unlimited', 'price'=> '181,818']);
		DB::table('package_addons')->insert(['addon' => 'Rp.20.000 Abo', 'price'=> '18,181']);

    }
}
