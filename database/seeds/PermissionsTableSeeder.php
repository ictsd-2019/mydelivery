<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert(['name' => 'manage user', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'manage role and permission', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'create delivery request', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'view salesorder', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'view delivery request', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'manage sales agent', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'manage basic package', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'manage add on package', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'manage device', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'manage signature', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'view sales agent', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'view basic package', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'view add on package', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'view device', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'view web salesorder', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'verificate sales order', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'create sales order', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'manage sales order', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'create transaction', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'manage transaction', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'add attachment', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'verificate bast', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'verificate delivery request', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'signed bast', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'view dashboard', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'view back office', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'check availability device', 'guard_name' => 'web']);
    }
}
