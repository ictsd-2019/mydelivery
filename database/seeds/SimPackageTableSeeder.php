<?php

use Illuminate\Database\Seeder;

class SimPackageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sim_packages')->insert(['name' => 'Pintar Freedom (SC-611) - 25K']);
        DB::table('sim_packages')->insert(['name' => 'Pintar Khusus Luar Jawa (SC-610) - 5K']);
        DB::table('sim_packages')->insert(['name' => 'Bluebird (SC-3880) - 294K']);
        DB::table('sim_packages')->insert(['name' => 'Pintar 2 (SC-570) - 3,4K']);
        DB::table('sim_packages')->insert(['name' => 'BTPN (SC-576) - 3,4K']);
        DB::table('sim_packages')->insert(['name' => 'Pintar Prime (SC-635) - 49K']);
        DB::table('sim_packages')->insert(['name' => 'Pintar Pro Freedom Biz (SC-641) - 45K']);
        DB::table('sim_packages')->insert(['name' => 'Pintar Pro Freedom Biz (SC-639) - 25K']);
        DB::table('sim_packages')->insert(['name' => 'Pintar Pro Freedom Biz (SC-642) - 90K']);
    }
}
