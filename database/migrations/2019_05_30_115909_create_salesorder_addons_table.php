<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesorderAddonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salesorder_addons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('id_addon')->nullable();
            $table->foreign('id_addon')
                  ->references('id')
                  ->on('package_addons')
                  ->onDelete('cascade');
            $table->unsignedBigInteger('id_transaction');
            $table->foreign('id_transaction')
                  ->references('id')
                  ->on('salesorder_transactions')
                  ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salesorder_addons');
    }
}
