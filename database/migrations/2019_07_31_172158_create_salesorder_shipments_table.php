<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesorderShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salesorder_shipments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_salesorder')->nullable();
            $table->foreign('id_salesorder')
                  ->references('id')
                  ->on('salesorder')
                  ->onDelete('cascade');
            $table->string('no_dr')->nullable();
            $table->date('request_date')->nullable();
            $table->string('sales')->nullable();
            $table->string('sales_dept')->nullable();
            $table->string('corporate_name')->nullable();
            $table->mediumText('address_1')->nullable();
            $table->mediumText('address_2')->nullable();
            $table->mediumText('address_3')->nullable();
            $table->string('pic_corporate_1')->nullable();
            $table->string('pic_phone_1')->nullable();
            $table->string('pic_corporate_2')->nullable();
            $table->string('pic_phone_2')->nullable();
            $table->string('rt_rw')->nullable();
            $table->string('zip_post')->nullable();
            $table->string('form_shipment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salesorder_shipments');
    }
}
