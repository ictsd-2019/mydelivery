<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSalesorderShipmentsAddCourier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salesorder_shipments', function (Blueprint $table) {
            $table->string('status')->after('bast_signed')->nullable();
            $table->string('name_customer')->after('status')->nullable();
            $table->string('photo_customer')->after('name_customer')->nullable();
            $table->string('sign_customer')->after('photo_customer')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salesorder_shipments', function (Blueprint $table) {
            //
        });
    }
}
