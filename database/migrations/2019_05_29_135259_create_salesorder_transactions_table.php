<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesorderTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salesorder_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_salesorder');
            $table->foreign('id_salesorder')
                  ->references('id')
                  ->on('salesorder')
                  ->onDelete('cascade');
            $table->string('msisdn');
            $table->string('type_bundling')->nullable();
            $table->string('type_prepaid')->nullable();
            $table->string('type_sim')->nullable();
            $table->string('brand')->nullable();
            $table->string('package_basic')->nullable();
            $table->string('type_payment')->nullable();
            $table->integer('tenor')->nullable();
            $table->string('device_price')->nullable();
            $table->string('device_mrc')->nullable();
            $table->string('price_total')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salesorder_transactions');
    }
}
