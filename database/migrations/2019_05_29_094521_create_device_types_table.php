<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('product');
            $table->string('cash_1')->nullable();
            $table->string('cash_2')->nullable();
            $table->string('cash_3')->nullable();
            $table->string('months_3_1')->nullable();
            $table->string('months_3_2')->nullable();
            $table->string('months_3_3')->nullable();
            $table->string('months_6_1')->nullable();
            $table->string('months_6_2')->nullable();
            $table->string('months_6_3')->nullable();
            $table->string('months_12_1')->nullable();
            $table->string('months_12_2')->nullable();
            $table->string('months_12_3')->nullable();
            $table->string('months_24_1')->nullable();
            $table->string('months_24_2')->nullable();
            $table->string('months_24_3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_types');
    }
}
