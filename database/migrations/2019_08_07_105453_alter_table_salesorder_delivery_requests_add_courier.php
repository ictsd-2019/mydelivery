<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSalesorderDeliveryRequestsAddCourier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salesorder_delivery_requests', function (Blueprint $table) {
            $table->string('name_customer')->after('bast_signed')->nullable();
            $table->string('photo_customer')->after('name_customer')->nullable();
            $table->string('sign_customer')->after('photo_customer')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salesorder_delivery_requests', function (Blueprint $table) {
            //
        });
    }
}
