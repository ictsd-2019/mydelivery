<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesorderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salesorder', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('corp_name')->nullable();
            $table->string('pic')->nullable();
            $table->string('pic_phone')->nullable();
            $table->string('pic_email')->nullable();
            $table->date('date_order')->nullable();
            $table->string('quote_no')->nullable();
            $table->string('ba_no')->nullable();
            $table->integer('quantity')->nullable();
            $table->string('status')->nullable();
            $table->string('sales_name')->nullable();
            $table->string('sales_dept')->nullable();
            $table->string('type_req')->nullable();
            $table->string('salesorder_code')->unique()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salesorder');
    }
}
