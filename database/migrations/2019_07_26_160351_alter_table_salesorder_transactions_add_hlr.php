<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSalesorderTransactionsAddHlr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salesorder_transactions', function (Blueprint $table) {
            $table->string('m2m')->after('type_prepaid')->nullable();
            $table->string('non_m2m')->after('m2m')->nullable();
            $table->string('package_type')->after('non_m2m')->nullable();
            $table->string('sim_specification')->after('package_type')->nullable();
            $table->string('hlr')->after('sim_specification')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salesorder_transactions', function (Blueprint $table) {
            //
        });
    }
}
