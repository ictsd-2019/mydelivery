<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreorderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preorder', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('corp_name');
            $table->string('pic');
            $table->string('pic_phone');
            $table->integer('device_id');
            $table->date('date_order');
            $table->string('quote_no');
            $table->string('ba_no');
            $table->string('msisdn');
            $table->integer('sales_name');
            $table->integer('sales_dept');
            $table->integer('type_req');
            $table->integer('type_bundling');
            $table->integer('type_prepaid');
            $table->integer('type_sim');
            $table->text('attachment')->nullable();
            $table->string('package_basic');
            $table->string('package_add');
            $table->integer('type_payment');
            $table->integer('tenor');
            $table->string('device_price');
            $table->string('device_mrc');
            $table->string('price_total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preorder');
    }
}
