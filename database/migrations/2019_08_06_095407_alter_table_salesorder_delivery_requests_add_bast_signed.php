<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSalesorderDeliveryRequestsAddBastSigned extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salesorder_delivery_requests', function (Blueprint $table) {
            $table->string('bast_signed')->after('rekap')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salesorder_delivery_requests', function (Blueprint $table) {
            //
        });
    }
}
