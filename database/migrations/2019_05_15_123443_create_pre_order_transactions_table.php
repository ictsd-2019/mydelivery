<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreOrderTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_order_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reg_code')->nullable();
            $table->integer('preorder_id')->nullable();
            $table->integer('device_id')->nullable();
            $table->integer('store_id')->nullable();
            $table->string('customer_name')->nullable();
            $table->string('msisdn')->nullable();
            $table->string('id_number')->nullable();
            $table->string('email')->nullable();
            $table->string('mother_name')->nullable();
            $table->integer('plan_type_id')->nullable();
            $table->integer('plan_id')->nullable();
            $table->integer('periode')->nullable();
            $table->string('invoice_number')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('payment_type')->nullable();
            $table->integer('bank_promo_id')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_order_transactions');
    }
}
