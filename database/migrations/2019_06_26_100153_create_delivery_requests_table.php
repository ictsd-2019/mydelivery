<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_so_dr')->nullable();
            $table->foreign('id_so_dr')
                  ->references('id')
                  ->on('salesorder_delivery_requests')
                  ->onDelete('cascade');
            $table->string('msisdn')->nullable();
            $table->string('brand')->nullable();
            $table->string('device_type')->nullable();
            $table->string('device_price')->nullable();
            $table->string('imei')->nullable();
            $table->integer('quantity')->nullable();
            $table->string('uom')->nullable();
            $table->mediumText('destination')->nullable();
            $table->string('packing')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_requests');
    }
}
