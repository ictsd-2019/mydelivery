<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSalesorderDeliveryRequestsAddStatusVerif extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salesorder_delivery_requests', function (Blueprint $table) {
            $table->string('status')->after('zip_post')->nullable();
            $table->string('requester')->after('status')->nullable();
            $table->string('manager')->after('requester')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salesorder_delivery_requests', function (Blueprint $table) {
            //
        });
    }
}
