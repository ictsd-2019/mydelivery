<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSalesorderAddIdSales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salesorder', function (Blueprint $table) {
            $table->unsignedBigInteger('id_sales')->after('status_helper')->nullable();
            $table->foreign('id_sales')
                  ->references('id')
                  ->on('sales_agents')
                  ->onDelete('cascade');
            $table->string('sales_group')->after('id_sales')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salesorder', function (Blueprint $table) {
            //
        });
    }
}
