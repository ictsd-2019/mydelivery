<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesorderAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salesorder_attachments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('salesorder_code')->nullable();
            $table->foreign('salesorder_code')
                  ->references('salesorder_code')
                  ->on('salesorder')
                  ->onDelete('cascade');
            $table->text('attachment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salesorder_attachments');
    }
}
