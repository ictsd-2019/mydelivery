<?php


use App\User;
use App\Message;
use App\MessageRecipient;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use UserAgentParser\Exception\NoResultFoundException;
use UserAgentParser\Provider\WhichBrowser;

/**
 * Check permission string and validate against permission rules
 * This is general function used to validate permissions
 * 
 * @param  String $permission Permission array
 * @return Function Abort and redirect to 403
 */
function validate_permission($permission)
{
    if($user = Auth::user())
    {	
		if(! Permission::where(['name' => $permission])->count())
		{
			Permission::create(['name' => $permission]);
		}
		else
		{
	        if(!Auth::user()->hasPermissionTo($permission))
	        {
	            abort(403);
	        } 
		}
	}
}

if (! function_exists('resize_image'))
{
	/**
	 * Resize Image
	 * 	
	 * @param  String $img       Path to image
	 * @param  Integer $new_width Target image width
	 * @param  String $save_to   Where new resized image will be save
	 */
	function resize_image($img, $new_width, $save_to) 
	{
		$r =explode("/",$img);
		$filename=end($r);
		
		$vdir_upload = $save_to;
		list($width_orig, $height_orig) = getimagesize($img);

		$dst_width = $new_width;
		$dst_height = ($dst_width/$width_orig)*$height_orig;
		$im = imagecreatetruecolor($dst_width,$dst_height);
		$image = imagecreatefromjpeg($img);

		imagecopyresampled($im, $image, 0, 0, 0, 0, $dst_width, $dst_height, $width_orig, $height_orig);
		imagejpeg($im,$vdir_upload . $filename);
		imagedestroy($im);
	}
}

if (! function_exists('render_value'))
{
	function render_value($val, $type)
	{
		if($type == "YA_TIDAK")
		{
			if($val == "1")
			{
				return "Ya";
			}
			else if($val == "0")
			{
				return "Tidak";
			}
			else
			{
				return "-";
			}
		}		
		if($type == "OK_TIDAK")
		{
			if($val == "1")
			{
				return "OK";
			}
			else if($val == "0")
			{
				return "Tidak";
			}
			else
			{
				return "-";
			}
		}

		if($type == "ADA_TIDAK_ADA")
		{
			if($val == "1")
			{
				return "Ada";
			}
			else if($val == "0")
			{
				return "Tidak Ada";
			}
			else
			{
				return "-";
			}
		}

		if($type == "ADA_KOSONG")
		{
			if($val == "1")
			{
				return "Ada";
			}
			else if($val == "0")
			{
				return "Kosong";
			}
			else
			{
				return "-";
			}
		}			
	}
}

if (! function_exists('is_mobile'))
{
	function is_mobile()
	{		
        $provider = new WhichBrowser();
        $client = $provider->parse($_SERVER['HTTP_USER_AGENT']);

		return $client->isMobile();
	}
}

if(!function_exists('download_data'))
{
	function download_data($data,$title,$ext = 'xls')
	{
		Excel::create($title, function($excel) use($data,$title)
		{
			// Set the title
			$excel->setTitle($title);

			// Chain the setters
			$excel->setCreator('MyRetail <sandhi.firmadani@indosatooredoo.com>')
					->setCompany('PT. Indosat Tbk')
					->setKeywords('MyRetail, Indosat, Data');

			// Call them separately
			$excel->setDescription('This file is generated via MyRetail application');

			$excel->sheet($title, function($sheet) use($data)
			{
				$sheet->freezeFirstRow();
				$sheet->fromModel($data);
			});

		})->export($ext);
	}
}