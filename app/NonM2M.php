<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NonM2M extends Model
{
    protected $table = 'non_m2ms';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'name'
    ];
}
