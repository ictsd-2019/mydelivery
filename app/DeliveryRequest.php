<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryRequest extends Model
{
    protected $table = 'delivery_requests';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'id_so_dr',
        'msisdn',
        'iccid',
        'brand',
        'device_type',
        'device_price',
        'imei',
        'quantity',
        'uom',
        'destination',
        'packing',
        'notes'
    ];
}
