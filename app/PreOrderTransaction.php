<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreOrderTransaction extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reg_code',
        'preorder_id',
        'device_id',
        'store_id',
        'customer_name',
        'msisdn',
        'id_number',
        'email',
        'mother_name',
        'plan_type_id',
        'plan_id',
        'periode',
        'invoice_number',
        'payment_method',
        'payment_type',
        'bank_promo_id',
        'status'
    ];

    public function store()
    {
        return $this->belongsTo('App\Store','store_id');
    }

    public function device()
    {
        return $this->belongsTo('App\PreOrderDevice', 'device_id');
    }

    public function plan()
    {
        return $this->belongsTo('App\Plan');
    }

    public function bank_promo()
    {
        return $this->belongsTo('App\PreOrderBankPromo','bank_promo_id');
    }

}
