<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesAdmin extends Model
{
    protected $table = 'sales_admins';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user',
        'nama',
        'department',
        'group'
    ];
}
