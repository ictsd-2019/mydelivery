<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SimSpecification extends Model
{
    protected $table = 'sim_specifications';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'name'
    ];
}
