<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesOrderShipment extends Model
{
    protected $table = 'salesorder_shipments';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'id_salesorder',
        'no_dr',
        'request_date',
        'sales',
        'sales_dept',
        'corporate_name',
        'address_1',
        'address_2',
        'address_3',
        'pic_corporate_1',
        'pic_phone_1',
        'pic_corporate_2',
        'pic_phone_2',
        'rt_rw',
        'zip_post',
        'form_shipment',
        'bast_signed',
        'status',
        'name_customer',
        'photo_customer',
        'sign_customer',
        'bast_date'
    ];
}
