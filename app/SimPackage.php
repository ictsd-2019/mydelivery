<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SimPackage extends Model
{
    protected $table = 'sim_packages';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'name'
    ];
}
