<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesAgent extends Model
{
    protected $table = 'sales_agents';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user',
        'nama',
        'department',
        'divisi',
        'group'
    ];

}
