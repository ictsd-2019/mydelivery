<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesOrderAttachment extends Model
{
    protected $table = 'salesorder_attachments';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'salesorder_code',
        'attachment'
    ];

}
