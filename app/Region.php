<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'category'
    ];


    public function ScopeCheckData($query, $value){

    	$query->where('name', $value);
    }
}
