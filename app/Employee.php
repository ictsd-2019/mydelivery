<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employees';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'nik',
        'name',
        'position',
        'group',
        'division',
        'department',
        'signature',
        'id_user'
    ];
}
