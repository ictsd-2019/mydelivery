<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Arrival extends Model
{
    protected $table = 'arrival';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'id_arrival',
        'id_outtask',
        'day_work',
        'arrival_fee',
        'arrival_price',
        'ket'
    ];
}
