<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesOrderRemarks extends Model
{
    protected $table = 'salesorder_remarks';
    
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'id_salesorder',
        'date_create',
        'remarks',
        'id_user'
    ];

}
