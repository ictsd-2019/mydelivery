<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutTask extends Model
{
    protected $table = 'outtask';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'id',
        'name'
    ];
}
