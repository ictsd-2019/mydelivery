<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageInfo extends Model
{
    protected $table = 'package_infos';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
       'package',
       'price'
   ];
}
