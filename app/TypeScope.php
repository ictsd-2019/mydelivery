<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeScope extends Model
{
    protected $table = 'type';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'id_type',
        'type_scope_work',
        'sub_type_price'
    ];
}
