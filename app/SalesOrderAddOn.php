<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesOrderAddOn extends Model
{
    protected $table = 'salesorder_addons';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_addon',
        'id_transaction'
    ];

    public function transactions()
    {
        return $this->belongsTo('App\SalesOrderTransaction','id_transaction');
    }

    public function add_on()
    {
        return $this->belongsTo('App\PackageAddOn','id');
    }
}
