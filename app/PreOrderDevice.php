<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreOrderDevice extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['preorder_id','model','photo','quota'];

    public function booked()
    {
        return $this->hasMany('App\PreOrderTransaction', 'device_id');
    }
}
