<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExportParam extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from' => 'required|date',
            'to' => 'required|date|after_or_equal:from'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'from.required' => 'Tanggal dari harus diisi',
            'to.required' => 'Tanggal sampai harus diisi',
            'to.after_or_equal' => 'Tanggal dari harus sebelum tanggal sampai'
        ];
    }
}
