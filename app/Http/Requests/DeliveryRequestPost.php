<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeliveryRequestPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'msisdn' => 'nullable',
            'iccid' => 'nullable',
            'brand' => 'nullable',
            'device_type' => 'required',
            'device_price' => 'required',
            'imei' => 'nullable',
            'quantity' => 'required',
            'uom' => 'nullable',
            'destination' => 'nullable',
            'packing' => 'nullable',
            'notes' => 'nullable'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'device_type.required' => 'Device Type harus diisi',
            'device_price.required' => 'Device Price harus diisi',
            'quantity.required' => 'Quantity harus diisi'
        ];
    }
}
