<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArrivalPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'day' => 'required',
            'vendor' => 'required',
            'fee' => 'required',
            'price' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'vendor.required' => 'Data Vendor harus diisi',
            'day.required' => 'Data Work Day harus diisi',
            'fee.required' => 'Data Fee harus diisi',
            'price.required' => 'Price harus diisi'
        ];
    }
}
