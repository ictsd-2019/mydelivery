<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PackageAddOnPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'addon' => 'required|max:255',
            'price' => 'required|numeric'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'addon.required' => 'Nama Package Add On harus diisi',
            'price.required' => 'Price harus diisi',
            'price.numeric' => 'Price harus diisi dengan angka'
        ];
    }
}
