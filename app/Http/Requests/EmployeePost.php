<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nik' => 'required|max:15',
            'name' => 'required|max:255',
            'position' => 'required',
            'group' => 'required',
            'division' => 'required',
            'department' => 'required',
            'signature' => 'nullable|mimes:jpeg,png,jpg'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nik.required' => 'NIK harus diisi',
            'name.required' => 'Nama Pegawai harus diisi',
            'position.required' => 'Posisi Pegawai harus diisi',
            'group.required' => 'Group Pegawai harus diisi',
            'division.required' => 'Divisi Pegawai harus diisi',
            'department.required' => 'Departemen Pegawai harus diisi'
        ];
    }
}
