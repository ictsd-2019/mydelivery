<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddNewUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    // public function authorize()
    // {
    //     return true;
    // }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|unique:users,username',
            // 'store_id' => 'required_if:role,Partner Store',
            'email' => 'email',
            'password' => 'required|confirmed'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            // 'store_id.required_if' => 'Silahkan pilih nama Store jika role Partner Store',
            'username.unique' => 'Username sudah digunakan, coba yang lainnya'
        ];
    }
}
