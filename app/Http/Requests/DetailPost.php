<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DetailPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'main' => 'required',
            'ven' => 'required',
            'start' => 'required',
            'end' => 'required',
            'detail' => 'required',
            'price' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'main.required' => 'Data Main harus diisi',
            'ven.required' => 'Data Vendor harus diisi',
            'start.required' => 'Data Start harus diisi',
            'end.required' => 'Notes End diisi',
            'detail.required' => 'Detail harus diisi',
            'price.required' => 'Price harus diisi',
        ];
    }
}
