<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MainPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ven' => 'required',
            'reg' => 'required',
            'act' => 'required',
            'note' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'ven.required' => 'Data Vendor harus diisi',
            'reg.required' => 'Data Regional harus diisi',
            'act.required' => 'Data Activity harus diisi',
            'note.required' => 'Notes harus diisi'
        ];
    }
}
