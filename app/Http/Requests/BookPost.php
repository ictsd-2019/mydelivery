<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           
            
            'name' => 'required',
            'start' => 'required',
            'end' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Name harus diisi',
            'start.required' => 'Start Date harus diisi',
            'end.required' => 'End Date harus diisi'
        ];
    }
}
