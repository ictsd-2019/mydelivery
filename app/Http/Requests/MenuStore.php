<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MenuStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'group' => 'required',
            'path' => 'required',
            'permission' => 'required',
            'icon3' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Title tidak boleh kosong',
            'group.required' => 'Group tidak boleh kosong',
            'path.required' => 'Path tidak boleh kosong',
            'permission.required' => 'Permission tidak boleh kosong',
            'icon3.required' => 'Icon tidak boleh kosong',
        ];
    }
}
