<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SalesOrderShipmentPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'no_dr' => 'nullable',
            'request_date' => 'required|date',
            'sales' => 'nullable|max:255',
            'sales_dept' => 'nullable|max:255',
            'corporate_name' => 'required',
            'address_1' => 'required',
            'address_2' => 'nullable',
            'address_3' => 'nullable',
            'rt_rw' => 'nullable',
            'zip_post' => 'nullable|numeric',
            'pic_corporate_1' => 'required|max:255',
            'pic_phone_1' => 'required|numeric',
            'pic_corporate_2' => 'nullable|max:255',
            'pic_phone_2' => 'nullable|numeric',
            'msisdn' => 'required',
            'form_shipment' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'no_dr.required' => 'Nomor Delivery Request harus diisi',
            'request_date.required' => 'Tanggal Request harus diisi',
            'corporate_name.required' => 'Nama Perusahaan harus diisi',
            'address_1.required' => 'Alamat 1 harus diisi',
            'zip_post.numeric' => 'Kode Pos harus angka',
            'pic_corporate_1.required' => 'Nama PIC harus diisi',
            'pic_phone_1.required' => 'Nomor HP PIC harus diisi',
            'pic_phone_1.numeric' => 'Nomor HP PIC harus angka',
            'pic_phone_2.numeric' => 'Nomor HP PIC harus angka'
        ];
    }
}
