<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DevicePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product' => 'required|max:255',
            'cash_1' => 'nullable|numeric',
            'cash_2' => 'nullable|numeric',
            'cash_3' => 'nullable|numeric',
            'months_3_1' => 'nullable|numeric',
            'months_3_2' => 'nullable|numeric',
            'months_3_3' => 'nullable|numeric',
            'months_6_1' => 'nullable|numeric',
            'months_6_2' => 'nullable|numeric',
            'months_6_3' => 'nullable|numeric',
            'months_12_1' => 'nullable|numeric',
            'months_12_2' => 'nullable|numeric',
            'months_12_3' => 'nullable|numeric',
            'months_24_1' => 'nullable|numeric',
            'months_24_2' => 'nullable|numeric',
            'months_24_3' => 'nullable|numeric'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'product.required' => 'Nama Product harus diisi',
            'cash_1.numeric' => 'Cash bagian 1 harus diisi dengan angka',
            'cash_2.numeric' => 'Cash bagian 2 harus diisi dengan angka',
            'cash_3.numeric' => 'Cash bagian 3 harus diisi dengan angka',
            'months_3_1.numeric' => 'Tenor 3 bulan bagian 1 harus diisi dengan angka',
            'months_3_2.numeric' => 'Tenor 3 bulan bagian 2 harus diisi dengan angka',
            'months_3_3.numeric' => 'Tenor 3 bulan bagian 3 harus diisi dengan angka',
            'months_6_1.numeric' => 'Tenor 6 bulan bagian 1 harus diisi dengan angka',
            'months_6_2.numeric' => 'Tenor 6 bulan bagian 2 harus diisi dengan angka',
            'months_6_3.numeric' => 'Tenor 6 bulan bagian 3 harus diisi dengan angka',
            'months_12_1.numeric' => 'Tenor 12 bulan bagian 1 harus diisi dengan angka',
            'months_12_2.numeric' => 'Tenor 12 bulan bagian 2 harus diisi dengan angka',
            'months_12_3.numeric' => 'Tenor 12 bulan bagian 3 harus diisi dengan angka',
            'months_24_1.numeric' => 'Tenor 24 bulan bagian 1 harus diisi dengan angka',
            'months_24_2.numeric' => 'Tenor 24 bulan bagian 2 harus diisi dengan angka',
            'months_24_3.numeric' => 'Tenor 24 bulan bagian 3 harus diisi dengan angka'
        ];
    }
}
