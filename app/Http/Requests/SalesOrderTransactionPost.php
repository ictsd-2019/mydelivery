<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SalesOrderTransactionPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'msisdn' => 'nullable|numeric',
            'iccid' => 'nullable|numeric',
            'type_bundling' => 'required',
            'type_prepaid' => 'required',
            'type_sim' => 'nullable',
            'brand' => 'nullable',
            'package_basic' => 'required',
            'type_payment' => 'nullable'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'msisdn.numeric' => 'MSISDN harus diisi dengan angka',
            'iccid.numeric' => 'ICCID harus diisi dengan angka',
            'type_bundling.required' => 'Bundling Type belum dipilih',
            'type_prepraid.required' => 'Prepaid Type belum dipilih',
            'package_basic.required' => 'Basic Package belum dipilih'
        ];
    }
}
