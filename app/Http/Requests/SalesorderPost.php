<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SalesOrderPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'corp_name' => 'required|max:255',
            'pic' => 'required|max:255',
            'pic_phone' => 'required|max:15',
            'pic_email' => 'required|email|max:120',
            'date_order' => 'nullable|date',
            'quote_no' => 'required',
            'ba_no' => 'required',
            'quantity' => 'required',
            'sales_name' => 'required',
            'type_req' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'corp_name.required' => 'Nama Perusahaan harus diisi',
            'pic.required' => 'Nama PIC harus diisi',
            'pic_phone.required' => 'Nomor Handphone harus diisi',
            'pic_email.required' => 'Alamat email harus diisi',
            'pic_email.email' => 'Alamat email harus memenuhi format email',
            'quote_no.required' => 'Nomor Quote harus diisi',
            'ba_no.required' => 'Nomor BA harus diisi',
            'quantity.required' => 'Quantity harus diisi',
            'sales_name.required' => 'Nama Sales Agent belum dipilih',
            'type_req.required' => 'Request Type belum dipilih'
        ];
    }
}
