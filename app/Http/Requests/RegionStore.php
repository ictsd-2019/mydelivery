<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class RegionStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    // public function authorize()
    // {
    //     return true;
    // }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100',
            'category' => 'required'
        ];
    }

    public function messages()
    {
        return[
            'name.required' => 'Nama Region tidak boleh kosong',
            'name.max' => 'Nama Region tidak dapat melebihi 100 huruf',
            'category.required' => 'Category tidak boleh kosong'
        ];
    }
}
