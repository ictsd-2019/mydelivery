<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PreOrderPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'corp_name' => 'required|max:255',
            'pic' => 'required|max:255',
            'pic_phone' => 'required|max:15',
            'msisdn' => 'required|max:16'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'corp_name.required' => 'Nama Perusahaan harus di isi',
            'pic.required' => 'Nomor Handphone harus di isi',
            'pic_phone.required' => 'Alamat email harus di isi',
            'date_order.required' => 'Nomor Kartu Keluarga harus di isi',
            'quote_no.required' => 'Photo KTP atau Kitas harus di isi',
            'ba_no.required' => 'Photo FAB harus di isi',
            'msisdn.required' => 'MSISDN harus di isi',
            'msisdn.unique' => 'MSISDN sudah terdaftar',
            'sales_name.required' => 'Paket Plan belum di pilih',
            'sales_dept.required' => 'Periode penggunaan Plan belum dipilih',
            'type_req.required' => 'Nama Sales Agent harus di isi',
            'type_bundling.required' => 'Email Sales Agent harus di isi',
            'type_prepraid.required' => 'Nama Kerabat tidak serumah harus di isi',
            'type_sim.required' => 'Nomor HP Kerabat tidak serumah harus di isi',
            'brand.required' => 'Hubungan Kerabat tidak serumah harus di isi',
            'package_basic.required' => 'Email Sales Agent harus di isi',
            // 'package_add.required' => 'Nama Kerabat tidak serumah harus di isi',
            'type_payment.required' => 'Nomor HP Kerabat tidak serumah harus di isi',
            'tenor.required' => 'Hubungan Kerabat tidak serumah harus di isi'
        ];
    }
}
