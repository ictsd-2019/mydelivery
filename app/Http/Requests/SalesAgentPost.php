<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SalesAgentPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required|max:255',
            'department' => 'required|max:255',
            'divisi' => 'required|max:255',
            'group' => 'required|max:255'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nama.required' => 'Nama Sales Agent harus diisi',
            'department.required' => 'Departemen Sales Agent harus diisi',
            'divisi.required' => 'Divisi Sales Agent harus diisi',
            'group.required' => 'Group Sales Agent harus diisi'
        ];
    }
}
