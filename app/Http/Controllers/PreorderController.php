<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Storage;
use App\Plan;
use App\PreOrder;
use App\PackageInfo;
use App\PackageAddOn;
use App\DeviceType;
use App\SalesAgent;
use App\PreOrderPlan;
use App\PreOrderAddOn;
use App\PreOrderDevice;
use App\PreOrderTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\PreOrderPost;

class PreOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_preorder = preorder::paginate(25);
        return view('preorder.index_preorder', [
            'data_preorder' => $data_preorder
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['current_date'] = date("Y-m-d");
        $data['plans'] = Plan::whereIn('plan_type_id', [2,8])->pluck('name','id');
        $data['add_on'] = PackageAddOn::pluck('addon','id');
        $data['basic'] = PackageInfo::pluck('package','id');
        $data['device'] = DeviceType::pluck('product','id');
        $data['sales_agent'] = SalesAgent::pluck('ae_name', 'id');
        $data['price_add_on'] = PackageAddOn::pluck('price','id');
        $data['price_basic'] = PackageInfo::pluck('price','id');

        return view('preorder.preorder_create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(preorderPost $request)
    {
        $data_preorder['corp_name'] = strtoupper($request->corp_name);
        $data_preorder['pic'] = strtoupper($request->pic);
        $data_preorder['pic_phone'] = preg_replace('|[^0-9]|i', '', preg_replace('/^0?/', '62', $request->pic_phone));
        $data_preorder['preorder_code'] = $this->get_preorder_code();
        if(substr( $request->msisdn, 0, 2 ) === "08")
        {
            $data_preorder['msisdn'] = preg_replace('|[^0-9]|i', '', preg_replace('/^0?/', '62', $request->msisdn));
        }
        else
        {
            $data_preorder['msisdn'] = preg_replace('|[^0-9]|i', '', $request->msisdn);
        }

        $data_preorder['order_date'] = Carbon::parse($request->order_date)->format('Y-m-d');
        $data_preorder['quote_no'] = $request->quote_no;
        $data_preorder['ba_no'] = $request->ba_no;
        $data_preorder['type_req'] = $request->type_req;
        $data_preorder['type_bundling'] = $request->type_bundling;
        $data_preorder['type_prepaid'] = $request->type_prepaid;
        $data_preorder['type_sim'] = $request->type_sim;

        if($request->attachment)
        {
            $preorder_data = substr($request->attachment, strpos($request->attachment, ",")+1);
            $uploaded_preorder = \Storage::disk('local')
                ->put('public/preorder/'. $data_preorder['preorder_code'] ."/preorder.jpg", base64_decode($preorder_data));

            $preorder_url = \Storage::url('public/preorder/'. $data_preorder['preorder_code'] .'/preorder.jpg');
            $data_preorder['attachment'] = $preorder_url;
        }

        $data_preorder['type_payment'] = $request->type_payment;
        $data_preorder['tenor'] = $request->tenor;
        $data_preorder['device_price'] = $request->device_price;
        $data_preorder['device_mrc'] = $request->device_mrc;
        $data_preorder['price_total'] = $request->price_total;

        $nama_salesagent = SalesAgent::where('id', $request->sales_name)->get();
        $data_preorder['sales_name'] = strtoupper($nama_salesagent[0]->ae_name);
        $data_preorder['sales_dept'] = strtoupper($nama_salesagent[0]->dept_mapping);

        $nama_brand = DeviceType::where('id', $request->brand)->get();
        $data_preorder['brand'] = $nama_brand[0]->product;

        $nama_package = PackageInfo::where('id', $request->package_basic)->get();
        $data_preorder['package_basic'] = $nama_package[0]->package;
        
        $sales_order = preorder::create($data_preorder);

        foreach($request->package_add as $package)
        {
            preorderAddOn::create([
                'idpreorder' => $sales_order->id,
                'idAddOn' => $package
            ]);
        }

        return redirect('/preorder')
            ->with('status','success')
            ->with('message','Sales Order created successfuly!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PreOrder  $preOrder
     * @return \Illuminate\Http\Response
     */
    public function show($preorder_id)
    {
        $data['preorder'] = PreOrder::where('id', $preorder_id)->first();

        if(is_null($data['preorder']))
        {
            return redirect(route('preorder.index'))
                ->with('status','danger')
                ->with('message','Pre-Order ID not found.');
        }

        $data['total_booked'] = $data['preorder']->transactions()->where('status',1)->count();
        $data['total_paid'] = $data['preorder']->transactions()->where('status',2)->count();

        if(Carbon::now() < $data['preorder']->startdate)
        {
            $data['preorder_status'] = "waiting";
        }
        else if(Carbon::now() < $data['preorder']->enddate)
        {
            $data['preorder_status'] = "running";
        }
        else
        {
            $data['preorder_status'] = "closed";
        }

        return view('preorder.manage_preorder_view', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PreOrder  $preOrder
     * @return \Illuminate\Http\Response
     */
    public function edit($preorder_id)
    {
        $data['preorder'] = PreOrder::where('id', $preorder_id)->first();

        $startdate = $data['preorder']->startdate;
        $enddate = $data['preorder']->enddate;

        $data['preorder']->startdate = Carbon::parse($startdate)->format('Y-m-d');
        $data['preorder']->enddate = Carbon::parse($enddate)->format('Y-m-d');
        $data['preorder']->starttime = Carbon::parse($startdate)->format('H:i');
        $data['preorder']->endtime = Carbon::parse($enddate)->format('H:i');
        $data['bank_promos'] = PreOrderBankPromo::where('preorder_id', $preorder_id)->get();
        $data['devices'] = PreOrderDevice::where('preorder_id', $preorder_id)->get();
        $data['active_plans'] = PreOrderPlan::where('preorder_id', $preorder_id)->get();
        $data['plans'] = Plan::whereIn('plan_type_id',[2,8])->pluck('name','id');
        $data['banks'] = Bank::pluck('name','id');

        return view('preorder.manage_preorder_edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PreOrder  $preOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $preorder_id)
    {
        $preorder_data['title'] = $request->title;
        $preorder_data['startdate'] = $request->startdate." ". $request->starttime;
        $preorder_data['enddate'] = $request->enddate." ". $request->endtime;

        PreOrder::where('id', $preorder_id)->update($preorder_data);

        // Update bank promo
        if($request->bank_promos)
        {
            foreach($request->bank_promos as $promo)
            {
                if(isset($promo['id']))
                {
                    PreOrderBankPromo::where('id', $promo['id'])->update([
                        'bank_id' => $promo['bank_id'],
                        'value' => $promo['value']
                    ]);
                }
                else
                {
                    PreOrderBankPromo::create([
                        'preorder_id' => $preorder_id,
                        'bank_id' => $promo['bank_id'],
                        'value' => $promo['value']
                    ]);
                }
            }
        }

        // Update pre-order devices
        if($request->devices)
        {
            $existing_device = PreOrderDevice::where('preorder_id', $preorder_id)->pluck('id')->toArray();
            $device_remain = [];

            // Update or add new device
            foreach($request->devices as $device)
            {
                if(isset($device['id']))
                {
                    array_push($device_remain,(int)$device['id']);

                    PreOrderDevice::where('id', $device['id'])->update([
                        'preorder_id' => $preorder_id,
                        'model' => $device['model'],
                        'quota' => $device['quota']
                    ]);
                }
                else
                {
                    PreOrderDevice::create([
                        'preorder_id' => $preorder_id,
                        'model' => $device['model'],
                        'quota' => $device['quota']
                    ]);
                }
            }

            // Delete device
            foreach($existing_device as $exist_id)
            {
                if(!in_array($exist_id, $device_remain))
                {
                    PreOrderDevice::where('id', $exist_id)->delete();
                }
            }
        }

        return redirect(route('preorder.manage.index'))
            ->with('status','success')
            ->with('message','Pre-Order "'.$request->title.'" has been updated successfuly.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PreOrder  $preOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $preorder_code = $request->preorder_code;
        $preorder_trans = PreOrderTransaction::where('preorder_code', $preorder_code)->get();

        dd($preorder_trans);
    }

    /*
     * Show all transactions in this preorder
     */
    public function transactions($preorder_id)
    {
        $startdate = Input::get('startdate') ? Input::get('startdate') : "";
        $enddate = Input::get('enddate') ? Input::get('enddate') : "";
        $customer = Input::get('customer') ? Input::get('customer') : "";
        $msisdn = Input::get('msisdn') ? Input::get('msisdn') : "";
        $reg_code = Input::get('reg_code') ? Input::get('reg_code') : "";
        $download = Input::get('download') ? Input::get('download') : "";

        $where = [];

        if($startdate)
        {
            array_push($where, ['created_at', '>=', $startdate ." 00:00:00"]);
        }

        if($enddate)
        {
            array_push($where, ['created_at', '<=', $enddate ." 23:59:59"]);
        }

        if($customer)
        {
            array_push($where, ['customer_name', 'LIKE', '%'. $customer .'%']);
        }

        if($msisdn)
        {
            array_push($where, ['msisdn', 'LIKE', '%'. $msisdn .'%']);
        }

        if($reg_code)
        {
            array_push($where, ['reg_code', 'LIKE', '%'. $reg_code .'%']);
        }

        $data['preorder'] = PreOrder::where('id', $preorder_id)->first();
        $transactions = $data['preorder']->transactions()->where('preorder_id', $data['preorder']->id)
                                    ->where($where)->orderBy('created_at','DESC');

        foreach($transactions->get() as $key => $trans)
        {
            if($trans->status == 1)
            {
                $status_info = "Booked";
            }
            else if($trans->status == 2)
            {
                $status_info = "Paid";
            }
            else
            {
                $status_info = "Canceled";
            }

            $download_data[$key]['Transaction Date'] = $trans->created_at;
            $download_data[$key]['Kode Registrasi'] = $trans->reg_code;
            $download_data[$key]['Nama Pelanggan'] = $trans->customer_name;
            $download_data[$key]['Nomor KTP'] = $trans->id_number;
            $download_data[$key]['Email'] = $trans->email;
            $download_data[$key]['Ibu Kandung'] = $trans->mother_name;
            $download_data[$key]['Device'] = $trans->device['model'];
            $download_data[$key]['MSISDN'] = $trans->msisdn;
            $download_data[$key]['Plan'] = $trans->plan['name'];
            $download_data[$key]['Periode'] = $trans->periode;
            $download_data[$key]['Nomor Invoice'] = $trans->invoice_number;
            $download_data[$key]['Payment Method'] = $trans->payment_method;
            $download_data[$key]['Payment Type'] = $trans->payment_type;
            $download_data[$key]['Promo Bank'] = isset($trans->bank_promo) ? $trans->bank_promo->bank['name'] ." Rp ". $trans->bank_promo['value'] : "";
            $download_data[$key]['Store'] = $trans->store['siebel_name'];
            $download_data[$key]['Status'] = $status_info;
        }

        if($download)
        {
            download_data($download_data, 'Data Transaksi', $download);
        }

        $data['transactions'] = $transactions->paginate(25);
        
        $data['total_booked'] = $data['preorder']->transactions()->where('status',1)->count();
        $data['total_paid'] = $data['preorder']->transactions()->where('status',2)->count();

        if(Carbon::now() < $data['preorder']->startdate)
        {
            $data['preorder_status'] = "waiting";
        }
        else if(Carbon::now() < $data['preorder']->enddate)
        {
            $data['preorder_status'] = "running";
        }
        else
        {
            $data['preorder_status'] = "closed";
        }

        $data['startdate'] = $startdate;
        $data['enddate'] = $enddate;
        $data['customer'] = $customer;
        $data['msisdn'] = $msisdn;
        $data['reg_code'] = $reg_code;

        if(is_null($data['preorder']))
        {
            return redirect(route('preorder.index'))
                ->with('status','danger')
                ->with('message','Pre-Order ID not found.');
        }

        return view('preorder.manage_preorder_transactions', $data);
    }

    public function store_report($preorder_id)
    {
        $startdate = Input::get('startdate') ? Input::get('startdate') : "";
        $enddate = Input::get('enddate') ? Input::get('enddate') : "";
        $where = [];

        if($startdate)
        {
            array_push($where, ['created_at', '>=', $startdate ." 00:00:00"]);
        }

        if($enddate)
        {
            array_push($where, ['created_at', '<=', $enddate ." 23:59:59"]);
        }

        $data['stores_data'] = [];
        $data['preorder'] = PreOrder::where('id', $preorder_id)->first();
        $stores = PreOrderTransaction::select('store_id')->where('preorder_id', $data['preorder']->id)
                                                ->where($where)
                                                ->groupBy('store_id')->get();

        $data['devices'] = PreOrderDevice::where('preorder_id', $preorder_id)->get();

        foreach($stores as $store)
        {
            foreach($data['devices'] as $device)
            {
                $data['stores_data'][$store->store['siebel_name']][$device->model] = $data['preorder']->transactions()
                                                ->where('preorder_id', $preorder_id)
                                                ->where($where)
                                                ->where('store_id', $store->store_id)
                                                ->where('device_id', $device->id)
                                                ->groupBy('device_id')->count();
            }

            $data['stores_data'][$store->store['siebel_name']]['total'] = $data['preorder']->transactions()
                                                ->where('preorder_id', $preorder_id)
                                                ->where($where)
                                                ->where('store_id', $store->store_id)
                                                ->count();
        }

        $data['total_booked'] = $data['preorder']->transactions()->where('status',1)->count();
        $data['total_paid'] = $data['preorder']->transactions()->where('status',2)->count();
        $data['startdate'] = $startdate;
        $data['enddate'] = $enddate;

        if(Carbon::now() < $data['preorder']->startdate)
        {
            $data['preorder_status'] = "waiting";
        }
        else if(Carbon::now() < $data['preorder']->enddate)
        {
            $data['preorder_status'] = "running";
        }
        else
        {
            $data['preorder_status'] = "closed";
        }

        return view('preorder.manage_preorder_store_report', $data);
    }

    public function input_preorder($preorder_id)
    {
        $data['preorder'] = PreOrder::where('id', $preorder_id)->first();
        $data['total_booked'] = $data['preorder']->transactions()->where('status',1)->count();
        $data['total_paid'] = $data['preorder']->transactions()->where('status',2)->count();
        $data['preorder_code'] = $data['preorder']->preorder_code;

        if(is_null($data['preorder']))
        {
            return redirect(route('preorder.index'));
        }

        if(Carbon::now() < $data['preorder']->startdate)
        {
            $data['preorder_status'] = "waiting";
        }
        else if(Carbon::now() < $data['preorder']->enddate)
        {
            $data['preorder_status'] = "running";
        }
        else
        {
            $data['preorder_status'] = "closed";
        }

        $data['devices'] = PreOrderDevice::where('preorder_id', $data['preorder']->id)->orderBy('id')->get();
        $data['plans'] = PreOrderPlan::where('preorder_id', $data['preorder']->id)->get()->pluck('plan.name', 'plan.id');
        $data['periode'] = PreOrderPlan::where('preorder_id', $data['preorder']->id)->groupBy('periode')->pluck('periode','periode');
        $bank_promos = PreOrderBankPromo::where('preorder_id', $data['preorder']->id)->get();

        foreach($bank_promos as $key => $promo)
        {
            $data['bank_promos'][$promo['id']] = $promo->bank['name'] ." Rp ". number_format($promo['value'],0,',','.');
        }

        array_unshift($data['bank_promos'],"- Tanpa Promo -");

        return view('preorder.manage_preorder_input', $data);
    }

    /**
     * Get random preorder unique code, keep generate if code exist.
     *
     * @return String unique registration code
     */
    public function get_preorder_code()
    {
        $preorder_code = str_random(5);

        $code_exist = PreOrder::where('preorder_code', $preorder_code)->count();

        if($code_exist > 0)
        {
            $this->get_preorder_code();
        }
        else
        {
            return $preorder_code;
        }
    }
}
