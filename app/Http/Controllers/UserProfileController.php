<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;

use Auth;
use Request;
use Session;
use App\User;
use App\ActivityLog;
use App\Http\Requests\UpdateUserProfile;
use App\Http\Requests\ChangePassword;
use Illuminate\Support\Facades\Hash;

class UserProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['user'] = User::where('id', Auth::user()->id)->first();
        $data['activities'] = ActivityLog::where('username', Auth::user()->username)->orderBy('created_at','desc')->get()->take(5);

        // Log activity
        // ActivityLog::info('PROFILE','View Profile page');

        return view('profile.index_profile', $data);
    }

    public function frontpage_profile(){

        $data['user'] = User::where('id', Auth::user()->id)->first();
        $data['activities'] = ActivityLog::where('username', Auth::user()->username)->orderBy('created_at','desc')->get()->take(5);

        // Log activity
        // ActivityLog::info('PROFILE','View Profile page');

        return view('profile.frontpage_profile', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $data['user'] = User::find(Auth::user()->id);

        // Log activity
        // ActivityLog::info('PROFILE','Edit Profile page');

        return view('profile.edit_profile', $data);          
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserProfile $request, User $user)
    {
        $data['fullname'] = $request->fullname;
        $data['nik'] = $request->nik;
        $data['email'] = $request->email;
        $data['phone'] = $request->phone;

        $updated = User::where('id', Auth::user()->id)->update($data);

        if($updated)
        {
            // Log activity
            // ActivityLog::info('PROFILE','Updated Profile item', ['username' => $request->username]);

            return redirect('profile')->with('message', 'Success update profile.');
        }
        else
        {
            return redicrect('profile/'. Auth::user()->id .'/edit')->with('message', 'Error update profile.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function change_password(User $user)
    {
        $data['user'] = $user;
        return view('profile.change_password_profile', $data);
    }

    public function change_password_submit(ChangePassword $request, User $user)
    {
        if (Hash::check($request->old_password, Auth::user()->password)) 
        {
            User::where('id', Auth::user()->id)->update(['password' => bcrypt($request->new_password)]);

            // Log activity
            ActivityLog::info('PROFILE','Updated Password', ['username' => Auth::user()->username]);

            Session::flash('message','Your password has been updated.');
            Session::flash('type', 'success');
            return redirect('profile');
        }
        else
        {
            // Log activity
            ActivityLog::info('PROFILE','Failed updating Password', ['username' => Auth::user()->username]);

            Session::flash('message','Your password isn\'t correct, please try again.');
            Session::flash('type', 'danger');
            return redirect('profile/change_password');
        }
    }
}
