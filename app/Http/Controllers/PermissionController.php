<?php

namespace App\Http\Controllers;

use Auth;
use App\ActivityLog;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        validate_permission('manage role and permission');        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['permissions'] = Permission::all();

        // Log activity
        // ActivityLog::info('PERMISSION','View Permission backend page');    

        return view('admin.permission.index_permission', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Log activity
        // ActivityLog::info('PERMISSION','View Create Permission page');    

        return view('admin.permission.create_permission');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $check = Permission::where('name', $request->name)->count();

        if($check >=1) {
            // ActivityLog::info('Region','Submit Faild, Region name already', ['name' => $req->name]);
            return redirect('config/permission/create')
                ->with('status','danger')
                ->with('message', 'Permission \''. $request['name'] .'\' already exist');
        } else {
            // ActivityLog::info('Region','Submit Success, Create new Region success', ['name' => $req->name]);
            Permission::create(['name' => $request['name']]);
            return redirect('config/permission')
                ->with('status','success')
                ->with('message', 'Permission \''. $request['name'] .'\' has been created successfuly.');
        }

        // Log activity
        // ActivityLog::info('PERMISSION','Submitted new Permission', ['permission' => $request['name']]);
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permission::findOrFail($id);
        $data = ['permission' => $permission];

        // Log activity
        ActivityLog::info('PERMISSION','Edit Permission page', ['permission_id' => $id]);    

        return view('admin.permission.edit_permission', $data);        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $check = Permission::where('name', $request->name)->where('id', '!=', $id)->count();

        if($check >=1) {
            // ActivityLog::info('Region','Submit Faild, Region name already', ['name' => $req->name]);
            return redirect('config/permission/'.$id.'/edit')
                ->with('status','danger')
                ->with('message', 'Permission \''. $request['name'] .'\' already exist');
        } else {
            // ActivityLog::info('Region','Submit Success, Create new Region success', ['name' => $req->name]);
            Permission::find($id)->update($request->all());
            return redirect('config/permission')
                ->with('status','success')
                ->with('message', 'Permission \''. $request['name'] .'\' has been updated!');
        }

        // Log activity
        // ActivityLog::info('PERMISSION','Updated Permission item', ['permission_id' => $id]);    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Permission::findOrFail($id)->delete();

        return redirect('/config/permission')
            ->with('status','success')
            ->with('message', 'Permission has been deleted!');
    }
}
