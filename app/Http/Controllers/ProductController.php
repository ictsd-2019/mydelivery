<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
    	$data_product = \App\Product::all();
    	return view('product.index_product',['data_product' => $data_product]);
    }
}
