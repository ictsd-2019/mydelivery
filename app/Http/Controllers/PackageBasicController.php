<?php

namespace App\Http\Controllers;

use Excel;
use App\Exports\PackageBasicExport;
use App\Imports\PackageBasicImport;
use App\PackageInfo;
use Illuminate\Http\Request;
use App\Http\Requests\PackageBasicPost;

class PackageBasicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['package'] = PackageInfo::orderBy('package', 'asc')->get();
        foreach($data['package'] as $pck){
            $pck['price'] = number_format($pck['price']);
        }

        return view('package_basic.manage_package_basic_index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('package_basic.manage_package_basic_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PackageBasicPost $request)
    {
        PackageInfo::create([
            'package' => $request['package'],
            'price' => $request['price']
        ]);

        return redirect('/package')
            ->with('status','success')
            ->with('message', 'Package '. $request['package'] .' has been created!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PackageInfo  $package_id
     * @return \Illuminate\Http\Response
     */
    public function edit($package_id)
    {
        $data['package'] = PackageInfo::find($package_id);

        return view('package_basic.manage_package_basic_edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PackageInfo  $package_id
     * @return \Illuminate\Http\Response
     */
    public function update(PackageBasicPost $request, $package_id)
    {
        PackageInfo::find($package_id)->update($request->all());

        return redirect('/package')
            ->with('status','success')
            ->with('message', 'Package '. $request['package'] .' has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PackageInfo  $package_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($package_id)
    {
        $package = PackageInfo::find($package_id);
        $nama_package = $package->package;
        $package->delete();

        return redirect('/package')
            ->with('status','success')
            ->with('message', 'Package '. $nama_package .' has been deleted!');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function export()
    {
        $current_date = Date("d-m-Y");
        return Excel::download(new PackageBasicExport, 'Daftar Basic Package ('.$current_date .').xlsx');
    }
}
