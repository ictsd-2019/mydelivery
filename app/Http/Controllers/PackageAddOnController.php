<?php

namespace App\Http\Controllers;

use Excel;
use App\Exports\PackageAddOnExport;
use App\Imports\PackageAddOnImport;
use App\PackageAddOn;
use Illuminate\Http\Request;
use App\Http\Requests\PackageAddOnPost;

class PackageAddOnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['addon'] = PackageAddOn::orderBy('addon', 'asc')->get();
        foreach($data['addon'] as $ao){
            $ao['price'] = number_format($ao['price']);
        }

        return view('package_addon.manage_package_addon_index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('package_addon.manage_package_addon_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PackageAddOnPost $request)
    {
        PackageAddOn::create([
            'addon' => $request['addon'],
            'price' => $request['price']
        ]);

        return redirect('/addon')
            ->with('status','success')
            ->with('message', 'Package \''. $request['addon'] .'\' has been created!');    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PackageAddOn  $add_on_id
     * @return \Illuminate\Http\Response
     */
    public function edit($addon_id)
    {
        $data['addon'] = PackageAddOn::find($addon_id);

        return view('package_addon.manage_package_addon_edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PackageAddOn  $add_on_id
     * @return \Illuminate\Http\Response
     */
    public function update(PackageAddOnPost $request, $addon_id)
    {
        PackageAddOn::find($addon_id)->update($request->all());

        return redirect('/addon')
            ->with('status','success')
            ->with('message', 'Package '. $request['addon'] .' has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PackageAddOn  $add_on_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($addon_id)
    {
        $addon = PackageAddOn::find($addon_id);
        $nama_addon = $addon->addon;
        $addon->delete();

        return redirect('/addon')
            ->with('status','success')
            ->with('message', 'Package '. $nama_addon .' has been deleted!');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function export()
    {
        $current_date = Date("d-m-Y");
        return Excel::download(new PackageAddOnExport, 'Daftar Add On Package ('.$current_date .').xlsx');
    }
}
