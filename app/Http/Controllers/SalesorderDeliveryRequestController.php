<?php

namespace App\Http\Controllers;

use PDF;
use Excel;
use App\Salesorder;
use App\Employee;
use App\SalesAgent;
use App\DeliveryRequest;
use App\SalesOrderTransaction;
use App\SalesOrderDeliveryRequest;
use App\Imports\DeliveryRequestImport;
use Illuminate\Http\Request;
use App\Http\Requests\SalesOrderDeliveryRequestPost;

class SalesorderDeliveryRequestController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($salesorder_id)
    {
        $data['current_date'] = date('Y-m-d');
        $data['salesorder'] = Salesorder::where('id', $salesorder_id)->first();
        $data['sales'] = SalesAgent::where('id', $data['salesorder']->id_sales)->first();

        return view('delivery_request.delivery_request_create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SalesOrderDeliveryRequestPost $request, $salesorder_id)
    {
        $data_so_dr['id_salesorder'] = $salesorder_id;
        $data_so_dr['no_dr'] = $request->no_dr;
        $data_so_dr['request_date'] = $request->request_date;
        $data_so_dr['sales'] = $request->sales;
        $data_so_dr['sales_dept'] = $request->sales_dept;
        $data_so_dr['corporate_name'] = $request->corporate_name;
        $data_so_dr['address_1'] = $request->address_1;
        $data_so_dr['address_2'] = $request->address_2;
        $data_so_dr['address_3'] = $request->address_3;
        $data_so_dr['rt_rw'] = $request->rt_rw;
        $data_so_dr['zip_post'] = $request->zip_post;
        $data_so_dr['pic_corporate_1'] = $request->pic_corporate_1;
        $data_so_dr['pic_phone_1'] = $request->pic_phone_1;
        $data_so_dr['pic_corporate_2'] = $request->pic_corporate_2;
        $data_so_dr['pic_phone_2'] = $request->pic_phone_2;
        $data_so_dr['status'] = 'Draft';

        $so_dr = SalesOrderDeliveryRequest::create($data_so_dr);

        return redirect('/delivery/'.$so_dr->id)
            ->with('status','success')
            ->with('message','Delivery Request '.$request->no_dr .' created successfuly!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SalesOrderDeliveryRequest  $so_dr_id
     * @return \Illuminate\Http\Response
     */
    public function edit($so_dr_id)
    {
        $data['delivery_request'] = DeliveryRequest::where('id_so_dr', $so_dr_id)->orderBy('id', 'asc')->get();
        $data['so_dr'] = SalesOrderDeliveryRequest::where('id', $so_dr_id)->first();
        $data['salesorder'] = Salesorder::where('id', $data['so_dr']->id_salesorder)->first();

        return view('delivery_request.delivery_request_edit', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(SalesOrderDeliveryRequestPost $request, $so_dr_id)
    {
        $data_so_dr['no_dr'] = $request->no_dr;
        $data_so_dr['request_date'] = $request->request_date;
        $data_so_dr['sales'] = $request->sales;
        $data_so_dr['sales_dept'] = $request->sales_dept;
        $data_so_dr['corporate_name'] = $request->corporate_name;
        $data_so_dr['address_1'] = $request->address_1;
        $data_so_dr['address_2'] = $request->address_2;
        $data_so_dr['address_3'] = $request->address_3;
        $data_so_dr['rt_rw'] = $request->rt_rw;
        $data_so_dr['zip_post'] = $request->zip_post;
        $data_so_dr['pic_corporate_1'] = $request->pic_corporate_1;
        $data_so_dr['pic_phone_1'] = $request->pic_phone_1;
        $data_so_dr['pic_corporate_2'] = $request->pic_corporate_2;
        $data_so_dr['pic_phone_2'] = $request->pic_phone_2;

        SalesOrderDeliveryRequest::find($so_dr_id)->update($data_so_dr);

        $so_dr = SalesOrderDeliveryRequest::where('id', $so_dr_id)->first();
        
        return redirect('/salesorder/'.$so_dr->id_salesorder)
            ->with('status','success')
            ->with('message','Delivery Request '.$request->no_dr .' has been updated!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request, $so_dr_id)
    {
        if($request->hasFile('select_file')){
            $this->validate($request, [
                'select_file' => 'required|mimes:xls,xlsx'
            ]);

            $file = $request->file('select_file');
            $nama_file = rand().$file->getClientOriginalName();
            $file->move('file_salesorder_delivery_request', $nama_file);
            Excel::import(new DeliveryRequestImport($so_dr_id), public_path('/file_salesorder_delivery_request/'.$nama_file));
        }

        return redirect('/delivery/'.$so_dr_id)
            ->with('status','success')
            ->with('message','Data Imported successfully!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import_edit(Request $request, $so_dr_id)
    {
        if($request->hasFile('select_file')){
            $this->validate($request, [
                'select_file' => 'required|mimes:xls,xlsx'
            ]);

            $file = $request->file('select_file');
            $nama_file = rand().$file->getClientOriginalName();
            $file->move('file_salesorder_delivery_request', $nama_file);
            Excel::import(new DeliveryRequestImport($so_dr_id), public_path('/file_salesorder_delivery_request/'.$nama_file));
        }

        return redirect('/salesorder/delivery/'.$so_dr_id.'/edit/edit')
            ->with('status','success')
            ->with('message','Data Imported successfully!');
    }

    public function export($so_dr_id)
    {
        $data['so_dr'] = SalesOrderDeliveryRequest::where('id', $so_dr_id)->first();
        $data['delivery_request'] = DeliveryRequest::where('id_so_dr', $data['so_dr']->id)->get();

        if($data['so_dr']->rekap == "on"){
            // $data['so_dr'] = SalesOrderDeliveryRequest::where('id', $so_dr_id)->first();
            // $data['dr'] = DeliveryRequest::where('id_so_dr', $data['so_dr']->id)->get();
            $data['salesorder'] = SalesOrder::where('id', $data['so_dr']->id_salesorder)->first();
            $msisdn = DeliveryRequest::where('id_so_dr', $data['so_dr']->id)->pluck('msisdn')->toArray();

            $last_dr = count($data['delivery_request'])-1;

            if($data['salesorder']->type_req == 'SIM Only'){
                $data['jenis'] = 'SIMCARD';
                foreach($data['delivery_request'] as $dr){
                    $dr['device_type'] = strtoupper($dr['type_bundling']);
                    $dr['brand'] = 'SIMCARD';
                }
            } else {
                $data['jenis'] = 'BUNDLING';
            }

            $total_price = 0;
            foreach($data['delivery_request'] as $dr){
                $price = str_replace( ',', '', $dr["device_price"] );
                $total_price += (int)$price;
            }

            $data['msisdn_min'] = min($msisdn);
            $data['msisdn_max'] = max($msisdn);

            // $trans_min = SalesOrderTransaction::where('msisdn', min($msisdn))->first();
            // $trans_max = SalesOrderTransaction::where('msisdn', max($msisdn))->first();
            $trans_min = DeliveryRequest::where('msisdn', min($msisdn))->first();
            $trans_max = DeliveryRequest::where('msisdn', max($msisdn))->first();
            
            $data['iccid_min'] = $trans_min->iccid;
            $data['iccid_max'] = $trans_max->iccid;
            $data['imei_min'] = $trans_min->imei;
            $data['imei_max'] = $trans_max->imei;
            $data['brand'] = $data['delivery_request'][$last_dr]["brand"];
            $data['device_type'] = $data['delivery_request'][$last_dr]["device_type"];
            $data['device_price'] = number_format($total_price);
            // $data['imei'] = $data['delivery_request'][$last_dr]["imei"];
            $data['quantity'] = number_format(count($data['delivery_request']));
            $data['uom'] = $data['delivery_request'][$last_dr]["uom"];
            $data['destination'] = $data['so_dr']->address_1;
            $data['packing'] = $data['delivery_request'][$last_dr]["packing"];
            $data['note'] = $data['delivery_request'][$last_dr]["note"];

            $data['so_dr']->quote_no = $data['salesorder']->quote_no;
    
            $code = explode('/', $data['so_dr']->no_dr);
            $data['so_dr']->nomor_dr = $code[0];
            $courier = explode('-', $code[1]);
            $data['so_dr']->courier = $courier[1];
    
            $timestamp = strtotime($data['so_dr']->request_date);
            $data['so_dr']->order_date = date("dmy", $timestamp);
    
            $data['tanggal'] = $this->get_tanggal_indo($data['so_dr']->request_date);
            $data['bulan'] = $this->get_bulan_indo($data['so_dr']->request_date);
            
            $data['requestor'] = Employee::where('id', $data['so_dr']->requester)->first();
            $data['manager'] = Employee::where('id', $data['so_dr']->manager)->first();
    
            $pdf = PDF::loadview('delivery_request.pdf.delivery_request_rekap_pdf', $data);
            return $pdf->download('DR '.$data['so_dr']->courier .' '.$data['so_dr']->nomor_dr .'-'.$data['so_dr']->corporate_name .'-'.$data['so_dr']->order_date .'.pdf');

        } else {
            $data['salesorder'] = SalesOrder::where('id', $data['so_dr']->id_salesorder)->first();
            $data['so_dr']->quote_no = $data['salesorder']->quote_no;

            if($data['salesorder']->type_req == 'SIM Only'){
                $data['jenis'] = 'SIMCARD';
                foreach($data['delivery_request'] as $dr){
                    $dr['device_type'] = strtoupper($dr['type_bundling']);
                    $dr['brand'] = 'SIMCARD';
                }
            } else {
                $data['jenis'] = 'BUNDLING';
            }
    
            $code = explode('/', $data['so_dr']->no_dr);
            $data['so_dr']->nomor_dr = $code[0];
            $courier = explode('-', $code[1]);
            $data['so_dr']->courier = $courier[1];
    
            $timestamp = strtotime($data['so_dr']->request_date);
            $data['so_dr']->order_date = date("dmy", $timestamp);
    
            $data['tanggal'] = $this->get_tanggal_indo($data['so_dr']->request_date);
            $data['bulan'] = $this->get_bulan_indo($data['so_dr']->request_date);
            
            $data['requestor'] = Employee::where('id', $data['so_dr']->requester)->first();
            $data['manager'] = Employee::where('id', $data['so_dr']->manager)->first();
    
            $pdf = PDF::loadview('delivery_request.pdf.delivery_request_manual_pdf', $data);
            return $pdf->download('DR '.$data['so_dr']->courier .' '.$data['so_dr']->nomor_dr .'-'.$data['so_dr']->corporate_name .'-'.$data['so_dr']->order_date .'.pdf');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SalesOrderDeliveryRequest  $so_dr_id
     * @return \Illuminate\Http\Response
     */
    public function edit_edit($so_dr_id)
    {
        $data['delivery_request'] = DeliveryRequest::where('id_so_dr', $so_dr_id)->orderBy('id', 'asc')->get();
        $data['so_dr'] = SalesOrderDeliveryRequest::where('id', $so_dr_id)->first();
        $data['salesorder'] = Salesorder::where('id', $data['so_dr']->id_salesorder)->first();

        return view('delivery_request.page_edit.delivery_request_edit', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update_edit(SalesOrderDeliveryRequestPost $request, $so_dr_id)
    {
        $data_so_dr['no_dr'] = $request->no_dr;
        $data_so_dr['request_date'] = $request->request_date;
        $data_so_dr['sales'] = $request->sales;
        $data_so_dr['sales_dept'] = $request->sales_dept;
        $data_so_dr['corporate_name'] = $request->corporate_name;
        $data_so_dr['address_1'] = $request->address_1;
        $data_so_dr['address_2'] = $request->address_2;
        $data_so_dr['address_3'] = $request->address_3;
        $data_so_dr['rt_rw'] = $request->rt_rw;
        $data_so_dr['zip_post'] = $request->zip_post;
        $data_so_dr['pic_corporate_1'] = $request->pic_corporate_1;
        $data_so_dr['pic_phone_1'] = $request->pic_phone_1;
        $data_so_dr['pic_corporate_2'] = $request->pic_corporate_2;
        $data_so_dr['pic_phone_2'] = $request->pic_phone_2;

        SalesOrderDeliveryRequest::find($so_dr_id)->update($data_so_dr);

        $so_dr = SalesOrderDeliveryRequest::where('id', $so_dr_id)->first();
        $salesorder = Salesorder::where('id', $so_dr->id_salesorder)->first();

        if($salesorder->status == "Activation"){
            return redirect('/salesorder/'.$so_dr->id_salesorder.'/activation')
                ->with('status','success')
                ->with('message','Delivery Request '.$request->no_dr .' has been updated!');
        } else {
            return redirect('/salesorder/'.$so_dr->id_salesorder)
                ->with('status','success')
                ->with('message','Delivery Request '.$request->no_dr .' has been updated!');
        }       
    }

    public function verification(Request $request, $so_dr_id){
        $so_dr = SalesOrderDeliveryRequest::where('id', $so_dr_id)->first();

        $data_so_dr['status'] = 'Waiting Approval';
        $data_so_dr['requester'] = $request->requester;
        $data_so_dr['manager'] = $request->manager;

        if($request->check == 'on'){
            $data_so_dr['rekap'] = 'on';
        } else {
            $data_so_dr['rekap'] = null;
        }

        SalesOrderDeliveryRequest::find($so_dr_id)->update($data_so_dr);

        return redirect('/salesorder/'.$so_dr->id_salesorder.'/activation')
            ->with('status','success')
            ->with('message','Delivery Request '.$so_dr->no_dr .' has been sent to Manager!');
    }

    public function courier($so_dr_id){
        $so_dr = SalesOrderDeliveryRequest::where('id', $so_dr_id)->first();

        // SalesOrder::where('id', $so_dr->id_salesorder)->update([
        //     "status" => "In Progress Courier"
        // ]);

        $data_so_dr['status'] = 'In Progress Courier';

        SalesOrderDeliveryRequest::find($so_dr_id)->update($data_so_dr);

        $dr = DeliveryRequest::where('id_so_dr', $so_dr->id)->select('msisdn')->get()->toArray();

        SalesOrderTransaction::whereIn('msisdn', $dr)->update([
            "Status" => 'In Progress Courier'
        ]);

        return redirect('/salesorder/'.$so_dr->id_salesorder.'/activation')
            ->with('status','success')
            ->with('message','No. DR '.$so_dr->no_dr .' has been sent to Courier!');
    }

    // public function signed(Request $request, $so_dr_id){
    //     $so_dr = SalesOrderDeliveryRequest::where('id', $so_dr_id)->first();

    //     $data_so_dr['status'] = 'Signed';

    //     if($request->hasFile('bast_signed')){
    //         $file_name = $request->file('bast_signed')->getClientOriginalName();
    //         $path_album ='/public/upload/bast_signed/'.$so_dr->id_salesorder;
    //         $path = $request->file('bast_signed')->storeAs($path_album, $file_name);
    //         $path_bast_signed = \Storage::url($path);
    //         $data_so_dr['bast_signed'] = $path_bast_signed;
    //     }

    //     SalesOrderDeliveryRequest::find($so_dr_id)->update($data_so_dr);

    //     $dr = DeliveryRequest::where('id_so_dr', $so_dr->id)->select('msisdn')->get()->toArray();

    //     SalesOrderTransaction::whereIn('msisdn', $dr)->update([
    //         "Status" => 'Delivered'
    //     ]);

    //     return redirect('/salesorder/'.$so_dr->id_salesorder.'/activation')
    //         ->with('status','success')
    //         ->with('message','BAST Signed '.$so_dr->no_dr .' has been uploaded!');
    // }

    public function get_tanggal_indo($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);
    
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }

    public function get_bulan_indo($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);
    
        return $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
}
