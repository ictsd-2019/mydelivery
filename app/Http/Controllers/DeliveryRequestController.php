<?php

namespace App\Http\Controllers;

use App\Salesorder;
use App\DeliveryRequest;
use App\SalesOrderTransaction;
use App\SalesOrderDeliveryRequest;
use Illuminate\Http\Request;
use App\Http\Requests\DeliveryRequestPost;

class DeliveryRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($so_dr_id)
    {
        $data['delivery_request'] = DeliveryRequest::where('id_so_dr', $so_dr_id)->orderBy('id', 'asc')->get();
        $data['so_dr'] = SalesOrderDeliveryRequest::where('id', $so_dr_id)->first();
        $data['salesorder'] = Salesorder::where('id', $data['so_dr']->id_salesorder)->first();

        return view('delivery_request.delivery_request_index', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DeliveryRequestPost $request, $so_dr_id)
    {
        $so_dr = SalesOrderDeliveryRequest::where('id', $so_dr_id)->first();

        $data_dr['id_so_dr'] = $so_dr_id;
        $data_dr['msisdn'] = $request->msisdn;
        $data_dr['iccid'] = $request->iccid;
        $data_dr['brand'] = $request->brand;
        $data_dr['device_type'] = $request->device_type;
        $data_dr['device_price'] = str_replace(",", "", $request->device_price);
        $data_dr['imei'] = $request->imei;
        $data_dr['quantity'] = str_replace(",", "", $request->quantity);
        $data_dr['uom'] = $request->uom;
        $data_dr['destination'] = $request->destination;
        $data_dr['packing'] = $request->packing;
        $data_dr['notes'] = $request->notes;

        DeliveryRequest::create($data_dr);

        return redirect('/delivery/'.$so_dr->id)
            ->with('status','success')
            ->with('message','Generate DR '.$request->no_dr .' created successfuly!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DeliveryRequest  $delivery_request_id
     * @return \Illuminate\Http\Response
     */
    public function update(DeliveryRequestPost $request, $delivery_request_id)
    {
        $data_delivery_request['msisdn'] = $request->msisdn;
        $data_delivery_request['iccid'] = $request->iccid;
        $data_delivery_request['brand'] = $request->brand;
        $data_delivery_request['device_type'] = $request->device_type;
        $data_delivery_request['device_price'] = $request->device_price;
        $data_delivery_request['imei'] = $request->imei;
        $data_delivery_request['quantity'] = $request->quantity;
        $data_delivery_request['uom'] = $request->uom;        
        $data_delivery_request['destination'] = $request->destination;
        $data_delivery_request['packing'] = $request->packing;
        $data_delivery_request['notes'] = $request->notes;

        DeliveryRequest::find($delivery_request_id)->update($data_delivery_request);

        $delivery_request = DeliveryRequest::where('id', $delivery_request_id)->first();
        $so_dr = SalesOrderDeliveryRequest::where('id', $delivery_request->id_so_dr)->first();
        
        return redirect('/delivery/'.$so_dr->id)
            ->with('status','success')
            ->with('message', 'Delivery Request has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DeliveryRequest  $delivery_request_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($delivery_request_id)
    {
        $delivery_request = DeliveryRequest::findOrFail($delivery_request_id);
        $delivery_request_device = $delivery_request['device_type'];

        SalesOrderTransaction::where('msisdn', $delivery_request['msisdn'])->update([
            'status' => 'Available'
        ]);

        $so_dr = SalesOrderDeliveryRequest::where('id', $delivery_request['id_so_dr'])->first();
        $delivery_request->delete();

        return redirect('/delivery/'.$so_dr->id)
            ->with('status','success')
            ->with('message', 'Delivery Request '. $delivery_request_device .' has been deleted!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_edit(DeliveryRequestPost $request, $so_dr_id)
    {
        $so_dr = SalesOrderDeliveryRequest::where('id', $so_dr_id)->first();

        $data_dr['id_so_dr'] = $so_dr_id;
        $data_dr['msisdn'] = $request->msisdn;
        $data_dr['iccid'] = $request->iccid;
        $data_dr['brand'] = $request->brand;
        $data_dr['device_type'] = $request->device_type;
        $data_dr['device_price'] = str_replace(",", "", $request->device_price);
        $data_dr['imei'] = $request->imei;
        $data_dr['quantity'] = str_replace(",", "", $request->quantity);
        $data_dr['uom'] = $request->uom;
        $data_dr['destination'] = $request->destination;
        $data_dr['packing'] = $request->packing;
        $data_dr['notes'] = $request->notes;

        DeliveryRequest::create($data_dr);

        return redirect('/salesorder/delivery/'.$so_dr_id.'/edit/edit')
            ->with('status','success')
            ->with('message','Generate DR '.$request->no_dr .' created successfuly!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DeliveryRequest  $delivery_request_id
     * @return \Illuminate\Http\Response
     */
    public function update_edit(DeliveryRequestPost $request, $delivery_request_id)
    {
        $data_delivery_request['msisdn'] = $request->msisdn;
        $data_delivery_request['iccid'] = $request->iccid;
        $data_delivery_request['brand'] = $request->brand;
        $data_delivery_request['device_type'] = $request->device_type;
        $data_delivery_request['device_price'] = $request->device_price;
        $data_delivery_request['imei'] = $request->imei;
        $data_delivery_request['quantity'] = $request->quantity;
        $data_delivery_request['uom'] = $request->uom;        
        $data_delivery_request['destination'] = $request->destination;
        $data_delivery_request['packing'] = $request->packing;
        $data_delivery_request['notes'] = $request->notes;

        DeliveryRequest::find($delivery_request_id)->update($data_delivery_request);

        $delivery_request = DeliveryRequest::where('id', $delivery_request_id)->first();
        $so_dr = SalesOrderDeliveryRequest::where('id', $delivery_request->id_so_dr)->first();

        return redirect('/salesorder/delivery/'.$so_dr->id.'/edit/edit')
            ->with('status','success')
            ->with('message', 'Delivery Request has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DeliveryRequest  $delivery_request_id
     * @return \Illuminate\Http\Response
     */
    public function destroy_edit($delivery_request_id)
    {
        $delivery_request = DeliveryRequest::findOrFail($delivery_request_id);
        $delivery_request_device = $delivery_request['device_type'];

        SalesOrderTransaction::where('msisdn', $delivery_request['msisdn'])->update([
            'status' => 'Available'
        ]);

        $so_dr = SalesOrderDeliveryRequest::where('id', $delivery_request['id_so_dr'])->first();
        $delivery_request->delete();

        return redirect('/salesorder/delivery/'.$so_dr->id.'/edit/edit')
            ->with('status','success')
            ->with('message', 'Delivery Request '. $delivery_request_device .' has been deleted!');
    }
}
