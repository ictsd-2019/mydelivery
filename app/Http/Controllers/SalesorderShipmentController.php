<?php

namespace App\Http\Controllers;

use PDF;
use App\Employee;
use App\Shipment;
use App\SalesOrder;
use App\SalesAgent;
use App\SalesOrderShipment;
use App\SalesOrderTransaction;
use App\Http\Requests\SalesOrderShipmentPost;
use Illuminate\Http\Request;

class SalesorderShipmentController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($salesorder_id)
    {
        $data['current_date'] = date('Y-m-d');
        $data['salesorder'] = SalesOrder::where('id', $salesorder_id)->first();
        $data['sales'] = SalesAgent::where('id', $data['salesorder']->id_sales)->first();
        $data['list_msisdn'] = SalesOrderTransaction::where('id_salesorder', $salesorder_id)->where('status', 'Available')->pluck('msisdn', 'msisdn');

        return view('shipment.shipment_create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SalesOrderShipmentPost $request, $id_salesorder)
    {
        $data_so_shipment['id_salesorder'] = $id_salesorder;
        $data_so_shipment['request_date'] = $request->request_date;
        $data_so_shipment['sales'] = $request->sales;
        $data_so_shipment['sales_dept'] = $request->sales_dept;
        $data_so_shipment['corporate_name'] = $request->corporate_name;
        $data_so_shipment['address_1'] = $request->address_1;
        $data_so_shipment['address_2'] = $request->address_2;
        $data_so_shipment['address_3'] = $request->address_3;
        $data_so_shipment['rt_rw'] = $request->rt_rw;
        $data_so_shipment['zip_post'] = $request->zip_post;
        $data_so_shipment['pic_corporate_1'] = $request->pic_corporate_1;
        $data_so_shipment['pic_phone_1'] = $request->pic_phone_1;
        $data_so_shipment['pic_corporate_2'] = $request->pic_corporate_2;
        $data_so_shipment['pic_phone_2'] = $request->pic_phone_2;
        $data_so_shipment['status'] = 'Draft';
        
        if($request->hasFile('form_shipment')){
            $file_name = $request->file('form_shipment')->getClientOriginalName();
            $path_album ='/public/upload/shipment/'.$id_salesorder;
            $path = $request->file('form_shipment')->storeAs($path_album, $file_name);
            $path_form_shipment = \Storage::url($path);
            $data_so_shipment['form_shipment'] = $path_form_shipment;
        }

        $shipment = SalesOrderShipment::create($data_so_shipment);

        $data_imei = explode("-", $request->imei);
        if(count($request->msisdn) != count($data_imei)){
            return redirect('/salesorder/shipment/'.$id_salesorder .'/activation')
                ->with('status','danger')
                ->with('message','Jumlah data MSISDN dengan IMEI harus sinkron!');
        }

        $counter = 0;
        foreach($request->msisdn as $msisdn){
            $data_shipment['id_so_shipment'] = $shipment->id;
            SalesOrderTransaction::where('msisdn', $msisdn)->update([
                'status' => 'On Shipment'
            ]);
            $data_shipment['imei'] = $data_imei[$counter];
            $data_shipment['msisdn'] = $msisdn;
            Shipment::create($data_shipment);
            $counter++;
        }

        return redirect('/salesorder/'.$id_salesorder .'/activation')
            ->with('status','success')
            ->with('message','File Shipment created successfuly!');
    }

/**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SalesOrderShipment  $so_shipment_id
     * @return \Illuminate\Http\Response
     */
    public function edit($so_shipment_id)
    {
        $data['shipment'] = SalesOrderShipment::where('id', $so_shipment_id)->first();
        $data['ship'] = Shipment::where('id_so_shipment', $so_shipment_id)->get();
        $data['salesorder'] = Salesorder::where('id', $data['shipment']->id_salesorder)->first();
        $data['list_msisdn'] = SalesOrderTransaction::where('id_salesorder', $data['shipment']->id_salesorder)->pluck('msisdn', 'msisdn');
        $data['selected_msisdn'] = Shipment::where('id_so_shipment', $so_shipment_id)->get()->toArray();
        $data['sales'] = SalesAgent::where('id', $data['salesorder']->id_sales)->first();
        $data['imei'] = '';
        $data['select_msisdn'] = [];
        
        foreach($data['selected_msisdn'] as $msisdn){
            array_push($data['select_msisdn'], $msisdn["msisdn"]);
        }

        for($i=0; $i<count($data['ship']); $i++){
            if($i == 0){
            $data['imei'] .= $data['ship'][$i]->imei;
            } else {
            $data['imei'] .= '-'.$data['ship'][$i]->imei;
            }
        }

        return view('shipment.shipment_edit', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $so_shipment_id)
    {
        $data_imei = explode("-", $request->imei);
        if(count($request->msisdn) != count($data_imei)){
            return redirect('/salesorder/shipment/'.$so_shipment_id .'/edit')
                ->with('status','danger')
                ->with('message','Jumlah data MSISDN dengan IMEI harus sinkron!');
        }

        $shipment = SalesOrderShipment::where('id', $so_shipment_id)->first();

        $data_so_shipment['request_date'] = $request->request_date;
        $data_so_shipment['sales'] = $request->sales;
        $data_so_shipment['sales_dept'] = $request->sales_dept;
        $data_so_shipment['corporate_name'] = $request->corporate_name;
        $data_so_shipment['address_1'] = $request->address_1;
        $data_so_shipment['address_2'] = $request->address_2;
        $data_so_shipment['address_3'] = $request->address_3;
        $data_so_shipment['rt_rw'] = $request->rt_rw;
        $data_so_shipment['zip_post'] = $request->zip_post;
        $data_so_shipment['pic_corporate_1'] = $request->pic_corporate_1;
        $data_so_shipment['pic_phone_1'] = $request->pic_phone_1;
        $data_so_shipment['pic_corporate_2'] = $request->pic_corporate_2;
        $data_so_shipment['pic_phone_2'] = $request->pic_phone_2;
        
        if($request->hasFile('form_shipment')){
            // PAKE str_replace YANG DI SPASI JADI _
            $file_name = $request->file('form_shipment')->getClientOriginalName();
            $path_album ='/public/upload/shipment/'.$shipment->id_salesorder;
            $path = $request->file('form_shipment')->storeAs($path_album, $file_name);
            $path_form_shipment = \Storage::url($path);
            $data_so_shipment['form_shipment'] = $path_form_shipment;
        }

        SalesOrderShipment::where('id', $so_shipment_id)->update($data_so_shipment);
        
        $ship = Shipment::where('id_so_shipment', $so_shipment_id)->pluck('msisdn')->toArray();
        SalesOrderTransaction::whereIn('msisdn', $ship)->update([
            'status' => 'Available'
        ]);
        Shipment::where('id_so_shipment', $so_shipment_id)->delete();

        $counter = 0;
        foreach($request->msisdn as $msisdn){
            $data_shipment['id_so_shipment'] = $shipment->id;
            SalesOrderTransaction::where('msisdn', $msisdn)->update([
                'status' => 'On Shipment'
            ]);
            
            $data_shipment['imei'] = $data_imei[$counter];
            $data_shipment['msisdn'] = $msisdn;
            
            Shipment::create($data_shipment);
            $counter++;
        }

        return redirect('/salesorder/'.$shipment->id_salesorder .'/activation')
            ->with('status','success')
            ->with('message','File Shipment update successfuly!');
    }

    public function export($so_shipment_id, Request $request)
    {
        // if($request->check == "on"){
        //     $data['so_dr'] = SalesOrderDeliveryRequest::where('id', $so_dr_id)->first();
        //     $data['dr'] = DeliveryRequest::where('id_so_dr', $data['so_dr']->id)->get();
        //     $quote = SalesOrder::where('id', $data['so_dr']->id_salesorder)->first();
        //     $msisdn = DeliveryRequest::where('id_so_dr', $data['so_dr']->id)->pluck('msisdn')->toArray();

        //     $last_dr = count($data['dr'])-1;

        //     $total_price = 0;
        //     foreach($data['dr'] as $dr){
        //         $price = str_replace( ',', '', $dr["device_price"] );
        //         $total_price += (int)$price;
        //     }

        //     $data['msisdn_min'] = min($msisdn);
        //     $data['msisdn_max'] = max($msisdn);

        //     $iccid_min = SalesOrderTransaction::where('msisdn', min($msisdn))->first();
        //     $iccid_max = SalesOrderTransaction::where('msisdn', max($msisdn))->first();

        //     $data['iccid_min'] = $iccid_min->iccid;
        //     $data['iccid_max'] = $iccid_max->iccid;
        //     $data['brand'] = $data['dr'][$last_dr]["brand"];
        //     $data['device_type'] = $data['dr'][$last_dr]["device_type"];
        //     $data['device_price'] = number_format($total_price);
        //     $data['imei'] = $data['dr'][$last_dr]["imei"];
        //     $data['quantity'] = number_format(count($data['dr']));
        //     $data['uom'] = $data['dr'][$last_dr]["uom"];
        //     $data['destination'] = $data['so_dr']->address_1;
        //     $data['packing'] = $data['dr'][$last_dr]["packing"];
        //     $data['note'] = $data['dr'][$last_dr]["note"];

        //     $data['so_dr']->quote_no = $quote->quote_no;
    
        //     $code = explode('/', $data['so_dr']->no_dr);
        //     $data['so_dr']->nomor_dr = $code[0];
        //     $courier = explode('-', $code[1]);
        //     $data['so_dr']->courier = $courier[1];
    
        //     $timestamp = strtotime($data['so_dr']->request_date);
        //     $data['so_dr']->order_date = date("dmy", $timestamp);
    
        //     $data['tanggal'] = $this->get_tanggal_indo($data['so_dr']->request_date);
        //     $data['bulan'] = $this->get_bulan_indo($data['so_dr']->request_date);
            
        //     $data['requestor'] = Employee::where('id', $request->requester)->first();
        //     $data['manager'] = Employee::where('id', $request->manager)->first();
    
        //     $pdf = PDF::loadview('delivery_request.pdf.delivery_request_rekap_pdf', $data);
        //     return $pdf->download('DR '.$data['so_dr']->courier .' '.$data['so_dr']->nomor_dr .'-'.$data['so_dr']->corporate_name .'-'.$data['so_dr']->order_date .'.pdf');

        // } else {
            $data['so_shipment'] = SalesOrderShipment::where('id', $so_shipment_id)->first();
            // $data['list_msisdn'] = Shipment::where('id_so_shipment', $data['so_shipment']->id)->pluck('msisdn')->toArray();
            $data['list_shipment'] = Shipment::where('id_so_shipment', $data['so_shipment']->id)->get();
            
            $data['transaction'] = [];
            for($i=0; $i<count($data['list_shipment']); $i++){
                $edit = SalesOrderTransaction::where('id_salesorder', $data['so_shipment']->id_salesorder)->where('msisdn', $data['list_shipment'][$i]->msisdn)->first();
                $edit['imei'] = $data['list_shipment'][$i]->imei;
                array_push($data['transaction'], $edit);
                
            }

            // $data['transaction'] = SalesOrderTransaction::where('id_salesorder', $data['so_shipment']->id_salesorder)->whereIn('msisdn', $data['list_msisdn'])->get();
            $quote = SalesOrder::where('id', $data['so_shipment']->id_salesorder)->first();

            $data['so_shipment']->quote_no = $quote->quote_no;

            if($quote->type_req == 'SIM Only'){
                $data['jenis'] = 'SIMCARD';
                foreach($data['transaction'] as $trans){
                    $trans['device_type'] = strtoupper($trans['type_bundling']);
                    $trans['brand'] = 'SIMCARD';
                }
            } else {
                $data['jenis'] = 'DEVICE';
            }
    
            $timestamp = strtotime($data['so_shipment']->request_date);
            $data['so_shipment']->order_date = date("dmy", $timestamp);
    
            $data['tanggal'] = $this->get_tanggal_indo($data['so_shipment']->request_date);
            $data['bulan'] = $this->get_bulan_indo($data['so_shipment']->request_date);
    
            $pdf = PDF::loadview('shipment.pdf.shipment_manual', $data);
            return $pdf->download('BAST '.$data['so_shipment']->corporate_name .' - '.$data['so_shipment']->sales .'.pdf');
        // }
    }

    public function courier($so_shipment_id){
        $so_shipment = SalesOrderShipment::where('id', $so_shipment_id)->first();

        $data_so_shipment['status'] = 'In Progress Courier';

        SalesOrderShipment::find($so_shipment_id)->update($data_so_shipment);

        $shipment = Shipment::where('id_so_shipment', $so_shipment->id)->select('msisdn')->get()->toArray();

        SalesOrderTransaction::whereIn('msisdn', $shipment)->update([
            "Status" => 'In Progress Courier'
        ]);

        return redirect('/salesorder/'.$so_shipment->id_salesorder.'/activation')
            ->with('status','success')
            ->with('message','The file has been sent to Courier!');
    }

    public function get_tanggal_indo($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);
    
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }

    public function get_bulan_indo($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);
    
        return $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }

}
