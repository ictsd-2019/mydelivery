<?php

namespace App\Http\Controllers;

use Excel;
use App\User;
use App\Employee;
use App\Exports\EmployeeExport;
use Illuminate\Http\Request;
use App\Http\Requests\EmployeePost;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $datas['employee'] = Employee::orderBy('name', 'asc')->get();
        //dd($data['employee']);
        return view('employee.manage_employee_index', $datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $data['user'] = User::orderBy('fullname', 'asc')->pluck('fullname', 'id');
        //dd($data['user']);
        return view('employee.manage_employee_create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeePost $request)
    {
        if($request->hasFile('signature')){
            $imageName = request()->signature->getClientOriginalName();
            $path      = request()->signature->storeAs('/public/upload', $imageName);
            $pathFoto  = \Storage::url($path);
        } else {
            $pathFoto = null;
        }

        Employee::create([
            'nik' => $request['nik'],
            'name' => $request['name'],
            'position' => $request['position'],
            'group' => $request['group'],
            'division' => $request['division'],
            'department' => $request['department'],
            'signature' => $pathFoto,
            'id_user' => $request['user']
        ]);

        return redirect('/employee')
            ->with('status','success')
            ->with('message', 'Signature '. $request['name'] .' has been created!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee_id
     * @return \Illuminate\Http\Response
     */
    public function edit($employee_id)
    {
   
        $data['employee'] = Employee::find($employee_id);
        //dd($data['employee']);
        $data['user'] = User::orderBy('fullname', 'asc')->pluck('fullname', 'id');
        // $data['selected_user'] = User::where('id', $data['employee']["id_user"])->first();
        $data['selected_user'] = User::orderBy('username', 'asc')->pluck('username', 'id');
        //dd($data['selected_user']);
        //$data['region'] = Region::pluck('name', 'id');
      
        return view('employee.manage_employee_edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee_id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeePost $request, $employee_id)
    {
        if($request->hasFile('signature')){
            $imageName = request()->signature->getClientOriginalName();
            $path = request()->signature->storeAs('/public/upload', $imageName);
            $pathFoto = \Storage::url($path);
            Employee::where('id', $employee_id)->update([
                'signature' => $pathFoto
            ]);
        }

        $employee = Employee::where('id', $employee_id)->first();
        $data_employee['nik'] = $request->nik;
        $data_employee['name'] = $request->name;
        $data_employee['position'] = $request->position;
        $data_employee['division'] = $request->division;
        $data_employee['department'] = $request->department;
        $data_employee['group'] = $request->group;
        $data_employee['id_user'] = $request->user;
        Employee::find($employee_id)->update($data_employee);

        return redirect('/employee')
            ->with('status','success')
            ->with('message', 'Signature '. $request['name'] .' has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($employee_id)
    {
        $employee = Employee::find($employee_id);
        $nama_employee = $employee->name;
        $employee->delete();

        return redirect('/employee')
            ->with('status','success')
            ->with('message', 'Signature '. $nama_employee .' has been deleted!');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function export()
    {
        $current_date = Date("d-m-Y");
        return Excel::download(new EmployeeExport, 'Daftar Employee ('.$current_date .').xlsx');
    }
}
