<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Arrival;
use App\OutTask;
use App\Http\Requests\ArrivalPost;
use Excel;

class ArrivalController extends Controller
{

    public function index()
    {
        //$datas['arrival'] = Arrival::orderBy('arrival_fee', 'asc')->get();
        $datas['arrival'] = Arrival::join('outtask', 'outtask.id', '=', 'arrival.id_outtask')
                            ->orderBy('arrival_fee', 'asc')->get(['arrival.*', 'outtask.name']);   
        return view('arrival.manage_arrival_index', $datas);
    }

    public function create()
    {
        $datas['selected_vendor'] = OutTask::orderBy('name', 'asc')->pluck('name', 'id');
        return view('arrival.manage_arrival_create', $datas);
    }

    public function store(ArrivalPost $request)
    {
        //$noUrutAkhir = \App\Arrival::max('id_arrival');
        $num         = mt_rand(1000, 9999);
        $no          = $num+1;

        Arrival::create([
            'id_arrival'    => $no,
            'id_outtask'    => $request['vendor'],
            'day_work'      => $request['day'],
            'arrival_fee'   => $request['fee'],
            'arrival_price' => $request['price'],
            'ket'           => $request['note']
        ]);

        return redirect('/arrival')
            ->with('status','success')
            ->with('message', 'Data Arriva Fee  has been created!');
    }

    public function edit($arrival_id)
    {

        $kode                     = base64_decode($arrival_id);
        $datas['arrival']         = Arrival::find($kode);
        $datas['selected_vendor'] = OutTask::orderBy('name', 'asc')->pluck('name', 'id');
        $datas['selected_day']    = Arrival::where('id', $kode)->first();
        return view('arrival.manage_arrival_edit', $datas);

    }

    public function update(ArrivalPost $request, $id)
    {
        $datas                      = Arrival::where('id', $id)->first();
        $data_type['id_arrival']    = $request->id;
        $data_type['id_outtask']    = $request->vendor;
        $data_type['day_work']      = $request->day;
        $data_type['arrival_fee']   = $request->fee;
        $data_type['arrival_price'] = $request->price;
        Arrival::find($id)->update($data_type);

        return redirect('/arrival')
            ->with('status','success')
            ->with('message', 'Data Arrival Fee has been updated!');
    }

    public function destroy($id)
    {
        $kode     = base64_decode($id);
        $datas    = Arrival::find($kode);
        $arrival  = $datas->arrival_fee;
        $datas->delete();

        return redirect('/arrival')
            ->with('status','success')
            ->with('message', 'Data Arrival Fee : '. $arrival .' has been deleted!');
    }
}