<?php

namespace App\Http\Controllers;

use App\Menu;
use Session;
use App\Http\Requests\MenuStore;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function index()
    {
        $menus = Menu::paginate(25);
        return view('admin.menu.index', compact('menus'));
    }

    public function create(Menu $menu)
    {
        return view('admin.menu.create', compact('menu'));
    }

    public function store(MenuStore $req)
    {
        $data['title'] = $req->title;
        $data['group'] = $req->group;
        $data['path'] = $req->path;
        $data['permission'] = $req->permission;
        $data['icon3'] = $req->icon3;
        $data['icon4'] = $req->icon3;
        $data['status'] = $req->status;

        Menu::create($data);
            
        return redirect()->route('menu.index')
            ->with('status','success')
            ->with('message', $req->title .' has been created!');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $menu = Menu::findOrFail($id);

        return view('admin.menu.edit', compact('menu'));
    }

    public function update(Request $req, $id)
    {
        $menu = Menu::findOrFail($id)->update($req->all());

        return redirect()->route('menu.index')
            ->with('status','success')
            ->with('message', $req->title .' has been updated!');
    }

    public function destroy($id)
    {
        Menu::findOrFail($id)->delete();

        return redirect()->route('menu.index')
            ->with('status','success')
            ->with('message', 'Menu has been deleted!');
    }
}
