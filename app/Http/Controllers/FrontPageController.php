<?php

namespace App\Http\Controllers;

use Auth;
use App\Menu;
use Illuminate\Http\Request;

class FrontPageController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $permissions = [];

        foreach($user->getAllPermissions() as $key => $permission)
        {
            $permissions[$key] = $permission->name;
        }

        $data['menus'] = Menu::whereIn('permission', $permissions)->where('status', 1)->get();

        return view('frontpage.index', $data);
    }
}
