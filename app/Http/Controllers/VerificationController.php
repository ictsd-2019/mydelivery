<?php

namespace App\Http\Controllers;

use Auth;
use App\Employee;
use App\DeliveryRequest;
use App\SalesOrderDeliveryRequest;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_delivery_request()
    {
        $employee = Employee::where('id_user', (string)Auth::user()->id)->first();
        $data['salesorder_delivery_request'] = SalesOrderDeliveryRequest::where('manager', $employee->id)
        ->where('status', 'Waiting Approval')
        ->orderBy('id', 'desc')->get();

        if($data['salesorder_delivery_request'] != null){
            for($i=0; $i<count($data['salesorder_delivery_request']); $i++){
                $data['salesorder_delivery_request'][$i]['request_date'] = implode('-', array_reverse(explode('-', $data['salesorder_delivery_request'][$i]['request_date'])));   

                $data['salesorder_delivery_request'][$i]['quantity'] = 0;
                $data['delivery_request'] = DeliveryRequest::where('id_so_dr', $data['salesorder_delivery_request'][$i]->id)->get();
                foreach($data['delivery_request'] as $dr){
                    $data['salesorder_delivery_request'][$i]['quantity'] += $dr['quantity'];
                }
                $data['salesorder_delivery_request'][$i]['quantity'] = number_format($data['salesorder_delivery_request'][$i]['quantity']);
            }
        }

        return view('delivery_request.verification.delivery_request_index', $data);
    }

    public function reject_dr($so_dr_id){
        $so_dr = SalesOrderDeliveryRequest::where('id', $so_dr_id)->first();

        $data_so_dr['status'] = 'Draft';

        SalesOrderDeliveryRequest::find($so_dr_id)->update($data_so_dr);

        return redirect('/approval-dr')
            ->with('status','success')
            ->with('message','Delivery Request '.$so_dr->no_dr .' has been rejected!');
    }

    public function approve_dr($so_dr_id){
        $so_dr = SalesOrderDeliveryRequest::where('id', $so_dr_id)->first();

        $data_so_dr['status'] = 'Approved';

        SalesOrderDeliveryRequest::find($so_dr_id)->update($data_so_dr);

        return redirect('/approval-dr')
            ->with('status','success')
            ->with('message','Delivery Request '.$so_dr->no_dr .' has been approved!');
    }
    
}
