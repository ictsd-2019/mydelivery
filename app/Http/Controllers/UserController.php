<?php

namespace App\Http\Controllers;

use Auth;
use Request;
use App\User;
use App\SalesAgent;
use App\SalesAdmin;
use App\Region;
// use App\UAProfile;
use Carbon\Carbon;
// use App\ActivityLog;
use App\Http\Requests\AddNewUser;
use App\Http\Requests\UpdateUser;
use Spatie\Permission\Models\Role;
// use Illuminate\Support\Facades\Input;
use Spatie\Permission\Models\Permission;
use App\Http\Controllers\Auth\RegisterController;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        validate_permission('manage user');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $data['users'] = User::orderBy('fullname', 'asc')->get();

        $current_date = Carbon::now()->format('Y-m-d');
        $data['total_users'] = User::count();
        $data['active_users'] = User::whereNotNull('last_login')->count();
        $data['disable_users'] = User::where('status', 2)->count();
        $data['today_active'] = User::where('last_login', '>=', $current_date ." 00:00:00")
                            ->where('last_login', '<=', $current_date ." 23:59:59")
                            ->count();

        foreach($data['users'] as $user)
        {
            if($user['region_id']){
                $user['region'] = Region::where('id', $user['region_id'])->first();
            }

            $roles = $user->roles()->get()->toArray();
            $user['role'] = implode(', ', json_decode($user->roles()->pluck('name')));
        }

        // Log activity
        // ActivityLog::info('USER','View User Management page');

    	return view('admin.user.index_user', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $array_status = [
            ['status' => ['name' => 'Active', 'value' => 1]],
            ['status' => ['name' => 'In-Active', 'value' => 2]]
        ];
    
        $status = array_pluck($array_status, 'status.name', 'status.value');
        $data['roles'] = Role::all()->pluck("name", "name")->except(['Administrator']);
        $data['manager'] = User::all()->pluck("fullname", "id")->prepend('- Select -', "");
        $data['region'] = Region::all()->pluck("name", "id")->prepend('- Select -', "");
        // $data['store'] = Store::where('status', 1)->isActive()->pluck("siebel_name", "id")->prepend('- Select -', "");
        $data['active_role'] = null;
        $data['active_manager'] = null;
        $data['active_region'] = null;
        $data['active_store'] = null;
        $data['status'] = $status;

        // Log activity
        // ActivityLog::info('USER','View Create User page');

        return view('admin.user.add_user', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddNewUser $request)
    {
        $user = User::create([
            'username' => $request['username'],
            'email' => $request['email'],
            'fullname' => $request['fullname'],
            'nik' => $request['nik'],
            'phone' => $request['phone'],
            'password' => bcrypt($request['password']),
            'manager_id' => $request['manager_id'],
            'region_id' => $request['region_id'],
            'store_id' => $request['store_id'],
            'status' => $request['status'] != null ? $request['status'] : 1,
            'last_login' => date("Y-m-d H:i:s", time())
        ]);
        
        foreach($request->role as $role){
            if($role == "Sales"){
                $data['id_user'] = $user->id;
                $data['nama'] = strtoupper($user->fullname);
                $data['department'] = strtoupper($user->region_id);
                $data['divisi'] = null;
                $data['group'] = null;
                $tes = SalesAgent::create($data);
            } else if($role == "Sales Admin"){
                SalesAdmin::create([
                    'id_user' => $user->id,
                    'nama' => $request['fullname'],
                    'department' => $request['region_id'],
                    'divisi' => null,
                    'group' => null
                ]);
            }
            
            // if($role == "Trainee")
            // {
            //     UAProfile::create(['user_id' => $user->id]);
            // }

            $user->assignRole($role);
        }
        // Log activity
        // ActivityLog::info('USER','Submitted new User', ['username' => $request['username']]);

        return redirect('config/user')
            ->with('status','success')
            ->with('message', 'User \''. $request['username'] .'\' have been created!');
    }
  
     /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('admin.user.show_user');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($user_id)
    {
        $user = User::find($user_id);
        $active_role = $user->roles()->pluck('name')->all();
        $user_roles = $user->roles()->pluck('name')->toArray();
        $array_status = [
            ['status' => ['name' => 'Active', 'value' => 1]],
            ['status' => ['name' => 'In-Active', 'value' => 2]]
        ];

        $status = array_pluck($array_status, 'status.name', 'status.value');

        if(in_array("Administrator", $user_roles) && Auth::user()->roles()->pluck('name')->first() != "Administrator")
        {
            // Log activity
            // ActivityLog::info('USER','Failed editing an Administrator user', ['user_id' => $user_id]);

            return view('admin.user.edit_admin_error');
        }
        else
        {
            $roles = Role::all()->pluck("name", "name")->except(['Administrator']);
            $user = User::findOrFail($user_id);
            $manager = User::orderBy('fullname','ASC')->pluck("fullname", "id")->prepend('- Select -', "");
            $region = Region::all()->pluck("name", "id")->prepend('- Select -', "");
            // $store = Store::pluck("siebel_name", "id")->prepend('- Select -', "");

            $data = [
                'status' => $status,
                'user' => $user,
                'roles' => $roles,
                'manager' => $manager,
                'region' => $region,
                // 'store' => $store,
                'active_manager' => $user->manager_id,
                'active_region' => $user->region_id,
                'active_store' => $user->store_id,
                'active_role' => $active_role
            ];

            // Log activity
            // ActivityLog::info('USER','Edit User page', ['user_id' => $user_id]);

            return view('admin.user.edit_user', $data);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUser $request, User $user)
    {

    	if($request['password'] === null)
    	{
    		$request['password'] = User::where('id', $user->id)->value('password');
    	}
    	else
    	{
    		$request['password'] = bcrypt($request['password']);
    	}

    	User::findOrFail($user->id)->update($request->all());

        $active_roles = $user->roles()->pluck('name');

        foreach($active_roles as $active_role)
        {
            $user->removeRole($active_role);
        }
   
        $user->assignRole($request['role']);

        // Log activity
        // ActivityLog::info('USER','Updated existing User', ['user_id' => $user->id]);

        return redirect('config/user')
            ->with('status','success')
            ->with('message', 'User \''. $request['username'] .'\' have been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
