<?php

namespace App\Http\Controllers;

use App\Salesorder;
use App\SalesOrderRemarks;
use Illuminate\Http\Request;

class SalesorderRemarksController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_remarks(Request $request, $salesorder_id)
    {
        $data['id_salesorder'] = $salesorder_id;
        $data['date_create'] = date("Y-m-d h:i:s");
        $data['remarks'] = $request->remarks;
        $data['id_user'] = \Auth::user()->id;

        SalesOrderRemarks::create($data);

        $salesorder = Salesorder::where('id', $salesorder_id)->first();

        if(\Auth::user()->hasPermissionTo('verificate sales order')){
            return redirect('/web/salesorder')
                ->with('status','success')
                ->with('message','Remarks Quote '.$salesorder->quote_no .' created successfuly!');
        }
        return redirect('/salesorder')
            ->with('status','success')
            ->with('message','Remarks Quote '.$salesorder->quote_no .' created successfuly!');
    }
}
