<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Auth\SessionGuard;

class SigninController extends Controller
{
    use AuthenticatesUsers;

    public function form()
    {
        return view('auth.signin');
    }

    public function attempt(Request $request)
    {
        // dd((bool) $request->remember);
        // $this->validate($request, [
        //     'username' => 'username|exists:users,username',
        //     'password' => 'required',
        // ]);

        // $this->validateLogin($request);
        // dd('m');
        $users = new User;
        $get_user = $users->where('username', $request->username)->first();

        if($get_user){

            if ($get_user->status == 2) {
                // Session::flash('status', 'danger');
                // Session::flash('message', 'Your account is not active, please contact admin.');
                return redirect()->back()->with('status', 'danger')->with('message', 'Your account is not active, please contact admin.');
            }

            $attempts = [
                'username' => $request->username,
                'password' => $request->password,
            ];

            if (Auth::attempt($attempts, (bool) $request->remember)) {
                if(Auth::user()->roles()->pluck('name')->first() == "Sales" || Auth::user()->roles()->pluck('name')->first() == "Sales Admin" || Auth::user()->roles()->pluck('name')->first() == "Verificate"){
                    return redirect('/web');
                } else {
                    return redirect('/profile');
                }
                // return redirect()->intended('/web');
            } else {
                return redirect()->back()->with('status', 'danger')->with('message', 'Your password is not correct!');
            }
        }

        return redirect()->back()->with('status', 'danger')->with('message', 'Your username is not correct!');
    }
}