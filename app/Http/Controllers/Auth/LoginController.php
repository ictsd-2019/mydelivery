<?php

namespace App\Http\Controllers\Auth;

use Session;
use App\User;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/profile';

    public function login(\Illuminate\Http\Request $request)
    {
        $this->validateLogin($request);

        $users = new User;
        $last_login = Carbon::now();

        $username = $request->username;
        $get_user = $users->where($this->username(), $username)->first();

        if ($this->guard()->validate($this->credentials($request)))
        {
            //$user = $this->guard()->getLastAttempted();

            if ($get_user->status == 2) {

                Session::flash('status', 'danger');
                Session::flash('message', 'Your account is not active, please contact admin.');
                return back();
            }

            if($this->attemptLogin($request)) {

                $update_last_login = $users->where('id', $get_user->id)
                                            ->update(['last_login' => $last_login]);
                // Send the normal successful login response
                return $this->sendLoginResponse($request);
            }

        }
 
        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'username';
    }

    /*protected function guard()
    {
        return Auth::guard('guard-name');
    }*/
}
