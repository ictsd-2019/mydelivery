<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\Http\Requests\TaskPost;
use Excel;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas['task'] = Task::orderBy('name', 'asc')->get();
        return view('task.index', $datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('task.created_task');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaskPost $request)
    {
        $noUrutAkhir = \App\Task::max('id_task');

        $kode = str_replace("ID", "", $noUrutAkhir);
       
    	$no = (int) $kode + 1;
 
    	if (strlen($kode) == 10) {
    		$addNol = "00";
    	} elseif (strlen($kode) == 20) {
    		$addNol = "0";
    	} elseif (strlen($kode == 30)) {
    		$addNol = "";
    	}

    	//$no = $incrementKode.$addNol;
        //dd($no);
        Task::create([
            'id_task'    => $no,
            'name'      => $request['name'],
            'start'   => $request['start'],
            'end' => $request['end']
        ]);

        return redirect('/task')
            ->with('status','success')
            ->with('message', 'Task Scope  has been created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $datas['coba'] = Task::find($id);
        //dd($datas['kode']);
        return view('task.edit', $datas);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TaskPost $request, $id)
    {
        $datas                = Task::where('id_task', $id)->first();
        $data_type['id_task'] = $request->id;
        $data_type['name']    = $request->name;
        $data_type['start']   = $request->start;
        $data_type['end']     = $request->end;
        Task::find($id)->update($data_type);

        return redirect('/task')
            ->with('status','success')
            ->with('message', 'Task Scope has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
