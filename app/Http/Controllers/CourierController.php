<?php

namespace App\Http\Controllers;

use PDF;
use Storage;
use App\Shipment;
use App\DeliveryRequest;
use App\SalesOrder;
use App\SalesOrderDeliveryRequest;
use App\SalesOrderShipment;
use App\SalesOrderTransaction;
use Illuminate\Http\Request;

class CourierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['bast'] = [];
        $data['salesorder_delivery_request'] = SalesOrderDeliveryRequest::where('status', 'In Progress Courier')->orWhere('status', 'Signed')->orderBy('id', 'desc')->get()->toArray();
        $data['salesorder_shipment'] = SalesOrderShipment::where('status', 'In Progress Courier')->orWhere('status', 'Signed')->orderBy('id', 'desc')->get()->toArray();

        foreach($data['salesorder_delivery_request'] as $dr){
            array_push($data['bast'], $dr);
        }

        foreach($data['salesorder_shipment'] as $shipment){
            array_push($data['bast'], $shipment);
        }

        if(count($data['bast']) != 0){
            for($i=0; $i<count($data['bast']); $i++){
                $data['bast'][$i]['request_date'] = implode('-', array_reverse(explode('-', $data['bast'][$i]['request_date'])));   

                $data['bast'][$i]['quantity'] = 0;
                if(array_key_exists("form_shipment", $data['bast'][$i])){
                    $data['shipment'] = SalesOrderShipment::where('id_salesorder', $data['bast'][$i]['id_salesorder'])->get();

                    if($data['shipment'] != null){
                        foreach($data['shipment'] as $so_shipment){
                            $data['bast'][$i]['quantity'] = Shipment::where('id_so_shipment', $so_shipment->id)->count();
                        }
                    }
                } else {
                    $data['delivery_request'] = DeliveryRequest::where('id_so_dr', $data['bast'][$i]['id'])->get();

                    foreach($data['delivery_request'] as $dr){
                        $data['bast'][$i]['quantity'] += $dr['quantity'];
                    }
                    $data['bast'][$i]['quantity'] = number_format($data['bast'][$i]['quantity']);
                }

            }
        }

        return view('bast.bast_index', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SalesOrderDeliveryRequest  $id_code
     * @param  \App\SalesOrderShipment  $id_code
     * @return \Illuminate\Http\Response
     */
    public function edit($id_code)
    {
        $data['id_code'] = $id_code;

        return view('bast.bast_edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SalesOrderDeliveryRequest  $id_code
     * @param  \App\SalesOrderShipment  $id_code
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_code)
    {
        $code = explode("-", $id_code);

        if($request->hasFile('photo_customer')){
            $file_name = $request->file('photo_customer')->getClientOriginalName();
            if($code[1] == 'dr'){
                $path_album ='/public/upload/photo_customer/dr/'.$code[0];
                $path = $request->file('photo_customer')->storeAs($path_album, $file_name);
                $path_photo_customer = Storage::url($path);
                $data['photo_customer'] = $path_photo_customer;
            } else {
                $path_album ='/public/upload/photo_customer/shipment/'.$code[0];
                $path = $request->file('photo_customer')->storeAs($path_album, $file_name);
                $path_photo_customer = Storage::url($path);
                $data['photo_customer'] = $path_photo_customer;
            }
        }

        if($request->signature){
            $signature_data = substr($request->signature, strpos($request->signature, ",")+1);
            if($code[1] == 'dr'){
                $uploaded_signature = Storage::disk('local')->put('public/signed_bast/dr/'. $code[0] ."/customer_signature.png", base64_decode($signature_data));
                $signature_url = Storage::url('public/signed_bast/dr/'. $code[0] .'/customer_signature.png');
                $data['sign_customer'] = $signature_url;
            } else {
                $uploaded_signature = Storage::disk('local')->put('public/signed_bast/shipment/'. $code[0] ."/customer_signature.png", base64_decode($signature_data));
                $signature_url = Storage::url('public/signed_bast/shipment/'. $code[0] .'/customer_signature.png');
                $data['sign_customer'] = $signature_url;
            }
        }

        $data['name_customer'] = $request->name_customer;
        $data['status'] = 'Signed';
        $data['bast_date'] = date('Y-m-d');

        if($code[1] == 'dr'){
            SalesOrderDeliveryRequest::where('id', (int)$code[0])->update($data);
            $dr_msisdn = DeliveryRequest::where('id_so_dr', (int)$code[0])->pluck('msisdn')->toArray();
            SalesOrderTransaction::whereIn('msisdn', $dr_msisdn)->update([
                "status" => "Delivered"
            ]);
        } else {
            SalesOrderShipment::where('id', (int)$code[0])->update($data);
            $shipment_msisdn = Shipment::where('id_so_shipment', (int)$code[0])->pluck('msisdn')->toArray();
            SalesOrderTransaction::whereIn('msisdn', $shipment_msisdn)->update([
                "status" => "Delivered"
            ]);
        }

        return redirect('/courier/sign-bast')
            ->with('status','success')
            ->with('message', 'BAST Customer '. $request->name_customer .' has been signed!');
    }

    public function export($id_code)
    {
        $code = explode("-", $id_code);
        
        if($code[1] == 'dr'){
            $data['bast'] = SalesOrderDeliveryRequest::where('id', (int)$code[0])->first();
            $data['transaction'] = DeliveryRequest::where('id_so_dr', $data['bast']->id)->get();

            if($data['bast']->rekap != null){
                $quote = SalesOrder::where('id', $data['bast']->id_salesorder)->first();
                $msisdn = DeliveryRequest::where('id_so_dr', $data['bast']->id)->pluck('msisdn')->toArray();
    
                $last_dr = count($data['transaction'])-1;
    
                if($quote->type_req == 'SIM Only'){
                    $data['jenis'] = 'SIMCARD';
                    // foreach($data['transaction'] as $dr){
                    //     $dr['device_type'] = strtoupper($dr['type_bundling']);
                    //     $dr['brand'] = 'SIMCARD';
                    // }
                } else {
                    $data['jenis'] = 'DEVICE';
                }

                $total_price = 0;
                foreach($data['transaction'] as $dr){
                    $price = str_replace( ',', '', $dr["device_price"] );
                    $total_price += (int)$price;

                    if($quote->type_req == 'SIM Only'){
                        $dr['device_type'] = strtoupper($dr['type_bundling']);
                        $dr['brand'] = 'SIMCARD';
                    }
                }
    
                $data['msisdn_min'] = min($msisdn);
                $data['msisdn_max'] = max($msisdn);
    
                $iccid_min = SalesOrderTransaction::where('msisdn', min($msisdn))->first();
                $iccid_max = SalesOrderTransaction::where('msisdn', max($msisdn))->first();
    
                $data['iccid_min'] = $iccid_min->iccid;
                $data['iccid_max'] = $iccid_max->iccid;
                $data['brand'] = $data['transaction'][$last_dr]["brand"];
                $data['device_type'] = $data['transaction'][$last_dr]["device_type"];
                $data['device_price'] = number_format($total_price);
                $data['imei_min'] = DeliveryRequest::where('msisdn', min($msisdn))->first();
                $data['imei_max'] = DeliveryRequest::where('msisdn', max($msisdn))->first();
                $data['quantity'] = number_format(count($data['transaction']));
                $data['uom'] = $data['transaction'][$last_dr]["uom"];
                $data['destination'] = $data['bast']->address_1;
                $data['packing'] = $data['transaction'][$last_dr]["packing"];
                $data['note'] = $data['transaction'][$last_dr]["note"];
    
                $data['bast']->quote_no = $quote->quote_no;
        
                $code = explode('/', $data['bast']->no_dr);
                $data['bast']->nomor_dr = $code[0];
                $courier = explode('-', $code[1]);
                $data['bast']->courier = $courier[1];
        
                $timestamp = strtotime($data['bast']->request_date);
                $data['bast']->order_date = date("dmy", $timestamp);
        
                $data['tanggal'] = $this->get_tanggal_indo($data['bast']->request_date);
                $data['bulan'] = $this->get_tanggal_indo(date('Y-m-d'));

                $pdf = PDF::loadview('bast.pdf.bast_signed_rekap', $data);
                return $pdf->download('DR '.$data['bast']->courier .' '.$data['bast']->nomor_dr .'-'.$data['bast']->corporate_name .'-'.$data['bast']->order_date .'.pdf');
    
            } else {
                $data['salesorder'] = SalesOrder::where('id', $data['bast']->id_salesorder)->first();
                $data['bast']->quote_no = $data['salesorder']->quote_no;
    
                if($data['salesorder']->type_req == 'SIM Only'){
                    $data['jenis'] = 'SIMCARD';
                    foreach($data['transaction'] as $dr){
                        $dr['device_type'] = strtoupper($dr['type_bundling']);
                        $dr['brand'] = 'SIMCARD';
                    }
                } else {
                    $data['jenis'] = 'DEVICE';
                }
        
                $code = explode('/', $data['bast']->no_dr);
                $data['bast']->nomor_dr = $code[0];
                $courier = explode('-', $code[1]);
                $data['bast']->courier = $courier[1];
        
                $timestamp = strtotime($data['bast']->request_date);
                $data['bast']->order_date = date("dmy", $timestamp);
        
                $data['tanggal'] = $this->get_tanggal_indo($data['bast']->request_date);
                $data['bulan'] = $this->get_tanggal_indo(date('Y-m-d'));
                // dd($data);
                $pdf = PDF::loadview('bast.pdf.bast_signed_manual', $data);
                return $pdf->download('BAST '.$data['bast']->corporate_name .' - '.$data['bast']->sales .'.pdf');
            }
        } else {
            $data['bast'] = SalesOrderShipment::where('id', (int)$code[0])->first();

            if($data['bast']->rekap != null){
                // $data['so_dr'] = SalesOrderDeliveryRequest::where('id', $so_dr_id)->first();
                // $data['dr'] = DeliveryRequest::where('id_so_dr', $data['so_dr']->id)->get();
                // $quote = SalesOrder::where('id', $data['so_dr']->id_salesorder)->first();
                // $msisdn = DeliveryRequest::where('id_so_dr', $data['so_dr']->id)->pluck('msisdn')->toArray();
    
                // $last_dr = count($data['dr'])-1;
    
                // $total_price = 0;
                // foreach($data['dr'] as $dr){
                //     $price = str_replace( ',', '', $dr["device_price"] );
                //     $total_price += (int)$price;
                // }
    
                // $data['msisdn_min'] = min($msisdn);
                // $data['msisdn_max'] = max($msisdn);
    
                // $iccid_min = SalesOrderTransaction::where('msisdn', min($msisdn))->first();
                // $iccid_max = SalesOrderTransaction::where('msisdn', max($msisdn))->first();
    
                // $data['iccid_min'] = $iccid_min->iccid;
                // $data['iccid_max'] = $iccid_max->iccid;
                // $data['brand'] = $data['dr'][$last_dr]["brand"];
                // $data['device_type'] = $data['dr'][$last_dr]["device_type"];
                // $data['device_price'] = number_format($total_price);
                // $data['imei'] = $data['dr'][$last_dr]["imei"];
                // $data['quantity'] = number_format(count($data['dr']));
                // $data['uom'] = $data['dr'][$last_dr]["uom"];
                // $data['destination'] = $data['so_dr']->address_1;
                // $data['packing'] = $data['dr'][$last_dr]["packing"];
                // $data['note'] = $data['dr'][$last_dr]["note"];
    
                // $data['so_dr']->quote_no = $quote->quote_no;
        
                // $code = explode('/', $data['so_dr']->no_dr);
                // $data['so_dr']->nomor_dr = $code[0];
                // $courier = explode('-', $code[1]);
                // $data['so_dr']->courier = $courier[1];
        
                // $timestamp = strtotime($data['so_dr']->request_date);
                // $data['so_dr']->order_date = date("dmy", $timestamp);
        
                // $data['tanggal'] = $this->get_tanggal_indo($data['so_dr']->request_date);
                // $data['bulan'] = $this->get_bulan_indo($data['so_dr']->request_date);
                
                // $data['requestor'] = Employee::where('id', $request->requester)->first();
                // $data['manager'] = Employee::where('id', $request->manager)->first();
        
                // $pdf = PDF::loadview('bast.pdf.bast_signed_rekap', $data);
                // return $pdf->download('DR '.$data['so_dr']->courier .' '.$data['so_dr']->nomor_dr .'-'.$data['so_dr']->corporate_name .'-'.$data['so_dr']->order_date .'.pdf');
            } else {
                // $data['so_shipment'] = SalesOrderShipment::where('id', $so_shipment_id)->first();
                // $data['so_shipment'] = SalesOrderShipment::where('id', $so_shipment_id)->first();
                // $data['list_msisdn'] = Shipment::where('id_so_shipment', $data['so_shipment']->id)->pluck('msisdn')->toArray();
                $data['list_shipment'] = Shipment::where('id_so_shipment', $data['bast']->id)->get();
                
                $data['transaction'] = [];
                for($i=0; $i<count($data['list_shipment']); $i++){
                    $edit = SalesOrderTransaction::where('id_salesorder', $data['bast']->id_salesorder)->where('msisdn', $data['list_shipment'][$i]->msisdn)->first();
                    $edit['imei'] = $data['list_shipment'][$i]->imei;
                    array_push($data['transaction'], $edit);
                    
                }
    
                // $data['transaction'] = SalesOrderTransaction::where('id_salesorder', $data['so_shipment']->id_salesorder)->whereIn('msisdn', $data['list_msisdn'])->get();
                $quote = SalesOrder::where('id', $data['bast']->id_salesorder)->first();
    

                
                // $data['list_msisdn'] = Shipment::where('id_so_shipment', $data['bast']->id)->pluck('msisdn')->toArray();
                
                // $data['transaction'] = SalesOrderTransaction::where('id_salesorder', $data['bast']->id_salesorder)->whereIn('msisdn', $data['list_msisdn'])->get();
                // $quote = SalesOrder::where('id', $data['bast']->id_salesorder)->first();
    
                $data['bast']->quote_no = $quote->quote_no;
    
                if($quote->type_req == 'SIM Only'){
                    $data['jenis'] = 'SIMCARD';
                    foreach($data['transaction'] as $trans){
                        $trans['device_type'] = strtoupper($trans['type_bundling']);
                        $trans['brand'] = 'SIMCARD';
                    }
                } else {
                    $data['jenis'] = 'DEVICE';
                }
        
                $timestamp = strtotime($data['bast']->request_date);
                $data['bast']->order_date = date("dmy", $timestamp);
        
                $data['tanggal'] = $this->get_tanggal_indo($data['bast']->request_date);
                $data['bulan'] = $this->get_tanggal_indo(date('Y-m-d'));

                $pdf = PDF::loadview('bast.pdf.bast_signed_manual', $data);
                return $pdf->download('BAST '.$data['bast']->corporate_name .' - '.$data['bast']->sales .'.pdf');
            }
        }
    }

    public function get_tanggal_indo($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);
    
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
}
