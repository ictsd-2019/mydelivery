<?php

namespace App\Http\Controllers;

use Session;
use App\Region;
use Illuminate\Http\Request;
use App\Http\Requests\RegionStore;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // ActivityLog::info('Region','View Region');
        $data['regions'] = Region::orderBy('name', 'asc')->get();

        return view('region.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // ActivityLog::info('Region','Create Region');
        $data['title'] = 'Add Region';
        
        return view('region.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegionStore $req)
    {
        $check = Region::checkData($req->name)->count();
        
        if($check >=1){
            // ActivityLog::info('Region','Submit Faild, Region name already', ['name' => $req->name]);
            return redirect(route('region.create'))->with('unsuccess', 'Region '.$req->name.' already exist!');
        }
        else{
            // ActivityLog::info('Region','Submit Success, Create new Region success', ['name' => $req->name]);
            $region = Region::Create($req->all());
            return redirect(route('region.index'))->with('message', 'Region '.$req->name.' has been created successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Region  $region
     * @return \Illuminate\Http\Response
     */
    public function show(Region $region)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Region  $region
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $req, $id)
    {
        $data['region'] = Region::FindOrFail($id);
        $data['title'] = 'Edit Region';

        // ActivityLog::info('Region','Edit region', ['name' => $region->name]);
        return view('region.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Region  $region
     * @return \Illuminate\Http\Response
     */
    public function update(RegionStore $request, $id)
    {
        $check = Region::checkData($request->name)->where('id', '!=', $id)->count();

        if($check >=1){
            // ActivityLog::info('Region','Submit Faild, Region name already', ['name' => $request->name]);
            return  redirect('region/'.$id.'/edit')
                ->with('status','danger')
                ->with('message', 'Region \''. $request->name .'\' already exist');
        } else {
            // ActivityLog::info('Region','Submit edit region success', ['name' => $req->name]);
            $region = Region::FindOrFail($id)->update($request->all());
            return  redirect(route('region.index'))
                ->with('status','success')
                ->with('message', 'Region \''. $request->name .'\' and its permission has been setup successfuly.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Region  $region
     * @return \Illuminate\Http\Response
     */
    public function destroy(Region $region)
    {
        //
    }
}
