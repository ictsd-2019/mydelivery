<?php

namespace App\Http\Controllers;

use Excel;
use DataTables;
use App\Exports\DeviceExport;
use App\Imports\DeviceImport;
use App\DeviceType;
use Illuminate\Http\Request;
use App\Http\Requests\DevicePost;
use App\Http\Requests\UpdateDevice;

class DeviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data['device'] = DeviceType::orderBy('product')->get();
        // foreach($data['device'] as $dv){
        //     $dv['cash_1'] = number_format($dv['cash_1']);
        //     $dv['cash_2'] = number_format($dv['cash_2']);
        //     $dv['cash_3'] = number_format($dv['cash_3']);
        //     $dv['months_3_1'] = number_format($dv['months_3_1']);
        //     $dv['months_3_2'] = number_format($dv['months_3_2']);
        //     $dv['months_3_3'] = number_format($dv['months_3_3']);
        //     $dv['months_6_1'] = number_format($dv['months_6_1']);
        //     $dv['months_6_2'] = number_format($dv['months_6_2']);
        //     $dv['months_6_3'] = number_format($dv['months_6_3']);
        //     $dv['months_12_1'] = number_format($dv['months_12_1']);
        //     $dv['months_12_2'] = number_format($dv['months_12_2']);
        //     $dv['months_12_3'] = number_format($dv['months_12_3']);
        //     $dv['months_24_1'] = number_format($dv['months_24_1']);
        //     $dv['months_24_2'] = number_format($dv['months_24_2']);
        //     $dv['months_24_3'] = number_format($dv['months_24_3']);
        // }
        // return view('device.manage_device_index', $data);
        return view('device.manage_device_index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('device.manage_device_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DevicePost $request)
    {
        DeviceType::create([
            'product' => $request['product'],
            'cash_1' => empty($request['cash_1']) ? 0 : str_replace(".", "", $request['cash_1']),
            'cash_2' => empty($request['cash_2']) ? 0 : str_replace(".", "", $request['cash_2']),
            'cash_3' => empty($request['cash_3']) ? 0 : str_replace(".", "", $request['cash_3']),
            'months_3_1' => empty($request['months_3_1']) ? 0 : str_replace(".", "", $request['months_3_1']),
            'months_3_2' => empty($request['months_3_2']) ? 0 : str_replace(".", "", $request['months_3_2']),
            'months_3_3' => empty($request['months_3_3']) ? 0 : str_replace(".", "", $request['months_3_3']),
            'months_6_1' => empty($request['months_6_1']) ? 0 : str_replace(".", "", $request['months_6_1']),
            'months_6_2' => empty($request['months_6_2']) ? 0 : str_replace(".", "", $request['months_6_2']),
            'months_6_3' => empty($request['months_6_3']) ? 0 : str_replace(".", "", $request['months_6_3']),
            'months_12_1' => empty($request['months_12_1']) ? 0 : str_replace(".", "", $request['months_12_1']),
            'months_12_2' => empty($request['months_12_2']) ? 0 : str_replace(".", "", $request['months_12_2']),
            'months_12_3' => empty($request['months_12_3']) ? 0 : str_replace(".", "", $request['months_12_3']),
            'months_24_1' => empty($request['months_24_1']) ? 0 : str_replace(".", "", $request['months_24_1']),
            'months_24_2' => empty($request['months_24_2']) ? 0 : str_replace(".", "", $request['months_24_2']),
            'months_24_3' => empty($request['months_24_3']) ? 0 : str_replace(".", "", $request['months_24_3'])
        ]);

        return redirect('/device')
            ->with('status','success')
            ->with('message', 'Device '. $request['product'] .' has been created!');    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DeviceType  $device_type_id
     * @return \Illuminate\Http\Response
     */
    public function edit($device_type_id)
    {
        //dd($device_type_id);
        $data['device'] = DeviceType::find($device_type_id);
        //dd($data['device']);
        return view('device.manage_device_edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DeviceType  $device_type_id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDevice $request, $device_type_id)
    {
        $device = DeviceType::where('id', $device_type_id)->first();
        $data_device_type['cash_1'] = $request->cash_1;
        $data_device_type['cash_2'] = $request->cash_2;
        $data_device_type['cash_3'] = $request->cash_3;
        $data_device_type['months_3_1'] = $request->months_3_1;
        $data_device_type['months_3_2'] = $request->months_3_2;
        $data_device_type['months_3_3'] = $request->months_3_3;
        $data_device_type['months_6_1'] = $request->months_6_1;
        $data_device_type['months_6_2'] = $request->months_6_2;
        $data_device_type['months_6_3'] = $request->months_6_3;
        $data_device_type['months_12_1'] = $request->months_12_1;
        $data_device_type['months_12_2'] = $request->months_12_2;
        $data_device_type['months_12_3'] = $request->months_12_3;
        $data_device_type['months_24_1'] = $request->months_24_1;
        $data_device_type['months_24_2'] = $request->months_24_2;
        $data_device_type['months_24_3'] = $request->months_24_3;
        DeviceType::find($device_type_id)->update($data_device_type);

        return redirect('/device')
            ->with('status','success')
            ->with('message', 'Device '. $device->product .' has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DeviceType  $device_type_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($device_type_id)
    {
        $device_type = DeviceType::findOrFail($device_type_id);
        $nama_device = $device_type['product'];
        $device_type->delete();

        return redirect('/device')
            ->with('status','success')
            ->with('message', 'Device '. $nama_device .' has been deleted!');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function export()
    {
        $current_date = Date("d-m-Y");
        return Excel::download(new DeviceExport, 'Daftar Device ('.$current_date .').xlsx');
    }

    public function json(){
        return Datatables::of(DeviceType::orderBy('product', 'asc')->get())->make(true);
    }


}
