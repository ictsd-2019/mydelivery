<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DetailActivity;
use App\MainActivity;
use App\OutTask;
use App\Http\Requests\DetailPost;
use Excel;

class DetailActivityController extends Controller
{
    public function index()
    {
        //$datas['detail'] = DetailActivity::join('outtask', 'outtask.id', '=', 'detail_activity.id_vendor')
                           //->join('main_activity', 'main_activity.id_vendor', '=', 'detail_activity.id_vendor')
                           //->orderBy('detail', 'asc')->get(['detail_activity.*', 'outtask.name','main_activity.activity']);   
       $datas['detail'] = DetailActivity::join('outtask', 'outtask.id', '=', 'detail_activity.id_vendor')
                           ->join('main_activity', 'main_activity.id', '=', 'detail_activity.id_main')
                           ->orderBy('detail', 'asc')->get(['detail_activity.*', 'outtask.name','main_activity.activity','main_activity.notes']);
        return view('detail_activity.index', $datas);
    }

    public function create()
    {
        $datas['selected_vendor'] = OutTask::orderBy('name', 'asc')->pluck('name', 'id');
        $datas['selected_main']   = MainActivity::orderBy('activity', 'asc')->pluck('activity', 'id');
        return view('detail_activity.created', $datas);
    }

    public function store(DetailPost $request)
    {

        $detail    = $request->get('detail');
        $price     = $request->get('price');
      
        for($i=0;$i < count($detail); $i++){

            DetailActivity::create([
                'id_main'   => $request['main'],
                'id_vendor' => $request['ven'],
                'detail'    => $detail[$i],
                'price'     => $price[$i],
                'start'     => date('Y-m-d', strtotime($request['start'])),
                'end'       => date('Y-m-d', strtotime($request['end']))
            ]);

        }

        return redirect('/detail')
            ->with('status','success')
            ->with('message', 'Data Detail Activity  has been created!');
    }

    public function edit($id)
    {
        $kode                     = base64_decode($id);
        $datas['detail']          = DetailActivity::find($kode);
        $datas['selected_main']   = MainActivity::orderBy('activity', 'asc')->pluck('activity', 'id');
        $datas['selected_vendor'] = OutTask::orderBy('name', 'asc')->pluck('name', 'id');
        return view('detail_activity.edit', $datas);
    }

    public function update(DetailPost $request, $id)
    {
        
        $datas                  = DetailActivity::where('id', $id)->first();
        $data_type['id_vendor'] = $request->ven;
        $data_type['id_main']   = $request->main;
        $data_type['detail']    = $request->detail;
        $data_type['price']     = $request->price;
        $data_type['start']     = date('Y-m-d', strtotime($request->start));
        $data_type['end']       = date('Y-m-d', strtotime($request->end));
        DetailActivity::find($id)->update($data_type);

        return redirect('/detail')
            ->with('status','success')
            ->with('message', 'Data Detail Activity has been updated!');
    }

    public function destroy($id)
    {
       
        $kode     = base64_decode($id);
        $datas    = DetailActivity::find($kode);
        $detail   = $datas->detail;
        $datas->delete();

        return redirect('/detail')
            ->with('status','success')
            ->with('message', 'Data Detail Actibity : '. $detail .' has been deleted!');
    }
}
