<?php

namespace App\Http\Controllers;

use Excel;
use App\Exports\SalesOrderTransactionExport;
use App\Imports\SalesOrderTransactionImport;
use App\PackageAddOn;
use App\PackageInfo;
use App\DeviceType;
use App\DevicePrice;
use App\SalesOrderAttachment;
use App\SalesAgent;
use App\Salesorder;
use App\SalesOrderAddOn;
use App\SalesOrderTransaction;
use App\SalesOrderShipment;
use App\Shipment;
use App\M2M;
use App\NonM2M;
use App\SimPackage;
use App\SimSpecification;
use Illuminate\Http\Request;
use App\Http\Requests\SalesOrderTransactionPost;

class SalesorderTransactionController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SalesOrderTransactionPost $request, $salesorder_id)
    {
        if($request->type_payment == "Installment"){
            if($request->tenor == null){
                return redirect('/web/salesorder/'.$salesorder_id)
                    ->with('status','danger')
                    ->with('message','Tenor Installment belum dipilih!');
            }
        }

        $salesorder = Salesorder::where('id', $salesorder_id)->first();
        $data_transaction['id_salesorder'] = $salesorder_id;

        if($request->msisdn != null){
            if(substr( $request->msisdn, 0, 2 ) === "08")
            {
                $data_transaction['msisdn'] = preg_replace('|[^0-9]|i', '', preg_replace('/^0?/', '62', $request->msisdn));
            }
            else
            {
                $data_transaction['msisdn'] = preg_replace('|[^0-9]|i', '', $request->msisdn);
            }
        }

        $data_transaction['iccid'] = $request->iccid;
        $data_transaction['type_bundling'] = $request->type_bundling;

        if($request->type_bundling == "Postpaid"){
            if($request->m2m == null){
                return redirect('/web/salesorder/'.$salesorder_id)
                    ->with('status','danger')
                    ->with('message','M2M belum dipilih!');
            }
            $data_m2m = M2M::where('id', $request->m2m)->first();
            $data_transaction['m2m'] = $data_m2m->name;
        } else if($request->type_bundling == "Prepaid"){
            if($request->package_type == null){
                return redirect('/web/salesorder/'.$salesorder_id)
                    ->with('status','danger')
                    ->with('message','Package Type belum dipilih!');
            }
            $data_package_type = SimPackage::where('id', $request->package_type)->first();
            $data_transaction['package_type'] = $data_package_type->name;
            $data_transaction['type_prepaid'] = $request->type_prepaid;
            $data_transaction['hlr'] = $request->hlr;
        }

        $data_non_m2m = NonM2M::where('id', $request->non_m2m)->first();
        $data_transaction['non_m2m'] = $data_non_m2m->name;

        $data_sim_spec = SimSpecification::where('id', $request->sim_specification)->first();
        $data_transaction['sim_specification'] = $data_sim_spec->name;

        $data_package = PackageInfo::where('id', $request->package_basic)->first();
        $data_transaction['package_basic'] = $data_package->package;

        $total_price = 0;
        if($salesorder->type_req == "Bundling"){
            $data_brand = DeviceType::where('id', $request->brand)->first();
            $data_transaction['brand'] = $data_brand->product;
            $data_transaction['type_payment'] = $request->type_payment;

            if($request->device_price != null){
                if($request->type_payment == "Cash"){
                    $data_transaction['tenor'] = 1;
                } else {
                    $data_transaction['tenor'] = $request->tenor;
                }
                $data_transaction['device_price'] = number_format($request->device_price);
                $total_price += ((int)$data_transaction['tenor']*(int)$request->device_price);
            } else {
                if($request->type_payment == "Cash"){
                    $data_transaction['tenor'] = 1;
                    if($salesorder->quantity <= 100){
                        $data_transaction['device_price'] = number_format($data_brand->cash_1);
                        $total_price += (int)$data_brand->cash_1;
                    } else if($salesorder->quantity <= 500){
                        $data_transaction['device_price'] = number_format($data_brand->cash_2);
                        $total_price += (int)$data_brand->cash_2;
                    } else {
                        $data_transaction['device_price'] = number_format($data_brand->cash_3);
                        $total_price += (int)$data_brand->cash_3;
                    }
                } else {
                    $data_transaction['tenor'] = $request->tenor;
                    if($request->tenor == 3){
                        if($salesorder->quantity <= 100){
                            $data_transaction['device_price'] = number_format($data_brand->months_3_1);
                            $total_price += (3*(int)$data_brand->months_3_1);
                        } else if($salesorder->quantity <= 500){
                            $data_transaction['device_price'] = number_format($data_brand->months_3_2);
                            $total_price += (3*(int)$data_brand->months_3_2);
                        } else {
                            $data_transaction['device_price'] = number_format($data_brand->months_3_3);
                            $total_price += (3*(int)$data_brand->months_3_3);
                        }
                    } else if($request->tenor == 6){
                        if($salesorder->quantity <= 100){
                            $data_transaction['device_price'] = number_format($data_brand->months_6_1);
                            $total_price += (6*(int)$data_brand->months_6_1);
                        } else if($salesorder->quantity <= 500){
                            $data_transaction['device_price'] = number_format($data_brand->months_6_2);
                            $total_price += (6*(int)$data_brand->months_6_2);
                        } else {
                            $data_transaction['device_price'] = number_format($data_brand->months_6_3);
                            $total_price += (6*(int)$data_brand->months_6_3);
                        }
                    } else if($request->tenor == 12){
                        if($salesorder->quantity <= 100){
                            $data_transaction['device_price'] = number_format($data_brand->months_12_1);
                            $total_price += (12*(int)$data_brand->months_12_1);
                        } else if($salesorder->quantity <= 500){
                            $data_transaction['device_price'] = number_format($data_brand->months_12_2);
                            $total_price += (12*(int)$data_brand->months_12_2);
                        } else {
                            $data_transaction['device_price'] = number_format($data_brand->months_12_3);
                            $total_price += (12*(int)$data_brand->months_12_3);
                        }
                    } else if($request->tenor == 24){
                        if($salesorder->quantity <= 100){
                            $data_transaction['device_price'] = number_format($data_brand->months_24_1);
                            $total_price += (24*(int)$data_brand->months_24_1);
                        } else if($salesorder->quantity <= 500){
                            $data_transaction['device_price'] = number_format($data_brand->months_24_2);
                            $total_price += (24*(int)$data_brand->months_24_2);
                        } else {
                            $data_transaction['device_price'] = number_format($data_brand->months_24_3);
                            $total_price += (24*(int)$data_brand->months_24_3);
                        }
                    }
                }   
            }
        } else {
            $total_price += $data_package->price;
            $data_transaction['device_price'] = number_format(0);
        }

        $data_transaction['device_mrc'] = number_format($data_package->price);
        $data_transaction['price_total'] = number_format($total_price);

        $transaction = SalesOrderTransaction::create($data_transaction);

        if($request->package_add[0] != null){
            foreach($request->package_add as $package)
            {
                SalesOrderAddOn::create([
                    'id_transaction' => $transaction->id,
                    'id_addon' => $package
                ]);
            }
        }

        return redirect('/web/salesorder/'.$salesorder_id)
            ->with('status','success')
            ->with('message','Transaction created successfuly!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SalesOrderTransaction  $transaction_id
     * @return \Illuminate\Http\Response
     */
    public function edit($transaction_id)
    {
        $data_transaction = SalesorderTransaction::where('id', $transaction_id)->first();
        $data['salesorder'] = SalesOrder::where('id', $data_transaction->id_salesorder)->first();

        if(is_null($data_transaction))
        {
            return redirect('/web/salesorder')
                ->with('status','danger')
                ->with('message','Transaction ID not found.');
        }

        $add_on = SalesOrderAddOn::where('id_transaction', $transaction_id)->get();

        $data['selected_add_on'] = array();
        for($i=0; $i<count($add_on); $i++){
            $data['selected_add_on'][$i] = PackageAddOn::where('id', $add_on[$i]["id_addon"])->first();
        }

        $data['total_selected_add_on'] = count($data['selected_add_on']);
        $data['add_on'] = PackageAddOn::pluck('addon','id');
        $data['basic'] = PackageInfo::pluck('package','id');
        $data['device'] = DeviceType::pluck('product','id');
        $data['list_m2m'] = M2M::orderBy('name', 'asc')->pluck('name','id');
        $data['list_non_m2m'] = NonM2M::orderBy('name', 'asc')->pluck('name','id');
        $data['list_sim_pack'] = SimPackage::orderBy('name', 'asc')->pluck('name','id');
        $data['list_sim_spec'] = SimSpecification::orderBy('name', 'asc')->pluck('name','id');

        if($data_transaction->type_bundling == 'Postpaid'){
            $data['selected_m2m'] = M2M::where('name', $data_transaction->m2m)->first();
        } else {

            $data['selected_package_type'] = SimPackage::where('name', $data_transaction->package_type)->first();
        }

        $data['selected_non_m2m'] = NonM2M::where('name', $data_transaction->non_m2m)->first();
        $data['selected_sim_specification'] = SimSpecification::where('name', $data_transaction->sim_specification)->first();
        $data['selected_device_type'] = DeviceType::where('product', $data_transaction->brand)->first();
        $data['selected_basic_package'] = PackageInfo::where('package', $data_transaction->package_basic)->first();

        return view('salesorder.salesorder_transaction_edit', compact('data_transaction'), $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SalesOrderTransaction  $transaction_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $transaction_id)
    {
        if($request->type_payment == "Installment"){
            if($request->tenor == null){
                return redirect('/web/salesorder/transaction/'.$transaction_id .'/edit')
                    ->with('status','danger')
                    ->with('message','Tenor Installment belum dipilih!');
            }
        }

        $last_transaction = SalesorderTransaction::where('id', $transaction_id)->first();
        $salesorder = Salesorder::where('id', $last_transaction->id_salesorder)->first();

        if($request->msisdn != null){
            if(substr( $request->msisdn, 0, 2 ) === "08")
            {
                $data_transaction['msisdn'] = preg_replace('|[^0-9]|i', '', preg_replace('/^0?/', '62', $request->msisdn));
            }
            else
            {
                $data_transaction['msisdn'] = preg_replace('|[^0-9]|i', '', $request->msisdn);
            }
        }

        $data_transaction['iccid'] = $request->iccid;
        $data_transaction['type_bundling'] = $request->type_bundling;

        if($request->type_bundling == "Postpaid"){
            if($request->m2m == null){
                return redirect('/web/salesorder/'.$salesorder_id)
                    ->with('status','danger')
                    ->with('message','M2M belum dipilih!');
            }
            $data_m2m = M2M::where('id', $request->m2m)->first();
            $data_transaction['m2m'] = $data_m2m->name;
            $data_transaction['package_type'] = null;
            $data_transaction['type_prepaid'] = null;
            $data_transaction['hlr'] = null;
        } else if($request->type_bundling == "Prepaid"){
            if($request->package_type == null){
                return redirect('/web/salesorder/'.$salesorder_id)
                    ->with('status','danger')
                    ->with('message','Package Type belum dipilih!');
            }
            $data_package_type = SimPackage::where('id', $request->package_type)->first();
            $data_transaction['package_type'] = $data_package_type->name;
            $data_transaction['type_prepaid'] = $request->type_prepaid;
            $data_transaction['hlr'] = $request->hlr;
            $data_transaction['m2m'] = null;
        }

        $data_non_m2m = NonM2M::where('id', $request->non_m2m)->first();
        $data_transaction['non_m2m'] = $data_non_m2m->name;

        $data_sim_spec = SimSpecification::where('id', $request->sim_specification)->first();
        $data_transaction['sim_specification'] = $data_sim_spec->name;

        $data_package = PackageInfo::where('id', $request->package_basic)->first();
        $data_transaction['package_basic'] = $data_package->package;

        $data_package = PackageInfo::where('id', $request->package_basic)->first();
        $data_transaction['package_basic'] = $data_package->package;

        $total_price = 0;
        if($salesorder->type_req == "Bundling"){
            $data_brand = DeviceType::where('id', $request->brand)->first();
            $data_transaction['brand'] = $data_brand->product;
            $data_transaction['type_payment'] = $request->type_payment;
            
            if(number_format($request->device_price) != $last_transaction->device_price){
                if($request->type_payment == "Cash"){
                    $data_transaction['tenor'] = 1;
                } else {
                    $data_transaction['tenor'] = $request->tenor;
                }

                $data_transaction['device_price'] = number_format($request->device_price);
                $total_price += ((int)$data_transaction['tenor']*(int)$request->device_price);
            } else {
                if($request->type_payment == "Cash"){
                    $data_transaction['tenor'] = 1;
                    if($salesorder->quantity <= 100){
                        $data_transaction['device_price'] = number_format($data_brand->cash_1);
                        $total_price += (int)$data_brand->cash_1;
                    } else if($salesorder->quantity <= 500){
                        $data_transaction['device_price'] = number_format($data_brand->cash_2);
                        $total_price += (int)$data_brand->cash_2;
                    } else {
                        $data_transaction['device_price'] = number_format($data_brand->cash_3);
                        $total_price += (int)$data_brand->cash_3;
                    }
                } else {
                    $data_transaction['tenor'] = $request->tenor;
                    if($request->tenor == 3){
                        if($salesorder->quantity <= 100){
                            $data_transaction['device_price'] = number_format($data_brand->months_3_1);
                            $total_price += (3*(int)$data_brand->months_3_1);
                        } else if($salesorder->quantity <= 500){
                            $data_transaction['device_price'] = number_format($data_brand->months_3_2);
                            $total_price += (3*(int)$data_brand->months_3_2);
                        } else {
                            $data_transaction['device_price'] = number_format($data_brand->months_3_3);
                            $total_price += (3*(int)$data_brand->months_3_3);
                        }
                    } else if($request->tenor == 6){
                        if($salesorder->quantity <= 100){
                            $data_transaction['device_price'] = number_format($data_brand->months_6_1);
                            $total_price += (6*(int)$data_brand->months_6_1);
                        } else if($salesorder->quantity <= 500){
                            $data_transaction['device_price'] = number_format($data_brand->months_6_2);
                            $total_price += (6*(int)$data_brand->months_6_2);
                        } else {
                            $data_transaction['device_price'] = number_format($data_brand->months_6_3);
                            $total_price += (6*(int)$data_brand->months_6_3);
                        }
                    } else if($request->tenor == 12){
                        if($salesorder->quantity <= 100){
                            $data_transaction['device_price'] = number_format($data_brand->months_12_1);
                            $total_price += (12*(int)$data_brand->months_12_1);
                        } else if($salesorder->quantity <= 500){
                            $data_transaction['device_price'] = number_format($data_brand->months_12_2);
                            $total_price += (12*(int)$data_brand->months_12_2);
                        } else {
                            $data_transaction['device_price'] = number_format($data_brand->months_12_3);
                            $total_price += (12*(int)$data_brand->months_12_3);
                        }
                    } else if($request->tenor == 24){
                        if($salesorder->quantity <= 100){
                            $data_transaction['device_price'] = number_format($data_brand->months_24_1);
                            $total_price += (24*(int)$data_brand->months_24_1);
                        } else if($salesorder->quantity <= 500){
                            $data_transaction['device_price'] = number_format($data_brand->months_24_2);
                            $total_price += (24*(int)$data_brand->months_24_2);
                        } else {
                            $data_transaction['device_price'] = number_format($data_brand->months_24_3);
                            $total_price += (24*(int)$data_brand->months_24_3);
                        }
                    }
                }
            }            
        } else {
            $total_price += $data_package->price;
            $data_transaction['device_price'] = number_format(0);
        }

        $data_transaction['device_mrc'] = number_format($data_package->price);
        $data_transaction['price_total'] = number_format($total_price);

        if($last_transaction->status == 'Out of Stock'){
            $data_transaction['status'] = 'In Progress RM';
        }

        SalesorderTransaction::where('id', $transaction_id)->update($data_transaction);

        SalesOrderAddOn::where('id_transaction', $transaction_id)->delete();
        
        if($request->package_add[0] != null){
            foreach($request->package_add as $package)
            {
                SalesOrderAddOn::create([
                    'id_transaction' => $transaction_id,
                    'id_addon' => $package
                ]);
            }
        }

        return redirect('/web/salesorder/'.$last_transaction->id_salesorder)
            ->with('status','success')
            ->with('message','Transaction has been updated successfuly.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SalesOrderTransaction  $transaction_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($transaction_id)
    {
        $transaction = SalesorderTransaction::findOrFail($transaction_id);
        $salesorder_id = $transaction->id_salesorder;
        $transaction->delete();

        return redirect('/web/salesorder/'.$salesorder_id)
            ->with('status','success')
            ->with('message','Transaction has been deleted successfuly.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request, $salesorder_id)
    {
        if($request->hasFile('select_file')){
            $this->validate($request, [
                'select_file' => 'required|mimes:xls,xlsx'
            ]);

            $file = $request->file('select_file');
            $nama_file = rand().$file->getClientOriginalName();
            $file->move('file_salesorder_transaction', $nama_file);
            $path = public_path().'/file_salesorder_transaction/'.$nama_file;

            // // $rows = Excel::load($file->getPathName(), function($reader) {})->get();
            // $rows = Excel::load($path, function($reader) {})->get();
            // dd($rows);
            // $totalRows = $rows->count();

            Excel::import(new SalesOrderTransactionImport($salesorder_id), $path);

            \File::delete($path);
        }

        return redirect('/web/salesorder/'.$salesorder_id)
            ->with('status','success')
            ->with('message','Excel Data Imported successfully! Please check the table below to make sure your data was uploaded perfectly!');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function export($salesorder_id)
    {
        $date = Date("Y-m-d");
        $salesorder = SalesOrder::where('id', $salesorder_id)->first();
        return Excel::download(new SalesOrderTransactionExport($salesorder_id), 'MSISDN QUOTE NO. '.$salesorder->quote_no .' ('.$date .').xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_manage($salesorder_id)
    {
        $data['salesorder'] = SalesOrder::where('id', $salesorder_id)->first();
        $data['salesorder_id'] = $salesorder_id;
        // $data['current_date'] = date("Y-m-d");
        $data['add_on'] = PackageAddOn::orderBy('addon', 'asc')->pluck('addon','id');
        $data['basic'] = PackageInfo::orderBy('package', 'asc')->pluck('package','id');
        $data['device'] = DeviceType::orderBy('product', 'asc')->pluck('product','id');
        $data['sales_agent'] = SalesAgent::pluck('nama', 'id');
        $data['list_m2m'] = M2M::orderBy('name', 'asc')->pluck('name','id');
        $data['list_non_m2m'] = NonM2M::orderBy('name', 'asc')->pluck('name','id');
        $data['list_sim_pack'] = SimPackage::orderBy('name', 'asc')->pluck('name','id');
        $data['list_sim_spec'] = SimSpecification::orderBy('name', 'asc')->pluck('name','id');

        return view('salesorder.manage_salesorder_transaction_create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_manage(SalesOrderTransactionPost $request, $salesorder_id)
    {
        if($request->type_payment == "Installment"){
            if($request->tenor == null){
                return redirect('/web/salesorder/'.$salesorder_id)
                    ->with('status','danger')
                    ->with('message','Tenor Installment belum dipilih!');
            }
        }

        $salesorder = Salesorder::where('id', $salesorder_id)->first();
        $data_transaction['id_salesorder'] = $salesorder_id;

        if($request->msisdn != null){
            if(substr( $request->msisdn, 0, 2 ) === "08")
            {
                $data_transaction['msisdn'] = preg_replace('|[^0-9]|i', '', preg_replace('/^0?/', '62', $request->msisdn));
            }
            else
            {
                $data_transaction['msisdn'] = preg_replace('|[^0-9]|i', '', $request->msisdn);
            }
        }

        $data_transaction['iccid'] = $request->iccid;
        $data_transaction['type_bundling'] = $request->type_bundling;

        if($request->type_bundling == "Postpaid"){
            if($request->m2m == null){
                return redirect('/web/salesorder/'.$salesorder_id)
                    ->with('status','danger')
                    ->with('message','M2M belum dipilih!');
            }
            $data_m2m = M2M::where('id', $request->m2m)->first();
            $data_transaction['m2m'] = $data_m2m->name;
        } else if($request->type_bundling == "Prepaid"){
            if($request->package_type == null){
                return redirect('/web/salesorder/'.$salesorder_id)
                    ->with('status','danger')
                    ->with('message','Package Type belum dipilih!');
            }
            $data_package_type = SimPackage::where('id', $request->package_type)->first();
            $data_transaction['package_type'] = $data_package_type->name;
            $data_transaction['type_prepaid'] = $request->type_prepaid;
            $data_transaction['hlr'] = $request->hlr;
        }

        $data_non_m2m = NonM2M::where('id', $request->non_m2m)->first();
        $data_transaction['non_m2m'] = $data_non_m2m->name;

        $data_sim_spec = SimSpecification::where('id', $request->sim_specification)->first();
        $data_transaction['sim_specification'] = $data_sim_spec->name;

        $data_package = PackageInfo::where('id', $request->package_basic)->first();
        $data_transaction['package_basic'] = $data_package->package;

        $total_price = 0;
        if($salesorder->type_req == "Bundling"){
            $data_brand = DeviceType::where('id', $request->brand)->first();
            $data_transaction['brand'] = $data_brand->product;

            $data_transaction['type_payment'] = $request->type_payment;

            if($request->type_payment == "Cash"){
                $data_transaction['tenor'] = 1;
                if($salesorder->quantity <= 100){
                    $data_transaction['device_price'] = number_format($data_brand->cash_1);
                    $total_price += (int)$data_brand->cash_1;
                } else if($salesorder->quantity <= 500){
                    $data_transaction['device_price'] = number_format($data_brand->cash_2);
                    $total_price += (int)$data_brand->cash_2;
                } else {
                    $data_transaction['device_price'] = number_format($data_brand->cash_3);
                    $total_price += (int)$data_brand->cash_3;
                }
            } else {
                $data_transaction['tenor'] = $request->tenor;
                if($request->tenor == 3){
                    if($salesorder->quantity <= 100){
                        $data_transaction['device_price'] = number_format($data_brand->months_3_1);
                        $total_price += (3*(int)$data_brand->months_3_1);
                    } else if($salesorder->quantity <= 500){
                        $data_transaction['device_price'] = number_format($data_brand->months_3_2);
                        $total_price += (3*(int)$data_brand->months_3_2);
                    } else {
                        $data_transaction['device_price'] = number_format($data_brand->months_3_3);
                        $total_price += (3*(int)$data_brand->months_3_3);
                    }
                } else if($request->tenor == 6){
                    if($salesorder->quantity <= 100){
                        $data_transaction['device_price'] = number_format($data_brand->months_6_1);
                        $total_price += (6*(int)$data_brand->months_6_1);
                    } else if($salesorder->quantity <= 500){
                        $data_transaction['device_price'] = number_format($data_brand->months_6_2);
                        $total_price += (6*(int)$data_brand->months_6_2);
                    } else {
                        $data_transaction['device_price'] = number_format($data_brand->months_6_3);
                        $total_price += (6*(int)$data_brand->months_6_3);
                    }
                } else if($request->tenor == 12){
                    if($salesorder->quantity <= 100){
                        $data_transaction['device_price'] = number_format($data_brand->months_12_1);
                        $total_price += (12*(int)$data_brand->months_12_1);
                    } else if($salesorder->quantity <= 500){
                        $data_transaction['device_price'] = number_format($data_brand->months_12_2);
                        $total_price += (12*(int)$data_brand->months_12_2);
                    } else {
                        $data_transaction['device_price'] = number_format($data_brand->months_12_3);
                        $total_price += (12*(int)$data_brand->months_12_3);
                    }
                } else if($request->tenor == 24){
                    if($salesorder->quantity <= 100){
                        $data_transaction['device_price'] = number_format($data_brand->months_24_1);
                        $total_price += (24*(int)$data_brand->months_24_1);
                    } else if($salesorder->quantity <= 500){
                        $data_transaction['device_price'] = number_format($data_brand->months_24_2);
                        $total_price += (24*(int)$data_brand->months_24_2);
                    } else {
                        $data_transaction['device_price'] = number_format($data_brand->months_24_3);
                        $total_price += (24*(int)$data_brand->months_24_3);
                    }
                }
            }
        } else {
            $total_price += $data_package->price;
            $data_transaction['device_price'] = number_format(0);
        }

        $data_transaction['device_mrc'] = number_format($data_package->price);
        $data_transaction['price_total'] = number_format($total_price);

        $transaction = SalesOrderTransaction::create($data_transaction);

        if($request->package_add[0] != null){
            foreach($request->package_add as $package)
            {
                SalesOrderAddOn::create([
                    'id_transaction' => $transaction->id,
                    'id_addon' => $package
                ]);
            }
        }



        // if($request->type_payment == "Installment"){
        //     if($request->tenor == null){
        //         return redirect('/web/salesorder/'.$salesorder_id)
        //             ->with('status','danger')
        //             ->with('message','Tenor Installment belum dipilih!');
        //     }
        // }

        // $salesorder = Salesorder::where('id', $salesorder_id)->first();
        // $data_transaction['id_salesorder'] = $salesorder_id;

        // if($request->msisdn != null){
        //     if(substr( $request->msisdn, 0, 2 ) === "08")
        //     {
        //         $data_transaction['msisdn'] = preg_replace('|[^0-9]|i', '', preg_replace('/^0?/', '62', $request->msisdn));
        //     }
        //     else
        //     {
        //         $data_transaction['msisdn'] = preg_replace('|[^0-9]|i', '', $request->msisdn);
        //     }
        // }

        // $data_transaction['iccid'] = $request->iccid;
        // $data_transaction['type_bundling'] = $request->type_bundling;
        // $data_transaction['type_prepaid'] = $request->type_prepaid;
        // $data_transaction['type_sim'] = $request->type_sim;

        // $data_brand = DeviceType::where('id', $request->brand)->first();
        // $data_transaction['brand'] = $data_brand->product;

        // $data_package = PackageInfo::where('id', $request->package_basic)->first();
        // $data_transaction['package_basic'] = $data_package->package;

        // $data_transaction['type_payment'] = $request->type_payment;

        // $total_price = 0;
        // if($request->type_payment == "Cash"){
        //     $data_transaction['tenor'] = 1;
        //     if($salesorder->quantity <= 100){
        //         $data_transaction['device_price'] = number_format($data_brand->cash_1);
        //         $total_price += (int)$data_brand->cash_1;
        //     } else if($salesorder->quantity <= 500){
        //         $data_transaction['device_price'] = number_format($data_brand->cash_2);
        //         $total_price += (int)$data_brand->cash_2;
        //     } else {
        //         $data_transaction['device_price'] = number_format($data_brand->cash_3);
        //         $total_price += (int)$data_brand->cash_3;
        //     }
        // } else {
        //     $data_transaction['tenor'] = $request->tenor;
        //     if($request->tenor == 3){
        //         if($salesorder->quantity <= 100){
        //             $data_transaction['device_price'] = number_format($data_brand->months_3_1);
        //             $total_price += (3*(int)$data_brand->months_3_1);
        //         } else if($salesorder->quantity <= 500){
        //             $data_transaction['device_price'] = number_format($data_brand->months_3_2);
        //             $total_price += (3*(int)$data_brand->months_3_2);
        //         } else {
        //             $data_transaction['device_price'] = number_format($data_brand->months_3_3);
        //             $total_price += (3*(int)$data_brand->months_3_3);
        //         }
        //     } else if($request->tenor == 6){
        //         if($salesorder->quantity <= 100){
        //             $data_transaction['device_price'] = number_format($data_brand->months_6_1);
        //             $total_price += (6*(int)$data_brand->months_6_1);
        //         } else if($salesorder->quantity <= 500){
        //             $data_transaction['device_price'] = number_format($data_brand->months_6_2);
        //             $total_price += (6*(int)$data_brand->months_6_2);
        //         } else {
        //             $data_transaction['device_price'] = number_format($data_brand->months_6_3);
        //             $total_price += (6*(int)$data_brand->months_6_3);
        //         }
        //     } else if($request->tenor == 12){
        //         if($salesorder->quantity <= 100){
        //             $data_transaction['device_price'] = number_format($data_brand->months_12_1);
        //             $total_price += (12*(int)$data_brand->months_12_1);
        //         } else if($salesorder->quantity <= 500){
        //             $data_transaction['device_price'] = number_format($data_brand->months_12_2);
        //             $total_price += (12*(int)$data_brand->months_12_2);
        //         } else {
        //             $data_transaction['device_price'] = number_format($data_brand->months_12_3);
        //             $total_price += (12*(int)$data_brand->months_12_3);
        //         }
        //     } else if($request->tenor == 24){
        //         if($salesorder->quantity <= 100){
        //             $data_transaction['device_price'] = number_format($data_brand->months_24_1);
        //             $total_price += (24*(int)$data_brand->months_24_1);
        //         } else if($salesorder->quantity <= 500){
        //             $data_transaction['device_price'] = number_format($data_brand->months_24_2);
        //             $total_price += (24*(int)$data_brand->months_24_2);
        //         } else {
        //             $data_transaction['device_price'] = number_format($data_brand->months_24_3);
        //             $total_price += (24*(int)$data_brand->months_24_3);
        //         }
        //     }
        // }

        // $data_transaction['device_mrc'] = number_format($data_package->price);
        // $data_transaction['price_total'] = number_format($total_price);

        // $transaction = SalesOrderTransaction::create($data_transaction);

        // if($request->package_add[0] != null){
        //     foreach($request->package_add as $package)
        //     {
        //         SalesOrderAddOn::create([
        //             'id_transaction' => $transaction->id,
        //             'id_addon' => $package
        //         ]);
        //     }
        // }

        return redirect('/salesorder/'.$salesorder_id.'/activation')
            ->with('status','success')
            ->with('message','Transaction created successfuly!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SalesOrderTransaction  $transaction_id
     * @return \Illuminate\Http\Response
     */
    public function edit_manage($transaction_id)
    {
        $data['transaction'] = SalesOrderTransaction::where('id', $transaction_id)->first();
        $data['salesorder'] = SalesOrder::where('id', $data['transaction']->id_salesorder)->first();

        if(is_null($data['transaction']))
        {
            return redirect('/salesorder')
                ->with('status','danger')
                ->with('message','Transaction ID not found.');
        }

        $add_on = SalesOrderAddOn::where('id_transaction', $transaction_id)->get();

        $data['selected_add_on'] = array();
        for($i=0; $i<count($add_on); $i++){
            $data['selected_add_on'][$i] = PackageAddOn::where('id', $add_on[$i]["id_addon"])->first();
        }

        $data['total_selected_add_on'] = count($data['selected_add_on']);
        $data['add_on'] = PackageAddOn::pluck('addon','id');
        $data['basic'] = PackageInfo::pluck('package','id');
        $data['device'] = DeviceType::pluck('product','id');
        $data['list_m2m'] = M2M::orderBy('name', 'asc')->pluck('name','id');
        $data['list_non_m2m'] = NonM2M::orderBy('name', 'asc')->pluck('name','id');
        $data['list_sim_pack'] = SimPackage::orderBy('name', 'asc')->pluck('name','id');
        $data['list_sim_spec'] = SimSpecification::orderBy('name', 'asc')->pluck('name','id');

        if($data['transaction']->type_bundling == 'Postpaid'){
            $data['selected_m2m'] = M2M::where('name', $data['transaction']->m2m)->first();
        } else {
            $data['selected_package_type'] = SimPackage::where('name', $data['transaction']->package_type)->first();
        }

        $data['selected_non_m2m'] = NonM2M::where('name', $data['transaction']->non_m2m)->first();
        $data['selected_sim_specification'] = SimSpecification::where('name', $data['transaction']->sim_specification)->first();
        $data['selected_device_type'] = DeviceType::where('product', $data['transaction']->brand)->first();
        $data['selected_basic_package'] = PackageInfo::where('package', $data['transaction']->package_basic)->first();

        return view('salesorder.manage_salesorder_transaction_edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SalesOrderTransaction  $transaction_id
     * @return \Illuminate\Http\Response
     */
    public function update_manage(Request $request, $transaction_id)
    {
        if($request->type_payment == "Installment"){
            if($request->tenor == null){
                return redirect('/web/salesorder/transaction/'.$transaction_id .'/edit')
                    ->with('status','danger')
                    ->with('message','Tenor Installment belum dipilih!');
            }
        }

        $last_transaction = SalesorderTransaction::where('id', $transaction_id)->first();
        $salesorder = Salesorder::where('id', $last_transaction->id_salesorder)->first();

        if($request->msisdn != null){
            if(substr( $request->msisdn, 0, 2 ) === "08")
            {
                $data_transaction['msisdn'] = preg_replace('|[^0-9]|i', '', preg_replace('/^0?/', '62', $request->msisdn));
            }
            else
            {
                $data_transaction['msisdn'] = preg_replace('|[^0-9]|i', '', $request->msisdn);
            }
        }

        $data_transaction['iccid'] = $request->iccid;
        $data_transaction['type_bundling'] = $request->type_bundling;
        $data_transaction['status'] = $request->status;

        if($request->type_bundling == "Postpaid"){
            if($request->m2m == null){
                return redirect('/salesorder/transaction/'.$transaction_id.'/edit')
                    ->with('status','danger')
                    ->with('message','M2M belum dipilih!');
            }
            $data_m2m = M2M::where('id', $request->m2m)->first();
            $data_transaction['m2m'] = $data_m2m->name;
            $data_transaction['package_type'] = null;
            $data_transaction['type_prepaid'] = null;
            $data_transaction['hlr'] = null;
        } else if($request->type_bundling == "Prepaid"){
            if($request->package_type == null){
                return redirect('/salesorder/transaction/'.$transaction_id.'/edit')
                    ->with('status','danger')
                    ->with('message','Package Type belum dipilih!');
            }
            $data_package_type = SimPackage::where('id', $request->package_type)->first();
            $data_transaction['package_type'] = $data_package_type->name;
            $data_transaction['type_prepaid'] = $request->type_prepaid;
            $data_transaction['hlr'] = $request->hlr;
            $data_transaction['m2m'] = null;
        }

        $data_non_m2m = NonM2M::where('id', $request->non_m2m)->first();
        $data_transaction['non_m2m'] = $data_non_m2m->name;

        $data_sim_spec = SimSpecification::where('id', $request->sim_specification)->first();
        $data_transaction['sim_specification'] = $data_sim_spec->name;

        $data_package = PackageInfo::where('id', $request->package_basic)->first();
        $data_transaction['package_basic'] = $data_package->package;

        $data_package = PackageInfo::where('id', $request->package_basic)->first();
        $data_transaction['package_basic'] = $data_package->package;

        $total_price = 0;
        if($salesorder->type_req == "Bundling"){
            $data_brand = DeviceType::where('id', $request->brand)->first();
            $data_transaction['brand'] = $data_brand->product;

            $data_transaction['type_payment'] = $request->type_payment;
            
            if($request->type_payment == "Cash"){
                $data_transaction['tenor'] = 1;
                if($salesorder->quantity <= 100){
                    $data_transaction['device_price'] = number_format($data_brand->cash_1);
                    $total_price += (int)$data_brand->cash_1;
                } else if($salesorder->quantity <= 500){
                    $data_transaction['device_price'] = number_format($data_brand->cash_2);
                    $total_price += (int)$data_brand->cash_2;
                } else {
                    $data_transaction['device_price'] = number_format($data_brand->cash_3);
                    $total_price += (int)$data_brand->cash_3;
                }
            } else {
                $data_transaction['tenor'] = $request->tenor;
                if($request->tenor == 3){
                    if($salesorder->quantity <= 100){
                        $data_transaction['device_price'] = number_format($data_brand->months_3_1);
                        $total_price += (3*(int)$data_brand->months_3_1);
                    } else if($salesorder->quantity <= 500){
                        $data_transaction['device_price'] = number_format($data_brand->months_3_2);
                        $total_price += (3*(int)$data_brand->months_3_2);
                    } else {
                        $data_transaction['device_price'] = number_format($data_brand->months_3_3);
                        $total_price += (3*(int)$data_brand->months_3_3);
                    }
                } else if($request->tenor == 6){
                    if($salesorder->quantity <= 100){
                        $data_transaction['device_price'] = number_format($data_brand->months_6_1);
                        $total_price += (6*(int)$data_brand->months_6_1);
                    } else if($salesorder->quantity <= 500){
                        $data_transaction['device_price'] = number_format($data_brand->months_6_2);
                        $total_price += (6*(int)$data_brand->months_6_2);
                    } else {
                        $data_transaction['device_price'] = number_format($data_brand->months_6_3);
                        $total_price += (6*(int)$data_brand->months_6_3);
                    }
                } else if($request->tenor == 12){
                    if($salesorder->quantity <= 100){
                        $data_transaction['device_price'] = number_format($data_brand->months_12_1);
                        $total_price += (12*(int)$data_brand->months_12_1);
                    } else if($salesorder->quantity <= 500){
                        $data_transaction['device_price'] = number_format($data_brand->months_12_2);
                        $total_price += (12*(int)$data_brand->months_12_2);
                    } else {
                        $data_transaction['device_price'] = number_format($data_brand->months_12_3);
                        $total_price += (12*(int)$data_brand->months_12_3);
                    }
                } else if($request->tenor == 24){
                    if($salesorder->quantity <= 100){
                        $data_transaction['device_price'] = number_format($data_brand->months_24_1);
                        $total_price += (24*(int)$data_brand->months_24_1);
                    } else if($salesorder->quantity <= 500){
                        $data_transaction['device_price'] = number_format($data_brand->months_24_2);
                        $total_price += (24*(int)$data_brand->months_24_2);
                    } else {
                        $data_transaction['device_price'] = number_format($data_brand->months_24_3);
                        $total_price += (24*(int)$data_brand->months_24_3);
                    }
                }
            }
        } else {
            $total_price += $data_package->price;
            $data_transaction['device_price'] = number_format(0);
        }

        $data_transaction['device_mrc'] = number_format($data_package->price);
        $data_transaction['price_total'] = number_format($total_price);

        SalesorderTransaction::where('id', $transaction_id)->update($data_transaction);

        SalesOrderAddOn::where('id_transaction', $transaction_id)->delete();
        
        if($request->package_add[0] != null){
            foreach($request->package_add as $package)
            {
                SalesOrderAddOn::create([
                    'id_transaction' => $transaction_id,
                    'id_addon' => $package
                ]);
            }
        }

        $count_stock = SalesOrderTransaction::where('id_salesorder', $salesorder->id)->where('status', 'Out of Stock')->get();
        if(count($count_stock) != 0){
            Salesorder::where('id', $salesorder->id)->update([
                'status_helper' => count($count_stock).' Out of Stock'
            ]);
        }

        return redirect('/salesorder/'.$last_transaction->id_salesorder.'/activation')
            ->with('status','success')
            ->with('message','Transaction has been updated successfuly.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SalesOrderTransaction  $transaction_id
     * @return \Illuminate\Http\Response
     */
    public function destroy_manage($transaction_id)
    {
        $transaction = SalesorderTransaction::findOrFail($transaction_id);
        $salesorder_id = $transaction->id_salesorder;
        $salesorder_msisdn = $transaction->msisdn;
        $transaction->delete();

        return redirect('/salesorder/'.$salesorder_id.'/activation')
            ->with('status','success')
            ->with('message','Transaction '.$salesorder_msisdn.' has been deleted successfuly.');
    }

}
