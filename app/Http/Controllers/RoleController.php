<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        validate_permission('manage role and permission');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Role $role)
    {
        validate_permission('manage role and permission');
    	$roles = $role::all();

        foreach($roles as $key => $role)
        {
            $role_list[$key]['id'] = $role->id;
            $role_list[$key]['name'] = $role->name;

            foreach(Permission::all() as $permission)
            {
                if($role->hasPermissionTo($permission->name))
                {
                    $role_list[$key]['permissions'][] = $permission->name;
                }
                else
                {
                    $role_list[$key]['permissions'][] = "";
                }
            }
        }

        $data = ['roles' => $role_list];

        // Log activity
        // ActivityLog::info('ROLE','View Role Management page');

    	return view('admin.role.index_role', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['permissions'] = Permission::all();         

        // Log activity
        // ActivityLog::info('ROLE','View Create Role page');

        return view('admin.role.create_role', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $permissions = $request['permission'];

        $role = Role::create([
            'name' => $request['name'],
            'landing_page' => $request['landing_page']
        ]);

        foreach($permissions as $permission)
        {
            $role->givePermissionTo($permission);
        }

        // Log activity
        // ActivityLog::info('ROLE','Submitted Role item', ['role_name' => $request['name']]);

        return redirect('config/role')
            ->with('status','success')
        	->with('message', 'Role \''. $request['name'] .'\' and its permission has been setup successfuly.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($role_id)
    {
  
    	$data['role'] = Role::findOrFail($role_id);
        $permissions = Permission::all();
        $role_permission = [];

        foreach($permissions as $key => $permission)
        {
            $role_permission[$key]['name'] = $permission->name;
            $role_permission[$key]['checked'] = $data['role']->hasPermissionTo($permission) ? "checked" : "";
        }

        $data['permissions'] = $role_permission;

        // Log activity
        // ActivityLog::info('ROLE','Edit Role page', ['role_id' => $role_id]);

        return view('admin.role.edit_role', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	Role::find($id)->update([
            'name' => $request->name,
            'landing_page' => $request->landing_page
        ]);

        $new_permissions = $request['permission'];
        $all_permissions = Permission::all();
        $role = Role::find($id);

        foreach($all_permissions as $all_permission)
        {
            $role->revokePermissionTo($all_permission);
        }

        foreach($new_permissions as $new_permission)
        {
            $role->givePermissionTo($new_permission);
        }                 

        // Log activity
        // ActivityLog::info('ROLE','Updated Role item', ['role_name' => $request->name]);

        return redirect('config/role')
            ->with('status','success')
            ->with('message', 'Role \''. $request['name'] .'\' have been updated!');
    	
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $usroleer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        //
    }

    /**
     * Get role name by role_id.
     *
     * @param  $role_id
     * @return $role->name
     */
    public function get_role_by_id($role_id)
    {
        $role = new Role;
        $role_name = $role->get_name_by_id($role_id);

        return $role_name;
    }   

    /**
     * Get role id by role->name.
     *
     * @param  $role_name
     * @return $role->id
     */
    public function get_role_by_name($role_name)
    {
        $role = new Role;
        $role_id = $role->get_id_by_name($role_name);

        return $role_id;
    }
}
