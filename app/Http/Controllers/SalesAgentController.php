<?php

namespace App\Http\Controllers;

use Excel;
use App\Region;
use App\Exports\SalesAgentExport;
use App\Imports\SalesAgentImport;
use App\SalesAgent;
use Illuminate\Http\Request;
use App\Http\Requests\SalesAgentPost;

class SalesAgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sales'] = SalesAgent::orderBy('nama')->get();
        
        foreach($data['sales'] as $sales)
        {
            if($sales['department']){
                $sales['department'] = Region::where('id', $sales['department'])->first();
            }
        }
        
        return view('salesagent.manage_salesagent_index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     return view('salesagent.manage_salesagent_create');
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(SalesAgentPost $request)
    // {
    //     SalesAgent::create([
    //         'ae_name' => strtoupper($request['ae_name']),
    //         'dept_mapping' => strtoupper($request['dept_mapping'])
    //     ]);

    //     return redirect('/sales')
    //         ->with('status','success')
    //         ->with('message', 'Sales Agent '. $request['ae_name'] .' has been created!');    
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SalesAgent  $sales_agent
     * @return \Illuminate\Http\Response
     */
    public function edit($sales_agent_id)
    {
        $data['sales'] = SalesAgent::find($sales_agent_id);
        $data['region'] = Region::pluck('name', 'id');
        
        return view('salesagent.manage_salesagent_edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SalesAgent  $sales_agent
     * @return \Illuminate\Http\Response
     */
    public function update(SalesAgentPost $request, $sales_agent_id)
    {
        $data['nama'] = strtoupper($request->nama);
        $data['department'] = strtoupper($request->department);
        $data['divisi'] = strtoupper($request->divisi);
        $data['group'] = strtoupper($request->group);

        SalesAgent::find($sales_agent_id)->update($data);

        SalesOrder::where('id_sales', $sales_agent_id)->update([
            'sales_group' => strtoupper($request->department)
        ]);

        return redirect('/sales')
            ->with('status','success')
            ->with('message', 'Sales Agent '. $request['nama'] .' has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SalesAgent  $sales_agent_id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($sales_agent_id)
    // {
    //     $sales_agent = SalesAgent::find($sales_agent_id);
    //     $nama_sales_agent = $sales_agent->ae_name;
    //     $sales_agent->delete();

    //     return redirect('/sales')
    //         ->with('status','success')
    //         ->with('message', 'Sales Agent '. $nama_sales_agent .' has been deleted!');
    // }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function export()
    {
        $current_date = Date("d-m-Y");
        return Excel::download(new SalesAgentExport, 'Daftar Sales Agent ('.$current_date .').xlsx');
    }
}
