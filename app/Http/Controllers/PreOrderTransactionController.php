<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Storage;
use App\Plan;
use App\PreOrder;
use App\PackageInfo;
use App\PackageAddOn;
use App\DeviceType;
use App\SalesAgent;
use App\PreOrderPlan;
use App\PreOrderAddOn;
use App\PreOrderDevice;
use App\PreOrderTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\PreOrderPost;

class PreOrderTransactionController extends Controller
{
    public function __construct()
    {
        //validate_permission('submit preorder');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['today'] = Carbon::now();
        $data['preorders'] = PreOrder::where('enddate','>', $data['today'])->get();

        return view('preorder.preorder_index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $preorder_code = Input::get('preorder_code');
        $data['preorder'] = PreOrder::where('preorder_code', $preorder_code)->first();

        if(is_null($data['preorder']))
        {
            return redirect(route('preorder.index'));
        }

        if(Carbon::now() < $data['preorder']->startdate)
        {
            $data['preorder_status'] = "waiting";
        }
        else if(Carbon::now() < $data['preorder']->enddate)
        {
            $data['preorder_status'] = "running";
        }
        else
        {
            $data['preorder_status'] = "closed";
        }

        $data['current_date'] = date("Y-m-d");
        $data['plans'] = Plan::whereIn('plan_type_id', [2,8])->pluck('name','id');
        $data['add_on'] = PackageAddOn::pluck('addon','id');
        $data['basic'] = PackageInfo::pluck('package','id');
        $data['device'] = DeviceType::pluck('product','id');
        $data['sales_agent'] = SalesAgent::pluck('ae_name', 'id');
        $data['price_add_on'] = PackageAddOn::pluck('price','id');
        $data['price_basic'] = PackageInfo::pluck('price','id');
        $data['transactions'] = $data['preorder']->transactions()->paginate(25);
        $data['preorder_code'] = $preorder_code;
        $data['devices'] = PreOrderDevice::where('preorder_id', $data['preorder']->id)->orderBy('id')->get();
        $data['plans'] = PreOrderPlan::where('preorder_id', $data['preorder']->id)->get()->pluck('plan.name', 'plan.id');
        $data['periode'] = PreOrderPlan::where('preorder_id', $data['preorder']->id)->groupBy('periode')->pluck('periode','periode');
        /*
        $bank_promos = PreOrderBankPromo::where('preorder_id', $data['preorder']->id)->get();

        if($bank_promos->count() > 0)
        {
            foreach($bank_promos as $key => $promo)
            {
                $data['bank_promos'][$promo['id']] = $promo->bank['name'] ." Rp ". number_format($promo['value'],0,',','.');
            }

            array_unshift($data['bank_promos'],"- Tanpa Promo -");
        }
		*/
        return view('preorder.preorder_create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubmitPreOrder $request)
    {
        $preorder_data = PreOrder::where('preorder_code', $request->preorder_code)->first();
        $store_id = Auth::user()->store_id;

        if(Carbon::now() < $preorder_data->startdate)
        {
            return redirect(route('preorder.show', ['preorder_code' => $request->preorder_code]))
                ->with('status','warning')
                ->with('message','Pre-Order belum dimulai, silahkan tunggu sampai '. Carbon::parse($preorder_data->startdate).'.');
        }
        else if(Carbon::now() < $preorder_data->enddate)
        {
            $booked = PreOrderTransaction::where('device_id', $request->device)->whereIn('status',[1,2])->count();
            $device = PreOrderDevice::where('id',$request->device)->first();

            // Check if device quota still available
            if($booked < $device->quota)
            {
                if(substr( $request->msisdn, 0, 2 ) === "08")
                {
                    $transaction['msisdn'] = preg_replace('|[^0-9]|i', '', preg_replace('/^0?/', '62', $request->msisdn));
                }
                else
                {
                    $transaction['msisdn'] = preg_replace('|[^0-9]|i', '', $request->msisdn);
                }

                $transaction['reg_code'] = $this->get_preorder_reg_code();
                $transaction['preorder_id'] = $preorder_data->id;
                $transaction['device_id'] = $request->device;
                $transaction['store_id'] = $store_id;
                $transaction['customer_name'] = $request->fullname;
                $transaction['id_number'] = $request->id_number;
                $transaction['email'] = $request->email;
                $transaction['mother_name'] = $request->mother_name;
                $transaction['plan_id'] = $request->plan_id;
                $transaction['periode'] = $request->periode;
                $transaction['payment_method'] = $request->payment_method;
                $transaction['payment_type'] = $request->payment_type;
                $transaction['bank_promo_id'] = $request->bank_promo;
                $transaction['status'] = 1; // Status : 1 = booking, 2 = paid

                PreOrderTransaction::create($transaction);

                // Log activity
                $customer_preorder = [
                    'customer' => $request->fullname,
                    'device' => $request->device,
                    'plan' => $request->plan_id,
                    'periode' => $request->periode
                ];

                ActivityLog::info('PREORDER','Success submit customer Pre-Order', $customer_preorder);

                return redirect(route('preorder.show', ['preorder_code' => $request->preorder_code]))
                    ->with('status','success')
                    ->with('message','Pre-Order untuk saudara/i "'.$request->fullname.'" telah sukses didaftarkan,
                                        silahkan lakukan pembayaran dan klik tombol "Konfirmasi Bayar".');
            }
            else
            {
                $device = PreOrderDevice::where('id',$request->device)->first();

                ActivityLog::info('PREORDER','Failed submit customer Pre-Order, reached quota.');

                return redirect(route('preorder.create') ."?preorder_code=". $request->preorder_code)
                    ->withInput()
                    ->with('status','danger')
                    ->with('message', 'Quota "'.$device->model .'" sudah habis, silahkan pilih model atau warna lain.');
            }
        }
        else
        {
            return redirect(route('preorder.show', ['preorder_code' => $request->preorder_code]))
                ->with('status','danger')
                ->with('message','Pre-Order sudah ditutup.');
        }
    }

    public function confirm_payment(Request $request)
    {
        $reg_id = $request->reg_id;
        $reg_code = $request->reg_code;
        $invoice_number = $request->invoice_number;
        $customer = $request->customer;
        $preorder_code = $request->preorder_code;

        if($invoice_number)
        {
            PreOrderTransaction::where('id', $reg_id)->where('reg_code',$reg_code)
                                ->update(['invoice_number' => $request->invoice_number, 'status' => 2]);

            $preorder_data = ['invoice_number' => $invoice_number];

            ActivityLog::info('PREORDER','Confirm payment customer Pre-Order.', $preorder_data);

            return redirect(route('preorder.show', ['preorder_code' => $preorder_code]))
                ->with('status','success')
                ->with('message','Pre-Order untuk saudara/i "'.$customer .'" sudah diupdate.');
        }
        else
        {
            return redirect(route('preorder.show', ['preorder_code' => $preorder_code]))
                ->with('status','danger')
                ->with('message','Silahkan masukan nomor invoice.');
        }
    }

    /**
     * Restore transaction
     */
    public function restore_trans(Request $request)
    {
        $preorder = PreOrderTransaction::where('id', $request->reg_id)->first();
        $prev_status = $preorder->status;

        if($prev_status == 1)
        {
            $status_label = "BOOKED";
        }
        else if($prev_status == 2)
        {
            $status_label = "PAID";
        }
        else
        {
            $status_label = "CANCELED";
        }

        if($request->status == 1)
        {
            $cur_status_label = "BOOKED";
        }
        else if($request->status == 2)
        {
            $cur_status_label = "PAID";
        }
        else
        {
            $cur_status_label = "CANCELED";
        }

        $preorder->update([
            'status' => $request->status
        ]);

        // Log activity
        ActivityLog::info('PREORDER','Status update for "'. $request->reg_code .'" from '. $status_label .' to '. $cur_status_label .'.');

        return redirect($request->url)
            ->with('status','success')
            ->with('message','Pre-order code "'. $request->reg_code .'" has been updated successfuly.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PreOrderTransaction  $preOrderTransaction
     * @return \Illuminate\Http\Response
     */
    public function show($preorder_code)
    {
        $customer = Input::get('customer') ? Input::get('customer') : "";
        $msisdn = Input::get('msisdn') ? Input::get('msisdn') : "";
        $reg_code = Input::get('reg_code') ? Input::get('reg_code') : "";
        $where = [];

        if($customer)
        {
            array_push($where, ['customer_name', 'LIKE', '%'. $customer .'%']);
        }

        if($msisdn)
        {
            array_push($where, ['msisdn', 'LIKE', '%'. $msisdn .'%']);
        }

        if($reg_code)
        {
            array_push($where, ['reg_code', 'LIKE', '%'. $reg_code .'%']);
        }

        $store_id = Auth::user()->store_id;
        $data['preorder'] = PreOrder::where('preorder_code', $preorder_code)->first();

        if(is_null($data['preorder']))
        {
            return redirect(route('preorder.index'));
        }

        $data['transactions'] = $data['preorder']->transactions()->where($where)->where('store_id', $store_id)
                                                ->orderBy('id','DESC')->paginate(25);

        $data['customer'] = $customer;
        $data['msisdn'] = $msisdn;
        $data['reg_code'] = $reg_code;

        if(Carbon::now() < $data['preorder']->startdate)
        {
            $data['preorder_status'] = "waiting";
        }
        else if(Carbon::now() < $data['preorder']->enddate)
        {
            $data['preorder_status'] = "running";
        }
        else
        {
            $data['preorder_status'] = "closed";
        }

        // Log activity
        ActivityLog::info('PREORDER','View Store Pre-Order detail page');

        return view('preorder.preorder_view', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PreOrderTransaction  $preOrderTransaction
     * @return \Illuminate\Http\Response
     */
    public function edit(PreOrderTransaction $preOrderTransaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PreOrderTransaction  $preOrderTransaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PreOrderTransaction $preOrderTransaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PreOrderTransaction  $preOrderTransaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(PreOrderTransaction $preOrderTransaction)
    {
        //
    }

    /**
     * Get random preorder unique code, keep generate if code exist.
     *
     * @return String unique registration code
     */
    public function get_preorder_reg_code()
    {
        $preorder_reg_code = str_random(5);

        $code_exist = PreOrderTransaction::where('reg_code', $preorder_reg_code)->count();

        if($code_exist > 0)
        {
            $this->get_preorder_reg_code();
        }
        else
        {
            return $preorder_reg_code;
        }
    }
}
