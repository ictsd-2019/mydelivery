<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MainActivity;
use App\OutTask;
use App\Http\Requests\MainPost;
use Excel;

class MainActivityController extends Controller
{
 
    public function index()
    {
        $datas['main'] = MainActivity::join('outtask', 'outtask.id', '=', 'main_activity.id_vendor')
                       ->orderBy('regional', 'asc')->get(['main_activity.*', 'outtask.name']);   
        return view('main_activity.index', $datas);
    }


    public function create()
    {
        $datas['selected_vendor'] = OutTask::orderBy('name', 'asc')->pluck('name', 'id');
        return view('main_activity.created', $datas);
    }


    public function store(MainPost $request)
    {
        //$num         = mt_rand(1000, 9999);
        //$no          = $num+1;
        MainActivity::create([
            'id_vendor'  => $request['ven'],
            'regional'   => $request['reg'],
            'activity'   => $request['act'],
            'notes'      => $request['note']
        ]);

        return redirect('/main')
            ->with('status','success')
            ->with('message', 'Data Main Activity  has been created!');
    }

    
    public function edit($id)
    {
        $kode                     = base64_decode($id);
        $datas['main']            = MainActivity::find($kode);
        $datas['selected_vendor'] = OutTask::orderBy('name', 'asc')->pluck('name', 'id');
        return view('main_activity.edit', $datas);
    }


    public function update(MainPost $request, $id)
    {
        $datas                  = MainActivity::where('id', $id)->first();
        $data_type['id_vendor'] = $request->ven;
        $data_type['regional']  = $request->reg;
        $data_type['activity']  = $request->act;
        $data_type['notes']     = $request->note;
        MainActivity::find($id)->update($data_type);

        return redirect('/main')
            ->with('status','success')
            ->with('message', 'Data Main Activity has been updated!');
    }

   
    public function destroy($id)
    {
        $kode     = base64_decode($id);
        $datas    = MainActivity::find($kode);
        $reg      = $datas->regional;
        $datas->delete();

        return redirect('/main')
            ->with('status','success')
            ->with('message', 'Data Main Activity Regional : '. $reg .' has been deleted!');
    }

}
