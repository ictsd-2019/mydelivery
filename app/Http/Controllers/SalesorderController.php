<?php

namespace App\Http\Controllers;

use Storage;
use Excel;
use DataTables;
use Auth;
use App\Exports\SalesOrderExport;
use Carbon\Carbon;
use App\Salesorder;
use App\Employee;
use App\PackageAddOn;
use App\PackageInfo;
use App\DeviceType;
use App\SalesAgent;
use App\SalesAdmin;
use App\Region;
use App\M2M;
use App\NonM2M;
use App\SimPackage;
use App\SimSpecification;
use App\Shipment;
use App\SalesOrderShipment;
use App\DeliveryRequest;
use App\SalesOrderAddOn;
use App\SalesOrderRemarks;
use App\SalesOrderAttachment;
use App\SalesOrderTransaction;
use App\SalesOrderDeliveryRequest;
use Illuminate\Http\Request;
use App\Http\Requests\SalesOrderPost;
use App\Http\Requests\ExportParam;

class SalesorderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $day = date("d");
        $month = date("m");
        $year = (string)((int)date("Y")-1);
        $last_year_date = date($year.'-'.$month.'-'.$day);

        if(Auth::user()->roles()->pluck('name')->first() == "Sales"){
            $sales = SalesAgent::where('id_user', Auth::user()->id)->first();
            $data['salesorder'] = Salesorder::where('created_at', '>=', $last_year_date)->where('id_sales', $sales->id)->orderBy('id', 'desc')->get();
        } else if (Auth::user()->roles()->pluck('name')->first() == "Sales Admin"){
            $sales = SalesAdmin::where('id_user', Auth::user()->id)->first();
            $sales_dept = Region::where('id', $sales->department)->first();
            $data['salesorder'] = Salesorder::where('created_at', '>=', $last_year_date)->where('sales_group', strtoupper($sales_dept['name']))->orderBy('id', 'desc')->get();
        } else if(Auth::user()->roles()->pluck('name')->first() == "Verificate"){
            $data['salesorder'] = Salesorder::where('status', 'Verification')->orWhere('status_helper', 'Re-verification')->orderBy('id', 'desc')->get();
        } else {
            $data['salesorder'] = Salesorder::where('created_at', '>=', $last_year_date)->orderBy('id', 'desc')->get();
        }

        for($i=0; $i<count($data['salesorder']); $i++){
            $transaction = SalesOrderTransaction::where('id_salesorder', $data['salesorder'][$i]['id'])->get();
            if(count($transaction) == 0){
                $data['salesorder'][$i]['total_price'] = 0;
            } else {
                $total_price = 0;
                if($data['salesorder'][$i]['type_req'] == "Bundling"){
                    for($j=0; $j<count($transaction); $j++){
                        $price = filter_var($transaction[$j]->price_total, FILTER_SANITIZE_NUMBER_INT);
                        $total_price += (int)$price;
                    }
                    $data['salesorder'][$i]['total_price'] = number_format($total_price);
                } else {
                    for($j=0; $j<count($transaction); $j++){
                        $price = filter_var($transaction[$j]->device_mrc, FILTER_SANITIZE_NUMBER_INT);
                        $total_price += (int)$price;
                    }
                    $data['salesorder'][$i]['total_price'] = number_format($total_price);
                }
            }

            $attachment = SalesOrderAttachment::where('salesorder_code', $data['salesorder'][$i]['salesorder_code'])->get();
            if (count($attachment) == 0){
                Salesorder::where('salesorder_code', $data['salesorder'][$i]['salesorder_code'])->update([
                    'status' => 'Draft'
                ]);
            }

            if($data['salesorder'][$i]['pic_quote'] != null){
                $user = \Auth::user()->where('id', $data['salesorder'][$i]['pic_quote'])->first();
                $data['salesorder'][$i]['pic_quote'] = $user["fullname"];
            }

            $data['salesorder'][$i]['date_order'] = implode('-', array_reverse(explode('-', $data['salesorder'][$i]['date_order'])));
            $data['salesorder'][$i]['quantity'] = number_format($data['salesorder'][$i]['quantity']);

            $data['salesorder'][$i]['remarks'] = SalesOrderRemarks::where('id_salesorder', $data['salesorder'][$i]['id'])->get();
            foreach($data['salesorder'][$i]['remarks'] as $remarks){
                $user = \Auth::user()->where('id', $remarks['id_user'])->first();
                $remarks['user'] = $user["fullname"];

                $remarks['created'] = date("d-m-Y h:i", strtotime($remarks['date_create']));
            }
            if(count($data['salesorder'][$i]['remarks']) != 0){
                $last_index = count($data['salesorder'][$i]['remarks'])-1;
                $last_remarks = $data['salesorder'][$i]['remarks'][$last_index];
                $data['salesorder'][$i]['last_remarks'] = '('.$last_remarks["created"].') '.$last_remarks["user"].': '.$last_remarks["remarks"];
            } else {
                $data['salesorder'][$i]['last_remarks'] = '';
            }
            
            $sales = SalesAgent::where('id', $data['salesorder'][$i]['id_sales'])->first();
            $data['salesorder'][$i]['sales_name'] = $sales->nama;
        }

    	return view('salesorder.salesorder_index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        validate_permission('create sales order');

        $data['current_date'] = date('Y-m-d');

        if(Auth::user()->roles()->pluck('name')->first() == "Sales Admin"){
            $sales_admin = SalesAdmin::where('id_user', Auth::user()->id)->first();
            $data['sales_agent'] = SalesAgent::where('department', $sales_admin->department)->orderBy('nama', 'asc')->pluck('nama', 'id');
        } else if(Auth::user()->roles()->pluck('name')->first() == "Administrator"){
            $data['sales_agent'] = SalesAgent::orderBy('nama', 'asc')->pluck('nama', 'id');
        }
        
        return view('salesorder.salesorder_create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SalesOrderPost $request)
    {
        $data_salesorder['corp_name'] = strtoupper($request->corp_name);
        $data_salesorder['pic'] = strtoupper($request->pic);
        $data_salesorder['pic_email'] = $request->pic_email;
        $data_salesorder['salesorder_code'] = $this->get_salesorder_code();

        if(substr( $request->pic_phone, 0, 2 ) === "08")
        {
            $data_salesorder['pic_phone'] = preg_replace('|[^0-9]|i', '', preg_replace('/^0?/', '62', $request->pic_phone));
        }
        else
        {
            $data_salesorder['pic_phone'] = preg_replace('|[^0-9]|i', '', $request->pic_phone);
        }

        $data_salesorder['date_order'] = $request->date_order;
        $data_salesorder['quote_no'] = $request->quote_no;
        $data_salesorder['ba_no'] = $request->ba_no;
        $data_salesorder['status'] = "Draft";
        $data_salesorder['type_req'] = $request->type_req;
        $data_salesorder['quantity'] = (int) $request->quantity;

        $sales_agent = SalesAgent::where('id', $request->sales_name)->get();
        $sales_dept = Region::where('id', $sales_agent[0]->department)->first();

        $data_salesorder['id_sales'] = strtoupper($sales_agent[0]->id);
        $data_salesorder['sales_group'] = strtoupper($sales_dept['name']);

        $salesorder = Salesorder::create($data_salesorder);

        return redirect('/web/salesorder/'.$salesorder->id)
            ->with('status','success')
            ->with('message','Quatation created successfuly!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SalesOrder  $salesOrder
     * @return \Illuminate\Http\Response
     */
    public function show($salesorder_id)
    {
        $data['salesorder'] = SalesOrder::where('id', $salesorder_id)->first();

        $data['checkAttachment'] = SalesOrderAttachment::where('salesorder_code', $data['salesorder']['salesorder_code'])->first();

        if(is_null($data['salesorder']))
        {
            return redirect('/web/salesorder')
                ->with('status','danger')
                ->with('message','Sales Order ID not found.');
        }

        $data['add_on'] = PackageAddOn::orderBy('addon', 'asc')->pluck('addon','id');
        $data['basic'] = PackageInfo::orderBy('package', 'asc')->pluck('package','id');
        $data['device'] = DeviceType::orderBy('product', 'asc')->pluck('product','id');
        $data['list_m2m'] = M2M::orderBy('name', 'asc')->pluck('name','id');
        $data['list_non_m2m'] = NonM2M::orderBy('name', 'asc')->pluck('name','id');
        $data['list_sim_pack'] = SimPackage::orderBy('name', 'asc')->pluck('name','id');
        $data['list_sim_spec'] = SimSpecification::orderBy('name', 'asc')->pluck('name','id');
        $data['transaction'] = SalesOrderTransaction::where('id_salesorder', (int)$salesorder_id)->orderBy('id', 'desc')->get();
        $data['total_row'] = count($data['transaction']);
        $data['salesorder']->sales = SalesAgent::where('id', $data['salesorder']->id_sales)->first();

        if($data['transaction']){
            for($i=0; $i<count($data['transaction']); $i++){
                $selected_add_on = '';
                $add_on = SalesOrderAddOn::where('id_transaction', $data['transaction'][$i]->id)->get()->toArray();
                if(count($add_on) != 0){
                    if(count($add_on) == 2){
                        for($j=0; $j<count($add_on); $j++){
                            $data_add_on = PackageAddOn::where('id', $add_on[$j]["id_addon"])->first();
                            if($j == 0){
                                $selected_add_on .= ''. $data_add_on->addon;
                            } else if ($j == (count($add_on)-1)){
                                $selected_add_on .= ' dan '. $data_add_on->addon;
                            }
                        }
                        $data['transaction'][$i]['addon'] = $selected_add_on;
                    } else {
                        for($j=0; $j<count($add_on); $j++){
                            $data_add_on = PackageAddOn::where('id', $add_on[$j]["id_addon"])->first();
                            if($j == 0){
                                $selected_add_on .= ''. $data_add_on->addon;
                            } else if ($j == (count($add_on)-1)){
                                $selected_add_on .= ', dan '. $data_add_on->addon;
                            } else {
                                $selected_add_on .= ', '. $data_add_on->addon;
                            }
                        }
                        $data['transaction'][$i]['addon'] = $selected_add_on;
                    }
                } else {
                    $data['transaction'][$i]['addon'] = '-';
                }
            }
        }

        return view('salesorder.salesorder_view', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SalesOrder  $salesOrder
     * @return \Illuminate\Http\Response
     */
    public function edit($salesorder_id)
    {
        $data_salesorder = SalesOrder::where('id', $salesorder_id)->first();

        if(is_null($data_salesorder))
        {
            return redirect('/web/salesorder')
                ->with('status','danger')
                ->with('message','Sales Order ID not found.');
        }

        if(Auth::user()->roles()->pluck('name')->first() == "Sales Admin"){
            $sales_admin = SalesAdmin::where('id_user', Auth::user()->id)->first();
            $data['sales_agent'] = SalesAgent::where('department', $sales_admin->department)->orderBy('nama', 'asc')->pluck('nama', 'id');
        } else if(Auth::user()->roles()->pluck('name')->first() == "Administrator"){
            $data['sales_agent'] = SalesAgent::orderBy('nama', 'asc')->pluck('nama', 'id');
        }

        $data['selected_sales_agent'] = SalesAgent::where('id', $data_salesorder->id_sales)->first();

        return view('salesorder.salesorder_edit', compact('data_salesorder'), $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SalesOrder  $salesOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $salesorder_id)
    {
        $total_transaction = SalesorderTransaction::where('id_salesorder', $salesorder_id)->get()->count();

        if($request->quantity < $total_transaction){
            return redirect('/web/salesorder/'.$salesorder_id)
                ->with('status','danger')
                ->with('message','Quantity of Sales Order is below than total transaction');
        }

        $data_salesorder['corp_name'] = strtoupper($request->corp_name);
        $data_salesorder['pic'] = strtoupper($request->pic);
        $data_salesorder['pic_email'] = $request->pic_email;

        if(substr( $request->pic_phone, 0, 2 ) === "08")
        {
            $data_salesorder['pic_phone'] = preg_replace('|[^0-9]|i', '', preg_replace('/^0?/', '62', $request->pic_phone));
        }
        else
        {
            $data_salesorder['pic_phone'] = preg_replace('|[^0-9]|i', '', $request->pic_phone);
        }

        $data_salesorder['quote_no'] = $request->quote_no;
        $data_salesorder['ba_no'] = $request->ba_no;
        $data_salesorder['type_req'] = $request->type_req;
        $data_salesorder['quantity'] = $request->quantity;

        $salesagent = SalesAgent::where('id', $request->id_sales)->first();
        $sales_dept = Region::where('id', $salesagent->department)->first();
        $data_salesorder['id_sales'] = $salesagent->id;
        $data_salesorder['sales_group'] = strtoupper($sales_dept->name);

        Salesorder::where('id', $salesorder_id)->update($data_salesorder);

        return redirect('/web/salesorder/'. $salesorder_id)
            ->with('status','success')
            ->with('message','Sales Order has been updated successfuly.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SalesOrder  $salesOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy($salesorder_id)
    {
        $salesorder = Salesorder::findOrFail($salesorder_id);
        $salesorder->delete();
        return redirect('/web/salesorder')
            ->with('status','success')
            ->with('message','Sales Order has been deleted successfuly.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_attachment($salesorder_code)
    {
        $data['attachment'] = SalesOrderAttachment::where('salesorder_code', $salesorder_code)->get();
        $data['checkAttachment'] = SalesOrderAttachment::where('salesorder_code', $salesorder_code)->first();

        $data['salesorder'] = SalesOrder::where('salesorder_code', $salesorder_code)->first();
        $data['num_transaction'] = SalesOrderTransaction::where('id_salesorder', $data['salesorder']->id)->count();
        $data['id_salesorder'] = $data['salesorder']->id;
        $data['salesorder_code'] = $salesorder_code;

        return view('salesorder.salesorder_attachment', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_attachment($salesorder_code, Request $request)
    {
        $imageName = request()->file->getClientOriginalName();
        $pathAlbum ='/public/upload/'.$salesorder_code;
        $path = request()->file->storeAs($pathAlbum, $imageName);
        $pathFoto = \Storage::url($path);
        
        $data_attachment['salesorder_code'] = $salesorder_code;
        $data_attachment['attachment'] = $pathFoto;
        SalesOrderAttachment::create($data_attachment);

        return response()->json(['uploaded' => '/upload/'.$imageName]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SalesOrderAttachment  $attachment_id
     * @return \Illuminate\Http\Response
     */
    public function destroy_attachment($attachment_id)
    {

        $attachment = SalesOrderAttachment::findOrFail($attachment_id);
        $salesorder_code = $attachment->salesorder_code;
        $attachment->delete();
        return redirect('/web/salesorder/'.$salesorder_code .'/attachment')
            ->with('status','success')
            ->with('message','Attachment has been deleted successfuly.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\SalesOrder  $salesorder_code
     * @return \Illuminate\Http\Response
     */
    public function verification($salesorder_code)
    {
        $attachment = SalesOrderAttachment::where('salesorder_code', $salesorder_code)->get();
        if(count($attachment) != 0){
            Salesorder::where('salesorder_code', $salesorder_code)->update([
                'status' => 'Verification'
            ]);
        } else {
            Salesorder::where('salesorder_code', $salesorder_code)->update([
                'status' => 'Draft'
            ]);
        }

        return redirect('/web/salesorder');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\SalesOrder  $salesorder_id
     * @return \Illuminate\Http\Response
     */
    public function reverification($salesorder_id)
    {
        $quote = Salesorder::where('id', $salesorder_id)->first();

        $attachment = SalesOrderAttachment::where('salesorder_code', $quote->salesorder_code)->get();

        if(count($attachment) != 0){
            Salesorder::where('id', $salesorder_id)->update([
                'status_helper' => 'Re-verification'
            ]);

            $data['id_salesorder'] = $salesorder_id;
            $data['date_create'] = date("Y-m-d h:i:s");
            $data['remarks'] = 'Data yang out of stock sudah diperbaiki';
            $data['id_user'] = \Auth::user()->id;

            SalesOrderRemarks::create($data);
        }

        return redirect('/web/salesorder')
            ->with('status','success')
            ->with('message','Quotation '.$quote->quote_no .' sedang di verifikasi ulang!');
    }

    /**
     * Pick the specified resource in storage.
     *
     * @param  \App\SalesOrder  $salesorder_id
     * @return \Illuminate\Http\Response
     */
    public function reject($salesorder_id)
    {
        $quote = Salesorder::where('id', $salesorder_id)->first();
        
        if($quote->status_helper != null){
            Salesorder::where('id', $salesorder_id)->update([
                'status_helper' => 'Rejected'
            ]);
        } else {
            Salesorder::where('id', $salesorder_id)->update([
                'status' => 'Rejected'
            ]);
        }

        return redirect('/web/salesorder')
            ->with('status','success')
            ->with('message','Quotation '.$quote->quote_no .' telah di-reject!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\SalesOrder  $salesorder_id
     * @return \Illuminate\Http\Response
     */
    public function revision($salesorder_id)
    {
        $data['id_salesorder'] = $salesorder_id;
        $data['date_create'] = date("Y-m-d h:i:s");
        $data['remarks'] = 'Revision Done';
        $data['id_user'] = \Auth::user()->id;

        SalesOrderRemarks::create($data);

        Salesorder::where('id', $salesorder_id)->update([
            'status' => 'Verification'
        ]);

        return redirect('/web/salesorder');
    }

    public function check_device($salesorder_id)
    {
        $data['salesorder'] = Salesorder::where('id', $salesorder_id)->first();
        $data['salesorder']['date_order'] = implode('-', array_reverse(explode('-', $data['salesorder']['date_order'])));
        $data['salesorder']['quantity'] = number_format($data['salesorder']['quantity']);
        $data['salesorder']['sales'] = SalesAgent::where('id', $data['salesorder']['id_sales'])->first();
        $data['transaction'] = SalesOrderTransaction::where('id_salesorder', (int)$salesorder_id)->where('status', 'In Progress RM')->orderBy('id', 'desc')->get();
        $data['total_row'] = count($data['transaction']);

        if($data['transaction']){
            for($i=0; $i<count($data['transaction']); $i++){
                $selected_add_on = '';
                $add_on = SalesOrderAddOn::where('id_transaction', $data['transaction'][$i]->id)->get()->toArray();
                if(count($add_on) != 0){
                    if(count($add_on) == 2){
                        for($j=0; $j<count($add_on); $j++){
                            $data_add_on = PackageAddOn::where('id', $add_on[$j]["id_addon"])->first();
                            if($j == 0){
                                $selected_add_on .= ''. $data_add_on->addon;
                            } else if ($j == (count($add_on)-1)){
                                $selected_add_on .= ' dan '. $data_add_on->addon;
                            }
                        }
                        $data['transaction'][$i]['addon'] = $selected_add_on;
                    } else {
                        for($j=0; $j<count($add_on); $j++){
                            $data_add_on = PackageAddOn::where('id', $add_on[$j]["id_addon"])->first();
                            if($j == 0){
                                $selected_add_on .= ''. $data_add_on->addon;
                            } else if ($j == (count($add_on)-1)){
                                $selected_add_on .= ', dan '. $data_add_on->addon;
                            } else {
                                $selected_add_on .= ', '. $data_add_on->addon;
                            }
                        }
                        $data['transaction'][$i]['addon'] = $selected_add_on;
                    }
                } else {
                    $data['transaction'][$i]['addon'] = '-';
                }
            }
        }

        $data['so'] = $data['salesorder'];
        
        return view('salesorder.manage_salesorder_device', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\SalesOrder  $salesorder_id
     * @return \Illuminate\Http\Response
     */
    public function update_device($salesorder_id, Request $request)
    {
        $transaction = SalesOrderTransaction::where('id_salesorder', $salesorder_id)->where('status', 'In Progress RM')->get()->toArray();
        $available_device = $request->all();
        $counter_stock = 0;

        foreach($transaction as $tr){
            if(array_key_exists($tr['id'], $available_device)){
                SalesOrderTransaction::where('id', $tr['id'])->update([
                    'status' => 'Available'
                ]);
            } else {
                SalesOrderTransaction::where('id', $tr['id'])->update([
                    'status' => 'Out of Stock'
                ]);
                $counter_stock++;
            }
        }

        $quote = Salesorder::where('id', $salesorder_id)->first();

        if($counter_stock != 0){
            Salesorder::where('id', $salesorder_id)->update([
                'status_helper' => $counter_stock .' Out of Stock'
            ]);
        } else {
            Salesorder::where('id', $salesorder_id)->update([
                'status_helper' => null
            ]);
        }

        // $data['id_salesorder'] = $salesorder_id;
        // $data['date_create'] = date("Y-m-d h:i:s");
        // $data['remarks'] = 'Data device yang out of stock telah berhasil diperbaiki';
        // $data['id_user'] = \Auth::user()->id;

        // SalesOrderRemarks::create($data);

        return redirect('/salesorder')
            ->with('status','success')
            ->with('message','Status device pada quote '.$quote->quote_no .' telah berhasil diperbaharui!');
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \App\SalesOrder  $salesorder_id
     * @return \Illuminate\Http\Response
     */
    public function submit($salesorder_id)
    {
        $quote = Salesorder::where('id', $salesorder_id)->first();
        
        if($quote->status_helper == 'Re-verification'){
            Salesorder::where('id', $salesorder_id)->update([
                'status_helper' => 'In Progress RM'
            ]);

            $data['id_salesorder'] = $salesorder_id;
            $data['date_create'] = date("Y-m-d h:i:s");
            $data['remarks'] = 'Device yang out of stock telah berhasil diperbaiki dan diverifikasi';
            $data['id_user'] = \Auth::user()->id;

            SalesOrderRemarks::create($data);

            SalesOrderTransaction::where('id_salesorder', $salesorder_id)->where('status', 'Out of Stock')->update([
                'status' => 'In Progress RM'
            ]);
            
        } else {
            Salesorder::where('id', $salesorder_id)->update([
                'status' => 'Submitted'
            ]);
        }

        return redirect('/web/salesorder')
            ->with('status','success')
            ->with('message','Quotation '.$quote->quote_no .' telah berhasil diverifikasi!');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_all()
    {
        $data['salesorder'] = Salesorder::orderBy('id', 'desc')->get();

        for($i=0; $i<count($data['salesorder']); $i++){
            $transaction = SalesOrderTransaction::where('id_salesorder', $data['salesorder'][$i]['id'])->get();
            if(count($transaction) == 0){
                $data['salesorder'][$i]['total_price'] = 0;
            } else {
                $total_price = 0;
                for($j=0; $j<count($transaction); $j++){
                    $price = filter_var($transaction[$j]->price_total, FILTER_SANITIZE_NUMBER_INT);
                    $total_price += (int)$price;
                }
                $data['salesorder'][$i]['total_price'] = number_format($total_price);
            }

            $attachment = SalesOrderAttachment::where('salesorder_code', $data['salesorder'][$i]['salesorder_code'])->get();
            if (count($attachment) == 0){
                Salesorder::where('salesorder_code', $data['salesorder'][$i]['salesorder_code'])->update([
                    'status' => 'Draft'
                ]);
            }
        }

    	return view('salesorder.salesorder_index_all', $data);
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function export()
    {
        $current_date = Date("Y-m-d");
        return Excel::download(new SalesOrderExport("2019-01-01", $current_date), 'QUOTATION ('.$current_date .').xlsx');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function export_param(ExportParam $request)
    {
        if($request->check == "on"){
            $request->from = "2019-01-01";
            $request->to = Date("Y-m-d");
        }
        return Excel::download(new SalesOrderExport($request->from, $request->to), 'QUOTATION ('.$request->from .'-'.$request->to .').xlsx');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_manage()
    {
        $day = date("d");
        $month = date("m");
        $year = (string)((int)date("Y")-1);
        $last_year_date = date($year.'-'.$month.'-'.$day);
        $data['current_date'] = date('Y-m-d');

        if(Auth::user()->roles()->pluck('name')->first() == "Tim SD"){
            $data['salesorder'] = Salesorder::where('created_at', '>=', $last_year_date)
                                    ->where('status', '!=', 'Draft')->where('status', '!=', 'Verification')
                                    ->where('status', '!=', 'Rejected')
                                    ->where(function($q) {
                                        $q->where('pic_quote', (string)Auth::user()->id)
                                          ->orWhere('pic_quote', null);
                                    })
                                    ->orderBy('id', 'desc')
                                    ->get();
        } elseif(Auth::user()->hasPermissionTo('check availability device')){
            $data['salesorder'] = Salesorder::where('created_at', '>=', $last_year_date)->where('status', 'In Progress RM')->orWhere('status_helper', 'In Progress RM')->orderBy('id', 'desc')->get();
        } else {
            $data['salesorder'] = Salesorder::where('created_at', '>=', $last_year_date)->where('status', '!=', 'Draft')->where('status', '!=', 'Verification')->where('status', '!=', 'Rejected')->orderBy('id', 'desc')->get();            
        }
        
        for($i=0; $i<count($data['salesorder']); $i++){
            $transaction = SalesOrderTransaction::where('id_salesorder', $data['salesorder'][$i]['id'])->get();
            if(count($transaction) == 0){
                $data['salesorder'][$i]['total_price'] = 0;
            } else {
                $total_price = 0;
                for($j=0; $j<count($transaction); $j++){
                    $price = filter_var($transaction[$j]->price_total, FILTER_SANITIZE_NUMBER_INT);
                    $total_price += (int)$price;
                }
                $data['salesorder'][$i]['total_price'] = number_format($total_price);
            }

            if($data['salesorder'][$i]['pic_quote'] != null){
                $user = \Auth::user()->where('id', $data['salesorder'][$i]['pic_quote'])->first();
                $data['salesorder'][$i]['pic_quote'] = $user["fullname"];
            }
            $data['salesorder'][$i]['date_order'] = implode('-', array_reverse(explode('-', $data['salesorder'][$i]['date_order'])));
            $data['salesorder'][$i]['quantity'] = number_format($data['salesorder'][$i]['quantity']);

            $data['salesorder'][$i]['remarks'] = SalesOrderRemarks::where('id_salesorder', $data['salesorder'][$i]['id'])->get();
            foreach($data['salesorder'][$i]['remarks'] as $remarks){
                $user = \Auth::user()->where('id', $remarks['id_user'])->first();
                $remarks['user'] = $user["fullname"];

                $remarks['created'] = date("d-m-Y h:i", strtotime($remarks['date_create']));
            }
            if(count($data['salesorder'][$i]['remarks']) != 0){
                $last_index = count($data['salesorder'][$i]['remarks'])-1;
                $data['salesorder'][$i]['last_remarks'] = $data['salesorder'][$i]['remarks'][$last_index];
            }

            $data['salesorder'][$i]['sales'] = SalesAgent::where('id', $data['salesorder'][$i]['id_sales'])->first();
        }

    	return view('salesorder.manage_salesorder_index', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SalesOrder  $salesOrder
     * @return \Illuminate\Http\Response
     */
    public function show_manage($salesorder_id)
    {
        $data['salesorder'] = SalesOrder::where('id', $salesorder_id)->first();

        $data['checkAttachment'] = SalesOrderAttachment::where('salesorder_code', $data['salesorder']['salesorder_code'])->first();

        $data['add_on'] = PackageAddOn::orderBy('addon', 'asc')->pluck('addon','id');
        $data['basic'] = PackageInfo::orderBy('package', 'asc')->pluck('package','id');
        $data['device'] = DeviceType::orderBy('product', 'asc')->pluck('product','id');
        // $data['sales_agent'] = SalesAgent::pluck('ae_name', 'id');
        $data['salesorder']['date_order'] = implode('-', array_reverse(explode('-', $data['salesorder']['date_order'])));
        $data['transaction'] = SalesOrderTransaction::where('id_salesorder', (int)$salesorder_id)->orderBy('id', 'desc')->get();

        if($data['transaction']){
            for($i=0; $i<count($data['transaction']); $i++){
                $selected_add_on = '';
                $add_on = SalesOrderAddOn::where('id_transaction', $data['transaction'][$i]->id)->get()->toArray();
                if(count($add_on) != 0){
                    if(count($add_on) == 2){
                        for($j=0; $j<count($add_on); $j++){
                            $data_add_on = PackageAddOn::where('id', $add_on[$j]["id_addon"])->first();
                            if($j == 0){
                                $selected_add_on .= ''. $data_add_on->addon;
                            } else if ($j == (count($add_on)-1)){
                                $selected_add_on .= ' dan '. $data_add_on->addon;
                            }
                        }
                        $data['transaction'][$i]['addon'] = $selected_add_on;
                    } else {
                        for($j=0; $j<count($add_on); $j++){
                            $data_add_on = PackageAddOn::where('id', $add_on[$j]["id_addon"])->first();
                            if($j == 0){
                                $selected_add_on .= ''. $data_add_on->addon;
                            } else if ($j == (count($add_on)-1)){
                                $selected_add_on .= ', dan '. $data_add_on->addon;
                            } else {
                                $selected_add_on .= ', '. $data_add_on->addon;
                            }
                        }
                        $data['transaction'][$i]['addon'] = $selected_add_on;
                    }
                } else {
                    $data['transaction'][$i]['addon'] = '-';
                }
            }
        }

        $data['salesorder_delivery_request'] = SalesOrderDeliveryRequest::where('id_salesorder', $data['salesorder']->id)->orderBy('id', 'desc')->get();

        if($data['salesorder_delivery_request'] != null){
            for($i=0; $i<count($data['salesorder_delivery_request']); $i++){
                $data['salesorder_delivery_request'][$i]['request_date'] = implode('-', array_reverse(explode('-', $data['salesorder_delivery_request'][$i]['request_date'])));   

                $data['salesorder_delivery_request'][$i]['quantity'] = 0;
                $data['delivery_request'] = DeliveryRequest::where('id_so_dr', $data['salesorder_delivery_request'][$i]->id)->get();
                foreach($data['delivery_request'] as $dr){
                    $data['salesorder_delivery_request'][$i]['quantity'] += $dr['quantity'];
                }
                $data['salesorder_delivery_request'][$i]['quantity'] = number_format($data['salesorder_delivery_request'][$i]['quantity']);
            }
        }

        $data['employee'] = Employee::orderBy('name', 'asc')->whereNotNull('signature')->pluck('name', 'id');

        $data['shipment'] = SalesOrderShipment::where('id_salesorder', $salesorder_id)->orderBy('id', 'desc')->get();
        
        if($data['shipment'] != null){
            foreach($data['shipment'] as $so_shipment){
                $so_shipment['request_date'] = implode('-', array_reverse(explode('-', $so_shipment['request_date'])));
                $so_shipment['quantity'] = Shipment::where('id_so_shipment', $so_shipment->id)->count();
            }
        }
        return view('salesorder.manage_salesorder_view', $data);
    }
    
    /**
     * Pick the specified resource in storage.
     *
     * @param  \App\SalesOrder  $salesorder_id
     * @return \Illuminate\Http\Response
     */
    public function pick_salesorder($salesorder_id)
    {
        $quote = Salesorder::where('id', $salesorder_id)->first();
        $user = \Auth::user();

        Salesorder::where('id', $salesorder_id)->update([
            'pic_quote' => $user->id,
            'status' => 'In Progress RM'
        ]);

        return redirect('/salesorder')
            ->with('status','success')
            ->with('message','Quotation '.$quote->quote_no .' berhasil di-pick!');
    }

    /**
     * Activation the specified resource in storage.
     *
     * @param  \App\SalesOrder  $salesorder_id
     * @return \Illuminate\Http\Response
     */
    public function activation_salesorder($salesorder_id)
    {
        $data['salesorder'] = Salesorder::where('id', $salesorder_id)->first();
        $data['salesorder']['date_order'] = implode('-', array_reverse(explode('-', $data['salesorder']['date_order'])));
        $data['salesorder']['quantity'] = number_format($data['salesorder']['quantity']);
        $data['salesorder']['sales'] = SalesAgent::where('id', $data['salesorder']['id_sales'])->first();
        $data['transaction'] = SalesOrderTransaction::where('id_salesorder', (int)$salesorder_id)->orderBy('id', 'desc')->get();
        $data['total_row'] = count($data['transaction']);

        if($data['transaction']){
            for($i=0; $i<count($data['transaction']); $i++){
                $selected_add_on = '';
                $add_on = SalesOrderAddOn::where('id_transaction', $data['transaction'][$i]->id)->get()->toArray();
                if(count($add_on) != 0){
                    if(count($add_on) == 2){
                        for($j=0; $j<count($add_on); $j++){
                            $data_add_on = PackageAddOn::where('id', $add_on[$j]["id_addon"])->first();
                            if($j == 0){
                                $selected_add_on .= ''. $data_add_on->addon;
                            } else if ($j == (count($add_on)-1)){
                                $selected_add_on .= ' dan '. $data_add_on->addon;
                            }
                        }
                        $data['transaction'][$i]['addon'] = $selected_add_on;
                    } else {
                        for($j=0; $j<count($add_on); $j++){
                            $data_add_on = PackageAddOn::where('id', $add_on[$j]["id_addon"])->first();
                            if($j == 0){
                                $selected_add_on .= ''. $data_add_on->addon;
                            } else if ($j == (count($add_on)-1)){
                                $selected_add_on .= ', dan '. $data_add_on->addon;
                            } else {
                                $selected_add_on .= ', '. $data_add_on->addon;
                            }
                        }
                        $data['transaction'][$i]['addon'] = $selected_add_on;
                    }
                } else {
                    $data['transaction'][$i]['addon'] = '-';
                }
            }
        }

        $data['so'] = $data['salesorder'];
        
        if($data['salesorder']->status == "In Progress RM"){

            return view('salesorder.manage_salesorder_activation', $data);
        
        } else if ($data['salesorder']->status == "Activation"){
            $data['salesorder_delivery_request'] = SalesOrderDeliveryRequest::where('id_salesorder', $data['salesorder']->id)->orderBy('id', 'desc')->get();

            if($data['salesorder_delivery_request'] != null){
                for($i=0; $i<count($data['salesorder_delivery_request']); $i++){
                    $data['salesorder_delivery_request'][$i]['request_date'] = implode('-', array_reverse(explode('-', $data['salesorder_delivery_request'][$i]['request_date'])));   
    
                    $data['salesorder_delivery_request'][$i]['quantity'] = 0;
                    $data['delivery_request'] = DeliveryRequest::where('id_so_dr', $data['salesorder_delivery_request'][$i]->id)->get();
                    foreach($data['delivery_request'] as $dr){
                        $data['salesorder_delivery_request'][$i]['quantity'] += $dr['quantity'];
                    }
                    $data['salesorder_delivery_request'][$i]['quantity'] = number_format($data['salesorder_delivery_request'][$i]['quantity']);
                }
            }

            $data['employee'] = Employee::orderBy('name', 'asc')->whereNotNull('signature')->pluck('name', 'id');

            $data['shipment'] = SalesOrderShipment::where('id_salesorder', $salesorder_id)->orderBy('id', 'desc')->get();
            
            if($data['shipment'] != null){
                foreach($data['shipment'] as $so_shipment){
                    $so_shipment['request_date'] = implode('-', array_reverse(explode('-', $so_shipment['request_date'])));
                    $so_shipment['quantity'] = Shipment::where('id_so_shipment', $so_shipment->id)->count();
                }
            }

            return view('salesorder.manage_salesorder_done', $data);
        }
    }

    /**
     * Activation the specified resource in storage.
     *
     * @param  \App\SalesOrder  $salesorder_id
     * @return \Illuminate\Http\Response
     */
    public function activated_salesorder(Request $request, $salesorder_id)
    {
        $transaction = SalesOrderTransaction::where('id_salesorder', $salesorder_id)->get()->toArray();
        $available_device = $request->all();
        $counter_stock = 0;

        foreach($transaction as $tr){
            if(array_key_exists($tr['id'], $available_device)){
                $tes = SalesOrderTransaction::where('id', $tr['id'])->update([
                    'status' => 'Available'
                ]);
            } else {
                SalesOrderTransaction::where('id', $tr['id'])->update([
                    'status' => 'Out of Stock'
                ]);
                $counter_stock++;
            }
        }

        $quote = Salesorder::where('id', $salesorder_id)->first();

        if($counter_stock != 0){
            Salesorder::where('id', $salesorder_id)->update([
                'status' => 'Activation',
                'status_helper' => $counter_stock .' Out of Stock'
            ]);
        } else {
            Salesorder::where('id', $salesorder_id)->update([
                'status' => 'Activation'
            ]);
        }

        return redirect('/salesorder')
            ->with('status','success')
            ->with('message','Quotation '.$quote->quote_no .' berhasil diaktivasi!');
    }

    /**
     * Get the specified resource in storage.
     *
     * @param  \App\SalesOrder  $salesorder_id
     * @return \Illuminate\Http\Response
     */
    public function done_salesorder($salesorder_id)
    {
        $quote = Salesorder::where('id', $salesorder_id)->first();

        if($quote->status_helper != null){
            return redirect('/salesorder')
                ->with('status','danger')
                ->with('message','Quotation '.$quote->quote_no .' masih terdapat masalah!');            
        }
        
        Salesorder::where('id', $salesorder_id)->update([
            'status' => 'Done'
        ]);

        return redirect('/salesorder')
            ->with('status','success')
            ->with('message','Quotation '.$quote->quote_no .' telah selesai!');
    }

    /**
     * Get random salesorder unique code, keep generate if code exist.
     *
     * @return String unique code
     */
    public function get_salesorder_code()
    {
        $salesorder_code = str_random(8);

        $code_exist = SalesOrder::where('salesorder_code', $salesorder_code)->count();

        if($code_exist > 0)
        {
            $this->get_salesorder_code();
        }
        else
        {
            return $salesorder_code;
        }
    }

    public function json(){
        $day = date("d");
        $month = date("m");
        $year = (string)((int)date("Y")-1);
        $last_year_date = date($year.'-'.$month.'-'.$day);

        if(Auth::user()->roles()->pluck('name')->first() == "Sales"){
            $sales = SalesAgent::where('id_user', Auth::user()->id)->first();
            $data['salesorder'] = Salesorder::where('created_at', '>=', $last_year_date)->where('id_sales', $sales->id)->orderBy('id', 'desc')->get();
        } else if (Auth::user()->roles()->pluck('name')->first() == "Sales Admin"){
            $sales = SalesAdmin::where('id_user', Auth::user()->id)->first();
            $sales_dept = Region::where('id', $sales->department)->first();
            $data['salesorder'] = Salesorder::where('created_at', '>=', $last_year_date)->where('sales_group', strtoupper($sales_dept['name']))->orderBy('id', 'desc')->get();
        } else {
            $data['salesorder'] = Salesorder::where('created_at', '>=', $last_year_date)->orderBy('id', 'desc')->get();
        }

        for($i=0; $i<count($data['salesorder']); $i++){
            $transaction = SalesOrderTransaction::where('id_salesorder', $data['salesorder'][$i]['id'])->get();
            if(count($transaction) == 0){
                $data['salesorder'][$i]['total_price'] = 0;
            } else {
                $total_price = 0;
                if($data['salesorder'][$i]['type_req'] == "Bundling"){
                    for($j=0; $j<count($transaction); $j++){
                        $price = filter_var($transaction[$j]->price_total, FILTER_SANITIZE_NUMBER_INT);
                        $total_price += (int)$price;
                    }
                    $data['salesorder'][$i]['total_price'] = number_format($total_price);
                } else {
                    for($j=0; $j<count($transaction); $j++){
                        $price = filter_var($transaction[$j]->device_mrc, FILTER_SANITIZE_NUMBER_INT);
                        $total_price += (int)$price;
                    }
                    $data['salesorder'][$i]['total_price'] = number_format($total_price);
                }
            }

            $attachment = SalesOrderAttachment::where('salesorder_code', $data['salesorder'][$i]['salesorder_code'])->get();
            if (count($attachment) == 0){
                Salesorder::where('salesorder_code', $data['salesorder'][$i]['salesorder_code'])->update([
                    'status' => 'Draft'
                ]);
            }

            if($data['salesorder'][$i]['pic_quote'] != null){
                $user = \Auth::user()->where('id', $data['salesorder'][$i]['pic_quote'])->first();
                $data['salesorder'][$i]['pic_quote'] = $user["fullname"];
            }

            $data['salesorder'][$i]['date_order'] = implode('-', array_reverse(explode('-', $data['salesorder'][$i]['date_order'])));
            $data['salesorder'][$i]['quantity'] = number_format($data['salesorder'][$i]['quantity']);

            $data['salesorder'][$i]['remarks'] = SalesOrderRemarks::where('id_salesorder', $data['salesorder'][$i]['id'])->get();
            foreach($data['salesorder'][$i]['remarks'] as $remarks){
                $user = \Auth::user()->where('id', $remarks['id_user'])->first();
                $remarks['user'] = $user["fullname"];

                $remarks['created'] = date("d-m-Y h:i", strtotime($remarks['date_create']));
            }
            if(count($data['salesorder'][$i]['remarks']) != 0){
                $last_index = count($data['salesorder'][$i]['remarks'])-1;
                $last_remarks = $data['salesorder'][$i]['remarks'][$last_index];
                $data['salesorder'][$i]['last_remarks'] = '('.$last_remarks["created"].') '.$last_remarks["user"].': '.$last_remarks["remarks"];
            } else {
                $data['salesorder'][$i]['last_remarks'] = '';
            }
            
            $sales = SalesAgent::where('id', $data['salesorder'][$i]['id_sales'])->first();
            $data['salesorder'][$i]['sales_name'] = $sales->nama;
        }

        return Datatables::of($data['salesorder'])->make(true);
    }

}
