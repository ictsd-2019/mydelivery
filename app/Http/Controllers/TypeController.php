<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TypeScope;
use App\Http\Requests\TypePost;
use Excel;


class TypeController extends Controller
{

    public function index()
    {
        $data['type'] = TypeScope::orderBy('type_scope_work', 'asc')->get();
        return view('type_scope.manage_type_index', $data);
    }

    public function create()
    {
        return view('type_scope.manage_type_create');
    }

    public function store(TypePost $request)
    {

        $noUrutAkhir = \App\TypeScope::max('id_type');
        $no          = $noUrutAkhir+1;

        TypeScope::create([
            'id_type'         => $no,
            'type_scope_work' => $request['type'],
            'sub_type_price'  => $request['price']
        ]);

        return redirect('/type')
            ->with('status','success')
            ->with('message', 'Type Scope '. $request['type'] .' has been created!');
    }

    public function edit($type_id)
    {
        $datas['type'] = TypeScope::find($type_id);
        return view('type_scope.manage_type_edit', $datas);
    }

    public function update(TypePost $request, $type_id)
    {

        $datas                        = TypeScope::where('id', $type_id)->first();
        $data_type['id_type']         = $request->id;
        $data_type['type_scope_work'] = $request->type;
        $data_type['sub_type_price']  = $request->price;
        TypeScope::find($type_id)->update($data_type);

        return redirect('/type')
            ->with('status','success')
            ->with('message', 'Type Scope '. $request['type_scope_work'] .' has been updated!');
    }

    public function destroy($type_id)
    {
        $datas    = TypeScope::find($type_id);
        $namatype = $datas->type_scope_work;
        $datas->delete();

        return redirect('/type')
            ->with('status','success')
            ->with('message', 'Type Scope '. $namatype .' has been deleted!');
    }

}