<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainActivity extends Model
{
    protected $table = 'main_activity';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'id_vendor',
        'regional',
        'activity',
        'notes'
    ];
}
