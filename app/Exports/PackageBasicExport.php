<?php

namespace App\Exports;

use App\PackageInfo;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PackageBasicExport implements FromView, ShouldAutoSize, WithEvents
{
    use Exportable;

    public function view(): View
    {
        $data['package'] = PackageInfo::orderBy('package', 'asc')->get();

        return view('package_basic.table_package_basic', $data);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:Z1';
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);
            },
        ];
    }
}
