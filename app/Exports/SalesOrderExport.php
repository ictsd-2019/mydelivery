<?php

namespace App\Exports;

use App\SalesOrder;
use App\SalesOrderTransaction;
use App\SalesOrderAttachment;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SalesOrderExport implements FromView, ShouldAutoSize, WithEvents
{
    use Exportable;

    public function __construct(string $from, string $to)
    {
        $this->from = $from;
        $this->to = $to;
    }

    public function view(): View
    {
        $data['salesorder'] = Salesorder::where('date_order', '>=', $this->from)
        ->where('date_order', '<=', $this->to)
        ->orderBy('id', 'desc')->get();

        for($i=0; $i<count($data['salesorder']); $i++){
            $transaction = SalesOrderTransaction::where('id_salesorder', $data['salesorder'][$i]['id'])->get();
            if(count($transaction) == 0){
                $data['salesorder'][$i]['total_price'] = 0;
            } else {
                $total_price = 0;
                for($j=0; $j<count($transaction); $j++){
                    $price = filter_var($transaction[$j]->price_total, FILTER_SANITIZE_NUMBER_INT);
                    $total_price += (int)$price;
                }
                $data['salesorder'][$i]['total_price'] = number_format($total_price);
            }

            $attachment = SalesOrderAttachment::where('salesorder_code', $data['salesorder'][$i]['salesorder_code'])->get();
            if (count($attachment) == 0){
                Salesorder::where('salesorder_code', $data['salesorder'][$i]['salesorder_code'])->update([
                    'status' => 'Draft'
                ]);
            }

            if($data['salesorder'][$i]['pic_quote'] != null){
                $user = \Auth::user()->where('id', $data['salesorder'][$i]['pic_quote'])->first();
                $data['salesorder'][$i]['pic_quote'] = $user["fullname"];
            }

            $data['salesorder'][$i]['date_order'] = implode('-', array_reverse(explode('-', $data['salesorder'][$i]['date_order'])));
        }

        return view('salesorder.table_salesorder', [
            'salesorder' => $data['salesorder']
        ]);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:Z1';
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);
            },
        ];
    }
}
