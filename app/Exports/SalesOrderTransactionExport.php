<?php

namespace App\Exports;

use App\SalesOrderAddOn;
use App\PackageAddOn;
use App\SalesOrder;
use App\SalesOrderTransaction;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SalesOrderTransactionExport implements FromView, ShouldAutoSize, WithEvents
{
    use Exportable;
    
    public function __construct(int $salesorder_id)
    {
        $this->salesorder_id = $salesorder_id;
    }

    public function view(): View
    {
        $data = SalesOrderTransaction::where('id_salesorder', $this->salesorder_id)->get();
        $salesorder = SalesOrder::where('id', $this->salesorder_id)->first();

        if(count($data) != 0) {
            for($i=0; $i<count($data); $i++){
                $selected_add_on = '';
                $add_on = SalesOrderAddOn::where('id_transaction', $data[$i]->id)->get()->toArray();
                if(count($add_on) != 0){
                    if(count($add_on) == 2){
                        for($j=0; $j<count($add_on); $j++){
                            $data_add_on = PackageAddOn::where('id', $add_on[$j]["id_addon"])->first();
                            if($j == 0){
                                $selected_add_on .= ''. $data_add_on->addon;
                            } else if ($j == (count($add_on)-1)){
                                $selected_add_on .= ' dan '. $data_add_on->addon;
                            }
                        }
                        $data[$i]['addon'] = $selected_add_on;
                    } else {
                        for($j=0; $j<count($add_on); $j++){
                            $data_add_on = PackageAddOn::where('id', $add_on[$j]["id_addon"])->first();
                            if($j == 0){
                                $selected_add_on .= ''. $data_add_on->addon;
                            } else if ($j == (count($add_on)-1)){
                                $selected_add_on .= ', dan '. $data_add_on->addon;
                            } else {
                                $selected_add_on .= ', '. $data_add_on->addon;
                            }
                        }
                        $data[$i]['addon'] = $selected_add_on;
                    }
                } else {
                    $data[$i]['addon'] = '-';
                }
            }
        }

        return view('salesorder.table_transaction', [
            'transaction' => $data,
            'salesorder' => $salesorder
        ]);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:Z1';
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);
            },
        ];
    }
}
