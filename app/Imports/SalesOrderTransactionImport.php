<?php

namespace App\Imports;

use App\M2M;
use App\NonM2M;
use App\SimPackage;
use App\SimSpecification;
use App\Salesorder;
use App\PackageAddOn;
use App\DeviceType;
use App\PackageInfo;
use App\SalesOrderAddOn;
use App\SalesOrderTransaction;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class SalesOrderTransactionImport implements ToModel, WithHeadingRow
{
    public function __construct(int $salesorder_id)
    {
        $this->salesorder_id = $salesorder_id;
    }

    public $count = 0;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        ++$this->count;

        $salesorder = Salesorder::where('id', $this->salesorder_id)->first();
        
        if($this->count > $salesorder->quantity){
            return;
        }

        $data_brand = DeviceType::where('id', $row['id_device_brand'])->first();
        $data_package = PackageInfo::where('id', $row['id_basic_package'])->first();
        $data_m2m = M2M::where('name', $row['m2m'])->first();
        $data_package_type = SimPackage::where('name', $row['package_type'])->first();
        $data_non_m2m = NonM2M::where('name', $row['non_m2m'])->first();
        $data_sim_spec = SimSpecification::where('name', $row['sim_specification'])->first();

        if($row['type_bundling'] == null || $data_package == null || $data_non_m2m == null || $data_sim_spec == null){
            return;
        }

        $data_transaction['id_salesorder'] = $salesorder->id;
        $data_transaction['msisdn'] = $row['msisdn'];
        $data_transaction['iccid'] = empty($row['iccid']) ? "" : sprintf('%.0f', $row['iccid']);
        $data_transaction['type_bundling'] = $row['type_bundling'];

        if($row['type_bundling'] == "Postpaid"){
            if($data_m2m == null){
                return;
            }
            $data_transaction['m2m'] = $data_m2m->name;
        } else if($row['type_bundling'] == "Prepaid"){
            if($data_package_type == null){
                return;
            }
            $data_transaction['package_type'] = $data_package_type->name;
            $data_transaction['type_prepaid'] = $row['type_prepaid'];
            $data_transaction['hlr'] = $row['hlr'];
        }

        $data_transaction['non_m2m'] = $data_non_m2m->name;
        $data_transaction['sim_specification'] = $data_sim_spec->name;
        $data_transaction['package_basic'] = $data_package->package;

        $total_price = 0;
        if($salesorder->type_req == "Bundling"){
            if($data_brand == null){
                return;
            } else if($row['device_price'] != null){
                if($row['type_payment'] == "Cash"){
                    $data_transaction['tenor'] = 1;
                } else {
                    $data_transaction['tenor'] = $row['tenor'];
                }
                $data_transaction['device_price'] = number_format($row['device_price']);
                $total_price += ((int)$data_transaction['tenor']*(int)$row['device_price']);
            } else {
                $data_transaction['brand'] = $data_brand->product;
                $data_transaction['type_payment'] = $row['type_payment'];

                if($row['type_payment'] == "Cash"){
                    $data_transaction['tenor'] = 1;
                    if($salesorder->quantity <= 100){
                        $data_transaction['device_price'] = number_format($data_brand->cash_1);
                        $total_price += (int)$data_brand->cash_1;
                    } else if($salesorder->quantity <= 500){
                        $data_transaction['device_price'] = number_format($data_brand->cash_2);
                        $total_price += (int)$data_brand->cash_2;
                    } else {
                        $data_transaction['device_price'] = number_format($data_brand->cash_3);
                        $total_price += (int)$data_brand->cash_3;
                    }
                } else {
                    $data_transaction['tenor'] = $row['tenor'];
                    if($row['tenor'] == 3){
                        if($salesorder->quantity <= 100){
                            $data_transaction['device_price'] = number_format($data_brand->months_3_1);
                            $total_price += (3*(int)$data_brand->months_3_1);
                        } else if($salesorder->quantity <= 500){
                            $data_transaction['device_price'] = number_format($data_brand->months_3_2);
                            $total_price += (3*(int)$data_brand->months_3_2);
                        } else {
                            $data_transaction['device_price'] = number_format($data_brand->months_3_3);
                            $total_price += (3*(int)$data_brand->months_3_3);
                        }
                    } else if($row['tenor'] == 6){
                        if($salesorder->quantity <= 100){
                            $data_transaction['device_price'] = number_format($data_brand->months_6_1);
                            $total_price += (6*(int)$data_brand->months_6_1);
                        } else if($salesorder->quantity <= 500){
                            $data_transaction['device_price'] = number_format($data_brand->months_6_2);
                            $total_price += (6*(int)$data_brand->months_6_2);
                        } else {
                            $data_transaction['device_price'] = number_format($data_brand->months_6_3);
                            $total_price += (6*(int)$data_brand->months_6_3);
                        }
                    } else if($row['tenor'] == 12){
                        if($salesorder->quantity <= 100){
                            $data_transaction['device_price'] = number_format($data_brand->months_12_1);
                            $total_price += (12*(int)$data_brand->months_12_1);
                        } else if($salesorder->quantity <= 500){
                            $data_transaction['device_price'] = number_format($data_brand->months_12_2);
                            $total_price += (12*(int)$data_brand->months_12_2);
                        } else {
                            $data_transaction['device_price'] = number_format($data_brand->months_12_3);
                            $total_price += (12*(int)$data_brand->months_12_3);
                        }
                    } else if($row['tenor'] == 24){
                        if($salesorder->quantity <= 100){
                            $data_transaction['device_price'] = number_format($data_brand->months_24_1);
                            $total_price += (24*(int)$data_brand->months_24_1);
                        } else if($salesorder->quantity <= 500){
                            $data_transaction['device_price'] = number_format($data_brand->months_24_2);
                            $total_price += (24*(int)$data_brand->months_24_2);
                        } else {
                            $data_transaction['device_price'] = number_format($data_brand->months_24_3);
                            $total_price += (24*(int)$data_brand->months_24_3);
                        }
                    }
                }
            }
        } else {
            $total_price += $data_package->price;
            $data_transaction['device_price'] = number_format(0);
        }

        $data_transaction['device_mrc'] = number_format($data_package->price);
        $data_transaction['price_total'] = number_format($total_price);

        $transaction = SalesOrderTransaction::create($data_transaction);

        if($row['id_add_on_package'] != null){
            $addon = explode(', ', $row['id_add_on_package']);
            foreach($addon as $ao) {
                $data_addon = PackageAddOn::where('id', $ao)->first();
                SalesOrderAddOn::create([
                    'id_transaction' => $transaction->id,
                    'id_addon' => $data_addon->id
                ]);
            }
        }

        return $transaction;
    }

    public function headingRow(): int
    {
        return 5;
    }
}
