<?php

namespace App\Imports;

use App\Salesorder;
use App\DeliveryRequest;
use App\SalesOrderTransaction;
use App\SalesOrderDeliveryRequest;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class DeliveryRequestImport implements ToModel, WithHeadingRow
{
    public function __construct(int $so_dr_id)
    {
        $this->so_dr_id = $so_dr_id;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if($row['msisdn'] == null){
            return;
        }

        $so_dr = SalesOrderDeliveryRequest::where('id', $this->so_dr_id)->first();
        $transaction = SalesOrderTransaction::where('id_salesorder', $so_dr->id_salesorder)->where('msisdn', $row['msisdn'])->first();
        $salesorder = SalesOrder::where('id', $transaction->id_salesorder)->first();

        if($transaction == null || $transaction->status != 'Available'){
            return;
        }

        $data_dr['id_so_dr'] = $so_dr->id;
        $data_dr['msisdn'] = $transaction->msisdn;
        $data_dr['iccid'] = $transaction->iccid;
        $data_dr['device_type'] = $transaction->brand;
        $data_dr['device_price'] = $transaction->price_total;

        if($salesorder->type_req == 'Bundling'){
            $data_dr['imei'] = empty($row['imei']) ? "" : sprintf('%.0f', $row['imei']);
        }

        $data_dr['quantity'] = 1;
        $data_dr['packing'] = $row['packing_yn'];

        $dr = DeliveryRequest::create($data_dr);

        SalesOrderTransaction::where('id', $transaction->id)->update([
            'status' => 'On DR'
        ]);

        return $dr;
    }

    public function headingRow(): int
    {
        return 4;
    }
}
