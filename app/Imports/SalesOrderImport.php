<?php

namespace App\Imports;

use App\SalesOrder;
use Maatwebsite\Excel\Concerns\ToModel;

class SalesOrderImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new SalesOrder([
            //
        ]);
    }
}
