<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailActivity extends Model
{
    protected $table = 'detail_activity';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'id_main',
        'id_vendor',
        'detail',
        'price',
        'start',
        'end'
    ];
}
