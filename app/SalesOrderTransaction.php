<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesOrderTransaction extends Model
{
    protected $table = 'salesorder_transactions';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'id_salesorder',
        'msisdn',
        'iccid',
        'type_bundling',
        'type_prepaid',
        'm2m',
        'non_m2m',
        'package_type',
        'sim_specification',
        'hlr',
        'type_sim',
        'brand',
        'package_basic',
        'type_payment',
        'tenor',
        'device_price',
        'device_mrc',
        'price_total',
        'status'
    ];

    public function salesorder()
    {
        return $this->belongsTo('App\Salesorder','id_salesorder');
    }

    public function add_on()
    {
        return $this->belongsTo('App\SalesOrderAddOn','id_transaction');
    }


}
