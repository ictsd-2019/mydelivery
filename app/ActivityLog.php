<?php

namespace App;

use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use UserAgentParser\Exception\NoResultFoundException;
use UserAgentParser\Provider\WhichBrowser;

class ActivityLog extends Model
{

    /**
     * Disable timestamp, use custom time
     * @var boolean
     */
	public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'type',
    	'username',
        'fullname',
    	'ip_address',
    	'device',
    	'module',
    	'log',
    	'data',
    	'created_at'
    ];

    /**
     * Activity Log type INFO
     * 
     * @param  String $module   Module name
     * @param  String $username Username of the active session
     * @param  String $log      Description of the log
     * @param  Array $data     	Raw data 
     * @return Obj           	Database Object
     */
    public static function info($module, $log, $data = NULL)
    {

        $filter_username = ['admin'];

        if(! in_array(Auth::user()->username, $filter_username))
        {
            $provider = new WhichBrowser();
            $client = $provider->parse($_SERVER['HTTP_USER_AGENT']);

    		if($client->isMobile() === true) 
    		{
    			$device = $client->getDevice()->getBrand() ." ". $client->getDevice()->getModel();
    			$browser = $client->getBrowser()->getName() ." ". $client->getBrowser()->getVersion()->getComplete(); 
    		}
    		else
    		{
    			$device = $client->getOperatingSystem()->getName() ." ". $client->getOperatingSystem()->getVersion()->getComplete();
    			$browser = $client->getBrowser()->getName() ." ". $client->getBrowser()->getVersion()->getComplete(); 
    		}

            if($data !== NULL)
            {
                $activity_data = json_encode($data);
            }
            else
            {
                $activity_data = NULL;
            }

    		$device_data = [ 	    	
    			'type' => 'INFO',
    	    	'username' => Auth::user()->username,
                'fullname' => Auth::user()->fullname ." [". Auth::user()->roles[0]->name ."]",
    	    	'ip_address' => $_SERVER['REMOTE_ADDR'],
    	    	'device' => $browser ." : ". $device,
    	    	'module' => $module,
    	    	'log' => $log,
    	    	'data' => $activity_data,
    	    	'created_at' => Carbon::now()
    	    ];
    	   
           return ActivityLog::create($device_data);
        }

    }


    /**
     * Activity Log type ERROR
     * 
     * @param  String $module   Module name
     * @param  String $username Username of the active session
     * @param  String $log      Description of the log
     * @param  Array $data    	Raw data 
     * @return Obj           	Database Object
     */
    public static function error($module, $username, $log, $data = NULL)
    {
        $filter_username = ['sandhi'];

        if(! in_array(Auth::user()->username, $filter_username))
        {
            $provider = new WhichBrowser();
            $client = $provider->parse($_SERVER['HTTP_USER_AGENT']);

            if($client->isMobile() === true) 
            {
                $device = $client->getDevice()->getBrand() ." ". $client->getDevice()->getModel();
                $browser = $client->getBrowser()->getName() ." ". $client->getBrowser()->getVersion()->getComplete(); 
            }
            else
            {
                $device = $client->getOperatingSystem()->getName() ." ". $client->getOperatingSystem()->getVersion()->getComplete();
                $browser = $client->getBrowser()->getName() ." ". $client->getBrowser()->getVersion()->getComplete(); 
            }

            if($data !== NULL)
            {
                $activity_data = json_encode($data);
            }
            else
            {
                $activity_data = NULL;
            }

            $device_data = [            
                'type' => 'INFO',
                'username' => Auth::user()->username,
                'fullname' => Auth::user()->fullname,
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'device' => $browser ." : ". $device,
                'module' => $module,
                'log' => $log,
                'data' => $activity_data,
                'created_at' => Carbon::now()
            ];

        	return ActivityLog::create($device_data);
        }        
    }    
}
