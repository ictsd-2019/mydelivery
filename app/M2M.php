<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M2M extends Model
{
    protected $table = 'm2ms';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'name'
    ];
}
