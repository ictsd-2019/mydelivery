<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salesorder extends Model
{
    protected $table = 'salesorder';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'corp_name',
        'pic',
        'pic_phone',
        'pic_email',
        'date_order',
        'quote_no',
        'ba_no',
        'quantity',
        'status',
        'status_helper',
        'id_sales',
        'sales_group',
        'type_req',
        'salesorder_code',
        'pic_quote',
        'remarks'
    ];

    public function transactions()
    {
        return $this->hasMany('App\SalesOrderTransaction','id_salesorder');
    }

}
