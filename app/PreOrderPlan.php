<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreOrderPlan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['preorder_id','plan_id','periode'];

    public function plan()
    {
        return $this->belongsTo('App\Plan');
    }
}
