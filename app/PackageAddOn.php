<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageAddOn extends Model
{
    protected $table = 'package_addons';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'addon',
        'price'
    ];

    public function salesorder()
    {
        return $this->hasMany('App\SalesOrderAddOn','id_addon');
    }
}
