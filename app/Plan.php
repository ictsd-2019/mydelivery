<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['plan_type_id','plan_code','name','price'];

    /**
     * Get the product type .
     */
    public function plan_type()
    {
        return $this->belongsTo('App\PlanType');
    }    
}
