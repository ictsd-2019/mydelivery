<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
        'username',
        'fullname',
        'nik',
        'phone',
        'email',
        'password',
        'manager_id',
        'region_id',
        'store_id',
        'last_login',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Relation to Region
     *
     * @return Obj Data Object ID
     */
    public function region()
    {
        return $this->belongsTo('App\Region');
    }

    // public function activations()
    // {
    //     return $this->hasMany('App\Activation', 'proceed_by');
    // }

    // public function store()
    // {
    //     return $this->belongsTo('App\Store');
    // }

    // public function profile()
    // {
    //     return $this->hasOne(UAProfile::class, 'user_id');
    // }

    // public function user_quiz(){
    //     return $this->hasMany(UAUserQuiz::class, 'user_id');
    // }

    public function getLoginDateAttribute()
    {

        return $this->last_login != null ? \Carbon\Carbon::parse($this->last_login)->diffForHumans() : '';
    }

    public function getCreateAtAttribute()
    {
        return $this->created_at != null ? $this->created_at : '';
    }

    public function getUserStatusAttribute()
    {
        if($this->status == 2)
        {
            return "<span class='badge badge-danger'>In-Active</span>";
        }else{
            return "<span class='badge badge-success'>Active</span>";
        }
    }
}
