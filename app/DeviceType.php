<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeviceType extends Model
{
    protected $table = 'device_types';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
       'product',
       'cash_1',
       'cash_2',
       'cash_3',
       'months_3_1',
       'months_3_2',
       'months_3_3',
       'months_6_1',
       'months_6_2',
       'months_6_3',
       'months_12_1',
       'months_12_2',
       'months_12_3',
       'months_24_1',
       'months_24_2',
       'months_24_3'
    ];

    public function price()
    {
        return $this->belongsTo('App\DevicePrice', 'id');
    }

}
