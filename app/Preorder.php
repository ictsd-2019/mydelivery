<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreOrder extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title','preorder_code','startdate','enddate','max_order','status'];

    /*
     * Show devices that relate this this pre-order
     */
    public function devices()
    {
        return $this->hasMany('App\PreOrderDevice','preorder_id');
    }

    /*
     * Show transactions of this Pre-Order
     */
    public function transactions()
    {
        return $this->hasMany('App\PreOrderTransaction','preorder_id');
    }

    /*
     * Relationship to Bank Promos
    public function bank_promos()
    {
        return $this->hasMany('App\PreOrderBankPromo','preorder_id');
    }
    */
}
