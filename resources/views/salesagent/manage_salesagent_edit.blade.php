@extends('layouts.app')

@section('content')
    <h1 class="title">Update Sales Agent</h1>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
    @include('layouts.error')
    <br>
    {!! Form::open(['action' => ['SalesAgentController@update', $sales->id], 'id' => 'edit-sales', 'class' => 'form-horizontal']) !!}
        @include('salesagent.form_manage_edit_salesagent')
    {!! Form::close() !!}
@endsection
