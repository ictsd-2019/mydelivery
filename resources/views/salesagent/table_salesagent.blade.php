<table id="example" class="table table-bordered table-hover dt-responsive text-center" style="width:100%">
    <thead>
        <tr>
            <th style="background:lightgrey; text-align:center; width:10%;">No</th>
            <th style="background:lightgrey; text-align:center; width:40%;">Name</th>
            <th style="background:lightgrey; text-align:center; width:35%;">Department</th>
            <th style="background:lightgrey; text-align:center; width:35%;">Divisi</th>
            <th style="background:lightgrey; text-align:center; width:35%;">Group</th>
        </tr>
    </thead>
    <tbody>
    @foreach($sales as $sa)
        <tr>
            <td> {{$loop->iteration}} </td>
            <td> {{$sa->nama}} </td>
            <td> {{$sa->department}} </td>
            <td> {{$sa->divisi}} </td>
            <td> {{$sa->group}} </td>
        </tr>
    @endforeach
    </tbody>
</table>