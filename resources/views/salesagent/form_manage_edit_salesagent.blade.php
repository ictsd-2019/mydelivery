<div class="row">
    <div class="col-md-6">
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#" style="color:black;">Sales Agent Info</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('nama', 'Sales Name', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('nama', $sales->nama, ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => 'Nama Sales']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('department', 'Departemen', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::select('department', $region, $sales->department, array('class'=>'form-control', 'placeholder' => '- Department Sales -', 'required' => 'required')) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('divisi', 'Divisi', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('divisi', $sales->divisi, ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => 'Divisi Sales']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('group', 'Group', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('group', $sales->group, ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => 'Group Sales']) !!}
                </div>
            </div>
        </div>
        <br>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <strong><sup class="required">*</sup> Required</strong>
        <br/><br/>
        <a href="/sales" class="btn btn-secondary m-1">Cancel</a>
        <input type="submit" value="Submit"  class="btn btn-primary m-1">
    </div>
</div>