@extends('layouts.app')

@section('content')
    <h1 class="title">Update Signature</h1>
    @include('layouts.error')
    <br>
    {!! Form::open(['action' => ['EmployeeController@update', $employee->id], 'id' => 'edit-employee', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
        @csrf
        @include('employee.form_manage_edit_employee')
    {!! Form::close() !!}
@endsection
