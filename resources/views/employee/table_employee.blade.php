<table id="example" class="table table-bordered table-hover dt-responsive text-center" style="width:100%">
    <thead>
        <tr>
            <th style="background:lightgrey; text-align:center; width:5%;">No</th>
            <th style="background:lightgrey; text-align:center; width:10%;">NIK</th>
            <th style="background:lightgrey; text-align:center; width:15%;">Name</th>
            <th style="background:lightgrey; text-align:center; width:15%;">Position</th>
            <th style="background:lightgrey; text-align:center; width:15%;">Group</th>
            <th style="background:lightgrey; text-align:center; width:15%;">Division</th>
            <th style="background:lightgrey; text-align:center; width:15%;">Departement</th>
            <th style="background:lightgrey; text-align:center; width:5%;">Signature</th>
        </tr>
    </thead>
    <tbody>
    @foreach($employee as $emp)
        <tr>
            <td> {{$loop->iteration}} </td>
            <td> {{$emp->nik}} </td>
            <td> {{$emp->name}} </td>
            <td> {{$emp->position}} </td>
            <td> {{$emp->group}} </td>
            <td> {{$emp->division}} </td>
            <td> {{$emp->department}} </td>
            @if($emp->signature == null)
                <td> Tidak Ada </td>
            @else
                <td> Ada </td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>