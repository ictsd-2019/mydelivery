@extends('layouts.app')

@section('content')
    <h1 class="title">Add Signature</h1>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif

    @include('layouts.error')

    <br>
    {!! Form::open(['action' => ['EmployeeController@store'], 'id' => 'create-employee', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
        @csrf
        @include('employee.form_manage_add_employee')
    {!! Form::close() !!}

@endsection
