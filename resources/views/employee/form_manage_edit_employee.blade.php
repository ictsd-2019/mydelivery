<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#" style="color:black;">Signature Info</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('nik', 'NIK', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('nik', $employee->nik, ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => 'NIK']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('name', 'Name', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('name', $employee->name, ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => 'Nama']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('position', 'Position', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('position', $employee->position, ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => 'Position']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('group', 'Group', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('group', $employee->group, ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => 'Group']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('division', 'Division', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('division', $employee->division, ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => 'Division']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('department', 'Department', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('department', $employee->department, ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => 'Department']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('signature', 'Signature', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    @if($employee->signature == null)
                        {!! Form::hidden('signature', null, ['id' => 'value-signature']) !!}
                        <img class="img-fluid" id="display-signature" src="" style="display: none;">
                        <input type="file" class="form-control m-1" name="signature" id="signature">
                    @else
                        {!! Form::hidden('signature', null, ['id' => 'value-signature']) !!}
                        <img class="img-fluid" id="display-signature" src="{{ asset($employee->signature) }}">
                        <img class="img-fluid" id="display-signature" src="" style="display: none;">
                        <input type="file" class="form-control m-1" name="signature" id="signature">
                    @endif
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('user', 'User', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                   {!! Form::select('user', $selected_user, $employee->id_user, array('class'=>'form-control', 'placeholder' => '- User Signature -', 'required' => 'required')) !!}
                </div>
            </div>
        </div>
        <br>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <strong><sup class="required">*</sup> Required</strong>
        <br/><br/>
        <a href="/employee" class="btn btn-secondary m-1">Cancel</a>
        <input type="submit" value="Submit"  class="btn btn-primary m-1">
    </div>
</div>