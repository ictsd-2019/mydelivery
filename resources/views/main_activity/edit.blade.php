@extends('layouts.app')

@section('content')
    <h1 class="title">Manage Update MainActivity</h1>
    @include('layouts.error')
    <br>
    {!! Form::open(['action' => ['MainActivityController@update', $main->id], 'id' => 'edit-main', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
        @csrf
        @include('main_activity.form_edit')
    {!! Form::close() !!}
@endsection
