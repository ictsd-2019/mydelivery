<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#" style="color:black;">Form Update MainActivity</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('id', 'ID Main', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-4">
                    {!! Form::text('id', $main->id, ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => 'NIK','readonly' => 'true']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('ven', 'Vendor', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                {!! Form::select('ven', $selected_vendor, $main->id_vendor, array('class' => 'form-control mb-2 mt-1 placeholder-fix', 'placeholder' => '- Select Vendor -', 'required' => 'required')) !!}
                </div>
            </div>
         
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('reg', 'Regional', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('reg', $main->regional, ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => ' Regional']) !!}
                </div>
            </div>

            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('act', 'Activity', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('act', $main->activity, ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => ' Activity']) !!}
                </div>
            </div>

            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('note', 'Notes', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::textarea('note', $main->notes, ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => 'Notes']) !!}
                </div>
            </div>
        </div>
        <br>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <strong><sup class="required">*</sup> Required</strong>
        <br/><br/>
        <a href="/main" class="btn btn-secondary m-1">Cancel</a>
        <input type="submit" value="Update Data"  class="btn btn-primary m-1">
    </div>
</div>