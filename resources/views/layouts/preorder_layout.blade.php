<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'MyDelivery Application') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/colorbox.css" rel="stylesheet">
    <link href="/css/bootstrap-datepicker.min.css" rel="stylesheet">    
    <link href="/css/indosat_style.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <style>
        html {
            background:#ffffff;
        }
        #header {
            background: #FCD612;
            text-align: left;
        }
        #logo {
            width: 250px;
        }
        #content {
            padding:0;
        }
        .total-store-activation {
            text-align: center;
            font-size: 24px;
            margin-top: 20px;
        }
        .chart-block {
            background: #f3f3f3;
            padding:10px;
            margin-top:10px;
        }
        ul.user-menu li ul {
            width:auto;
        }
        .dropdown-menu {
            min-width: auto;
        }
        .nav-tabs>li.active>a, 
        .nav-tabs>li.active>a:focus, 
        .nav-tabs>li.active>a:hover {
            background: #da292c;
            /*
            background: -webkit-linear-gradient(#da292c, #dcdcdc);
            background: -o-linear-gradient(#da292c,#dcdcdc);
            background: -moz-linear-gradient(#da292c, #dcdcdc);
            background: linear-gradient(#da292c, #dcdcdc); 
            */           
            color: #ffffff;
        }
        .btn-warning, .label-warning {
            background-color: #e2bd00;
            border-color: #caab02;
        }
    </style>
</head>
<body>
    <div id="general-page">
        <header id="header">
            <div class="container">
                <div id="header" class="row text-center">
                    <div id="logo" style="margin-top: 10px; margin-bottom: 10px;">
                        <img src="/img/im3ooredoo-logo.png" class="img-responsive" />
                    </div>
                    @if(Auth::user())
                    <ul class="dropdown user-menu" style="position: absolute; top: 10px;">
                        <li>
                            <a class="dropdown-toggle display-user-name" type="button" id="dropdownMenu1" data-toggle="dropdown"> {{ Auth::user()->fullname }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li> 
                                    <a href="{{ url('/logout') }}" 
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();"> Logout </a>
                                    <form id="logout-form" 
                                    action="{{ url('/logout') }}" 
                                    method="POST" 
                                    style="display: none;">
                                    {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                            <br/><span id="online-status" style="float:right"></span>
                        </li>
                    </ul>                     
                    @endif                      
                </div>
            </div>
        </header>
        <section id="content">
            <div class="container">
                @yield('content')
                <br/>
            </div>
        </section>
    </div>
    @include('layouts.footer')
    @section('scripts')
        <!-- Scripts -->
        <script src="/js/app.js"></script>     
        <script src="/js/jquery.colorbox-min.js"></script>
        <script src="/js/jqsignature/jq-signature.min.js"></script>
        <script src="/js/jquery.countdown/jquery.countdown.min.js"></script>
        <script src="/js/activation.js"></script>
        <script src="/js/bootstrap-datepicker.min.js"></script>
        <script>

            checkOnline();

            setInterval(checkOnline(), 5000);

            function checkOnline()
            {
                if(navigator.onLine)
                {
                    $('#online-status').html('<span class="label label-success">Online</span>');
                    $('input[type=submit]').attr('disabled', false);
                }
                else
                {
                    $('#online-status').html('<span class="label label-danger">Offline</span>');
                    $('input[type=submit]').attr('disabled', true);
                }
            }
        </script>
        <script>
            setTimeout(function () {
                $("#top-notification").slideDown();
            }, 1000);

            setInterval(function() {
                $('.blinking').fadeOut(1000);
                $('.blinking').fadeIn(1000);
            }, 1000);
        </script>
    @show    
</body>
</html>