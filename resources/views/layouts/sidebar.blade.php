            @section('sidebar')
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                    <div id="sidebar">
                        <div class="menu-handler"> Menu </div>
                        <h2 data-toggle="collapse" data-target="#PreOrder">Preorder Sales</h2>
                        <div id="PreOrder" class="collapse">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="fa fa-retweet" aria-hidden="true"></i> Preorder List Data
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <h2 data-toggle="collapse" data-target="#SalesOrder">SalesOrder</h2>
                        <div id="SalesOrder" class="collapse">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="fa fa-retweet" aria-hidden="true"></i> Sales Order List Data
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <h2 data-toggle="collapse" data-target="#Product">Product</h2>
                        <div id="Product" class="collapse">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="fa fa-retweet" aria-hidden="true"></i> Product List Data
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <h2 data-toggle="collapse" data-target="#Sales">Sales</h2>
                        <div id="Sales" class="collapse">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::path() == 'sales/manage' ? 'active' : '' }}" href="{{ url('sales/manage') }}">
                                        <i class="fa fa-barcode" aria-hidden="true"></i> Manage Sales
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::path() == 'sales/manager' ? 'active' : '' }}" href="{{ url('sales/manager') }}">
                                        <i class="fa fa-user-circle-o" aria-hidden="true"></i> Regions
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <ul class="nav flex-column">
                            <li class="nva-item">
                                <a class="nav-link" href="/config">
                                    <i class="fa fa-cog" aria-hidden="true"></i> Configuration
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            @show