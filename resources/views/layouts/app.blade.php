<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'MyDelivery Application') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link href="/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="/js/jquery.chosen/chosen.min.css" rel="stylesheet">
    <link href="/css/bootstrap-chosen.css" rel="stylesheet">
    <link href="/js/jquery.select2/select2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/js/jquery.confirm/jquery-confirm.min.css">
    <link href="/css/colorbox.css" rel="stylesheet">
    <link href="/css/indosat_style.css" rel="stylesheet">
    <link href="/css/summernote-bs4.css" rel="stylesheet">
    <style>
        .sidebar, h2:hover { cursor: pointer; }
    </style>
    <style>
    .placeholder-fix:focus::-webkit-input-placeholder  {color:transparent;}
    .placeholder-fix:focus::-moz-placeholder   {color:transparent;}
    .placeholder-fix:-moz-placeholder   {color:transparent;}
    
    thead input {
        width: 100%;
    }
    th, td { 
        font-size: 12px;
    }
    .table tbody tr td{
        padding-top:4px;
        padding-bottom:4px;
    }
    .tooltip {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
    }

    .tooltip .tooltiptext {
        visibility: hidden;
        width: 120px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        position: absolute;
        z-index: 1;
        top: 150%;
        left: 50%;
        margin-left: -60px;
    }

    .tooltip .tooltiptext::after {
        content: "";
        position: absolute;
        bottom: 100%;
        left: 50%;
        margin-left: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: transparent transparent black transparent;
    }

    .tooltip:hover .tooltiptext {
        visibility: visible;
    }
    </style>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117545218-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-117545218-1');
    </script>
    <!-- Scripts -->

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            ]) !!};
    </script>
</head>
    <div id="app" class="container-fluid">
        <div>
            <div id="header" class="row">
                <div id="content-loader" style="display: none;">
                    <img src="/img/content-loader.gif" alt=""> <span class="random-load"></span>...
                </div>
                <div class="col-md-6">
                    <div id="logo" class="pull-left">
                        <a href="/"><img src="/img/mydelivery-logo.png" class="img-fluid"/></a>
                    </div>
                </div>
                <div class="col-md-6">
                    <h6 class="ml-3 text-right mt-3"><i class="fa fa-clock-o"></i>
                        <script>
                            var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                            var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum&#39;at', 'Sabtu'];
                            var date = new Date();
                            var day = date.getDate();
                            var month = date.getMonth();
                            var thisDay = date.getDay(),
                                    thisDay = myDays[thisDay];
                            var yy = date.getYear();
                            var year = (yy < 1000) ? yy + 1900 : yy;
                            document.write(thisDay + ", " + day + " " + months[month] + " " + year);
                        </script>
                        <span id="clock"></span>
                    </h6>

                    <ul class="m-0 p-0 dropdown user-menu pull-right pb-3">
                        <li class="pl-0 ml-0">
                            <div class="btn-group">
                                <a href="" id="inbox" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Inbox </a>
                                <div class="dropdown-menu">
                                     
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="">View Inbox</a>
                                </div>
                            </div>
                        </li>
                        <li class="pl-0 ml-0"><a href="/dashboard">Dashboard</a></li>
                        @if(Auth::user())
                        <li class="pl-0 ml-0"><span id="online-status"></span>
                            <a class="dropdown-toggle" data-toggle="dropdown">
                                {{ Auth::user()->fullname ? Auth::user()->fullname : Auth::user()->username }}
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li class="dropdown-item"><a href="{{ action('UserProfileController@index') }}">Profile</a></li>
                                <li class="dropdown-item"><a href="/profile/change_password">Change Password</a></li>
                                <li role="separator" class="divider"></li>
                                <li class="dropdown-item">
                                    <a href="{{ url('/logout') }}"
                                       onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();"> Logout </a>
                                    <form id="logout-form"
                                          action="{{ url('/logout') }}"
                                          method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>

        <div id="body-wrapper">
            <div class="row no-gutters">
                <div class="col-md-2">
                    @section('sidebar')
                    <div id="sidebar">
                        <div class="menu-handler"> Menu </div>
                        <h2 data-toggle="collapse" data-target="#Master">Master Data</h2>
                        <div id="Master" class="collapse {{ Request::path() == 'device' || Request::path() == 'sales' || Request::path() == 'package' || Request::path() == 'addon' || Request::path() == 'employee' ? 'show' : '' }}">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::path() == 'device' ? 'active' : '' }}" href="{{ url('device') }}">
                                        <i class="fa fa-tablet" aria-hidden="true"></i> Device
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::path() == 'sales' ? 'active' : '' }}" href="{{ url('sales') }}">
                                        <i class="fa fa-users" aria-hidden="true"></i> Sales Agent
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::path() == 'package' ? 'active' : '' }}" href="{{ url('package') }}">
                                        <i class="fa fa-indent" aria-hidden="true"></i> Basic Package
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::path() == 'addon' ? 'active' : '' }}" href="{{ url('addon') }}">
                                        <i class="fa fa-plus" aria-hidden="true"></i> Add On Package
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::path() == 'employee' ? 'active' : '' }}" href="{{ url('employee') }}">
                                        <i class="fa fa-paperclip" aria-hidden="true"></i> Signature
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::path() == 'm2m' ? 'active' : '' }}" href="{{ url('m2m') }}">
                                        <i class="fa fa-plus" aria-hidden="true"></i> M2M
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::path() == 'card-size' ? 'active' : '' }}" href="{{ url('card-size') }}">
                                        <i class="fa fa-plus" aria-hidden="true"></i> Non M2M (Card Size)
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::path() == 'simpack' ? 'active' : '' }}" href="{{ url('simpack') }}">
                                        <i class="fa fa-plus" aria-hidden="true"></i> SIM Package
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::path() == 'simspec' ? 'active' : '' }}" href="{{ url('simspec') }}">
                                        <i class="fa fa-plus" aria-hidden="true"></i> SIM Specification
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <h2 data-toggle="collapse" data-target="#PreOrder">Preorder Sales</h2>
                        <div id="PreOrder" class="collapse">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="fa fa-retweet" aria-hidden="true"></i> Preorder List Data
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <!-- <h2 data-toggle="collapse" data-target="#Product">Product</h2>
                        <div id="Product" class="collapse">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="fa fa-retweet" aria-hidden="true"></i> Product List Data
                                    </a>
                                </li>
                            </ul>
                        </div> -->

                        <!-- <h2 data-toggle="collapse" data-target="#Sales">Sales</h2>
                        <div id="Sales" class="collapse">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::path() == 'sales/manage' ? 'active' : '' }}" href="{{ url('sales/manage') }}">
                                        <i class="fa fa-user-circle-o" aria-hidden="true"></i> Manage Sales
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::path() == 'sales/manager' ? 'active' : '' }}" href="{{ url('sales/manager') }}">
                                        <i class="fa fa-map" aria-hidden="true"></i> Regions
                                    </a>
                                </li>
                            </ul>
                        </div> -->

                        <h2 data-toggle="collapse" data-target="#SalesOrder">Sales Order</h2>
                        <div id="SalesOrder" class="collapse {{ preg_match( '/^salesorder\/*/', Request::path()) ? 'show' : '' }}">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::path() == 'salesorder' ? 'active' : '' }}" href="{{ url('salesorder') }}">
                                        <i class="fa fa-retweet" aria-hidden="true"></i> Activation
                                    </a>
                                </li>
                            </ul>
                        </div>

                        @if(Auth::user()->hasPermissionTo('verificate delivery request'))
                        <h2 data-toggle="collapse" data-target="#Approval">Approval</h2>
                        <div id="Approval" class="collapse {{ preg_match( '/^approval-dr\/*/', Request::path()) ? 'show' : '' }}">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::path() == 'approval-dr' ? 'active' : '' }}" href="{{ url('approval-dr') }}">
                                        <i class="fa fa-rocket" aria-hidden="true"></i> Delivery Request
                                    </a>
                                </li>
                            </ul>
                        </div>
                        @endif

                        @if(Auth::user()->hasPermissionTo('signed bast'))
                        <h2 data-toggle="collapse" data-target="#Document">Document</h2>
                        <div id="Document" class="collapse {{ preg_match( '/^courier\/*/', Request::path()) ? 'show' : '' }}">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::path() == 'courier/sign-bast' ? 'active' : '' }}" href="{{ url('courier/sign-bast') }}">
                                        <i class="fa fa-reorder" aria-hidden="true"></i> Sign BAST
                                    </a>
                                </li>
                            </ul>
                        </div>
                        @endif
                        <h2 data-toggle="collapse" data-target="#PriceBook">Master PriceBook</h2>
                        <div id="PriceBook" class="collapse {{ Request::path() == 'book' || Request::path() == 'arrival' || Request::path() == 'task' || Request::path() == 'main' || Request::path() == 'detail' ? 'show' : '' }}">
                            <ul class="nav flex-column">
                               <li class="nav-item">
                                    <a class="nav-link {{ Request::path() == 'arrival' ? 'active' : '' }}" href="{{ url('arrival') }}">
                                        <i class="fa fa-database" aria-hidden="true"></i> Arrival Fee
                                    </a>
                                </li>

                                <!--<li class="nav-item">
                                    <a class="nav-link {{ Request::path() == 'type' ? 'active' : '' }}" href="{{ url('type') }}">
                                        <i class="fa fa-database" aria-hidden="true"></i> Type Scope
                                    </a>
                                </li>-->

                                <li class="nav-item">
                                    <a class="nav-link {{ Request::path() == 'main' ? 'active' : '' }}" href="{{ url('main') }}">
                                        <i class="fa fa-database" aria-hidden="true"></i> Main Activity
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link {{ Request::path() == 'detail' ? 'active' : '' }}" href="{{ url('detail') }}">
                                        <i class="fa fa-database" aria-hidden="true"></i> Detail Activity
                                    </a>
                                </li>
                             
                                <!--<li class="nav-item">
                                    <a class="nav-link {{ Request::path() == 'task' ? 'active' : '' }}" href="{{ url('task') }}">
                                        <i class="fa fa-file" aria-hidden="true"></i> Task Scope
                                    </a>
                                </li>-->

                                <li class="nav-item">
                                    <a class="nav-link {{ Request::path() == 'book' ? 'active' : '' }}" href="{{ url('book') }}">
                                        <i class="fa fa-file-excel-o" aria-hidden="true"></i> Price Book Vendor
                                    </a>
                                </li>
                            </ul>
                        </div>

                        @if(Auth::user()->hasPermissionTo('manage user') || Auth::user()->hasPermissionTo('manage role and permission'))
                            <ul class="nav flex-column">
                                <li class="nva-item">
                                    <a class="nav-link {{ strpos(Request::url(),route('config')) !== false ? 'active' : '' }}" href="/config">
                                        <i class="fa fa-cog" aria-hidden="true"></i> Configuration
                                    </a>
                                </li>
                            </ul>
                        @endif
                    </div>
                    @show
                </div>
                <div class="col-md-10">
                    <div id="content">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.footer')
    @section('scripts')
    <!-- Scripts -->
    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/jquery.select2/select2.min.js"></script>
    <script src="/js/jqsignature/jq-signature.min.js"></script>
    <script src="/js/jquery.chosen/chosen.jquery.min.js"></script>
    <script src="/js/jquery.confirm/jquery-confirm.min.js"></script>
    <script src="/js/jquery.countdown/jquery.countdown.min.js"></script>
    <script src="/js/jquery.colorbox-min.js"></script>
    <script src="/js/bootstrap-datepicker.min.js"></script>
    <script src="/js/script.js"></script>
    <!-- include summernote css/js -->
    <script src="/js/summernote-bs4.js"></script>
    <script>

        //Show clock
        function startTime()
        {
            var today=new Date(),
                    curr_hour=today.getHours(),
                    curr_min=today.getMinutes(),
                    curr_sec=today.getSeconds();
            curr_hour=checkTime(curr_hour);
            curr_min=checkTime(curr_min);
            curr_sec=checkTime(curr_sec);
            document.getElementById('clock').innerHTML=curr_hour+":"+curr_min+":"+curr_sec;
        }

        function checkTime(i)
        {
            if (i<10) { i="0" + i; }
            return i;
        }

        setInterval(startTime, 500);

        setInterval(function()
        {
            if(navigator.onLine)
            {
                $('#online-status').html('<span class="badge badge-success">Online</span>');
                $('input[type=submit]').attr('disabled', false);
            }
            else
            {
                $('#online-status').html('<span class="badge badge-danger">Offline</span>');
                $('input[type=submit]').attr('disabled', true);
            }
        }, 1000);
    </script>
    <script>
        setTimeout(function () {
            $("#top-notification").slideDown();
        }, 1000);

        setInterval(function() {
            $('.blinking').fadeOut(1000);
            $('.blinking').fadeIn(1000);
        }, 1000);

    </script>
    <script>
        $(document).ready(function(){
            $('.selectpicker').select2();
            
            $("a,input[type=submit]").not('.lightbox').click(function(){

                var strWords = [
                    "You'are awesome",
                    "I'm working",
                    "Watchout!",
                    "Getting data",
                    "Wait please wait",
                    "I'm stuck!",
                    "Indosat is going digital",
                    "MyRetail is working",
                    "Krik..krik..krik",
                    "Please waiting Loading",
                    "You look beautiful",
                    "Please waiting Loading",
                    "I'm gone crazy",
                    "Go Retail go",
                    "Go Indosat go",
                    "What time now?!",
                    "I'm underpressure",
                    "You are the future",
                    "Slowdown please"
                ];
                var chosenWord = strWords[Math.floor(Math.random() * strWords.length)];

                $(".random-load").html(chosenWord);
                $("#content-loader").slideDown();
            });
        });
    </script>
    <script>
        $('#inbox').tooltip('show')
        
        //textarea summernote
        $(document).ready(function() {
            $('#textarea').summernote({
                height: 200
            });
        });
    </script>
    @show
</body>
</html>
