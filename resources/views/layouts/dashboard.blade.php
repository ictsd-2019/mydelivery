<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.min.css"> 
    <link href="/css/indosat_style.css" rel="stylesheet">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117545218-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-117545218-1');
    </script>
    <!-- Scripts -->

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <style>
        html {
            background:#ffffff;
        }
    </style>
</head>
<body>
    <div id="dashboard-page">
        <div class="container-fluid">
            <div id="header" class="row">
                <div class="col-md-2">
                    <div id="logo">
                        <a href="/"><img src="/img/indosatooredoo-logo.png" class="img-fluid pull-left" /></a>
                    </div>
                </div>
                <div class="col-md-8">
                    <ul class="main-menu">
                        <li><a href="/dashboard" class="{{ Request::path() == 'dashboard' ? 'active' : '' }}"><i class="fa fa-home"></i><br/>Home</a></li>
                        <li><a href="/dashboard/stores" class="{{ Request::path() == 'dashboard/stores' ? 'active' : '' }}"><i class="fa fa-shopping-bag"></i><br/>Stores</a></li>
                        <li><a href="#" class="{{ Request::path() == 'dashboard/inventory' ? 'active' : '' }}"><i class="fa fa-cubes"></i><br/>Inventory</a></li>
                        <li><a href="{{ url('dashboard/direct-sales') }}" class="{{ Request::path() == 'dashboard/direct-sales' ? 'active' : '' }}"><i class="fa fa-area-chart"></i><br/>Direct Sales</a></li>
                        <!--<li><a href="/dashboard/webtools" class="{{ Request::path() == 'dashboard/activation' ? 'active' : '' }}"><i class="fa fa-signal"></i><br/>Webtools</a></a></li>-->
                    </ul>
                </div>
                @if(Auth::user())
                <ul class="dropdown user-menu">
                    <li>
                        <a class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown"> {{ Auth::user()->fullname }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li class="dropdown-item">
                                <a href="{{ url('/logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();"> Logout </a>
                                <form id="logout-form"
                                action="{{ url('/logout') }}"
                                method="POST"
                                style="display: none;">
                                {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                </ul>
                @endif
                <div class="dashboard-time">
                    <span class="day">{{ Carbon\Carbon::now()->format('F dS, Y h:m a') }}</span>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    @if(Request::path() == 'dashboard/direct-sales')
                        {!! Charts::scripts() !!}
                        {!! $day_chart->script() !!}
                        {!! $month_chart->script() !!}
                        {!! $year_chart->script() !!}
                    @endif

                    <script type="text/javascript" src="/js/jquery-3.2.1.min.js"></script>
                    <script type="text/javascript" src="/js/popper.min.js"></script>
                    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
                    <script src="/js/bootstrap-datepicker.min.js"></script>
                    <script src="/js/highcharts.js"></script>
                    <script src="/js/exporting.js"></script>

                    <div id="dashboard">
                        @yield('content')
                    </div>

                    <script type="text/javascript">
                        $(function(){
                            $('.datepicker').datepicker({
                                autoclose: true,
                                format: 'dd M yyyy'
                            });
                            $('[data-toggle="tooltip"]').tooltip();
                        });
                    </script>  

                </div>
            </div>
        </div>
        
    </div>

<!-- Modal -->
<div class="modal fade" id="fetchData" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="background: #fdde00; color: #da251d;">
      <div class="modal-body">
        <h3>Please wait, downloading data ...</h3>
      </div>
    </div>
  </div>
</div>

@include('layouts.footer')

</body>
</html>
