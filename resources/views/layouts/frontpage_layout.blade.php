<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'MyDelivery Application') }}</title>

    <!-- Styles -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="/js/jquery.chosen/chosen.min.css" rel="stylesheet">
    <link href="/css/bootstrap-chosen.css" rel="stylesheet">
    <link href="/js/jquery.select2/select2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/js/jquery.confirm/jquery-confirm.min.css">
    <link href="/css/colorbox.css" rel="stylesheet">
    <link href="/css/indosat_style.css" rel="stylesheet">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117545218-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-117545218-1');
    </script>
    <!-- Scripts -->
   
    @hasrole('Partner Store')
            <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117545218-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-117545218-2');
    </script>
    @endhasrole

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <style>
        html {
            background:#ffffff;
        }
        #header {
            background: #ffca00;
        }
        .menu-item {
            box-shadow: 0 0 5px rgba(0,0,0,0.3);
            background: #f4f4f4;
            margin-bottom: 20px;
            padding: 20px;
        }
        .dropdown.user-menu {
            margin-bottom: 10px;
        }
        .total-store-activation {
            text-align: center;
            font-size: 24px;
            margin-top: 20px;
        }
        .btn {
            border-radius: 20px;
        }
        thead input {
            width: 100%;
        }
        th, td { 
            font-size: 12px;
        }
        .table tbody tr td{
            padding-top:4px;
            padding-bottom:4px;
        }
        .required {
            color: red;
        }
        .tooltip {
            position: relative;
            display: inline-block;
            border-bottom: 1px dotted black;
        }

        .tooltip .tooltiptext {
            visibility: hidden;
            width: 120px;
            background-color: black;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 5px 0;
            position: absolute;
            z-index: 1;
            top: 150%;
            left: 50%;
            margin-left: -60px;
        }

        .tooltip .tooltiptext::after {
            content: "";
            position: absolute;
            bottom: 100%;
            left: 50%;
            margin-left: -5px;
            border-width: 5px;
            border-style: solid;
            border-color: transparent transparent black transparent;
        }

        .tooltip:hover .tooltiptext {
            visibility: visible;
        }
    </style>
</head>
<div id="login-page">
    <header id="header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div id="logo">
                        <a href="/web">
                            <img src="/img/mydelivery-indosat-logo.png" style="height: 70px;" />
                        </a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="pull-right text-right mt-3">
                        <h6><i class="fa fa-clock-o"></i>
                            <script>
                                var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                                var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum&#39;at', 'Sabtu'];
                                var date = new Date();
                                var day = date.getDate();
                                var month = date.getMonth();
                                var thisDay = date.getDay(),
                                        thisDay = myDays[thisDay];
                                var yy = date.getYear();
                                var year = (yy < 1000) ? yy + 1900 : yy;
                                document.write(thisDay + ", " + day + " " + months[month] + " " + year);
                            </script>
                            <span id="time"></span>
                        </h6>
                        <ul class="m-0 p-0 dropdown user-menu pull-right pb-3">
                             <li class="pl-0 ml-0">
                                <div class="btn-group">
                                    <a href="" id="inbox" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Inbox</a>
                                    <div class="dropdown-menu">
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="">View Inbox</a>
                                    </div>
                                </div>
                            </li>
                            @if(Auth::user())
                           <li><span id="online-status"></span>
                                <a class="dropdown-toggle" data-toggle="dropdown">
                                    {{ Auth::user()->fullname ? Auth::user()->fullname : Auth::user()->username }} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <li class="dropdown-item">
                                    @if(Auth::user()->hasRole('Trainee') === true)
                                        <a href="{{ route('profile.index') }}">Profile</a>
                                    @else
                                        <a href="{{ route('web.profile') }}">Profile</a>
                                    @endif
                                    </li>
                                    <li class="dropdown-item"><a href="{{ route('web.profile.reset_password') }}">Change Password</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li class="dropdown-item">
                                        <a href="{{ url('/logout') }}"
                                           onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();"> Logout </a>
                                        <form id="logout-form"
                                              action="{{ url('/logout') }}"
                                              method="POST"
                                              style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                            @endif
                        </ul>
                    </div>
                    
                </div>
            </div>
        </div>
    </header>

    <div class="container" style="min-height: 400px;">
        <div class="row">
            <div class="col-md-12">
                @yield('content')
            </div>
        </div>
    </div>
    <br>
    @include('layouts.footer')
</div>
@section('scripts')
<!-- Scripts -->
<script src="/js/jquery-3.3.1.min.js"></script>
<script src="/js/popper.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.colorbox-min.js"></script>
<script src="/js/jqsignature/jq-signature.min.js"></script>
<script src="/js/jquery.countdown/jquery.countdown.min.js"></script>
<script src="/js/jquery.chosen/chosen.jquery.min.js"></script>
<script src="/js/jquery.select2/select2.min.js"></script>
<script src="/js/bootstrap-datepicker.min.js"></script>
<script src="/js/activation.js"></script>
<script src="/js/store_audit.js"></script>
<script src="/js/jquery.confirm/jquery-confirm.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>

<script>

    setTimeout(function () {
        $("#top-notification").slideDown();
    }, 1000);

    setInterval(function() {
        $('.blinking').fadeOut(1000);
        $('.blinking').fadeIn(1000);
    }, 1000);

    //times
    function startTime() {
    var today=new Date(),
        curr_hour=today.getHours(),
        curr_min=today.getMinutes(),
        curr_sec=today.getSeconds();
        curr_hour=checkTime(curr_hour);
        curr_min=checkTime(curr_min);
        curr_sec=checkTime(curr_sec);
        document.getElementById('time').innerHTML=curr_hour+":"+curr_min+":"+curr_sec;
    }
    function checkTime(i) {
        if (i<10) { i="0" + i; }
        return i;
    }
    setInterval(startTime, 500);

    $('#clock').countdown('2018/07/05 23:00:00', function(event) {

        if(event.strftime('%-D') == 0)
        {
            $(this).html("<strong>"+ event.strftime('%H:%M:%S') +"</strong>");
        }
        else
        {
            $(this).html("<strong>"+ event.strftime('%-D hari %H:%M:%S') +"</strong>");
        }
    }).on('finish.countdown', function(){
        location.reload();
    });
</script>
@show
</body>
</html>
