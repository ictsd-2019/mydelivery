<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="/css/indosat_style.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <style>
        html {
            background:#ffffff;
        }
    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117545218-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-117545218-1');
    </script>
</head>
<body>
    <div id="login-page">
        <div class="container">
            <div id="header" class="text-center">
                <div id="logo">
                    <img src="/img/mydelivery-indosat-logo.png" class="img-fluid" style="width:480px;height:auto;margin-top:100px;" />
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                        @yield('content')
                </div>
            </div>
        </div>
    </div>
    <!-- Scripts -->
    <script src="/js/jquery-3.3.1.min.js"></script>
    <script>
        $(function(){
            $("form#login-form").submit(function(){
                $("#login-form-wrapper").hide();
                $("#login-loader").show();
            });
        });
    </script>
</body>
</html>