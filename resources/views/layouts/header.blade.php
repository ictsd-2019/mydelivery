<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'My Delivery') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="/css/colorbox.css">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/indosat_style.css">
    <link rel="stylesheet" href="/css/summernote-bs4.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/bootstrap-chosen.css">
    <link rel="stylesheet" href="/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="/js/jquery.chosen/chosen.min.css">
    <link rel="stylesheet" href="/js/jquery.select2/select2.min.css">
    <link rel="stylesheet" href="/js/jquery.confirm/jquery-confirm.min.css">
    <style>
        .sidebar, h2:hover { cursor: pointer; }
    </style>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117545218-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-117545218-1');
    </script>
    <!-- Scripts -->

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            ]) !!};
        </script>
    </head>
    <body>
    <div id="app" class="container-fluid">
        <div>
            <div id="header" class="row">
                <div id="content-loader" style="display: none;">
                    <img src="/img/content-loader.gif" alt=""> <span class="random-load"></span>...
                </div>
                <div class="col-md-6">
                    <div id="logo" class="pull-left">
                        <a href="/"><img src="/img/indosat-logo.png" class="img-fluid" /></a>
                    </div>
                </div>
                <div class="col-md-6">
                    <h6 class="ml-3 text-right mt-3"><i class="fa fa-clock-o"></i>
                        <script>
                            var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                            var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum&#39;at', 'Sabtu'];
                            var date = new Date();
                            var day = date.getDate();
                            var month = date.getMonth();
                            var thisDay = date.getDay(),
                                    thisDay = myDays[thisDay];
                            var yy = date.getYear();
                            var year = (yy < 1000) ? yy + 1900 : yy;
                            document.write(thisDay + ", " + day + " " + months[month] + " " + year);
                        </script>
                        <span id="clock"></span>
                    </h6>

                    <ul class="m-0 p-0 dropdown user-menu pull-right pb-3">
                        <li class="pl-0 ml-0"><a href="/dashboard">Dashboard</a></li>
                        <li class="pl-0 ml-0"><span id="online-status"></span>
                            <a class="dropdown-toggle" data-toggle="dropdown">
                                Administrator<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li class="dropdown-item"><a href="#">Profile</a></li>
                                <li class="dropdown-item"><a href="#">Change Password</a></li>
                                <li role="separator" class="divider"></li>
                                <li class="dropdown-item">
                                    <a href="{{ url('/logout') }}"
                                       onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();"> Logout </a>
                                    <form id="logout-form"
                                          action="{{ url('/logout') }}"
                                          method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="body-wrapper">
            <div class="row no-gutters">