@section('configuration_tab')
    <h1 class="title">Configurations</h1>
    <ul class="nav nav-tabs">
        <li role="presentation" class="nav-item"><a class="nav-link {{ Request::path() == 'config' ? 'active' : '' }}" href="{{ route('config') }}">Main</a></li>
        <!-- <li role="presentation" class="nav-item"><a class="nav-link {{ Request::path() == 'config/menu' || Request::path() == 'config/menu/create' ? 'active' : '' }}" href="{{ route('menu.index') }}"> Menu Links</a></li> -->
        @if(Auth::user()->hasPermissionTo('manage user'))
            <li role="presentation" class="nav-item"><a class="nav-link {{ Request::path() == 'config/user' || Request::path() == 'config/user/create' ? 'active' : '' }}" href="{{ route('user.index') }}"> Users</a></li>
        @endif
        @if(Auth::user()->hasPermissionTo('manage role and permission'))
            <li role="presentation" class="nav-item"><a class="nav-link {{ Request::path() == 'config/role' || Request::path() == 'config/role/create' ? 'active' : '' }}" href="{{ route('role.index') }}"> Roles</a></li>
        @endif
    </ul>
@show