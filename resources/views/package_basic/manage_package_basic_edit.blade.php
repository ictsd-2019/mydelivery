@extends('layouts.app')

@section('content')
    <h1 class="title">Update Basic Package</h1>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
    @include('layouts.error')
    <br>
    {!! Form::open(['action' => ['PackageBasicController@update', $package->id], 'id' => 'edit-package', 'class' => 'form-horizontal']) !!}
        @include('package_basic.form_manage_edit_package_basic')
    {!! Form::close() !!}
@endsection
