<table id="example" class="table table-bordered table-hover dt-responsive text-center" style="width:100%">
    <thead>
        <tr>
            <th style="background:lightgrey; text-align:center; width:10%;">ID Package</th>
            <th style="background:lightgrey; text-align:center; width:40%;">Package</th>
            <th style="background:lightgrey; text-align:center; width:35%;">Price</th>
        </tr>
    </thead>
    <tbody>
    @foreach($package as $pck)
        <tr>
            <td> {{$pck->id}} </td>
            <td> {{$pck->package}} </td>
            <td> {{$pck->price}} </td>
        </tr>
    @endforeach
    </tbody>
</table>