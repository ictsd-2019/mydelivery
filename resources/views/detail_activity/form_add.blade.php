<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#" style="color:black;">DetailActivity Form</a>
            </li>
        </ul>
        <div class="tab-content">
           <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('main', 'Main Activity', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::select('main', $selected_main, array('placeholder' => '- Select Main -', 'required' => 'required'), ['class' => 'form-control mb-2 mt-1 placeholder-fix']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('ven', 'Vendor', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::select('ven', $selected_vendor, array('placeholder' => '- Select Vendor -', 'required' => 'required'), ['class' => 'form-control mb-2 mt-1 placeholder-fix']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('start', 'Start Date', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('start', null, ['class' => 'form-control mb-2 mt-1 placeholder-fix', 'required' => 'required', 'placeholder' => 'mm/dd/yy']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('end', 'End Date', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('end', null, ['class' => 'form-control mb-2 mt-1 placeholder-fix', 'required' => 'required', 'placeholder' => 'mm/dd/yy']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('detail', 'More Detail', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    <div class="table-responsive">  
                        <table class="table table-bordered" id="dynamic_field" style="margin-left: -9px">  
                            <tr>  
                                <td><input type="text" name="detail[]" placeholder="Detail Activity" class="form-control mb-2 mt-1 placeholder-fix" /></td>  
                                <td><input type="number" name="price[]" placeholder="Price Fee" class="form-control mb-2 mt-1 placeholder-fix" /></td>  
                                <td><button type="button" name="add" id="add" class="btn btn-success">Add</button></td>  
                            </tr>  
                        </table>  
                    </div>  
                </div>  
              </div>  
          </div>
        <br>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <strong><sup class="required">*</sup> Required</strong>
        <br/><br/>
        <a href="/detail" class="btn btn-secondary m-1">Cancel</a>
        <input type="submit" value="Save Data"  class="btn btn-primary m-1">
    </div>
</div>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  

<script>  
 $(document).ready(function(){  
      var i=1;  
      $('#add').click(function(){  
           i++;  
           $('#dynamic_field').append('<tr id="row'+i+'"><td><input type="text" name="detail[]" placeholder="Detail Activity" class="form-control name_list" /></td><td><input type="number" name="price[]" placeholder="Price fee" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">Delete</button></td></tr>');  
      });  
      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  
      
 });  
 </script>


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 <script type="text/javascript">
   $(function() {
     $( "#start" ).datepicker(
         {
          dateFormat: "yy/mm/dd"
         });
   });

   $(function() {
     $( "#end" ).datepicker(
         {
          dateFormat: "yy/mm/dd"
         });
   });
 </script>

 
