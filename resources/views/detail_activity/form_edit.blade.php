<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#" style="color:black;">Form Update DetailActivity</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('id', 'ID Main', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-4">
                    {!! Form::text('id', $detail->id, ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => 'NIK','readonly' => 'true']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('main', 'Main Activity', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                {!! Form::select('main', $selected_main, $detail->id_main, array('class' => 'form-control mb-2 mt-1 placeholder-fix', 'placeholder' => '- Select Main -', 'required' => 'required')) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('ven', 'Vendor', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                {!! Form::select('ven', $selected_vendor, $detail->id_vendor, array('class' => 'form-control mb-2 mt-1 placeholder-fix', 'placeholder' => '- Select Vendor -', 'required' => 'required')) !!}
                </div>
            </div>
         
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('detail', 'Detail', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('detail', $detail->detail, ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => ' Detail']) !!}
                </div>
            </div>

            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('price', 'Price fee', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::number('price', $detail->price, ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => ' Price fee']) !!}
                </div>
            </div>

            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('start', 'Start Date', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('start', date('m/d/Y', strtotime($detail->start)), ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => 'Start Date']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('end', 'End Date', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('end', date('m/d/Y', strtotime($detail->end)), ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => 'End Date']) !!}
                </div>
            </div>
        </div>
        <br>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <strong><sup class="required">*</sup> Required</strong>
        <br/><br/>
        <a href="/detail" class="btn btn-secondary m-1">Cancel</a>
        <input type="submit" value="Update Data"  class="btn btn-primary m-1">
    </div>
</div>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 <script type="text/javascript">
   $(function() {
     $( "#start" ).datepicker(
         {
          dateFormat: "yy/mm/dd"
         });
   });

   $(function() {
     $( "#end" ).datepicker(
         {
          dateFormat: "yy/mm/dd"
         });
   });
 </script>
