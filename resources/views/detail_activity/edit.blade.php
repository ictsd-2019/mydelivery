@extends('layouts.app')

@section('content')
    <h1 class="title">Manage Update DetailActivity</h1>
    @include('layouts.error')
    <br>
    {!! Form::open(['action' => ['DetailActivityController@update', $detail->id], 'id' => 'edit-detail', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
        @csrf
        @include('detail_activity.form_edit')
    {!! Form::close() !!}
@endsection
