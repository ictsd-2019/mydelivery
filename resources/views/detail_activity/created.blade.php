@extends('layouts.app')
@section('content')
    <h1 class="title">Add DetailActivity</h1>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
    @include('layouts.error')
    <br>
    {!! Form::open(['action' => ['DetailActivityController@store'], 'id' => 'create-detail', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
        @csrf
        @include('detail_activity.form_add')
    {!! Form::close() !!}
@endsection