<!-- Nav tabs -->
<ul id="gender" class="nav nav-tabs" role="tablist">
	<li role="presentation" class="nav-item active">
		<a class="nav-link" href="#gender" role="tab" data-toggle="tab">Device Select</a>
	</li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <div class="row">
        <div class="col-md-12">
            <ul style="padding:0;margin:0;">
            @foreach($devices as $device)
                <li class="radio text-center" style="list-style-type:none;float: right;;">
                    <label style="padding:10px;width: 160px;height: 120px;border:3px solid #c5c5c5;margin-right:5px;font-size:16px;">
                        <input type="radio" name="device" id="device" value="{{ $device['id'] }}"><br>
                        {{ $device->model }}
                    </label>
                </li>
            @endforeach
            </ul>
        </div>
    </div>
</div>