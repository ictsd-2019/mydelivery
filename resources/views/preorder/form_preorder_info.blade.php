<!-- Nav tabs -->
<ul id="gender" class="nav nav-tabs" role="tablist">
	<li role="presentation" class="nav-item active">
		<a class="nav-link" href="#gender" role="tab" data-toggle="tab">Pre-Order Info</a>
	</li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
	<div role="tabpanel" class="tab-pane active" id="gender">
        <div class="row form-group">
			<div class="col-md-4 col-sm-5 d-none d-md-block">
		    	{!! Form::label('order_date', 'Order Date', ['class' => 'col-form-label']) !!}
			</div>
			<div class="col-md-8 col-sm-7">
		        {!! Form::date('order_date', $current_date, ['class' => 'form-control', 'min' => $current_date]) !!} <sup class="mobile required">*</sup>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-md-4 col-sm-5 d-none d-md-block">
				{!! Form::label('quote_no', 'No. Quote', ['class' => 'col-form-label']) !!}
			</div>
			<div class="col-md-8 col-sm-7">
				{!! Form::text('quote_no', null, ['class' => 'form-control', 'placeholder' => 'No. Quote', 'required' => 'required']) !!} <sup class="mobile required">*</sup>
			</div>
		</div>
        <div class="row form-group">
			<div class="col-md-4 col-sm-5 d-none d-md-block">
				{!! Form::label('ba_no', 'No. BA', ['class' => 'col-form-label']) !!}
			</div>
			<div class="col-md-8 col-sm-7">
				{!! Form::text('ba_no', null, ['class' => 'form-control', 'placeholder' => 'No. BA', 'required' => 'required']) !!} <sup class="mobile required">*</sup>
			</div>
		</div>
        <div class="row form-group">
			<div class="col-md-4 col-sm-5 d-none d-md-block">
				{!! Form::label('msisdn', 'MSISDN', ['class' => 'col-form-label']) !!}
			</div>
			<div class="col-md-8 col-sm-7">
				{!! Form::text('msisdn', null, ['class' => 'form-control', 'placeholder' => 'MSISDN', 'required' => 'required']) !!} <sup class="mobile required">*</sup>
			</div>
		</div>
        <div class="row form-group">
            <div class="col-md-4 col-sm-5 d-none d-md-block">
				<label>Sales Agent</label>
			</div>
			<div class="col-md-8 col-sm-7">
                    {!! Form::select('sales_name', $sales_agent, null, array('class'=>'form-control', 'placeholder' => '- Sales Agent -')) !!}
			</div>
		</div>
        <div class="row form-group">
			<div class="col-md-4 col-sm-5 d-none d-md-block">
		    	{!! Form::label('type_req', 'Request Type', ['class' => 'col-form-label']) !!}
			</div>
			<div class="col-md-8 col-sm-7">
		        {!! Form::radio('type_req', 'Bundling', ['class' => 'form-control']) !!} Bundling
                {!! Form::radio('type_req', 'SIM Only', ['class' => 'form-control']) !!} SIM Only
			</div>
		</div>
        <div class="row form-group">
			<div class="col-md-4 col-sm-5 d-none d-md-block">
		    	{!! Form::label('type_bundling', 'Bundling Type', ['class' => 'col-form-label']) !!}
			</div>
			<div class="col-md-8 col-sm-7">
		        {!! Form::radio('type_bundling', 'Prepaid', ['class' => 'form-control']) !!} Pre-paid
                {!! Form::radio('type_bundling', 'Postpaid', ['class' => 'form-control']) !!} Post-paid
			</div>
		</div>
        <div class="row form-group">
			<div class="col-md-4 col-sm-5 d-none d-md-block">
		    	{!! Form::label('type_prepaid', 'Prepaid Type', ['class' => 'col-form-label']) !!}
			</div>
			<div class="col-md-8 col-sm-7">
		        {!! Form::radio('type_prepaid', 'Unmanaged', ['class' => 'form-control']) !!} Unmanaged
                {!! Form::radio('type_prepaid', 'Business Partner', ['class' => 'form-control']) !!} Business Partner
			</div>
		</div>
        <div class="row form-group">
            <div class="col-md-4 col-sm-5 d-none d-md-block">
				<label>SIM Type</label>
			</div>
			<div class="col-md-8 col-sm-7">
				{!! Form::select('type_sim', [
                    '' => '- Select SIM Type -',
                    'DCP MIM' => 'DCP MIM',
                    'DCP Non MIM' => 'DCP Non MIM',
                    'Non DCP MIM' => 'Non DCP MIM',
                    'Non DCP Non MIM' => 'Non DCP Non MIM',
                    'Non DCP' => 'Non DCP',
                    'DCP' => 'DCP',
                    'MIM NON DCP' => 'MIM NON DCP',

                    'Combo' => 'Combo',
                    'Multi' => 'Multi',
                    'Reguler' => 'Reguler',
                    'Replacement HLR' => 'Replacement HLR',
                    'Mikro' => 'Mikro',
                    'Makro' => 'Makro',

                    'Pintar Freedom (SC-611) - 25K' => 'Pintar Freedom (SC-611) - 25K',
                    'Pintar Khusus Luar Jawa (SC-610) - 5K' => 'Pintar Khusus Luar Jawa (SC-610) - 5K',
                    'Bluebird (SC-3880) - 294K' => 'Bluebird (SC-3880) - 294K',
                    'Pintar 2 (SC-570) - 3,4K' => 'Pintar 2 (SC-570) - 3,4K',
                    'BTPN (SC-576) - 3,4K' => 'BTPN (SC-576) - 3,4K',
                    'Pintar Prime (SC-635) - 49K' => 'Pintar Prime (SC-635) - 49K',
                    'Pintar Pro Freedom Biz (SC-641) - 45K' => 'Pintar Pro Freedom Biz (SC-641) - 45K',
                    'Pintar Pro Freedom Biz (SC-639) - 25K' => 'Pintar Pro Freedom Biz (SC-639) - 25K',
                    'Pintar Pro Freedom Biz (SC-642) - 90K' => 'Pintar Pro Freedom Biz (SC-642) - 90K'
                    ], null, ['class' => 'form-control']) !!}
			</div>
		</div>
        <div class="row form-group">
			<div class="col-md-4 col-sm-6 col-xs-4">
				{!! Form::label('attachment', 'Attachment', ['class' => 'col-form-label']) !!}
			</div>
			<div class="col-md-8 col-sm-6 col-xs-8">
				{!! Form::file('attachment', null, ['class' => 'form-control', 'required' => 'required']) !!}
			</div>
		</div>
	</div>
</div>