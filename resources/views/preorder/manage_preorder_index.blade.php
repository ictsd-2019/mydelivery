@extends('layouts.app')

@section('sidebar')
    @parent
@endsection

@section('content')
    <div class="btn-group pull-right" role="group">
        <a href="{{ route('preorder.manage.create') }}" type="button" class="btn btn-success">New Pre-Order</a>
    </div>
    <h1 class="title">Manage Pre-Order</h1>
    <br><br>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a href="#" class="close" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
    <table class="table table-striped table-sm table-hover">
        <thead>
        <tr>
            <th>No.</th>
            <th>Title</th>
            <th># Device</th>
            <th>Quota</th>
            <th>Booked</th>
            <th>Paid</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            @foreach($preorders as $preorder)
                <tr>
                    <td>{{ (($preorders->currentPage() - 1) * $preorders->perPage()) + $loop->iteration }}</td>
                    <td>{{ $preorder->title }}</td>
                    <td>{{ $preorder->devices->count() }}</td>
                    <td>{{ $preorder->devices->sum('quota') }}</td>
                    <td>{{ $preorder->transactions->where('status',1)->count() }}</td>
                    <td>{{ $preorder->transactions->where('status',2)->count() }}</td>
                    <td>{{ \Carbon\Carbon::parse($preorder->startdate)->format('Y-m-d h:i') }}</td>
                    <td>{{ \Carbon\Carbon::parse($preorder->enddate)->format('Y-m-d h:i') }}</td>
                    <td>
                        <a href="{{ route('preorder.manage.show', ['preorder_id' => $preorder->id]) }}" class="btn btn-info btn-sm">View</a>
                        <a href="{{ route('preorder.manage.edit', ['preorder_id' => $preorder->id]) }}" class="btn btn-warning btn-sm">Edit</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
