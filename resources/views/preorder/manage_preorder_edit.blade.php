@extends('layouts.app')

@section('sidebar')
    @parent
@endsection

@section('content')
    <h1 class="title">Edit Pre-Order</h1>
    @include('layouts.error')
    <br>
    {!! Form::model($preorder, ['action' => ['PreOrderController@update', $preorder->id],'id' => 'edit-preorder','class' => 'form-horizontal', 'method' => 'PATCH']) !!}
        @include('preorder.form_manage_preorder')
    {!! Form::close() !!}
@endsection
