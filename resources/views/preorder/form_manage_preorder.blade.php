<div class="row">
    <div class="col-md-6">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" href="#">Pre-Order Info</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row">
                <div class="col-md-3">
                    {!! Form::label('title', 'Title', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-9">
                    {!! Form::text('title', null, ['class' => 'form-control mb-2 mt-1', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    {!! Form::label('startdate', 'Start Date', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-9 form-inline">
                    {!! Form::text('startdate', null, ['class' => 'form-control mb-2 mt-1 p-1 datepicker', 'style' => 'padding: 4px !important', 'required' => 'required']) !!}
                    {!! Form::time('starttime', null, ['class' => 'form-control mb-2 mt-1', 'style' => 'padding: 3px;']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    {!! Form::label('enddate', 'End Date', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-9 form-inline">
                    {!! Form::text('enddate', null, ['class' => 'form-control mb-2 mt-1 datepicker', 'required' => 'required']) !!}
                    {!! Form::time('endtime', null, ['class' => 'form-control mb-2 mt-1', 'style' => 'padding: 3px;']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    {!! Form::label('max_order', 'Max. Order', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-9">
                    {!! Form::number('max_order', 0, ['class' => 'form-control mb-2 mt-1 col-md-2', 'min' => '0']) !!}
                </div>
            </div>
        </div>
        <br>
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" href="#">Bank Promo</a>
            </li>
        </ul>
        <div class="tab-content">
            <table id="preorder-bank-promo-table" class="table table-sm">
                <thead>
                <tr>
                    <th width="70%">Bank</th>
                    <th>Value</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    @if(isset($bank_promos))
                        @foreach($bank_promos as $promo_count => $promo)
                            <tr>
                                <td>{{ $promo->bank['name'] }}</td>
                                <td><input type="text" name="bank_promos[{{ $promo_count }}][value]" value="{{ $promo->value }}" class="form-control"></td>
                                <td>
                                    <input type="hidden" name="bank_promos[{{ $promo_count }}][id]" value="{{ $promo->id }}">
                                    <input type="hidden" name="bank_promos[{{ $promo_count }}][bank_id]" value="{{ $promo->bank_id }}">
                                    <a href="#" class="delete-preorder-bank-promo text-danger d-block mt-2"><i class="fa fa-minus-circle"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        <script>
                            window.promoCount = {{ $bank_promos->count() + 1 }};
                        </script>
                    @else
                        <script>
                            window.promoCount = 1;
                        </script>
                    @endif
                </tbody>
            </table>
            <div class="row preorder-bank-promo">
                <div class="col-md-12 form-inline">
                    {!! Form::select('bank_id', $banks, null, ['class' => 'form-control', 'style' => 'width: 70%;']) !!}
                    {!! Form::number('bank_value', null, ['class' => 'form-control', 'style' => 'width:30%;']) !!}
                </div>
            </div>
            <a href="#" class="btn btn-success add-preorder-bank-promo">Add Bank Promo</a>
        </div>
    </div>
    <div class="col-md-6">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" href="#">Device Info</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row mb-2">
                <div class="col-md-9"><strong>Device Model</strong></div>
                <div class="col-md-2"><strong>Quota</strong></div>
            </div>
            @if(isset($preorder->devices))
                @foreach($preorder->devices as $id => $device)
                    <div class="row preorder-device">
                        <div class="col-md-12 form-inline">
                            {!! Form::hidden('devices['. $id .'][id]', $device['id']) !!}
                            {!! Form::text('devices['. $id .'][model]', $device['model'], ['class' => 'form-control col-md-8','Placeholder' => 'Device Model', 'required' => 'required']) !!}
                            {!! Form::number('devices['. $id .'][quota]', $device['quota'], ['class' => 'form-control col-md-3','Placeholder' => 'Quota', 'required' => 'required']) !!}
                            <div class="col-md-1">
                                <a href="#" class="delete-preorder-device text-danger col-md-8" d-block><i class="fa fa-minus-circle"></i></a>
                            </div>
                        </div>
                    </div>
                @endforeach
                <script>
                    window.deviceCount = {{ $preorder->devices->count() + 1 }};
                </script>
            @else
                <div class="row preorder-device">
                    <div class="col-md-12 form-inline">
                        {!! Form::text('devices[0][model]', null, ['class' => 'form-control col-md-9','Placeholder' => 'Device Model', 'required' => 'required']) !!}
                        {!! Form::number('devices[0][quota]', null, ['class' => 'form-control col-md-3','Placeholder' => 'Quota', 'required' => 'required']) !!}
                    </div>
                </div>
                <script>
                    window.deviceCount = 1;
                </script>
            @endif

            <div id="preorder-device-list">
            </div>

            <a href="#" class="btn btn-success add-preorder-device">Add Device</a>
        </div>
        <br>
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" href="#">Plan Info</a>
            </li>
        </ul>
        <div class="tab-content">
            <table id="preorder-plan-table" class="table table-sm">
                <thead>
                <tr>
                    <th width="70%">Plan</th>
                    <th>Periode</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if(isset($active_plans))
                    @foreach($active_plans as $plan_id => $plan)
                        <tr>
                            <td>{{ $plan->plan['name'] }}</td>
                            <td>{{ $plan->periode }} month</td>
                            <td>
                                <input type="hidden" name="plans[{{ $plan_id }}][id]" value="{{ $plan->id }}">
                                <input type="hidden" name="plans[{{ $plan_id }}][plan_id]" value="{{ $plan->plan_id }}">
                                <input type="hidden" name="plans[{{ $plan_id }}][periode]" value="{{ $plan->periode }}">
                                <a href="#" class="delete-preorder-plan text-danger"><i class="fa fa-minus-circle"></i></a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            <div class="row preorder-plan">
                <div class="col-md-12 form-inline">
                    {!! Form::select('plan_id', $plans, null, ['class' => 'form-control', 'style' => 'width: 70%;']) !!}
                    {!! Form::select('plan_periode', ["12" => "12", "24" => "24"], null, ['class' => 'form-control', 'style' => 'width:30%;']) !!}
                </div>
            </div>
            <a href="#" class="btn btn-success add-preorder-plan">Add Plan</a>
        </div>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-12">
        <a href="{{ route('preorder.manage.index') }}" class="btn btn-secondary mb-2">Cancel</a>
        <input type="submit" value="Submit"  class="btn btn-primary mb-2">
    </div>
</div>