@extends('layouts.frontpage_layout')

@section('sidebar')
    @parent
@endsection

@section('content')
    <h1 class="title">Device Pre-Order</h1>

    <div class="row">
        @if($preorders->count() > 0)
            @foreach($preorders as $preorder)
                <div class="col-md-4">
                    <div class="card card-default">
                        <div class="card-header">{{ $preorder->title }}</div>
                        <div class="card-body">
                            Open Date : <strong>{{ \Carbon\Carbon::parse($preorder->startdate)->format('Y-m-d h:i') }}</strong><br>
                            Close Date : <strong>{{ \Carbon\Carbon::parse($preorder->enddate)->format('Y-m-d h:i') }}</strong><br>
                            Device Quota: <strong>{{ $preorder->devices->sum('quota') }}</strong> devices <br>
                            Current Booked : <strong>{{ $preorder->transactions->count() }}</strong> devices
                            <br><br>
                            @if(\Carbon\Carbon::parse($preorder->startdate)->gt($today))
                                <a href="{{ route('preorder.show', ['preorder_code' => $preorder->preorder_code]) }}" class="btn btn-warning">
                                    Open in {{ \Carbon\Carbon::parse($preorder->startdate)->format('M dS') }}
                                </a>
                            @else
                                <a href="{{ route('preorder.show', ['preorder_code' => $preorder->preorder_code]) }}" class="btn btn-success">Opening Now</a>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <p>No Pre-Order seasons open.</p>
        @endif
    </div>
@endsection
