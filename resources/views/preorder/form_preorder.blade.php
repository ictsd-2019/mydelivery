<style>
    form .row {
        margin-bottom:5px;
    }
</style>
<div class="row">
    <div class="col-md-6">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" href="#">Customer Info</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row">
                <div class="col-md-3">
                    {!! Form::label('corp_name', 'Corp. Name', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-9">
                    {!! Form::text('corp_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    {!! Form::label('pic_name', 'PIC', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-9">
                    {!! Form::text('pic_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    {!! Form::label('handphone', 'No. Handphone', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-9">
                    {!! Form::text('handphone', null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
            </div>
        </div>
        <br>
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" href="#">Pre-Order Info</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row">
                <div class="col-md-3">
                    {!! Form::label('order_date', 'Order Date', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-9">
                    {!! Form::date('order_date', null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    {!! Form::label('no_quote', 'No. Quote', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-9">
                    {!! Form::text('no_quote', null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    {!! Form::label('no_ba', 'NO. BA', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-9">
                    {!! Form::text('no_ba', null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    {!! Form::label('msisdn', 'MSISDN', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-9">
                    {!! Form::text('msisdn', null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    {!! Form::label('sales_agent', 'Sales Agent', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-9">
                    {!! Form::select('sales_agent', $periode, null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    {!! Form::label('type_req', 'Request Type', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-9" style="margin-top:5px;">
                    {!! Form::radio('type_req', 'Bundling', ['class' => 'form-control']) !!} Bundling &nbsp
                    {!! Form::radio('type_req', 'SIM Only', ['class' => 'form-control']) !!} SIM Only
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    {!! Form::label('type_bundling', 'Bundling Type', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-9" style="margin-top:5px;">
                    {!! Form::radio('type_bundling', 'Pre-paid', ['class' => 'form-control']) !!} Pre-paid &nbsp
                    {!! Form::radio('type_bundling', 'Post-paid', ['class' => 'form-control']) !!} Post-paid
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    {!! Form::label('type_prepaid', 'Prepaid Type', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-9" style="margin-top:5px;">
                    {!! Form::radio('type_prepaid', 'Unmanaged', ['class' => 'form-control']) !!} Unmanaged &nbsp
                    {!! Form::radio('type_prepaid', 'Business Partner', ['class' => 'form-control']) !!} Business Partner
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    {!! Form::label('type_sim', 'SIM Type', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-9">
                    {!! Form::select('type_sim', $periode, null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    {!! Form::label('attachment', 'Attachment', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-9">
                    {!! Form::file('attachment', null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" href="#">Device Select</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row">
                <div class="col-md-12">
                    <ul style="padding:0;margin:0;">
                    @foreach($devices as $device)
                        <li class="radio text-center" style="list-style-type:none;float: right;;">
                            <label style="padding:10px;width: 160px;height: 120px;border:3px solid #c5c5c5;margin-right:5px;font-size:16px;">
                                <input type="radio" name="device" id="device" value="{{ $device['id'] }}"><br>
                                {{ $device->model }}
                            </label>
                        </li>
                    @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <br>
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" href="#">Customer Info</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row">
                <div class="col-md-4">
                    {!! Form::label('basic_pack', 'Basic Package', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    {!! Form::select('basic_pack', $periode, null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    {!! Form::label('add_pack', 'Add On Package', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::select('package_add[]', ['' => '- Add On Package -', 'DS New Acquisition' => 'FREE GPS', 'DS Migration' => 'Pro Freedom Silver'], null, ['class' => 'form-control']) !!}
                </div>
                <div class="col-md-2">
                    <button class="btn btn-secondary" type="button" name="add" id="add">+</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    {!! Form::label('type_pay', 'Payment Type', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8" style="margin-top:5px;">
                    {!! Form::radio('type_pay', 'Cash', ['class' => 'form-control']) !!} Cash &nbsp
                    {!! Form::radio('type_pay', 'Installment', ['class' => 'form-control']) !!} Installment
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    {!! Form::label('tenor', 'Tenor Installment', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    {!! Form::select('tenor', $periode, null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
            </div>
        </div>
        <br>
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" href="#">Pricing</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row">
                <div class="col-md-3">
                    {!! Form::label('device_only', 'Device Only', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-9">
                    {!! Form::text('device_only', null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    {!! Form::label('mrc', 'MRC/Device Month', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-9">
                    {!! Form::text('mrc', null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    {!! Form::label('total', 'Total Price', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-9">
                    {!! Form::text('total', null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-12">
        {!! Form::hidden('preorder_code', $preorder_code) !!}
        <a href="{{ route('preorder.show', ['preorder_code' => $preorder_code]) }}" class="btn btn-secondary">Cancel</a>
        <input type="submit" value="Submit"  class="btn btn-primary">
    </div>
</div>

@section('scripts')
    @parent
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
    <script>
    $( document ).ready(function() {
        var postURL = "<?php echo url('addmore'); ?>";
        console.log(postURL);
        var i=1;  
        $('#add').click(function(){  
            i++;
            $('#dynamic_field').append('<div class="col-md-6 col-sm-7 dynamic-added" id="row'+i+'">{!! Form::select('package_add[]', ['' => '- Add On Package -', 'DS New Acquisition' => 'FREE GPS', 'DS Migration' => 'Pro Freedom Silver'], null, ['class' => 'form-control']) !!}</div>');
        });
    });
    </script>
    <script>
        $(function(){
            $('input[name=device]').click(function(){
                $('input[name=device]').parent('label').css('border','3px solid #c5c5c5');
                $('input[name=device]').parent('label').css('color','#000');
                $(this).parent('label').css('border','3px solid #87b713');
                $(this).parent('label').css('color','#3d5404');
            });
        });
    </script>
@endsection