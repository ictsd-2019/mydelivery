@extends('layouts.frontpage_layout')

@section('sidebar')
    @parent
@endsection

@section('content')
    <style>
        .progress {
            height: 5px;
            margin: 5px;
        }
    </style>
    <h1 class="title">{{ $preorder->title }} : Create Pre-Order</h1>
    <div class="row">
        <div class="col-md-4">
            <div class="preorder-countdown" style="margin-top:30px;">
                <span id="clock"></span>
            </div>
        </div>
        <div class="col-md-8">
            <ul>
                @foreach($preorder->devices as $device)
                    <li class="preorder-device-status-block pull-right" style="margin:5px;display: none;width: 200px;list-style-type: none;">
                        <h3 class="text-center" style="margin:0;font-weight: bold;"><span id="device-ordered-{{$device['id'] }}"></span>/<span id="device-quota-{{ $device['id'] }}"></span> </h3>
                        <div class="progress">
                            <div id="progress-device-{{ $device['id'] }}" class="progress-bar bg-success" role="progressbar" style="width: 0%">
                                <span class="sr-only">0% Complete (success)</span>
                            </div>
                        </div>
                        <div id="device-model-{{ $device['id'] }}" class="text-center"></div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a href="#" class="close" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
    @include('layouts.error')
    <br>
    {!! Form::open(['action' => ['PreOrderTransactionController@store'], 'id' => 'add-preorder', 'class' => 'form-horizontal']) !!}
        <div class="row">
            <div class="col-md-6 col-sm-6">
                @include('preorder.form_customer_info')
                <br/>
                @include('preorder.form_preorder_info')
                <br/>
            </div>  
            <div class="col-md-6 col-sm-6">
                @include('preorder.form_device')
                <br/>
                @include('preorder.form_package_info')
                <br/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <strong><sup class="required">*</sup><span class="glyphicon glyphicon-asterisk"></span></sup> Required</strong><br/>
                <br/><br/>
                <center>
                <a href="/preorder" class="btn btn-secondary m-1">CANCEL</a>
                <input type="submit" value="SUBMIT" class="btn btn-primary m-1">
                </center>
            </div>
        </div>
    {!! Form::close() !!}
@endsection


@section('scripts')
    @parent

    <script>
        $(function(){
            @if($preorder_status == "waiting")
                $('#clock').countdown('{{ $preorder->startdate }}', function(event) {

                if(event.strftime('%-D') == 0)
                {
                    $(this).html("Dibuka dalam <br/> <h1>"+ event.strftime('%H:%M:%S') +"</h1>");
                }
                else
                {
                    $(this).html("Dibuka dalam<br/> <h1>"+ event.strftime('%-D hari %H:%M:%S') +"</h1>");
                }
            });
            @elseif($preorder_status == "running")
                $('#clock').countdown('{{ $preorder->enddate }}', function(event) {
                    if(event.strftime('%-D') == 0)
                    {
                        $(this).html("Pre-Order tutup :<br/> <h1>"+ event.strftime('%H:%M:%S') +"</h1>");
                    }
                    else
                    {
                        $(this).html("Pre-Order tutup :<br/> <h1>"+ event.strftime('%-D hari %H:%M:%S') +"</h1>");
                    }
                }).on('finish.countdown', function(){
                    window.location.replace("{{ route('preorder.show', ['preorder_code' => $preorder_code]) }}");
                });
            @else
                window.location.replace("{{ route('preorder.show', ['preorder_code' => $preorder_code]) }}");
            @endif

            $.getJSON('/api/preorder/{{ $preorder->id }}/devices-status', function(data) {

                for (var key in data)
                {
                    var ordered = data[key].booked + data[key].paid;
                    var quotaRemain = data[key].quota - ordered;
                    var progressPercentage = (quotaRemain/data[key].quota)*100;

                    $('#device-ordered-'+ data[key].id).text(quotaRemain);
                    $('#device-quota-'+ data[key].id).text(data[key].quota);
                    $('#progress-device-'+ data[key].id).css('width', progressPercentage +'%');
                    $('#device-model-'+ data[key].id).text(data[key].model);
                    $('.preorder-device-status-block').show();
                }
            });

            setInterval(function(){
                $.getJSON('/api/preorder/{{ $preorder->id }}/devices-status', function(data) {

                    for (var key in data)
                    {
                        var ordered = data[key].booked + data[key].paid;
                        var quotaRemain = data[key].quota - ordered;
                        var progressPercentage = (quotaRemain/data.quota)*100;

                        $('#device-ordered-'+ data[key].id).text(quotaRemain);
                        $('#device-quota-'+ data[key].id).text(data.quota);
                        $('#progress-device-'+ data[key].id).css('width', progressPercentage +'%');
                        $('#device-model-'+ data[key].id).text(data.model);
                        $('.preorder-device-status-block').show();
                    }
                });
            }, 10000);
        });
    </script>
@endsection