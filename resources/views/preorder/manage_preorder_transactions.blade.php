@extends('layouts.app')

@section('sidebar')
    @parent
@endsection

@section('content')
    <style>
        .preorder-countdown-admin #countdown {
            font-size: 14px  !important;
        }

        #countdown h1 {
            font-size: 24px;
            text-shadow: 0px 2px 2px rgba(0,0,0,0.2);
        }
        #total-booked h1 {
            font-size: 24px;
            text-shadow: 0px 2px 2px rgba(0,0,0,0.2);
        }
        #total-paid h1 {
            font-size: 24px;
            text-shadow: 0px 2px 2px rgba(0,0,0,0.2);
        }
    </style>
    <h1 class="title">Detail Pre-Order : <span class="text-muted"> {{ $preorder['title'] }}</span></h1>
    <div id="preorder-status-info" class="row">
        <div class="col-md-3"></div>
        <div class="col-md-2">
            <div class="preorder-countdown-admin text-center">
                <span id="countdown"></span>
            </div>
        </div>
        <div class="col-md-2">
            <div id="total-booked" class="text-center">Total Booked :<br>
                <h1>{{ $total_booked }}</h1></div>
        </div>
        <div class="col-md-2">
            <div id="total-paid" class="text-center">Total Paid :<br>
                <h1>{{ $total_paid }}</h1></div>
        </div>
        <div class="col-md-3"></div>
    </div>
    <ul class="row justify-content-md-center">
        @foreach($preorder->devices as $device)
            <div class="col-md-2">
                <div id="device{{ $device['id'] }}" style="width: 100%;height: 150px;"></div>
                <div class="text-center">{{ $device['model'] }}</div>
            </div>
        @endforeach
    </ul>
    <br>
    <ul class="nav nav-tabs">
        <li role="presentation" class="nav-item"><a class="nav-link" href="{{ route('preorder.manage.show', ['preorder_id' => $preorder['id']]) }}">Pre-Order Summary</a></li>
        <li role="presentation" class="nav-item"><a class="nav-link active" href="#">Transactions Report</a></li>
        <li role="presentation" class="nav-item"><a class="nav-link" href="{{ route('preorder.manage.store_report', ['preorder_id' => $preorder->id]) }}">Store Report</a></li>
    </ul>
    <div class="pre-order-tab">
        <br>
        <form id="search-preorder-transaction" class="form-inline">
            <div class="form-group">
                <input type="text" name="startdate" class="form-control datepicker m-1" style="width: 100px;" value="{{ $startdate }}" placeholder="From"/>
            </div>
            <div class="form-group">
                <input type="text" name="enddate" class="form-control m-1 datepicker" style="width: 100px;" value="{{ $enddate }}" placeholder="To"/>
            </div>
            <div class="form-group">
                <input type="text" name="reg_code" class="form-control m-1" value="{{ $reg_code }}" placeholder="Registration Code"/>
            </div>
            <div class="form-group">
                <input type="text" name="customer" class="form-control m-1" value="{{ $customer }}" placeholder="Pelanggan"/>
            </div>
            <div class="form-group">
                <input type="text" name="msisdn" class="form-control m-1" value="{{ $msisdn }}" placeholder="MSISDN"/>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary m-1" value="Search" />
                <a href="{{ route('preorder.manage.transactions', ['preorder_id' => $preorder->id]) }}" class="btn btn-secondary m-1">Reset</a>
            </div>
        </form>
        @if (session('message'))
            <div class="mt-2 alert alert-{{ session('status') }}">
                <a href="#" class="close" data-dismiss="alert"
                   aria-label="close">&times;</a> {{ session('message') }}
            </div>
        @endif
        <div class="table-wrapper mt-3">
            <table class="table table-sm table-striped table-hover">
                <tr><th>No.</th>
                    <th width="140">Transaction Date</th>
                    <th>Pre-Order Code</th>
                    <th>Customer Name</th>
                    <th>MSISDN</th>
                    <th>Device Model</th>
                    <th width="120">Plan</th>
                    <th>Installment</th>
                    <th>Store</th>
                    <th>Payment Method</th>
                    <th>Payment Type</th>
                    <th>Invoice Number</th>
                    <th>Bank Promo</th>
                    <th>Status</th>
                </tr>
                @foreach($transactions as $tran)
                    <tr>
                        <td>{{ (($transactions->currentPage() - 1) * $transactions->perPage()) + $loop->iteration }}</td>
                        <td>{{ $tran->created_at }}</td>
                        <td><stront>{{ $tran->reg_code }}</stront></td>
                        <td>{{ $tran->customer_name }}</td>
                        <td>{{ $tran->msisdn }}</td>
                        <td>{{ $tran->device['model'] }}</td>
                        <td>{{ $tran->plan['name'] }}</td>
                        <td>{{ $tran->periode }} month</td>
                        <td>{{ $tran->store['siebel_name'] }}</td>
                        <td>
                            @if($tran->payment_method == "cash")
                                Cash
                            @elseif($tran->payment_method == "debit")
                                Debit Card
                            @else
                                Credit Card
                            @endif
                        </td>
                        <td>{{ $tran->payment_type }}</td>
                        <td>{{ $tran->invoice_number }}</td>
                        <td>{{ isset($tran->bank_promo->bank) ? $tran->bank_promo->bank['name'] : "" }}
                            {{ isset($tran->bank_promo->bank) ? "Rp ". $tran->bank_promo['value'] : "" }}</td>
                        <td>
                            @if($tran->status == 1)
                                <div href="#restore-transaction" class="btn btn-warning btn-sm restore-trans"
                                     data-toggle="modal"
                                     data-preorder-code="{{ $tran->preorder_code }}"
                                     data-reg-code="{{$tran->reg_code}}"
                                     data-reg-id="{{ $tran->id }}"
                                     data-reg-url="{{ url()->full()}}"
                                     data-cust-name="{{ $tran->customer_name }}"
                                     data-cust-id-number="{{ $tran->id_number }}"
                                     data-device="{{ $tran->device->model }}"
                                     data-reg-msisdn="{{ $tran->msisdn }}">Booked</div>
                            @elseif($tran->status == 2)
                                <div href="#restore-transaction" class="btn btn-success btn-sm restore-trans"
                                     data-toggle="modal"
                                     data-preorder-code="{{ $tran->preorder_code }}"
                                     data-reg-code="{{$tran->reg_code}}"
                                     data-reg-id="{{ $tran->id }}"
                                     data-reg-url="{{ url()->full()}}"
                                     data-cust-name="{{ $tran->customer_name }}"
                                     data-cust-id-number="{{ $tran->id_number }}"
                                     data-device="{{ $tran->device->model }}"
                                     data-reg-msisdn="{{ $tran->msisdn }}">Paid</div>
                            @else
                                <div href="#restore-transaction" class="btn btn-secondary btn-sm restore-trans"
                                     data-toggle="modal"
                                     data-preorder-code="{{ $tran->preorder_code }}"
                                     data-reg-code="{{$tran->reg_code}}"
                                     data-reg-id="{{ $tran->id }}"
                                     data-reg-url="{{ url()->full()}}"
                                     data-cust-name="{{ $tran->customer_name }}"
                                     data-cust-id-number="{{ $tran->id_number }}"
                                     data-device="{{ $tran->device->model }}"
                                     data-reg-msisdn="{{ $tran->msisdn }}">Canceled</div>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>

        Found : <strong>{{ $transactions->total() }}</strong> transactions<br/>
        <a href="{{ Request::fullUrlWithQuery(["download"=>"xls"]) }}" class="btn btn-success btn-sm pull-right"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Download</a>
        <br>
        {{ $transactions->appends([
                'reg_code' => $reg_code,
                'customer' => $customer,
                'msisdn' => $msisdn
                ])->links('vendor.pagination.bootstrap-4') }}
    </div>
    <a href="{{ route('preorder.manage.index') }}" class="btn btn-outline-secondary">Back</a>

    <div id="restore-transaction" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background: #ffe000;color: #3a3a3a;">
                    <h4 class="modal-title">Update Customer Transaction</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" style="background-color: #fbfbfb;">
                    {!! Form::open(['action' => ['PreOrderTransactionController@restore_trans'], 'class' => 'form-horizontal']) !!}
                    <table class="table table-sm table-bordered">
                        <tr><th width="150">Reg Code</th><td><strong><span id="display-reg-code"></span></strong></td></tr>
                        <tr><th width="150">Customer</th><td><span id="display-customer-name"></span> </td></tr>
                        <tr><th>MSISDN</th><td><span id="display-msisdn"></span> </td></tr>
                        <tr><th>Device</th><td><span id="display-device"></span> </td></tr>
                        <tr><th>Status</th><td>{{ Form::select('status', ['1' => 'Booked','2' => 'Paid', '3' => 'Cancel'], null, ['class' => 'form-control','placeholder' => '- Select New Status -']) }}</td></tr>
                    </table>
                    <div class="row" style="margin-bottom: 15px;">
                        <div class="col-md-9 form-inline">
                            {!! Form::hidden('url') !!}
                            {!! Form::hidden('reg_code') !!}
                            {!! Form::hidden('reg_id') !!}
                            {!! Form::submit('Update', ['class' => 'btn btn-success mr-2', 'style' => 'border-radius:0;']) !!}
                            <a href="#" data-dismiss="modal" class="btn btn-default" style="border-radius: 0;">Cancel</a>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script src="/js/highcharts.js"></script>
    <script src="/js/highcharts-more.js"></script>
    <script src="/js/solid-gauge.js"></script>
    <script src="/js/jquery.countdown/jquery.countdown.min.js"></script>

    <script>
        $(function(){

            $(".restore-trans").click(function (e) {
                var customerName = $(this).data('cust-name');
                var msisdn = $(this).data('reg-msisdn');
                var device = $(this).data('device');
                var url = $(this).data('reg-url');
                var idReg = $(this).data('reg-id');
                var regCode = $(this).data('reg-code');

                $("#display-customer-name").text(customerName);
                $("#display-msisdn").text(msisdn);
                $("#display-device").text(device);
                $("#display-reg-code").text(regCode);
                $("input[name=reg_id]").val(idReg);
                $("input[name=reg_code]").val(regCode);
                $("input[name=url]").val(url);
            });

            @if($preorder_status == "waiting")
                $('#countdown').countdown('{{ $preorder->startdate }}', function(event) {

                    if(event.strftime('%-D') == 0)
                    {
                        $(this).html("Open in <br/> <h1>"+ event.strftime('%H:%M:%S') +"</h1>");
                    }
                    else
                    {
                        $(this).html("Open in<br/> <h1>"+ event.strftime('%-D hari %H:%M:%S') +"</h1>");
                    }
                }).on('finish.countdown', function(){
                    location.reload();
                });
            @elseif($preorder_status == "running")
                $('#countdown').countdown('{{ $preorder->enddate }}', function(event) {
                    if(event.strftime('%-D') == 0)
                    {
                        $(this).html("Pre-Order close :<br/> <h1>"+ event.strftime('%H:%M:%S') +"</h1>");
                    }
                    else
                    {
                        $(this).html("Pre-Order close :<br/> <h1>"+ event.strftime('%-D hari %H:%M:%S') +"</h1>");
                    }
                }).on('finish.countdown', function(){
                    location.reload();
                });
            @else
                $('#countdown').html("Pre-Order sudah ditutup.");
            @endif

            var chartOptions = {

                'chart': {
                    'type': 'solidgauge'
                },
                credits: {enabled: false},
                title: null,
                'tooltip': {
                    'enabled': false
                },
                'pane': {
                    'center': ['50%', '50%'],
                    'startAngle': 0,
                    'endAngle': 360,
                    'background': {
                        'backgroundColor': '#EEE',
                        'innerRadius': '90%',
                        'outerRadius': '100%',
                        'borderWidth': 0
                    }
                },
                'yAxis': {
                    'min': 0,
                    'max': 100,
                    'labels': {
                        'enabled': false
                    },
                    stops: [
                        [0.1, '#DF5353'], // green
                        [0.5, '#DDDF0D'], // yellow
                        [0.9, '#55BF3B'] // red
                    ],
                    'lineWidth': 0,
                    'minorTickInterval': null,
                    'tickPixelInterval': 400,
                    'tickWidth': 0
                },

                'plotOptions': {
                    'solidgauge': {
                        'innerRadius': '90%'
                    }
                },
                'series': [{
                    'name': 'Speed',
                    'data': [],
                    'dataLabels': {
                        enabled: true,
                        align: 'center',
                        verticalAlign: 'middle',
                        y: 0,
                        borderWidth: 0,
                        color: '#888',
                        style: {
                            fontSize: '18px',
                            fontFamily: 'Arial',
                            fontColor: '#e2e2e2'
                        }
                    }
                }]
            };

    @foreach($preorder->devices as $device)

            var chart{{ $device['id'] }} = Highcharts.chart('device{{ $device['id'] }}', chartOptions);

            chart{{ $device['id'] }}.update({
                series: {
                    dataLabels: {
                        format: '{y} / {{$device['quota']}}'
                    },
                    data: [{{$device['quota'] - $preorder->transactions->where('device_id', $device['id'])->whereIn('status',[1,2])->count()}}]
                },
                yAxis: {
                    max: {{ $device['quota'] }}
                        }
            });

            setInterval(function(){
                $.getJSON('/api/preorder/device/{{$device['id']}}', function(data) {

                    chart{{ $device['id'] }}.update({
                        series: {
                            dataLabels: {
                                format: '{y} / '+ data.quota
                            },
                            data: [ (data.quota - data.ordered)]
                        },
                        yAxis: {
                            max: data.quota
                        }
                    });
                });
            }, 5000);

            @endforeach

            setInterval(function(){
                $.getJSON('/api/preorder/{{ $preorder->id }}/order-status', function(data) {

                    $('#total-booked h1').html(data.booked);
                    $('#total-paid h1').html(data.paid);
                });
            }, 10000);
        });

    </script>
@endsection