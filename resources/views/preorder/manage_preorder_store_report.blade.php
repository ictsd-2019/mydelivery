@extends('layouts.app')

@section('sidebar')
    @parent
@endsection

@section('content')
    <style>
        .preorder-countdown-admin #countdown {
            font-size: 14px  !important;
        }

        #countdown h1 {
            font-size: 24px;
            text-shadow: 0px 2px 2px rgba(0,0,0,0.2);
        }
        #total-booked h1 {
            font-size: 24px;
            text-shadow: 0px 2px 2px rgba(0,0,0,0.2);
        }
        #total-paid h1 {
            font-size: 24px;
            text-shadow: 0px 2px 2px rgba(0,0,0,0.2);
        }
    </style>
    <h1 class="title">Pre-Order Store Report : <span class="text-muted"> {{ $preorder->title }}</span></h1>
    <div id="preorder-status-info" class="row">
        <div class="col-md-3"></div>
        <div class="col-md-2">
            <div class="preorder-countdown-admin text-center">
                <span id="countdown"></span>
            </div>
        </div>
        <div class="col-md-2">
            <div id="total-booked" class="text-center">Total Booked :<br>
                <h1>{{ $total_booked }}</h1></div>
        </div>
        <div class="col-md-2">
            <div id="total-paid" class="text-center">Total Paid :<br>
                <h1>{{ $total_paid }}</h1></div>
        </div>
        <div class="col-md-3"></div>
    </div>
    <ul class="row justify-content-md-center">
        @foreach($preorder->devices as $device)
            <div class="col-md-2">
                <div id="device{{ $device['id'] }}" style="width: 100%;height: 150px;"></div>
                <div class="text-center">{{ $device['model'] }}</div>
            </div>
        @endforeach
    </ul>
    <br>
    <ul class="nav nav-tabs">
        <li role="presentation" class="nav-item"><a class="nav-link" href="{{ route('preorder.manage.show', ['preorder_id' => $preorder['id']]) }}">Pre-Order Summary</a></li>
        <li role="presentation" class="nav-item"><a class="nav-link" href="{{ route('preorder.manage.transactions', ['preorder_id' => $preorder->id]) }}">Transactions Report</a></li>
        <li role="presentation" class="nav-item"><a class="nav-link active" href="#">Store Report</a></li>
    </ul>
    <div class="pre-order-tab">
        <br>
        <form id="search-preorder-transaction" class="form-inline">
            <div class="form-group">
                <input type="text" name="startdate" class="form-control datepicker m-1" style="width: 100px;" value="{{ $startdate }}" placeholder="From"/>
            </div>
            <div class="form-group">
                <input type="text" name="enddate" class="form-control m-1 datepicker" style="width: 100px;" value="{{ $enddate }}" placeholder="To"/>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary m-1" value="Search" />
                <a href="{{ route('preorder.manage.transactions', ['preorder_id' => $preorder->id]) }}" class="btn btn-secondary m-1">Reset</a>
            </div>
        </form>
            <table class="table table-sm table-striped table-hover">
                <tr><th>No.</th>
                    <th>Store Name</th>
                    @foreach($devices as $device)
                        <th class="text-center">{{ $device->model }}</th>
                    @endforeach
                    <th class="text-center">Total</th>
                </tr>
                @foreach($stores_data as $store_name => $data)
                    <tr>
                        <td>{{ $loop->iteration }}</td><td>{{ $store_name }}</td>
                        @foreach($data as $device_count)
                            @if($loop->last)
                                <td class="text-center"><strong>{{ $device_count }}</strong></td>
                            @else
                                <td class="text-center">{{ $device_count }}</td>
                            @endif
                        @endforeach
                    </tr>
                @endforeach
            </table>
    </div>
    <a href="{{ route('preorder.manage.index') }}" class="btn btn-outline-secondary">Back</a>
@endsection
@section('scripts')
    @parent
    <script src="/js/highcharts.js"></script>
    <script src="/js/highcharts-more.js"></script>
    <script src="/js/solid-gauge.js"></script>
    <script src="/js/jquery.countdown/jquery.countdown.min.js"></script>

    <script>
        $(function(){

            @if($preorder_status == "waiting")
                $('#countdown').countdown('{{ $preorder->startdate }}', function(event) {

                if(event.strftime('%-D') == 0)
                {
                    $(this).html("Open in <br/> <h1>"+ event.strftime('%H:%M:%S') +"</h1>");
                }
                else
                {
                    $(this).html("Open in<br/> <h1>"+ event.strftime('%-D hari %H:%M:%S') +"</h1>");
                }
            }).on('finish.countdown', function(){
                location.reload();
            });
            @elseif($preorder_status == "running")
                $('#countdown').countdown('{{ $preorder->enddate }}', function(event) {
                if(event.strftime('%-D') == 0)
                {
                    $(this).html("Pre-Order close :<br/> <h1>"+ event.strftime('%H:%M:%S') +"</h1>");
                }
                else
                {
                    $(this).html("Pre-Order close :<br/> <h1>"+ event.strftime('%-D hari %H:%M:%S') +"</h1>");
                }
            }).on('finish.countdown', function(){
                location.reload();
            });
            @else
                $('#countdown').html("Pre-Order sudah ditutup.");
                    @endif

                    var chartOptions = {

                        'chart': {
                            'type': 'solidgauge'
                        },
                        credits: {enabled: false},
                        title: null,
                        'tooltip': {
                            'enabled': false
                        },
                        'pane': {
                            'center': ['50%', '50%'],
                            'startAngle': 0,
                            'endAngle': 360,
                            'background': {
                                'backgroundColor': '#EEE',
                                'innerRadius': '90%',
                                'outerRadius': '100%',
                                'borderWidth': 0
                            }
                        },
                        'yAxis': {
                            'min': 0,
                            'max': 100,
                            'labels': {
                                'enabled': false
                            },
                            stops: [
                                [0.1, '#DF5353'], // green
                                [0.5, '#DDDF0D'], // yellow
                                [0.9, '#55BF3B'] // red
                            ],
                            'lineWidth': 0,
                            'minorTickInterval': null,
                            'tickPixelInterval': 400,
                            'tickWidth': 0
                        },

                        'plotOptions': {
                            'solidgauge': {
                                'innerRadius': '90%'
                            }
                        },
                        'series': [{
                            'name': 'Speed',
                            'data': [],
                            'dataLabels': {
                                enabled: true,
                                align: 'center',
                                verticalAlign: 'middle',
                                y: 0,
                                borderWidth: 0,
                                color: '#888',
                                style: {
                                    fontSize: '18px',
                                    fontFamily: 'Arial',
                                    fontColor: '#e2e2e2'
                                }
                            }
                        }]
                    };

                    @foreach($preorder->devices as $device)

                            var chart{{ $device['id'] }} = Highcharts.chart('device{{ $device['id'] }}', chartOptions);

            chart{{ $device['id'] }}.update({
                series: {
                    dataLabels: {
                        format: '{y} / {{$device['quota']}}'
                    },
                    data: [{{$device['quota'] - $preorder->transactions->where('device_id', $device['id'])->whereIn('status',[1,2])->count()}}]
                },
                yAxis: {
                    max: {{ $device['quota'] }}
                        }
            });

            setInterval(function(){
                $.getJSON('/api/preorder/device/{{$device['id']}}', function(data) {

                    chart{{ $device['id'] }}.update({
                        series: {
                            dataLabels: {
                                format: '{y} / '+ data.quota
                            },
                            data: [ (data.quota - data.ordered)]
                        },
                        yAxis: {
                            max: data.quota
                        }
                    });
                });
            }, 5000);

            @endforeach

            setInterval(function(){
                $.getJSON('/api/preorder/{{ $preorder->id }}/order-status', function(data) {

                    $('#total-booked h1').html(data.booked);
                    $('#total-paid h1').html(data.paid);
                });
            }, 10000);
        });

    </script>
@endsection