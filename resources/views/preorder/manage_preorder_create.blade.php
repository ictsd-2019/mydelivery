@extends('layouts.app')

@section('sidebar')
    @parent
@endsection

@section('content')
    <h1 class="title">Create Pre-Order</h1>
    @include('layouts.error')
    <br>
    {!! Form::open(['action' => ['PreOrderController@store'], 'id' => 'add-preorder', 'class' => 'form-horizontal']) !!}
        @include('preorder.form_manage_preorder')
    {!! Form::close() !!}
@endsection
