@extends('layouts.frontpage_layout')

@section('sidebar')
    @parent
@endsection

@section('content')
    <style>
        .progress {
            height: 5px;
            margin: 5px;
        }
    </style>
    <h1 class="title">{{ $preorder->title }}</h1>

    <div class="row">
        <div class="col-md-4">
            <div class="preorder-countdown">
                <span id="clock"></span>
            </div>
            <div class="text-center" role="group">
                @if($preorder_status == "waiting")
                <a href="#" type="button" class="btn btn-warning">
                    <i class="glyphicon glyphicon-time"></i> Dimulai pada
                        {{ \Carbon\Carbon::parse($preorder->startdate)->format('d M') }}
                        pukul {{ \Carbon\Carbon::parse($preorder->startdate)->format('h:i A') }}
                </a>
                @elseif($preorder_status == "running")
                    <a href="{{ route('preorder.create') }}?preorder_code={{ $preorder->preorder_code }}" type="button" class="btn btn-success">
                        <i class="glyphicon glyphicon-plus"></i> Daftar Pre-Order Sekarang
                    </a>
                @else
                    <a href="#" type="button" class="btn btn-default">
                        <i class="glyphicon glyphicon-remove-circle"></i> Pre-Order sudah Tutup
                    </a>
                @endif
            </div>
        </div>
        <div class="col-md-8">
            <ul>
                @foreach($preorder->devices as $device)
                    <li class="preorder-device-status-block float-right" style="margin:5px;display: none;list-style: none;width: 200px;">
                        <h3 class="text-center" style="margin:0;font-weight: bold;"><span id="device-ordered-{{$device['id'] }}"></span>/<span id="device-quota-{{ $device['id'] }}"></span> </h3>
                        <div class="progress">
                            <div id="progress-device-{{ $device['id'] }}" class="progress-bar bg-success" role="progressbar" style="width: 0%">
                                <span class="sr-only">0% Complete (success)</span>
                            </div>
                        </div>
                        <div id="device-model-{{ $device['id'] }}" class="text-center"></div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    <br>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a href="#" class="close" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
    <form id="search-audit" class="form-inline pull-right">
        <div class="form-group">
            <input type="text" name="reg_code" class="form-control m-1" value="{{ $reg_code }}" placeholder="Registration Code"/>
        </div>
        <div class="form-group">
            <input type="text" name="customer" class="form-control m-1" value="{{ $customer }}" placeholder="Pelanggan"/>
        </div>
        <div class="form-group">
            <input type="text" name="msisdn" class="form-control m-1" value="{{ $msisdn }}" placeholder="MSISDN"/>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary m-1" value="Search" />
            <a href="{{ route('preorder.show', ['preorder_code' => $preorder->preorder_code]) }}" class="btn btn-secondary m-1">Reset</a>
        </div>
    </form>
    <table class="table table-condensed table-striped table-hover">
        <thead>
            <tr>
                <th>No.</th>
                <th>Tanggal</th>
                <th>Kode</th>
                <th>Invoice</th>
                <th>Pelanggan</th>
                <th>Device</th>
                <th>MSISDN</th>
                <th width="150">Plan</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @if($transactions->count() > 0)
                @foreach($transactions as $trans)
                    <tr>
                        <td>{{ (($transactions->currentPage() - 1) * $transactions->perPage()) + $loop->iteration }}</td>
                        <td width="105">{{ \Carbon\Carbon::parse($trans->created_at)->format('d-M h:i') }}</td>
                        <td><strong>{{ $trans->reg_code }}</strong></td>
                        <td>{{ $trans->invoice_number }}</td>
                        <td>{{ $trans->customer_name }}</td>
                        <td>{{ $trans->device['model'] }}</td>
                        <td>{{ $trans->msisdn }}</td>
                        <td>{{ $trans->plan['name'] }}</td>
                        <td>@if($trans->status == 1)
                                <a href="#confirm-payment-form"
                                   class="btn btn-warning btn-sm confirm-payment"
                                   style="border-radius: 0px;"
                                   data-toggle="modal"
                                   data-preorder-code="{{ $preorder->preorder_code }}"
                                   data-reg-code="{{$trans->reg_code}}"
                                   data-reg-id="{{ $trans->id }}"
                                   data-reg-name="{{ $trans->customer_name }}"
                                   data-reg-msisdn="{{ $trans->msisdn }}"
                                   data-reg-device="{{ $trans->device['model'] }}">Konfirmasi Bayar
                                </a>
                            @endif

                            @if($trans->status == 2)
                                <a href="#show-paid-transaction"
                                   class="btn btn-success btn-sm show-paid"
                                   style="border-radius: 0px;"
                                   data-toggle="modal"
                                   data-preorder-code="{{ $preorder->preorder_code }}"
                                   data-reg-code="{{$trans->reg_code}}"
                                   data-reg-id="{{ $trans->id }}"
                                   data-reg-name="{{ $trans->customer_name }}"
                                   data-reg-mother-name="{{ $trans->mother_name }}"
                                   data-reg-id-number="{{ $trans->id_number }}"
                                   data-reg-msisdn="{{ $trans->msisdn }}"
                                   data-reg-plan="{{ $trans->plan['name'] }}"
                                   data-reg-periode="{{ $trans->periode }}"
                                   data-reg-invoice-number="{{ $trans->invoice_number }}"
                                   data-reg-payment-method="{{ $trans->payment_method }}"
                                   data-reg-payment-type="{{ $trans->payment_type }}"
                                   data-reg-bank-promo="{{  isset($trans->bank_promo->bank['name']) ? $trans->bank_promo->bank['name'] ." - Rp ". $trans->bank_promo['value'] : "" }}"
                                   data-reg-device="{{ $trans->device['model'] }}">Paid
                                </a>
                            @endif

                            @if($trans->status == 3)
                                <a href="#"
                                   class="btn btn-secondary btn-sm"
                                   style="border-radius: 0px;">Canceled
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            @else
                <td colspan="10">Tidak ada transaksi.</td>
            @endif
        </tbody>
    </table>
    Ditemukan : <strong>{{ $transactions->total() }}</strong> transaksi<br/>
    {{ $transactions->links() }}

    <div id="confirm-payment-form" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background: #ffe000;color: #3a3a3a;">
                    <h4 class="modal-title">Konfirmasi Pembayaran Pre-Order</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</span></button>
                </div>
                <div class="modal-body" style="background-color: #fbfbfb;">
                    <table class="table table-sm table-bordered">
                        <tr><th width="150">Nama Pelanggan</th><td><span id="display-customer-name"></span> </td></tr>
                        <tr><th>MSISDN</th><td><span id="display-msisdn"></span> </td></tr>
                        <tr><th>Device</th><td><span id="display-device"></span> </td></tr>
                    </table>
                    {!! Form::open(['action' => ['PreOrderTransactionController@confirm_payment'], 'class' => 'form-horizontal']) !!}
                    <div class="row" style="margin-bottom: 15px;">
                        <div class="col-md-3">
                            {!! Form::label('invoice_number', 'Nomor Invoice', ['class' => 'col-form-label']) !!}
                        </div>
                        <div class="col-md-9 form-inline">
                            {!! Form::hidden('preorder_code') !!}
                            {!! Form::hidden('reg_id') !!}
                            {!! Form::hidden('reg_code') !!}
                            {!! Form::hidden('customer') !!}
                            {!! Form::text('invoice_number', null, ['class' => 'form-control']) !!}
                            {!! Form::submit('Konfirmasi', ['class' => 'btn btn-success', 'style' => 'border-radius:0;']) !!}
                            <a href="#" data-dismiss="modal" class="btn btn-default" style="border-radius: 0;">Cancel</a>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div id="show-paid-transaction" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background: #ffe000;color: #3a3a3a;">
                    <h4 class="modal-title">Detail Customer Pre-Order</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</span></button>
                </div>
                <div class="modal-body" style="background-color: #fbfbfb;">
                    <table class="table table-condensed table-bordered">
                        <tr><th colspan="2" class="bg-warning">Informasi Pelanggan</th></tr>
                        <tr><td width="150">Nama Pelanggan</td><td><span id="paid-customer-name"></span> </td></tr>
                        <tr><td width="150">Nomor KTP</td><td><span id="paid-id-number"></span> </td></tr>
                        <tr><td width="150">Ibu Kandung</td><td><span id="paid-mother-name"></span> </td></tr>
                        <tr><th colspan="2" class="bg-warning">Informasi Device dan Paket</th></tr>
                        <tr><td>MSISDN</td><td><span id="paid-msisdn"></span> </td></tr>
                        <tr><td>Device</td><td><span id="paid-device"></span> </td></tr>
                        <tr><td>Plan</td><td><span id="paid-plan"></span> </td></tr>
                        <tr><td>Periode</td><td><span id="paid-periode"></span> </td></tr>
                        <tr><th colspan="2" class="bg-warning">Informasi Pembayaran</th></tr>
                        <tr><td>Invoice Number</td><td><span id="paid-invoice-number"></span> </td></tr>
                        <tr><td>Payment Method</td><td><span id="paid-payment-method"></span> </td></tr>
                        <tr><td>Payment Type</td><td><span id="paid-payment-type"></span> </td></tr>
                        <tr><td>Bank Promo</td><td><span id="paid-bank-promo"></span> </td></tr>
                    </table>
                    <div class="row" style="margin-bottom: 15px;">
                        <div class="col-md-12 form-inline">
                            <a href="#" data-dismiss="modal" class="btn btn-success pull-right">Close</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent

    <script>
        $(function(){

            @if($preorder_status == "waiting")
                $('#clock').countdown('{{ $preorder->startdate }}', function(event) {

                if(event.strftime('%-D') == 0)
                {
                    $(this).html("Dibuka dalam <br/> <h1>"+ event.strftime('%H:%M:%S') +"</h1>");
                }
                else
                {
                    $(this).html("Dibuka dalam<br/> <h1>"+ event.strftime('%-D hari %H:%M:%S') +"</h1>");
                }
                }).on('finish.countdown', function(){
                    location.reload();
                });
            @elseif($preorder_status == "running")
                $('#clock').countdown('{{ $preorder->enddate }}', function(event) {
                    if(event.strftime('%-D') == 0)
                    {
                        $(this).html("Pre-Order tutup :<br/> <h1>"+ event.strftime('%H:%M:%S') +"</h1>");
                    }
                    else
                    {
                        $(this).html("Pre-Order tutup :<br/> <h1>"+ event.strftime('%-D hari %H:%M:%S') +"</h1>");
                    }
                }).on('finish.countdown', function(){
                    location.reload();
                });
            @else
                $('#clock').html("Pre-Order sudah ditutup.");
            @endif
        });

        $('#confirm-payment-form').on('show.bs.modal', function (e) {

            var regCode =  $(e.relatedTarget).data('reg-code');
            var regId = $(e.relatedTarget).data('reg-id');
            var custName = $(e.relatedTarget).data('reg-name');
            var msisdn = $(e.relatedTarget).data('reg-msisdn');
            var device = $(e.relatedTarget).data('reg-device');
            var preorderCode = $(e.relatedTarget).data('preorder-code');

            $('input[name=reg_id]').val(regId);
            $('input[name=reg_code]').val(regCode);
            $('input[name=customer]').val(custName);
            $('input[name=preorder_code]').val(preorderCode);

            $('#display-customer-name').text(custName);
            $('#display-msisdn').text(msisdn);
            $('#display-device').text(device);
        })

        $('#show-paid-transaction').on('show.bs.modal', function (e) {

            var regCode =  $(e.relatedTarget).data('reg-code');
            var regId = $(e.relatedTarget).data('reg-id');
            var custName = $(e.relatedTarget).data('reg-name');
            var idNumber = $(e.relatedTarget).data('reg-id-number');
            var motherName = $(e.relatedTarget).data('reg-mother-name');
            var msisdn = $(e.relatedTarget).data('reg-msisdn');
            var device = $(e.relatedTarget).data('reg-device');
            var plan = $(e.relatedTarget).data('reg-plan');
            var periode = $(e.relatedTarget).data('reg-periode');
            var invoiceNumber = $(e.relatedTarget).data('reg-invoice-number');
            var paymentMethod = $(e.relatedTarget).data('reg-payment-method');
            var paymentType = $(e.relatedTarget).data('reg-payment-type');
            var bankPromo = $(e.relatedTarget).data('reg-bank-promo');
            var preorderCode = $(e.relatedTarget).data('preorder-code');

            $('input[name=reg_id]').val(regId);
            $('input[name=reg_code]').val(regCode);
            $('input[name=customer]').val(custName);
            $('input[name=preorder_code]').val(preorderCode);

            $('#paid-customer-name').text(custName);
            $('#paid-id-number').text(idNumber);
            $('#paid-mother-name').text(motherName);
            $('#paid-msisdn').text(msisdn);
            $('#paid-device').text(device);
            $('#paid-plan').text(plan);
            $('#paid-periode').text(periode);
            $('#paid-invoice-number').text(invoiceNumber);
            $('#paid-payment-method').text(paymentMethod);
            $('#paid-payment-type').text(paymentType);
            $('#paid-bank-promo').text(bankPromo);
        })


        $.getJSON('/api/preorder/{{ $preorder->id }}/devices-status', function(data) {

            for (var key in data)
            {
                var ordered = data[key].booked + data[key].paid;
                var quotaRemain = data[key].quota - ordered;
                var progressPercentage = (quotaRemain/data[key].quota)*100;

                $('#device-ordered-'+ data[key].id).text(quotaRemain);
                $('#device-quota-'+ data[key].id).text(data[key].quota);
                $('#progress-device-'+ data[key].id).css('width', progressPercentage +'%');
                $('#device-model-'+ data[key].id).text(data[key].model);
                $('.preorder-device-status-block').show();
            }

        });

        setInterval(function(){
            $.getJSON('/api/preorder/{{ $preorder->id }}/devices-status', function(data) {

                for (var key in data)
                {
                    var ordered = data[key].booked + data[key].paid;
                    var quotaRemain = data[key].quota - ordered;
                    var progressPercentage = (quotaRemain/data[key].quota)*100;

                    console.log(progressPercentage)

                    if(progressPercentage < 10)
                    {
                        $('#progress-device-'+ data[key].id).removeClass('bg-success').removeClass('bg-warning').addClass('bg-danger');
                    }
                    else if(progressPercentage < 50)
                    {
                        $('#progress-device-'+ data[key].id).removeClass('bg-success').removeClass('bg-danger').addClass('bg-warning');
                    }
                    else
                    {
                        $('#progress-device-'+ data[key].id).removeClass('bg-warning').removeClass('bg-danger').addClass('bg-success');
                    }

                    $('#device-ordered-'+ data[key].id).text(quotaRemain);
                    $('#device-quota-'+ data[key].id).text(data[key].quota);
                    $('#progress-device-'+ data[key].id).css('width', progressPercentage +'%');
                    $('#device-model-'+ data[key].id).text(data[key].model);
                    $('.preorder-device-status-block').show();
                }
            });
        }, 10000);

    </script>
@endsection
