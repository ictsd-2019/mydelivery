@extends('layouts.app')

@section('sidebar')
    @parent
@endsection

@section('content')
    <style>
        .preorder-countdown-admin #clock {
            font-size: 14px  !important;
        }

        #clock h1 {
            font-size: 24px;
            text-shadow: 0px 2px 2px rgba(0,0,0,0.2);
        }
        #total-booked h1 {
            font-size: 24px;
            text-shadow: 0px 2px 2px rgba(0,0,0,0.2);
        }
        #total-paid h1 {
            font-size: 24px;
            text-shadow: 0px 2px 2px rgba(0,0,0,0.2);
        }
    </style>
    <h1 class="title">Detail Pre-Order : <span class="text-muted"> {{ $preorder->title }}</span></h1>
    <div id="preorder-status-info" class="row justify-content-md-center">
        <div class="col-md-2">
            <div class="preorder-countdown-admin text-center">
                <span id="clock"></span>
            </div>
        </div>
        <div class="col-md-2">
            <div id="total-booked" class="text-center">Total Booked :<br>
                <h1>{{ $total_booked }}</h1></div>
        </div>
        <div class="col-md-2">
            <div id="total-paid" class="text-center">Total Paid :<br>
                <h1>{{ $total_paid }}</h1></div>
        </div>
    </div>
    <ul class="row justify-content-md-center">
        @foreach($preorder->devices as $device)
            <div class="col-md-2">
                <div id="device{{ $device['id'] }}" style="width: 100%;height: 150px;"></div>
                <div class="text-center">{{ $device['model'] }}</div>
            </div>
        @endforeach
    </ul>
    <br>
    <ul class="nav nav-tabs">
        <li role="presentation" class="nav-item"><a class="nav-link active" href="#">Pre-Order Summary</a></li>
        <li role="presentation" class="nav-item"><a class="nav-link" href="{{ route('preorder.manage.transactions', ['preorder_id' => $preorder->id]) }}">Transactions Report</a></li>
        <li role="presentation" class="nav-item"><a class="nav-link" href="{{ route('preorder.manage.store_report', ['preorder_id' => $preorder->id]) }}">Store Report</a></li>
    </ul>
    <div class="pre-order-tab">
        <br>
        @if (session('message'))
            <div class="alert alert-{{ session('status') }}">
                <a href="#" class="close" data-dismiss="alert"
                   aria-label="close">&times;</a> {{ session('message') }}
            </div>
        @endif
        @include('layouts.error')
        {!! Form::open(['action' => ['PreOrderTransactionController@store'], 'id' => 'add-preorder', 'class' => 'form-horizontal']) !!}
            @include('preorder.form_preorder')
        {!! Form::close() !!}
    </div>
    <a href="{{ route('preorder.manage.index') }}" class="btn btn-outline-secondary">Back</a>
@endsection

@section('scripts')
    @parent
    <script src="/js/highcharts.js"></script>
    <script src="/js/highcharts-more.js"></script>
    <script src="/js/solid-gauge.js"></script>
    <script src="/js/jquery.countdown/jquery.countdown.min.js"></script>

    <script>
        $(function(){

            @if($preorder_status == "waiting")
                $('#clock').countdown('{{ $preorder->startdate }}', function(event) {

                if(event.strftime('%-D') == 0)
                {
                    $(this).html("Pre-Order open in <br/> <h1>"+ event.strftime('%H:%M:%S') +"</h1>");
                }
                else
                {
                    $(this).html("Pre-Order open in <br/> <h1>"+ event.strftime('%-D hari %H:%M:%S') +"</h1>");
                }
            }).on('finish.countdown', function(){
                location.reload();
            });
            @elseif($preorder_status == "running")
                $('#clock').countdown('{{ $preorder->enddate }}', function(event) {
                if(event.strftime('%-D') == 0)
                {
                    $(this).html("Pre-Order close in:<br/> <h1>"+ event.strftime('%H:%M:%S') +"</h1>");
                }
                else
                {
                    $(this).html("Pre-Order close in :<br/> <h1>"+ event.strftime('%-D hari %H:%M:%S') +"</h1>");
                }
            }).on('finish.countdown', function(){
                location.reload();
            });
            @else
                $('#clock').html("Pre-Order sudah ditutup.");
                    @endif

                            var chartOptions = {

                        'chart': {
                            'type': 'solidgauge'
                        },
                        credits: {enabled: false},
                        title: null,
                        'tooltip': {
                            'enabled': false
                        },
                        'pane': {
                            'center': ['50%', '50%'],
                            'startAngle': 0,
                            'endAngle': 360,
                            'background': {
                                'backgroundColor': '#EEE',
                                'innerRadius': '90%',
                                'outerRadius': '100%',
                                'borderWidth': 0
                            }
                        },
                        'yAxis': {
                            'min': 0,
                            'max': 100,
                            'labels': {
                                'enabled': false
                            },
                            stops: [
                                [0.1, '#DF5353'], // green
                                [0.5, '#DDDF0D'], // yellow
                                [0.9, '#55BF3B'] // red
                            ],
                            'lineWidth': 0,
                            'minorTickInterval': null,
                            'tickPixelInterval': 400,
                            'tickWidth': 0
                        },

                        'plotOptions': {
                            'solidgauge': {
                                'innerRadius': '90%'
                            }
                        },
                        'series': [{
                            'name': 'Speed',
                            'data': [],
                            'dataLabels': {
                                enabled: true,
                                align: 'center',
                                verticalAlign: 'middle',
                                y: -3,
                                borderWidth: 0,
                                color: '#888',
                                style: {
                                    fontSize: '18px',
                                    fontFamily: 'Arial',
                                    fontColor: '#e2e2e2'
                                }
                            }
                        }]
                    };

                    @foreach($preorder->devices as $device)
                        var chart{{ $device['id'] }} = Highcharts.chart('device{{ $device['id'] }}', chartOptions);

            chart{{ $device['id'] }}.update({
                series: {
                    dataLabels: {
                        format: '{y} / {{$device['quota']}}'
                    },
                    data: [{{$device['quota'] - $preorder->transactions->where('device_id', $device['id'])->whereIn('status',[1,2])->count()}}]
                },
                yAxis: {
                    max: {{ $device['quota'] }}
                        }
            });

            setInterval(function(){
                $.getJSON('/api/preorder/device/{{$device['id']}}', function(data) {

                    chart{{ $device['id'] }}.update({
                        series: {
                            dataLabels: {
                                format: '{y} / '+ data.quota
                            },
                            data: [ (data.quota - data.ordered)]
                        },
                        yAxis: {
                            max: data.quota
                        }
                    });
                });
            }, 5000);

            @endforeach

            $.getJSON('/api/preorder/{{ $preorder->id }}/stream', function(data) {

                var rowData = [];

                for (var key in data)
                {
                    rowData += '<tr>';
                    rowData += '<td>'+ data[key].time +'</td>';
                    rowData += '<td>'+ data[key].customer_name +'</td>';
                    rowData += '<td>'+ data[key].model +'</td>';
                    rowData += '<td>'+ data[key].store +'</td>';
                    rowData += '<td><div class="badge badge-'+ data[key].css_class +'">'+ data[key].status +'</div></td>';
                    rowData += '</tr>';
                }

                $('#preorder-stream tbody').html(rowData);
            });

            setInterval(function(){
                $.getJSON('/api/preorder/{{ $preorder->id }}/stream', function(data) {

                    var rowData = [];

                    for (var key in data)
                    {
                        rowData += '<tr>';
                        rowData += '<td>' + data[key].time +'</td>';
                        rowData += '<td>'+ data[key].customer_name +'</td>';
                        rowData += '<td>'+ data[key].model +'</td>';
                        rowData += '<td>'+ data[key].store +'</td>';
                        rowData += '<td><div class="badge badge-'+ data[key].css_class +'">'+ data[key].status +'</div></td>';
                        rowData += '</tr>';
                    }

                    $('#preorder-stream tbody').html(rowData);
                });

                $.getJSON('/api/preorder/{{ $preorder->id }}/order-status', function(data) {

                    $('#total-booked h1').html(data.booked);
                    $('#total-paid h1').html(data.paid);
                });
            }, 10000);
        });

    </script>
@endsection