<!-- Nav tabs -->
<ul id="gender" class="nav nav-tabs" role="tablist">
	<li role="presentation" class="nav-item active">
		<a class="nav-link" href="#gender" role="tab" data-toggle="tab">Package Info</a>
	</li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
	<div role="tabpanel" class="tab-pane active" id="gender">
        <div class="row form-group">
            <div class="col-md-4 col-sm-5 d-none d-md-block">
				<label>Basic Package</label>
			</div>
			<div class="col-md-8 col-sm-7">
                {!! Form::select('package_basic', $basic, null, array('class'=>'form-control', 'placeholder' => '- Basic Package -', 'required' => 'required')) !!}
			</div>
		</div>
        <div class="row form-group">
            <table class="table table-borderless" id="dynamic_field">
                <tr>
                    <td>
                        <div class="col-md-4 col-sm-4 d-md-block"><label>Add On Package</label></div>
                    </td>
			        <td>
                        <div class="col-md-8 col-sm-8" style="margin-left:32px;">
                        {!! Form::select('package_add[]', $add_on, null, array('class'=>'form-control', 'placeholder' => '- Add On Package -', 'required' => 'required')) !!}</div>
                    </td>
			        <td>
                        <div class="col-md-1 col-sm-1"><button class="btn btn-secondary" type="button" name="add" id="add">+</button></div>
                    </td>
                </tr>
            </table>
		</div>
        <div class="row form-group">
			<div class="col-md-4 col-sm-5 d-none d-md-block">
		    	{!! Form::label('type_payment', 'Payment Type', ['class' => 'col-form-label']) !!}
			</div>
			<div class="col-md-8 col-sm-7">
		        {!! Form::radio('type_payment', 'cash', ['class' => 'form-control']) !!} Cash 
                {!! Form::radio('type_payment', 'installment', ['class' => 'form-control']) !!} Installment
			</div>
		</div>
        <div class="row form-group">
            <div class="col-md-4 col-sm-5 d-none d-md-block">
				<label>Tenor Installment</label>
			</div>
			<div class="col-md-8 col-sm-7">
				{!! Form::select('tenor', [
                    '' => '- Tenor -',
                    '3' => '3',
                    '6' => '6',
                    '12' => '12',
                    '24' => '24'
                    ], null, ['class' => 'form-control']) !!} <sup class="mobile required">*</sup>
			</div>
		</div>
        <br/>
        <div class="row form-group">
            <div class="col-md-5 col-sm-5 d-none d-md-block">
				<label><b><u>Pricing</u></b></label>
			</div>
        </div>
        <div class="row form-group">
			<div class="col-md-4 col-sm-5 d-none d-md-block">
		    	{!! Form::label('device_price', 'Device Only', ['class' => 'col-form-label']) !!}
			</div>
			<div class="col-md-8 col-sm-7">
                <div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">Rp</span>
					</div>
                    <input class="price form-control" type="text" name="device_price"/>
				</div>
			</div>
		</div>
        <div class="row form-group">
            <div class="col-md-4 col-sm-5 d-none d-md-block">
		    	{!! Form::label('device_mrc', 'MRC Device/Month', ['class' => 'col-form-label']) !!}
			</div>
			<div class="col-md-8 col-sm-7">
            <div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">Rp</span>
					</div>
                    <input class="price form-control" type="text" name="device_mrc"/>
				</div>
			</div>
        </div>
        <div class="row form-group">
            <div class="col-md-4 col-sm-5 d-none d-md-block">
		    	{!! Form::label('price_total', 'Total Price', ['class' => 'col-form-label']) !!}
			</div>
			<div class="col-md-8 col-sm-7">
                <div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">Rp</span>
					</div>
                    <input class="form-control" type="text" name="price_total" id="totalPrice"/>
				</div>
			</div>
        </div>
	</div>
</div>

@section('scripts')
    @parent
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
        <script>
        $( document ).ready(function() {
            var postURL = "<?php echo url('addmore'); ?>";
            console.log(postURL);
            var i=1;  
            $('#add').click(function(){  
                i++;
                $('#dynamic_field').append('<tr><td><div id="row'+i+'" class="dynamic-added row form-group"><div class="col-md-4 col-sm-4 d-none d-md-block"></div></td><td><div class="col-md-8 col-sm-8" style="margin-left:32px;">{!! Form::select('package_add[]', $add_on, null, array('class'=>'form-control', 'placeholder' => '- Add On Package -')) !!}</div></td><td><div class="col-md-2 col-sm-1"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></div></td></tr>');
            });
            $(document).on('click', '.btn_remove', function(){  
                var button_id = $(this).attr("id");   
                $('#row'+button_id+'').remove();  
            });

            $('.js-example-basic-single').select2();

            $('.price').blur(function(){
                var total = 0;
                $('.price').each(function(){
                    if($(this).val()!=""){
                        total += parseFloat($(this).val());
                    }
                });
                $("#totalPrice").html(total);
            });
        });
        </script>
@endsection