@extends('layouts.app')

@section('content')
    @include('layouts.configuration_tab')
    <br>
    <div class="card-columns">
        @if(Auth::user()->hasPermissionTo('manage user'))
            <a href="{{ route('user.index') }}">
                <div class="card text-center">
                    <div class="card-body">
                        <h3 class="card-title"><i class="fa fa-user" aria-hidden="true"></i> <br>
                            Users</h3>
                        <p class="card-text">Manage Users</p>
                    </div>
                </div>
            </a>
        @endif
        @if(Auth::user()->hasPermissionTo('manage role and permission'))
        <a href="{{ route('role.index') }}">
            <div class="card text-center">
                <div class="card-body">
                    <h3 class="card-title"><i class="fa fa-universal-access" aria-hidden="true"></i> <br>
                        Role &amp; Permission</h3>
                    <p class="card-text">Manage Role and Permission</p>
                </div>
            </div>
        </a>
        @endif
    </div>
@endsection
