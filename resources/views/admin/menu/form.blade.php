<div class="form-group row">
	<div class="col-md-2">
		<label for="">Title</label>	
	</div>
	<div class="col-md-5">
		{!! Form::text('title', null, ['class' => 'form-control']) !!}	
	</div>
</div>

<div class="form-group row">
	<div class="col-md-2">
		<label for="">Group</label>	
	</div>
	<div class="col-md-5">
		{!! Form::text('group', null, ['class' => 'form-control']) !!}	
	</div>
</div>

<div class="form-group row">
	<div class="col-md-2">
		<label for="">Path</label>	
	</div>
	<div class="col-md-5">
		{!! Form::text('path', null, ['class' => 'form-control']) !!}	
	</div>
</div>

<div class="form-group row">
	<div class="col-md-2">
		<label for="">Permission</label>	
	</div>
	<div class="col-md-5">
		{!! Form::text('permission', null, ['class' => 'form-control']) !!}	
	</div>
</div>

<div class="form-group row">
	<div class="col-md-2">
		<label for="">Icon</label>	
	</div>
	<div class="col-md-5">
		{!! Form::text('icon3', null, ['class' => 'form-control', 'placeholder' => 'example: fa fa-email']) !!}	
	</div>
</div>

<div class="form-group row">
	<div class="col-md-2">
		<label for="">Select</label>	
	</div>
	<div class="col-md-5">
		{!! Form::select('status', ['1' => 'Active', '2' => 'In-Active'], null, ['class' => 'form-control', 'placeholder' => 'Choose Status']) !!}	
	</div>
</div>

<div class="form-group row mt-5">
	<div class="col-md-6">
		<a href="{{ route('menu.index') }}" class="btn btn-secondary">Cancel</a>
		<button type="submit" class="btn btn-primary">Submit</button>
	</div>
	<div class="col-md-5"></div>
</div>