@extends('layouts.app')

@section('sidebar')
    @parent
@endsection

@section('content')
	
	@include('layouts.configuration_tab')
	
	<div id="config-user" class="tab-content">
		<div role="tabpanel" class="tab-pane active">
			@include('layouts.error')

            {!! Form::model($menu, ['class' => 'form-horizontal', 'action' => 'MenuController@store']) !!}

            @include('admin.menu.form')
            {!! Form::close() !!}
		</div>
	</div>
@endsection