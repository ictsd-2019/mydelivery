@extends('layouts.app')

@section('sidebar')
    @parent
@endsection

@section('content')
	<div class="btn-group pull-right" role="group">
		<a href="{{ route('menu.create') }}" type="button" class="btn btn-success">Add Menu</a>
	</div>
	@include('layouts.configuration_tab')

	<div id="config-user" class="tab-content">
		<div role="tabpanel" class="tab-pane active">
            @if (session('message'))
                <div class="alert alert-{{ session('status') }}">
                    <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
                    aria-label="close">&times;</a> {{ session('message') }}
                </div>
            @endif
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th>No</th>
						<th>Title</th>
						<th>Group</th>
						<th>Path</th>
						<th>Permission</th>
						<th>Icon</th>
						<th>Status</th>
						<th>#</th>
					</tr>
				</thead>
				<tbody>
					@foreach($menus as $menu)
					<tr>
						<td>{{ (($menus->currentPage() - 1) * $menus->perPage()) + $loop->iteration }}</td>
						<td>{{ $menu->title }}</td>
						<td>{{ $menu->group }}</td>
						<td>{{ $menu->path }}</td>
						<td>{{ $menu->permission }}</td>
						<td>{{ $menu->icon3 }}</td>
						<td>
							@if($menu->status == 1) Active
							@elseif($menu->status == 2) In-Active
							@endif					
						</td>
						<td class="text-center">
							<div class="btn-toolbar">
								<div class="btn-group mr-1">
									<a href="{{ route('menu.edit', $menu->id) }}" class="btn btn-secondary btn-sm ml-1"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
								</div>
								<div class="btn-group">
									{!! Form::open(['method' => 'DELETE', 'route' => ['menu.destroy', $menu->id]]) !!}
									<button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-close" aria-hidden="true"></i></button>
									{!! Form::close() !!}
								</div>	
							</div>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

			{{ $menus->appends( Request::query() )->render('vendor.pagination.bootstrap-4') }}
		</div>
	</div>
@endsection