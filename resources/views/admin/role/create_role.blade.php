@extends('layouts.app')

@section('content')
    @include('layouts.configuration_tab')
    <!-- Tab panes -->
    <div id="config-user" class="tab-content">
        <div role="tabpanel" class="tab-pane active">
            @include('layouts.error')
            {!! Form::open(['class' => 'form-horizontal', 'action' => 'RoleController@store']) !!}
            @include('admin.role.form_role', ['submitButtonText' => 'Create Role'])
            {!! Form::close() !!}
        </div>
    </div>
@endsection