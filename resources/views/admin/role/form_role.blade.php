<div class="form-group row{{ $errors->has('username') ? ' has-error' : '' }}">
    {!! Form::label('name', 'Role Name', ['class' => 'col-md-3 col-form-label']) !!}

    <div class="col-md-6">
        {!! Form::text('name',null,['class' => 'form-control m-1', 'required' => 'required']) !!}
    </div>
</div>
<div class="form-group row{{ $errors->has('username') ? ' has-error' : '' }}">
    {!! Form::label('permission', 'Permissions', ['class' => 'col-md-3 col-form-label']) !!}

    <div class="col-md-6">
        <div class="well">
            <ul class="permission-list">
                @foreach($permissions as $permission)
                    <li>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="permission[]" {!! $permission['checked'] !!} value="{!! $permission['name'] !!}"> {!! $permission['name'] !!}
                            </label>
                        </div>                            
                    </li>                    
                @endforeach
            </ul>    
        </div>
    </div>
</div>
<div class="form-group row{{ $errors->has('landing_page') ? ' has-error' : '' }}">
    {!! Form::label('landing_page', 'Landing Page', ['class' => 'col-md-3 col-form-label']) !!}

    <div class="col-md-6">
        {!! Form::text('landing_page',null,['class' => 'form-control m-1', 'required' => 'required']) !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-md-6 col-md-offset-3">
        <a href="/config/role" class="btn btn-secondary btn-sm m-1">Cancel</a>
        {!! Form::submit( $submitButtonText , ['class'  => 'btn btn-primary btn-sm m-1']) !!}
    </div>
</div>