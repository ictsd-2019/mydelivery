@extends('layouts.app')

@section('sidebar')
    @parent
@endsection

@section('content')
    @include('layouts.configuration_tab')
    <!-- Tab panes -->
    <div id="config-user" class="tab-content">
        <div role="tabpanel" class="tab-pane active">
            @include('layouts.error')
            {!! Form::model($role,['method' => 'PATCH','class' => 'form-horizontal', 'action' => ['RoleController@update', $role->id]]) !!}
            @include('admin.role.form_role', ['submitButtonText' => 'Update Role'])
            {!! Form::close() !!}
        </div>
    </div>
@endsection