@extends('layouts.app')

@section('content')
	<div class="btn-group pull-right" role="group">
		<a href="{{ route('role.create') }}" type="button" class="btn btn-success btn-sm m-1">Add Role</a>
		<a href="{{ route('permission.create') }}" type="button" class="btn btn-success btn-sm m-1">Add Permission</a>
	</div>
	@include('layouts.configuration_tab')
	<!-- Tab panes -->
	<div id="config-user" class="tab-content">
		<div role="tabpanel" class="tab-pane active">
            @if (session('message'))
                <div class="alert alert-{{ session('status') }}">
                    <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
                    aria-label="close">&times;</a> {{ session('message') }}
                </div>
            @endif
            <div class="table-wrapper container" style="overflow-y: hidden; overflow-x: auto;">
                <table id="example" class="table table-striped table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th style="width:5%; text-align:center;">No</th>
                            <th style="width:20%; text-align:center;">Name</th>
                            <th style="width:70%; text-align:center;">Permission</th>
                            <th style="width:5%; text-align:center;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($roles as $role)
                        <tr>
                            <td style="text-align:center;">{{ $loop->iteration }}</td>
                            <td>{{ $role['name'] }}</td>
                            <td>
                                <ul class="role-permissions">
                                    @foreach($role['permissions'] as $permission)
                                        @if($permission != "")
                                            <li>{{ $permission }}</li>
                                        @endif
                                    @endforeach
                                </ul>
                            </td>
                            <td style="text-align:center;">
                                <a href="/config/role/{{ $role['id'] }}/edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
		</div>
	</div>
    <style>
    thead input {
        width: 100%;
    }
    th, td {
        font-size: 12px; 
    }
    </style>
@endsection

@section('scripts')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script>
    $(document).ready(function() {
	    $('#example thead tr').clone(true).appendTo( '#example thead' );
        $('#example thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            if(title === 'Aksi'){
                $(this).html('');
            } else {
                $(this).html( '<input type="text" style="text-align:center; font-family:system-ui, FontAwesome;" placeholder="&#xF002;" class="placeholder-fix" />' );
    
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                } );
            }
        } );
    
        var table = $('#example').DataTable( {
            orderCellsTop: true,
            fixedHeader: true,
        } );
	} );
    </script>
@endsection