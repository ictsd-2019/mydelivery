@extends('layouts.app')

@section('content')
<h1 class="title">Add New Permission</h1>
<hr/>
@if (session('message'))
    <div class="alert alert-{{ session('status') }}">
        <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
        aria-label="close">&times;</a> {{ session('message') }}
    </div>
@endif
@include('layouts.error')
{!! Form::open(['class' => 'form-horizontal', 'action' => 'PermissionController@store']) !!}
    @include('admin.permission.form_permission', ['submitButtonText' => 'Create Permission'])
{!! Form::close() !!}
@endsection
