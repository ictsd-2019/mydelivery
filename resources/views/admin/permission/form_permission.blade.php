<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    {!! Form::label('name', 'Permission Name', ['class' => 'col-md-2 col-form-label']) !!}

    <div class="col-md-6">
        {!! Form::text('name',null,['class' => 'form-control m-1 col-md-8', 'required' => 'required']) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-12">
        <a href="/config/permission" class="btn btn-secondary btn-sm m-1">Cancel</a>
        {!! Form::submit( $submitButtonText , ['class'  => 'btn btn-primary btn-sm m-1']) !!}
    </div>
</div>