@extends('layouts.app')

@section('content')
<h1 class="title">Edit Permission</h1>
<hr/>
@if (session('message'))
    <div class="alert alert-{{ session('status') }}">
        <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
        aria-label="close">&times;</a> {{ session('message') }}
    </div>
@endif
@include('layouts.error')
{!! Form::model($permission, ['method' => 'PATCH', 'action' => ['PermissionController@update', $permission->id], 'class' => 'form-horizontal']) !!}
    @include('admin.permission.form_permission', ['submitButtonText' => 'Update Permission'])
{!! Form::close() !!}
@endsection