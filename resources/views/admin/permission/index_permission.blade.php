@extends('layouts.app')

@section('content')
	<h1 class="title">Permissions List</h1>
	@if (session('message'))
		<div class="alert alert-{{ session('status') }}">
			<a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
			aria-label="close">&times;</a> {{ session('message') }}
		</div>
	@endif
	<div class="btn-group pull-right" role="group">
		<a href="{{ route('permission.create') }}" type="button" class="btn btn-success btn-sm m-1">Add Permission</a>
	</div>
	<div class="table-wrapper container" style="overflow-y: hidden; overflow-x: auto;">
		<table id="example" class="table table-hover table-striped" style="width:100%">
			<thead>
				<tr>
					<th style="width:5%; text-align:center;">No.</th>
					<th style="width:90%; text-align:center;">Name</th>
					<th style="width:5%; text-align:center;">Aksi</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($permissions as $permission)
				<tr>
					<td style="text-align:center;">{{ $loop->iteration }}</td>
					<td>{{ $permission->name }}</td>
					<td style="text-align:center;">
						<a href="/config/permission/{{ $permission->id }}/edit"><span class="glyphicon glyphicon-edit"></span></a>
						<!-- <a href="/config/permission/{{ $permission->id }}" onclick="return confirm('Are you sure you want to delete?');"><span class="glyphicon glyphicon-trash"></span></a> -->
						<!-- {{Form::open([ 'method'  => 'delete', 'route' => [ 'permission.destroy', $permission->id ] ])}}
							{{ Form::submit('Delete', ['class' => 'btn btn-danger', 'onclick' => 'return confirm("Are you sure you want to delete?");']) }}
						{{ Form::close() }} -->
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
@endsection

@section('scripts')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script>
    $(document).ready(function() {
	    $('#example thead tr').clone(true).appendTo( '#example thead' );
        $('#example thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            if(title === 'Aksi'){
                $(this).html('');
            } else {
                $(this).html( '<input type="text" style="text-align:center; font-family:system-ui, FontAwesome;" placeholder="&#xF002;" class="placeholder-fix" />' );
    
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                } );
            }
        } );
    
        var table = $('#example').DataTable( {
            orderCellsTop: true,
            fixedHeader: true,
        } );
	} );
    </script>
@endsection