@extends('layouts.app')

@section('content')
    @include('layouts.configuration_tab')
    <!-- Tab panes -->
    <div id="config-user" class="tab-content">
        <div role="tabpanel" class="tab-pane active">
            @include('layouts.error')
            {!! Form::open(['class' => 'form-horizontal', 'action' => 'UserController@store']) !!}
            @include('admin.user.form_user', ['submitButtonText' => 'Create User'])
            {!! Form::close() !!}
        </div>
    </div>
@endsection
