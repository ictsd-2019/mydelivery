<div class="row">
    <div class="col-md-6">
        <div class="form-group row{{ $errors->has('username') ? ' has-error' : '' }}">
            {!! Form::label('username', 'Username', ['class' => 'col-md-4 col-form-label']) !!}

            <div class="col-md-8">
                {!! Form::text('username',null,['class' => 'form-control m-1', 'required' => 'required']) !!}
            </div>
        </div>

        <div class="form-group row{{ $errors->has('email') ? ' has-error' : '' }}">
            {!! Form::label('email', 'Email Address', ['class' => 'col-md-4 col-form-label']) !!}
            
            <div class="col-md-8">
                {!! Form::text('email',null ,['class' => 'form-control m-1', 'required' => 'required']) !!}
            </div>
        </div>

        <div class="form-group row{{ $errors->has('fullname') ? ' has-error' : '' }}">
            {!! Form::label('fullname', 'Full Name', ['class' => 'col-md-4 col-form-label', 'required' => 'required']) !!}

            <div class="col-md-8">
                {!! Form::text('fullname',null,['class' => 'form-control m-1', 'required' => 'required']) !!}
            </div>
        </div>
        
        <div class="form-group row{{ $errors->has('nik') ? ' has-error' : '' }}">
            {!! Form::label('nik', 'NIK', ['class' => 'col-md-4 col-form-label']) !!}

            <div class="col-md-8">
                {!! Form::text('nik',null,['class' => 'form-control m-1']) !!}
            </div>
        </div>
        
        <div class="form-group row{{ $errors->has('phone') ? ' has-error' : '' }}">
            {!! Form::label('phone', 'Mobile Phone', ['class' => 'col-md-4 col-form-label']) !!}

            <div class="col-md-8">
                {!! Form::text('phone',null,['class' => 'form-control m-1']) !!}
            </div>
        </div>
        
        <div class="form-group row{{ $errors->has('password') ? ' has-error' : '' }}">
            {!! Form::label('password', 'Password', ['class' => 'col-md-4 col-form-label']) !!}

            <div class="col-md-8">
                @if($submitButtonText == "Create User")
                    {!! Form::password('password', ['class' => 'form-control m-1', 'required' => 'required']) !!}
                @else
                    {!! Form::password('password', ['class' => 'form-control m-1']) !!}
                @endif
            </div>
        </div>

        <div class="form-group row">
            {!! Form::label('password-confirm', 'Confirm Password', ['class' => 'col-md-4 col-form-label']) !!}

            <div class="col-md-8">
                @if($submitButtonText == "Create User")
                    {!! Form::password('password_confirmation', ['class' => 'form-control m-1', 'required' => 'required']) !!}
                @else
                    {!! Form::password('password_confirmation', ['class' => 'form-control m-1']) !!}
                @endif
            </div>
        </div>            
    </div>
    <div class="col-md-6">
        <div class="form-group row{{ $errors->has('role') ? ' has-error' : '' }}">
            {!! Form::label('role', 'User Role', ['class' => 'col-md-3 col-form-label']) !!}
            
            <div class="col-md-8">
                {!! Form::select('role[]', $roles , $active_role, ['class' => 'form-control chosen-select',  'data-placeholder' => '  - Select -', 'required' => 'required', 'multiple']) !!}
            </div>
        </div>
        <div class="form-group row{{ $errors->has('region') ? ' has-error' : '' }}">
            {!! Form::label('region_id', 'Region', ['class' => 'col-md-3 col-form-label']) !!}
            
            <div class="col-md-8">
                {!! Form::select('region_id', $region , $active_region, ['class' => 'form-control chosen-select', 'required' => 'required']) !!}
            </div>
        </div>
        <div class="form-group row{{ $errors->has('manager') ? ' has-error' : '' }}">
            {!! Form::label('manager_id', 'Manager', ['class' => 'col-md-3 col-form-label']) !!}
            
            <div class="col-md-8">
                {!! Form::select('manager_id', $manager , $active_manager, ['class' => 'form-control chosen-select']) !!}
            </div>
        </div> 
        <div class="form-group row{{ $errors->has('status') ? ' has-error' : '' }}">
            {!! Form::label('status', 'Status', ['class' => 'col-md-3 col-form-label']) !!}
            
            <div class="col-md-8">
                {!! Form::select('status', $status , null, ['class' => 'form-control', 'placeholder' => '--Select--']) !!}
            </div>
        </div>          
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group row">
            <div class="col-md-8 col-md-offset-4">
                <a href="/config/user" class="btn btn-secondary btn-sm m-1">Cancel</a>
                {!! Form::submit( $submitButtonText , ['class'  => 'btn btn-primary btn-sm m-1']) !!}
            </div>
        </div> 
    </div>
</div>

