@extends('layouts.app')

@section('content')
    @include('layouts.configuration_tab')
    <!-- Tab panes -->
    <div id="config-user" class="tab-content">
        <div role="tabpanel" class="tab-pane active">
            <p>You are not allowed to edit <strong>"ADMINISTRATOR"</strong> users.</p>
            <br/>
            <a href="/config/user" class="btn btn-primary btn-sm m-1">Back</a>
        </div>
    </div>
@endsection