@extends('layouts.app')

@section('content')
    @include('layouts.configuration_tab')
    <!-- Tab panes -->
    <div id="config-user" class="tab-content">
        <div role="tabpanel" class="tab-pane active">
            @include('layouts.error')

            {!! Form::model($user, ['method' => 'PATCH', 'action' => ['UserController@update', $user->id], 'class' => 'form-horizontal']) !!}
            @include('admin.user.form_user', ['submitButtonText' => 'Update User'])
            {!! Form::close() !!}
        </div>
    </div>
@endsection
