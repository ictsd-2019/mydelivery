@extends('layouts.app')

@section('content')
	<div class="btn-group pull-right" role="group">
		<a href="{{ route('user.create') }}" type="button" class="btn btn-success btn-sm m-1">Add User</a>
	</div>
	@include('layouts.configuration_tab')
	
	<!-- Tab panes -->
	<div id="config-user" class="tab-content">
		<div role="tabpanel" class="tab-pane active">
			@if (session('message'))
                <div class="alert alert-{{ session('status') }}">
                    <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
                    aria-label="close">&times;</a> {{ session('message') }}
                </div>
            @endif
            <div class="table-wrapper container" style="overflow-y: hidden; overflow-x: auto;">
                <table id="example" class="table table-striped table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th style="width:1%;">No</th>
                            <th style="text-align:center; width:10%;">Username</th>
                            <th style="text-align:center; width:18%;">Name</th>
                            <th style="text-align:center; width:17%;">Role</th>
                            <th style="text-align:center; width:21%;">Region</th>
                            <th style="text-align:center; width:16%;">Added Date</th>
                            <th style="text-align:center; width:12%;">Last Login</th>
                            <th style="text-align:center; width:2%;">Status</th>
                            <th style="text-align:center; width:1%;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td style="text-align:center;"> {{$loop->iteration}} </td>
                            <td> {{$user->username}} </td>
                            <td> {{$user->fullname}} </td>
                            <td> {{$user->role}} </td>
                            <td> {{$user->region['name']}} </td>
                            <td>{!! $user->create_at !!}</td>
							<td style="text-align:center;">{!! $user->login_date !!}</td>
							<td>{!! $user->user_status !!}</td>
                            <td style="text-align:center;"><a href="/config/user/{{ $user->id }}/edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
			<div class="row mt-4">
				<div class="col-md-2">
					<table class="table">
						<tr>
							<th>User Statistic</th>
						</tr>
					</table>
				</div>
				<div class="col-md-2">
					<table class="table">
						<tr>
							<td>Total Users</td>
							<td class="text-center"><span class="text-muted">{{$total_users}}</span></td>
						</tr>
					</table>
				</div>
				<div class="col-md-2">
					<table class="table">
						<tr>
							<td>Active Users</td>
							<td class="text-center"><span class="text-muted">{{$active_users}}</span></td>
						</tr>
					</table>
				</div>
				<div class="col-md-3">
					<table class="table">
						<tr>
							<td>Today Active Users</td>
							<td class="text-center"><span class="text-muted">{{$today_active}}</span></td>
						</tr>
					</table>
				</div>
				<div class="col-md-3">
					<table class="table">
						<tr>
							<td>Total Disabled Users</td>
							<td class="text-center"><span class="text-muted">{{$disable_users}}</span></td>
						</tr>
					</table>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	</div>
    <!-- <style>
    thead input {
        width: 100%;
    }
    th, td { 
        font-size: 12px;
    }
    .table tbody tr td{
        padding-top:4px;
        padding-bottom:4px;
    }
    </style> -->
@endsection

@section('scripts')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script>
    $(document).ready(function() {
	    $('#example thead tr').clone(true).appendTo( '#example thead' );
        $('#example thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            if(title === 'Aksi'){
                $(this).html('');
            } else {
                $(this).html( '<input type="text" style="text-align:center; font-family:system-ui, FontAwesome;" placeholder="&#xF002;" class="placeholder-fix" />' );
    
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                } );
            }
        } );
    
        var table = $('#example').DataTable( {
            orderCellsTop: true,
            fixedHeader: true,
        } );
	} );
    </script>
@endsection