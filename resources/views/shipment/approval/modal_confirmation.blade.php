<div id="confirmation_{{$so_dr->id}}" class="modal fade">
    <div class="modal-dialog modal-sm" role="document" >
        <div class="modal-content" style="border-radius:2%;">
            <div class="modal-header">
                <p class="modal-title" style="font-size:25px;"><b>Verify DR</b></p>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                    <br/>
                    <center>
                    <h5>Are you sure you want to verify {{$so_dr->no_dr}} ?</h5>
                    <center>
                    <br/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <center>
                        <a type="button" class="btn btn-secondary m-1" href="/approval-dr/{{$so_dr->id}}/reject">Reject</a>                        
                        <a type="button" class="btn btn-primary m-1" href="/approval-dr/{{$so_dr->id}}/approve">Approve</a>
                        <center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>