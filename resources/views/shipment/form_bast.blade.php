<div class="row">
    <div class="col-md-6">
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#" style="color:black;">Request Info</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('request_date', 'Request Date', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    {!! Form::date('request_date', $current_date, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('sales', 'Sales Name', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('sales', $sales['nama'], ['class' => 'form-control placeholder-fix', 'placeholder' => 'Sales Name', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('sales_dept', 'Sales Department', ['class' => 'col-form-label']) !!}<sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('sales_dept', $salesorder->sales_group, ['class' => 'form-control placeholder-fix', 'placeholder' => 'Sales Department', 'required' => 'required']) !!}
                </div>
            </div>
        </div>
        <br>
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#" style="color:black;">Shipment Info</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="form-group row">
                <div class="col-md-4">
                    {!! Form::label('msisdn', 'MSISDN', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::select('msisdn[]', $list_msisdn , null, ['class' => 'form-control chosen-select',  'data-placeholder' => '   - Select MSISDN - ', 'required' => 'required', 'multiple']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('imei', 'List IMEI', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::textarea('imei', null, ['rows' => 3, 'class' => 'form-control placeholder-fix', 'placeholder' => '(Pisahkan dengan strip) Contoh: 359447095805156-359447095805156-359447095805156', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('form_shipment', 'File Shipment', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    <input class="form-control" type="file" name="form_shipment" required/>
                    <small>Format nama file tanpa spasi</small>
                    <!-- {!! Form::text('form_shipment', null, ['class' => 'form-control placeholder-fix', 'placeholder' => 'Sales Name', 'required' => 'required']) !!} -->
                </div>
            </div>
        </div>
        <br>
    </div>
    <div class="col-md-6">
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#" style="color:black;">Corporate Info</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('corporate_name', 'Corporate Name', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('corporate_name', $salesorder->corp_name, ['class' => 'form-control placeholder-fix', 'placeholder' => 'Corporate Name', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('address_1', 'Address 1', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::textarea('address_1', null, ['rows' => 3,'class' => 'form-control placeholder-fix', 'placeholder' => 'Address', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('rt_rw', 'RT/RW', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-3">
                    {!! Form::text('rt_rw', null, ['class' => 'form-control placeholder-fix', 'placeholder' => 'No. RT/RW']) !!}
                </div>
                <div class="col-md-1">
                    {!! Form::label('zip_post', 'Zip', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-4">
                    {!! Form::text('zip_post', null, ['class' => 'form-control placeholder-fix', 'placeholder' => 'Zip Code']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('address_2', 'Address 2', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('address_2', null, ['class' => 'form-control placeholder-fix', 'placeholder' => 'Address', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('address_3', 'Address 3', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    {!! Form::text('address_3', null, ['class' => 'form-control placeholder-fix', 'placeholder' => 'Address']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('pic_corporate_1', 'PIC Corporate 1', ['class' => 'col-form-label']) !!} <sup class=" required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('pic_corporate_1', null, ['class' => 'form-control placeholder-fix', 'placeholder' => 'PIC Name', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('pic_phone_1', 'No. HP', ['class' => 'col-form-label']) !!} <sup class=" required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::tel('pic_phone_1', null, ['class' => 'form-control placeholder-fix', 'placeholder' => 'No. Handphone', 'required' => 'required', 'maxlength' => '16']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('pic_corporate_2', 'PIC Corporate 2', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    {!! Form::text('pic_corporate_2', null, ['class' => 'form-control placeholder-fix', 'placeholder' => 'PIC Name']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('pic_phone_2', 'No. HP', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    {!! Form::tel('pic_phone_2', null, ['class' => 'form-control placeholder-fix', 'placeholder' => 'No. Handphone', 'maxlength' => '16']) !!}
                </div>
            </div>
        </div>
        <br>
    </div>
</div>