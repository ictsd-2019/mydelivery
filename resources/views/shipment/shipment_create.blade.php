@extends('layouts.app')

@section('content')
    <h1 class="title">Input Form Shipment</h1>
	<hr/>
	@include('layouts.error')
    {!! Form::open(['action' => ['SalesorderShipmentController@store', $salesorder->id], 'class' => 'form-horizontal', 'files' => true, 'id' => 'sales-form', 'enctype' => 'multipart/form-data']) !!}
        @csrf
        <div class="row">
            <div class="col-md-12 col-sm-12">
                @include('shipment.form_bast')
                <br/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <strong><sup class="required">*</sup> Required</strong>
                <br/><br/>
                @if($salesorder->status == "Done")
                    <a href="/salesorder/{{$salesorder->id}}" class="btn btn-secondary m-1">Cancel</a>
                @else
                    <a href="/salesorder/{{$salesorder->id}}/activation" class="btn btn-secondary m-1">Cancel</a>
                @endif
                <input type="submit" value="Submit" class="btn btn-primary m-1">
            </div>
        </div>
	{!! Form::close() !!}
@endsection
