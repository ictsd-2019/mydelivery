@extends('layouts.app')

@section('content')
    <h1 class="title">Edit Form Shipment</h1>
	<hr/>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
	@include('layouts.error')
    {!! Form::open(['action' => ['SalesorderShipmentController@update', $shipment->id], 'class' => 'form-horizontal', 'id' => 'sales-form', 'enctype' => 'multipart/form-data']) !!}
        <div class="row">
            <div class="col-md-12 col-sm-12">
                @include('shipment.form_edit_bast')
                <br/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <strong><sup class="required">*</sup> Required</strong>
                <br/><br/>
                @if($salesorder->status == "Activation")
                    <a href="/salesorder/{{$salesorder->id}}/activation" class="btn btn-secondary m-1">Cancel</a>
                @else
                    <a href="/salesorder/{{$salesorder->id}}" class="btn btn-secondary m-1">Cancel</a>
                @endif
                <input type="submit" value="Submit" class="btn btn-primary m-1">
            </div>
        </div>
	{!! Form::close() !!}
@endsection
