<table>
    <thead>
        <tr>
            <th style="background:lightgrey; text-align:center; width:5%;">No</th>
            <th style="background:lightgrey; text-align:center; width:10%;">BAST Date</th>
            <th style="background:lightgrey; text-align:center; width:10%;">No. Quote</th>
            <th style="background:lightgrey; text-align:center; width:10%;">No. BA</th>
            <th style="background:lightgrey; text-align:center; width:12.5%;">Corporate Name</th>
            <th style="background:lightgrey; text-align:center; width:10%;">Quantity</th>
            <th style="background:lightgrey; text-align:center; width:12.5%;">Price</th>
            <th style="background:lightgrey; text-align:center; width:10%;">Status</th>
            <th style="background:lightgrey; text-align:center; width:10%;">PIC</th>
        </tr>
    </thead>
    <tbody>
    @foreach($salesorder as $so)
        <tr>
            <td> {{$loop->iteration}} </td>
            <td> {{$so->date_order}} </td>
            <td> {{$so->quote_no}} </td>
            <td> {{$so->ba_no}} </td>
            <td> {{$so->corp_name}} </td>
            <td> {{$so->quantity}} </td>
            <td> {{$so->total_price}} </td>
            <td> {{$so->status}} </td>
            @if($so->pic_quote == null)
                <td> - </td>
            @else
                <td> {{$so->pic_quote}} </td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>