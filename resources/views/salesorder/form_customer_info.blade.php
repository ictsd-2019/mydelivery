<!-- Nav tabs -->
<ul id="gender" class="nav nav-tabs" role="tablist">
	<li role="presentation" class="nav-item active">
		<a class="nav-link" href="#gender" role="tab" data-toggle="tab">Customer Info</a>
	</li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
	<div role="tabpanel" class="tab-pane active" id="gender">
		<div class="row form-group">
			<div class="col-md-4 col-sm-5">
			{!! Form::label('corp_name', 'Corporate Name', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
			</div>
			<div class="col-md-8 col-sm-7">
			{!! Form::text('corp_name', null, ['class' => 'form-control', 'placeholder' => 'Corporate Name', 'required' => 'required']) !!}
			</div>
		</div>
		<div class="row form-group">
			<div class="col-md-4 col-sm-5">
			{!! Form::label('pic', 'PIC', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
			</div>
			<div class="col-md-8 col-sm-7">
			{!! Form::text('pic', null, ['class' => 'form-control', 'placeholder' => 'PIC', 'required' => 'required']) !!}
			</div>
		</div>
		<div class="row form-group">
			<div class="col-md-4 col-sm-5">
			{!! Form::label('pic_phone', 'No. Handphone', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
			</div>
			<div class="col-md-8 col-sm-7">
			{!! Form::tel('pic_phone', null, ['class' => 'form-control', 'placeholder' => 'No. Handphone', 'required' => 'required', 'maxlength' => '16']) !!}
			</div>
		</div>
		<div class="row form-group">
			<div class="col-md-4 col-sm-5">
			{!! Form::label('pic_email', 'Email', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
			</div>
			<div class="col-md-8 col-sm-7">
			{!! Form::email('pic_email', null, ['class' => 'form-control', 'placeholder' => 'Email', 'required' => 'required']) !!}
			</div>
		</div>
	</div>
</div>
