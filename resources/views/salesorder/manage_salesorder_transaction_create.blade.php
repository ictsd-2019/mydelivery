@extends('layouts.app')

@section('content')
    <h1 class="title">Create Transaction</h1>
	<hr/>
	@include('layouts.error')
    {!! Form::open(['action' => ['SalesorderTransactionController@store_manage', $salesorder_id], 'class' => 'form-horizontal', 'id' => 'sales-form']) !!}
        <div class="row">
            <div class="col-md-6 col-sm-6">
                @include('salesorder.form_transaction_info')
                <br/>
            </div>
            <div class="col-md-6 col-sm-6">
                @include('salesorder.form_device')
                <br/>
                @include('salesorder.form_package_info')
                <br/>
            </div>
        </div>
		<div class="row">
			<div class="col-md-12">
				<strong><sup class="required">*</sup> Required</strong>
                <br/><br/>
				<a href="/salesorder/{{$salesorder_id}}/activation" onclick="return confirm('Are you sure you want to cancel?');" class="btn btn-secondary m-1">Cancel</a>
				<input type="submit" value="Submit" class="btn btn-primary m-1">
			</div>
		</div>
	{!! Form::close() !!}
@endsection


@section('scripts')
    @parent
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <script>
    $( document ).ready(function() {
        var postURL = "<?php echo url('addmore'); ?>";
        console.log(postURL);
        var i=1;  
        $('#add').click(function(){
            i++;
            $('#dynamic_field').append('<tr id="row'+i+'"><td width="35%" style="padding-left:3%"></td><td width="58%">{!! Form::select('package_add[]', $add_on, null, array('class'=>'form-control', 'placeholder' => '- Add On Package -', 'required' => 'required')) !!}</td><td width="7%"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
        });

        $(document).on('click', '.btn_remove', function(){  
            var button_id = $(this).attr("id");   
            $('#row'+button_id+'').remove();  
        });
    });
    </script>
    <script type="text/javascript">
        function tenorCheck() {
            if (document.getElementById('installment').checked) {
                document.getElementById('tenor').style.display = 'block';
            } else {
                document.getElementById('tenor').style.display = 'none';
            }
        }
    </script>
    <script type="text/javascript">
        function typeCheck() {
            if (document.getElementById('prepaid').checked) {
                document.getElementById('package_type').style.display = 'block';
                document.getElementById('prepaid_type').style.display = 'block';
                document.getElementById('hlr').style.display = 'block';
                document.getElementById('m2m').style.display = 'none';
            } else {
                document.getElementById('package_type').style.display = 'none';
                document.getElementById('prepaid_type').style.display = 'none';
                document.getElementById('hlr').style.display = 'none';
                document.getElementById('m2m').style.display = 'block';
            }
        }
    </script>
@endsection
