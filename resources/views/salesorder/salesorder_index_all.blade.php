@extends('layouts.frontpage_layout')

@section('sidebar')
    @parent
@endsection

@section('content')
    <h1 class="title">All Order</h1>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
    <br/>
    <div class="activation-history-wrapper container" style="overflow-y: hidden; overflow-x: auto;">
        <table id="example" class="table table-sm table-bordered table-hover dt-responsive text-center" style="width:100%">
            <thead>
                <tr>
                    <th style="background:lightgrey; text-align:center; width:5%;">No</th>
                    <th style="background:lightgrey; text-align:center; width:10%;">BAST Date</th>
                    <th style="background:lightgrey; text-align:center; width:10%;">No. Quote</th>
                    <th style="background:lightgrey; text-align:center; width:10%;">No. BA</th>
                    <th style="background:lightgrey; text-align:center; width:12.5%;">Corporate Name</th>
                    <th style="background:lightgrey; text-align:center; width:10%;">Quantity</th>
                    <th style="background:lightgrey; text-align:center; width:12.5%;">Price</th>
                    <th style="background:lightgrey; text-align:center; width:10%;">Status</th>
                    <th style="background:lightgrey; text-align:center; width:10%;">PIC</th>
                    <th style="background:lightgrey; text-align:center; width:10%;">Aksi</th>
                </tr>
            </thead>
            <tbody>
            @foreach($salesorder as $so)
                <tr>
                    <td> {{$loop->iteration}} </td>
                    <td> {{$so->date_order}} </td>
                    <td> {{$so->quote_no}} </td>
                    <td> {{$so->ba_no}} </td>
                    <td> {{$so->corp_name}} </td>
                    <td> {{$so->quantity}} </td>
                    <td> {{$so->total_price}} </td>
                    <td> {{$so->status}} </td>
                    @if($so->pic_quote == null)
                        <td> - </td>
                    @else
                        <td> {{$so->pic_quote}} </td>
                    @endif
                    <td style="text-align:center;">
                        <a href="/web/salesorder/{{$so->id}}/edit" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
                        <a href="/web/salesorder/{{$so->id}}" class="btn btn-warning btn-sm"><i class="fa fa-search"></i></a>
                        <a href="/web/salesorder/{{$so->id}}/delete" onclick="return confirm('Are you sure you want to delete this order?');" class="btn btn-primary btn-sm"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <br/>
    <center>
        <a href="/web/salesorder" class="btn btn-secondary">BACK</a>
    </center>
@endsection

@section('scripts')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script>
    $(document).ready(function() {
	    $('#example').DataTable();
	} );
    </script>
@endsection