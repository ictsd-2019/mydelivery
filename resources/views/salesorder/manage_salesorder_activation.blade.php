@extends('layouts.app')

@section('content')
    <h1 class="title">Device Check Quotation #{{$salesorder->quote_no}}</h1>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
	<br/>
	@include('layouts.error')
        <div class="row">
            <div class="col-md-6">
                <div class="card" style="box-shadow: 0 2px 2px rgba(0,0,0,0.1);">
                    <div class="card-header">Customer Info</div>
                    <div class="card-body">
                        <table class="table table-sm">
                            <tr>
                                <th><strong>Corporate Name</strong></th>
                                <td>{{ $salesorder->corp_name }}</td>
                            </tr>
                            <tr>
                                <th><strong>PIC</strong></th>
                                <td>{{ $salesorder->pic }}</td>
                            </tr>
                            <tr>
                                <th><strong>No. Handphone</strong></th>
                                <td>{{ $salesorder->pic_phone }}</td>
                            </tr>
                            <tr>
                                <th><strong>Email</strong></th>
                                <td>{{ $salesorder->pic_email }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card" style="box-shadow: 0 2px 2px rgba(0,0,0,0.1);">
                    <div class="card-header">Pre-Order Info</div>
                    <div class="card-body">
                        <table class="table table-sm">
                            <tr>
                                <th><strong>Request Date</strong></th>
                                <td>{{ $salesorder->date_order }}</td>
                            </tr>
                            <tr>
                                <th><strong>No. Quote</strong></th>
                                <td>{{ $salesorder->quote_no }}</td>
                            </tr>
                            <tr>
                                <th><strong>No. BA</strong></th>
                                <td>{{ $salesorder->ba_no }}</td>
                            </tr>
                            <tr>
                                <th><strong>Sales Agent</strong></th>
                                <td>{{ $salesorder->sales['nama'] }}</td>
                            </tr>
                            <tr>
                                <th><strong>Sales Dept.</strong></th>
                                <td>{{ $salesorder->sales_group }}</td>
                            </tr>
                            <tr>
                                <th><strong>Request Type</strong></th>
                                <td>{{ $salesorder->type_req }}</td>
                            </tr>
                            <tr>
                                <th><strong>Quantity</strong></th>
                                <td>{{ $salesorder->quantity }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <center>
                    <h4 class="title">TRANSACTION</h4>
                </center>
            </div>
        </div>
        @if(count($transaction) < $salesorder->quantity)
        <div class="btn-group pull-right" role="group">
            <a type="button" class="btn btn-success btn-sm m-1" href="/salesorder/transaction/{{$salesorder->id}}/create">Create Transaction</a>
        </div>
        @endif
        <form action="/salesorder/{{$so->id}}/activated" method="get">
        <div class="activation-history-wrapper container" style="overflow-y: hidden; overflow-x: auto;">
            @if($salesorder->type_req == 'Bundling')
            <table id="example" class="table table-striped table-hover text-center" style="width:250%">
            @else
            <table id="example" class="table table-striped table-hover text-center" style="width:200%">
            @endif
                @if($salesorder->type_req == 'Bundling')
                <thead>
                    <tr>
                        <th style="text-align:center; width:2%">No</th>
                        <th style="text-align:center; width:6%">MSISDN</th>
                        <th style="text-align:center; width:6%">ICCID</th>
                        <th style="text-align:center; width:5%">Type Bundling</th>
                        <th style="text-align:center; width:5%">Non M2M</th>
                        <th style="text-align:center; width:5%">M2M</th>
                        <th style="text-align:center; width:5%">Type Prepaid</th>
                        <th style="text-align:center; width:5%">Type Package</th>
                        <th style="text-align:center; width:5%">HLR</th>
                        <th style="text-align:center; width:3%">SIM Specification</th>
                        <th style="text-align:center; width:7%">Brand</th>
                        <th style="text-align:center; width:7%">Basic Package</th>
                        <th style="text-align:center; width:7%">Add On Package</th>
                        <th style="text-align:center; width:5%">Type Payment</th>
                        <th style="text-align:center; width:5%">Package MRC (Rp)</th>
                        <th style="text-align:center; width:4%">Tenor</th>
                        <th style="text-align:center; width:4%">Device Price (Rp)</th>
                        <th style="text-align:center; width:5%">Total Device Price (Rp)</th>
                        <th style="text-align:center; width:4%">Status</th>
                        <th style="text-align:center; width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($transaction as $tr)
                    <tr>
                        <td> {{$loop->iteration}} </td>
                        <td> {{$tr->msisdn}} </td>
                        <td> {{$tr->iccid}} </td>
                        <td> {{$tr->type_bundling}} </td>
                        <td> {{$tr->non_m2m}} </td>
                        <td> {{$tr->m2m}} </td>
                        <td> {{$tr->type_prepaid}} </td>
                        <td> {{$tr->package_type}} </td>
                        <td> {{$tr->hlr}} </td>
                        <td> {{$tr->sim_specification}} </td>
                        <td> {{$tr->brand}} </td>
                        <td> {{$tr->package_basic}} </td>
                        <td> {{$tr->addon}} </td>
                        <td> {{$tr->type_payment}} </td>
                        <td> {{$tr->device_mrc}} </td>
                        <td> {{$tr->tenor}} bulan </td>
                        <td> {{$tr->device_price}} </td>
                        <td> {{$tr->price_total}} </td>
                        <td>
                            @if(Auth::user()->hasPermissionTo('check availability device'))
                            <label class="switch">
                                <input type="checkbox" name="{{$tr->id}}" checked>
                                <span class="slider round"></span>
                            </label>
                            @else
                                In Progress RM
                            @endif
                        </td>
                        <td style="text-align:center;">
                            <a class="btn btn-success btn-sm m-1" href="/salesorder/transaction/{{$tr->id}}/edit"><i class="fa fa-edit"></i></a>
                            <a href="/salesorder/transaction/{{$tr->id}}/delete" onclick="return confirm('Are you sure you want to delete this transaction?');" class="btn btn-primary btn-sm m-1"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                @else
                <thead>
                    <tr>
                        <th style="text-align:center; width:3%">No</th>
                        <th style="text-align:center; width:7%">MSISDN</th>
                        <th style="text-align:center; width:7%">ICCID</th>
                        <th style="text-align:center; width:5%">Type Bundling</th>
                        <th style="text-align:center; width:5%">Non M2M</th>
                        <th style="text-align:center; width:5%">M2M</th>
                        <th style="text-align:center; width:5%">Type Prepaid</th>
                        <th style="text-align:center; width:5%">Type Package</th>
                        <th style="text-align:center; width:5%">HLR</th>
                        <th style="text-align:center; width:3%">SIM Specification</th>
                        <th style="text-align:center; width:10%">Basic Package</th>
                        <th style="text-align:center; width:10%">Add On Package</th>
                        <th style="text-align:center; width:5%">Package MRC (Rp)</th>
                        <th style="text-align:center; width:5%">Status</th>
                        <th style="text-align:center; width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($transaction as $tr)
                    <tr>
                        <td> {{$loop->iteration}} </td>
                        <td> {{$tr->msisdn}} </td>
                        <td> {{$tr->iccid}} </td>
                        <td> {{$tr->type_bundling}} </td>
                        <td> {{$tr->non_m2m}} </td>
                        <td> {{$tr->m2m}} </td>
                        <td> {{$tr->type_prepaid}} </td>
                        <td> {{$tr->package_type}} </td>
                        <td> {{$tr->hlr}} </td>
                        <td> {{$tr->sim_specification}} </td>
                        <td> {{$tr->package_basic}} </td>
                        <td> {{$tr->addon}} </td>
                        <td> {{$tr->device_mrc}} </td>
                        <td>
                            @if(Auth::user()->hasPermissionTo('check availability device'))
                            <label class="switch">
                                <input type="checkbox" name="{{ $tr->id }}" checked>
                                <span class="slider round"></span>
                            </label>
                            @else
                                In Progress RM
                            @endif
                        </td>
                        <td style="text-align:center;">
                            <a class="btn btn-success btn-sm m-1" href="/salesorder/transaction/{{$tr->id}}/edit"><i class="fa fa-edit"></i></a>
                            <a href="/salesorder/transaction/{{$tr->id}}/delete" onclick="return confirm('Are you sure you want to delete this transaction?');" class="btn btn-primary btn-sm m-1"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                @endif
            </table>
        </div>
        <br/>
        <div class="row">
			<div class="col-md-12">
    			<a href="/salesorder" class="btn btn-secondary m-1">Cancel</a>
                <a type="button" data-toggle="modal" data-target="#confirmation_{{$so->id}}" class="btn btn-primary m-1 text-white">Activate</a>
                @include('salesorder.modal_confirmation')
			</div>
		</div>
        </form>

        <style>
        thead input {
            width: 100%;
        }
        body {
            padding-right: 0px !important;
        }
        .switch {
        position: relative;
        display: inline-block;
        width: 30px;
        height: 17px;
        }

        .switch input { 
        opacity: 0;
        width: 0;
        height: 0;
        }

        .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
        }

        .slider:before {
        position: absolute;
        content: "";
        height: 13px;
        width: 13px;
        left: 2px;
        bottom: 2px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
        }

        input:checked + .slider {
        background-color: #2196F3;
        }

        input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
        -webkit-transform: translateX(13px);
        -ms-transform: translateX(13px);
        transform: translateX(13px);
        }

        /* Rounded sliders */
        .slider.round {
        border-radius: 17px;
        }

        .slider.round:before {
        border-radius: 50%;
        }
        </style>
@endsection

@section('scripts')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script>
    $( document ).ready(function() {
        $('#example thead tr').clone(true).appendTo( '#example thead' );
        $('#example thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            
            if ( title !== 'Aksi' ) {
                $(this).html( '<input type="text" style="text-align:center; font-family:system-ui, FontAwesome;" placeholder="&#xF002;" class="placeholder-fix" />' );
    
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                } );
            } else {
                $(this).html('');
            }
        } );
    
        var table = $('#example').DataTable( {
            orderCellsTop: true,
            fixedHeader: true
        } );

        $('form').on('submit', function(e){
            var $form = $(this);

            table.$('input[type="checkbox"]').each(function(){
                if(!$.contains(document, this)){
                    if(this.checked){
                        $form.append(
                        $('<input>')
                            .attr('type', 'hidden')
                            .attr('name', this.name)
                            .val(this.value)
                        );
                    }
                } 
            });          
            });
    });
    </script>
@endsection