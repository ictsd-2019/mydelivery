<!-- Nav tabs -->
<ul id="gender" class="nav nav-tabs" role="tablist">
	<li role="presentation" class="nav-item active">
		<a class="nav-link" href="#gender" role="tab" data-toggle="tab">Import Data</a>
	</li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
	<div role="tabpanel" class="tab-pane active" id="gender">
        <div class="row form-group">
            <div class="col-md-4 col-sm-5">
				<label>Select File for Upload</label>
			</div>
			<div class="col-md-8 col-sm-7">
                <input type="file" name="select_file" required/>
                <p><small>.xls, .xslx</small></p>
			</div>
		</div>
        <div class="row form-group">
            <div class="col-md-4 col-sm-5">
				<label></label>
			</div>
			<div class="col-md-8 col-sm-7">
				<a href={{ asset('/storage/upload/TemplateSalesOrder.xlsx') }}>Download Template</a>
				<br>
				<a href={{ asset('/storage/upload/KetentuanDanContohUploadMSISDN.xlsx') }}>Download Panduan</a>
				<br>
				<a href="/device/export">Download Daftar Device</a>
				<br>
				<a href="/package/export">Download Daftar Basic Package</a>
				<br>
				<a href="/addon/export">Download Daftar Add On Package</a>
			</div>
		</div>
	</div>
</div>