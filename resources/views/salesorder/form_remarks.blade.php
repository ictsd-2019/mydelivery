<div id="create_remarks_{{$so->id}}" class="modal fade">
    <div class="modal-dialog modal-lg" role="document" >
        <div class="modal-content" style="border-radius:2%;">
            <div class="modal-header">
                <p class="modal-title" style="font-size:25px;"><b>Remarks {{$so->quote_no}}</b></p>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <ul>
                    @foreach($so->remarks as $remark)
                        <li>({{$remark->created}}) {{$remark->user}}: {{$remark->remarks}}</li>
                    @endforeach
                </ul>
                <br/>
                <form role="form" method="POST" action="/salesorder/{{$so->id}}/remarks">
                    {{ csrf_field() }}
                    <div class="row form-group">
                        <div class="col-md-1 col-sm-1">
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <label>Remarks</label>
                        </div>
                        <div class="col-md-8 col-sm-8">
                            <textarea class="form-control placeholder-fix" rows="4" name="remarks" placeholder="Enter remarks" required></textarea>
                        </div>
                        <div class="col-md-1 col-sm-1">
                        </div>
                    </div>
                    <center>
                    <div class="row">
                        <div class="col-md-1 col-sm-1">
                        </div>
                        <div class="col-md-11 col-sm-11">
                            <button type="button" class="btn btn-secondary m-1" data-dismiss="modal">Cancel</button>
                            <input type="submit" value="Submit" class="btn btn-primary m-1">
                        </div>
                    </div>
                    </center>
                </form>
            </div>
        </div>
    </div>
</div>