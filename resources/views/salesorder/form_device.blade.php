<!-- Nav tabs -->
<ul id="gender" class="nav nav-tabs" role="tablist">
	<li role="presentation" class="nav-item active">
		<a class="nav-link" href="#gender" role="tab" data-toggle="tab">Device Select</a>
	</li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
	<div role="tabpanel" class="tab-pane active" id="gender">
        <div class="row form-group">
            <div class="col-md-4 col-sm-5">
				<label>Brand</label> <sup class="required">*</sup>
			</div>
			<div class="col-md-8 col-sm-7">
                {!! Form::select('brand', $device, null, array('class'=>'form-control', 'placeholder' => '- Brand -', 'required' => 'required')) !!}
			</div>
		</div>
	</div>
</div>