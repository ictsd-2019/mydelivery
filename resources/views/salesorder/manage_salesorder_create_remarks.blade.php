<!-- @extends('layouts.app')

@section('sidebar')
    @parent
@endsection

@section('content')
    <h1 class="title">Create Remarks for Quote {{$salesorder->quote_no}}</h1>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
    @include('layouts.error')
    {!! Form::open(['action' => ['SalesorderRemarksController@store_remarks', $salesorder->id], 'class' => 'form-horizontal', 'id' => 'sales-form']) !!}
		<div class="row">
			<div class="col-md-12 col-sm-12">
                @include('salesorder.form_remarks')
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="/salesorder" class="btn btn-secondary m-1">Cancel</a>
				<input type="submit" value="Submit" class="btn btn-primary m-1">
			</div>
		</div>
	{!! Form::close() !!}
@endsection -->
