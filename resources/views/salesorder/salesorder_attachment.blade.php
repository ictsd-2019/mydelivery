@extends('layouts.frontpage_layout')

@section('sidebar')
    @parent
@endsection

@section('content')
    @if($checkAttachment != null)
        <h1 class="title">ATTACHMENT<i class="fa fa-check" aria-hidden="true" style="color:green;"></i></h1>
    @else
        <h1 class="title">ATTACHMENT</h1>
    @endif
    
    @include('layouts.error')
    <div class="container">
    <br>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
        @endif

        @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
        <div class="container">
            <div class="container border rounded" style="height:500px; overflow:scroll; overflow-x:auto">
                @if(count($attachment) == 0)
                    <p style="text-align:center">Belum ada attachment</p>
                @else
                    @foreach ($attachment as $att)
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="thumbnail">
                                <embed src="{{ asset($att->attachment) }}" class="responsive" width="700px" height="450px" style="margin-top:5%; margin-left:33%;"/>
                            </div>
                            @if(!Auth::user()->hasPermissionTo('verificate sales order'))
                                @if($salesorder->status == 'Draft' || $salesorder->status == 'Rejected' || ($salesorder->status_helper != null && $salesorder->status_helper != 'Re-verification'))
                                    <a href="/web/salesorder/{{$att->id}}/delete-attachment" style="margin-left:102%; text-align:center;" onclick="return confirm('Are you sure you want to delete this attachment?');" class="btn btn-primary btn-sm"><i class="fa fa-trash"></i></a>
                                @endif
                            @endif
                        </div>
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
        @if(Auth::user()->hasPermissionTo('add attachment'))
            @if($salesorder->status == 'Draft' || $salesorder->status == 'Rejected' || ($salesorder->status_helper != null && $salesorder->status_helper != 'Re-verification'))
            <br/>
            <div class="container">
                {{csrf_field()}}
                <div class="form-group">
                    <div class="file-loading">
                        <input id="file-1" type="file" name="file" multiple class="file" data-overwrite-initial="false" data-browse-on-zone-click="true">
                    </div>
                </div>
            </div>
            @endif
        @endif
    </div>
        <br>
    <center>
        <div>
            <a href="/web/salesorder/{{$id_salesorder}}" class="btn btn-secondary m-1">BACK</a>
            @if(Auth::user()->hasPermissionTo('add attachment') && $salesorder->status == 'Draft')
            <a href="/web/salesorder/{{$salesorder_code}}/attachment" class="btn btn-secondary m-1">REFRESH</a>
            <a href="/web/salesorder" onclick="return confirm('Apakah Anda telah mengupload semua attachment sebagai DRAFT?');" class="btn btn-secondary m-1">SAVE AS DRAFT</a>
                @if($num_transaction == $salesorder->quantity)
                <a href="/web/salesorder/{{$salesorder_code}}/verification" onclick="return confirm('Apakah Anda telah mengupload semua attachment?');" class="btn btn-primary m-1">SUBMIT</a>
                @endif
            @elseif($salesorder->status == 'Rejected')
            <a href="/web/salesorder/{{$salesorder_code}}/attachment" class="btn btn-secondary m-1">REFRESH</a>
            <a href="/web/salesorder/{{$id_salesorder}}/revision" onclick="return confirm('Apakah Anda telah mengupload semua revisi/tambahan attachment?');" class="btn btn-primary m-1">DONE</a>
            @elseif($salesorder->status_helper != null && $salesorder->status_helper != 'Re-verification')
            <a href="/web/salesorder/{{$salesorder_code}}/attachment" class="btn btn-secondary m-1">REFRESH</a>
            <a href="/web/salesorder/{{$id_salesorder}}/reverification" onclick="return confirm('Apakah Anda telah memperbaiki semua perubahan yang ada?');" class="btn btn-primary m-1">SUBMIT</a>
            @endif
        </div>
    </center>
    <br/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>
    <style type="text/css">
        .main-section{
            margin:0 auto;
            padding: 20px;
            margin-top: 100px;
            background-color: #fff;
            box-shadow: 0px 0px 20px #c1c1c1;
        }
        .fileinput-remove,
        .fileinput-upload{
            display: none;
        }
        .image-circular {
            width: 110px;
            height: 110px;
            background-size: cover;
            display: block;
        }
        .count:before {
        counter-increment: section;
        content: counter(section);
        }
        .btn-sm-edit{
        background-color:#00A6A0;
        color:white;
        border-radius: 25px;
        width: 130px !important;
        }
        .btn-sm-delete{
        background-color:#EA4764;
        color:white;
        border-radius: 25px;
        }
        .btn-sm-activate{
        background-color:#5773FF;
        color:white;
        border-radius: 25px;
        }

        .img-vendor{
            box-shadow: 3px 2px 6px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

        div.porto {
            width: 30%;
            margin-bottom: 25px;
        }

        div.text-porto {
            width:50%;
            text-align:center;
            padding: 0px 0px;
            margin-bottom: 25px;
        }
    </style>
@endsection

@section('scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        $("#file-1").fileinput({
            theme: 'fa',
            uploadUrl: "/web/salesorder/{{$salesorder_code}}/store-attachment",
            overwriteInitial: false,
            maxFileSize: 25000,
            timeout: 60000,
            uploadExtraData: function() {
                return {
                    _token: $("input[name='_token']").val(),
                };
            },
            slugCallback: function (filename) {
                return filename.replace('(', '_').replace(']', '_');
            }
        });
    </script>
@endsection

