@extends('layouts.app')

@section('content')
    <h1 class="title">View Quotation #{{$salesorder->quote_no}}</h1>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif

	@include('layouts.error')
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="active nav-link" role="presentation" data-toggle="tab" href="#quotation">Quotation Details</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" role="presentation" data-toggle="tab" href="#delivery_request">Delivery Request</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" role="tab" data-toggle="tab" href="#form_shipment">Form Shipment</a>
        </li>
    </ul>
    <div class="tab-content" style="background-color:#ffffff;">
    <div class="tab-pane show active" id="quotation">
            <div class="row">
                <div class="col-md-6">
                    <div class="card" style="box-shadow: 0 2px 2px rgba(0,0,0,0.1);">
                        <div class="card-header">Customer Info</div>
                        <div class="card-body">
                            <table class="table table-sm">
                                <tr>
                                    <th><strong>Corporate Name</strong></th>
                                    <td>{{ $salesorder->corp_name }}</td>
                                </tr>
                                <tr>
                                    <th><strong>PIC</strong></th>
                                    <td>{{ $salesorder->pic }}</td>
                                </tr>
                                <tr>
                                    <th><strong>No. Handphone</strong></th>
                                    <td>{{ $salesorder->pic_phone }}</td>
                                </tr>
                                <tr>
                                    <th><strong>Email</strong></th>
                                    <td>{{ $salesorder->pic_email }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card" style="box-shadow: 0 2px 2px rgba(0,0,0,0.1);">
                        <div class="card-header">Pre-Order Info</div>
                        <div class="card-body">
                            <table class="table table-sm">
                                <tr>
                                    <th><strong>BAST Date</strong></th>
                                    <td>{{ $salesorder->date_order }}</td>
                                </tr>
                                <tr>
                                    <th><strong>No. Quote</strong></th>
                                    <td>{{ $salesorder->quote_no }}</td>
                                </tr>
                                <tr>
                                    <th><strong>No. BA</strong></th>
                                    <td>{{ $salesorder->ba_no }}</td>
                                </tr>
                                <tr>
                                    <th><strong>Sales Agent</strong></th>
                                    <td>{{ $salesorder->sales['nama'] }}</td>
                                </tr>
                                <tr>
                                    <th><strong>Sales Dept.</strong></th>
                                    <td>{{ $salesorder->sales_group }}</td>
                                </tr>
                                <tr>
                                    <th><strong>Request Type</strong></th>
                                    <td>{{ $salesorder->type_req }}</td>
                                </tr>
                                <tr>
                                    <th><strong>Quantity</strong></th>
                                    <td>{{ $salesorder->quantity }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <center>
                        <h4 class="title">TRANSACTION</h4>
                    </center>
                </div>
            </div>
            <!-- <div class="row">
                <div class="col-md-12">
                    <div class="btn-group pull-right" role="group">
                        <a type="button" class="btn btn-success btn-sm m-1" href="/salesorder/transaction/{{$salesorder->id}}/create">Create Transaction</a>
                    </div>
                </div>
            </div> -->
            <div class="row">
                <div class="activation-history-wrapper container" style="overflow-y: hidden; overflow-x: auto;">
                @if($salesorder->type_req == 'Bundling')
                    <table id="example" class="table table-striped table-hover text-center" style="width:300%">
                @else
                    <table id="example" class="table table-striped table-hover text-center" style="width:200%">
                @endif
                    @if($salesorder->type_req == 'Bundling')
                        <thead>
                            <tr>
                                <th style="text-align:center; width:2%">No</th>
                                <th style="text-align:center; width:6%">MSISDN</th>
                                <th style="text-align:center; width:6%">ICCID</th>
                                <th style="text-align:center; width:5%">Type Bundling</th>
                                <th style="text-align:center; width:4%">Non M2M</th>
                                <th style="text-align:center; width:5%">M2M</th>
                                <th style="text-align:center; width:5%">Type Prepaid</th>
                                <th style="text-align:center; width:5%">Type Package</th>
                                <th style="text-align:center; width:5%">HLR</th>
                                <th style="text-align:center; width:3%">SIM Specification</th>
                                <th style="text-align:center; width:7%">Brand</th>
                                <th style="text-align:center; width:8%">Basic Package</th>
                                <th style="text-align:center; width:8%">Add On Package</th>
                                <th style="text-align:center; width:4%">Type Payment</th>
                                <th style="text-align:center; width:5%">Package MRC (Rp)</th>
                                <th style="text-align:center; width:4%">Tenor</th>
                                <th style="text-align:center; width:6%">Device Price (Rp)</th>
                                <th style="text-align:center; width:5%">Total Device Price (Rp)</th>
                                <th style="text-align:center; width:3%">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($transaction as $tr)
                            <tr>
                                <td> {{$loop->iteration}} </td>
                                <td> {{$tr->msisdn}} </td>
                                <td> {{$tr->iccid}} </td>
                                <td> {{$tr->type_bundling}} </td>
                                <td> {{$tr->non_m2m}} </td>
                                <td> {{$tr->m2m}} </td>
                                <td> {{$tr->type_prepaid}} </td>
                                <td> {{$tr->package_type}} </td>
                                <td> {{$tr->hlr}} </td>
                                <td> {{$tr->sim_specification}} </td>
                                <td> {{$tr->brand}} </td>
                                <td> {{$tr->package_basic}} </td>
                                <td> {{$tr->addon}} </td>
                                <td> {{$tr->type_payment}} </td>
                                <td> {{$tr->device_mrc}} </td>
                                <td> {{$tr->tenor}} bulan </td>
                                <td> {{$tr->device_price}} </td>
                                <td> {{$tr->price_total}} </td>
                                <td> {{$tr->status}} </td>
                            </tr>
                        @endforeach
                        </tbody>
                    @else
                        <thead>
                            <tr>
                                <th style="text-align:center; width:3%">No</th>
                                <th style="text-align:center; width:7%">MSISDN</th>
                                <th style="text-align:center; width:7%">ICCID</th>
                                <th style="text-align:center; width:5%">Type Bundling</th>
                                <th style="text-align:center; width:5%">Non M2M</th>
                                <th style="text-align:center; width:5%">M2M</th>
                                <th style="text-align:center; width:7%">Type Prepaid</th>
                                <th style="text-align:center; width:8%">Type Package</th>
                                <th style="text-align:center; width:5%">HLR</th>
                                <th style="text-align:center; width:3%">SIM Specification</th>
                                <th style="text-align:center; width:10%">Basic Package</th>
                                <th style="text-align:center; width:10%">Add On Package</th>
                                <th style="text-align:center; width:8%">Package MRC (Rp)</th>
                                <th style="text-align:center; width:5%">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($transaction as $tr)
                            <tr>
                                <td> {{$loop->iteration}} </td>
                                <td> {{$tr->msisdn}} </td>
                                <td> {{$tr->iccid}} </td>
                                <td> {{$tr->type_bundling}} </td>
                                <td> {{$tr->non_m2m}} </td>
                                <td> {{$tr->m2m}} </td>
                                <td> {{$tr->type_prepaid}} </td>
                                <td> {{$tr->package_type}} </td>
                                <td> {{$tr->hlr}} </td>
                                <td> {{$tr->sim_specification}} </td>
                                <td> {{$tr->package_basic}} </td>
                                <td> {{$tr->addon}} </td>
                                <td> {{$tr->device_mrc}} </td>
                                <td> {{$tr->status}} </td>
                            </tr>
                        @endforeach
                        </tbody>
                    @endif
                    </table>
                </div>
            </div>
        </div>
        <!-- TAB DELIVERY REQUEST -->
        <div class="tab-pane" id="delivery_request">
            <div class="activation-history-wrapper container" style="overflow-y: hidden; overflow-x: auto;">
                <table id="example2" class="table table-striped table-hover text-center" style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align:center; width:3%;">No</th>
                            <th style="text-align:center; width:15%;">No DR</th>
                            <th style="text-align:center; width:10%;">Request Date</th>
                            <th style="text-align:center; width:20%;">Corporate Name</th>
                            <th style="width:28%;">Destination</th>
                            <th style="text-align:center; width:6%;">Quantity</th>
                            <th style="text-align:center; width:7%;">Status</th>
                            <th style="text-align:center; width:11%;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($salesorder_delivery_request as $so_dr)
                        <tr>
                            <td> {{$loop->iteration}} </td>
                            <td> {{$so_dr->no_dr}} </td>
                            <td> {{$so_dr->request_date}} </td>
                            <td align="left"> {{$so_dr->corporate_name}} </td>
                            <td align="left"> {{$so_dr->address_1}} </td>
                            <td> {{$so_dr->quantity}} </td>
                            <td> {{$so_dr->status}} </td>
                            <td style="text-align:center;">
                                @if($so_dr->status == 'Draft')
                                    <a href="/salesorder/delivery/{{$so_dr->id}}/edit/edit" class="btn btn-success btn-sm m-1"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-warning btn-sm m-1" data-toggle="modal" data-target="#export_pdf_{{$so_dr->id}}"><i class="fa fa-file-pdf-o"></i></a>
                                    @include('delivery_request.verification.send_verification')
                                @elseif($so_dr->status == 'Approved')
                                    <a href="/delivery/export/{{$so_dr->id}}" class="btn btn-warning btn-sm m-1 text-white"><i class="fa fa-download"></i></a>
                                    <a data-toggle="modal" data-target="#upload_{{$so_dr->id}}" class="btn btn-success btn-sm m-1 text-white"><i class="fa fa-upload"></i></a>
                                    @include('delivery_request.signed_bast.modal_upload')
                                @elseif($so_dr->status == 'Signed')
                                    <a href="/delivery/export/{{$so_dr->id}}" class="btn btn-warning btn-sm m-1 text-white"><i class="fa fa-download"></i></a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <!-- TAB FORM SHIPMENT -->
        <div class="tab-pane" id="form_shipment">
            <div class="activation-history-wrapper container" style="overflow-y: hidden; overflow-x: auto;">
                <table id="example3" class="table table-striped table-hover text-center" style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align:center; width:5%;">No</th>
                            <th style="text-align:center; width:15%;">Request Date</th>
                            <th style="width:20%;">Corporate Name</th>
                            <th style="width:25%;">Destination</th>
                            <th style="text-align:center; width:10%;">Quantity</th>
                            <th style="text-align:center; width:10%;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($shipment as $ship)
                        <tr>
                            <td> {{$loop->iteration}} </td>
                            <td> {{$ship->request_date}} </td>
                            <td align="left"> {{$ship->corporate_name}} </td>
                            <td align="left"> {{$ship->address_1}} </td>
                            <td> {{$ship->quantity}} </td>
                            <td style="text-align:center;">
                                <a href="/salesorder/shipment/{{$ship->id}}/edit" class="btn btn-success btn-sm m-1"><i class="fa fa-edit"></i></a>
                                <a class="btn btn-warning btn-sm m-1" data-toggle="modal" data-target="#export_pdf_{{$ship->id}}"><i class="fa fa-file-pdf-o"></i></a>
                                    <!-- MODAL FORM -->
                                @include('shipment.form_export_pdf')
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-12">
            <a href="/salesorder" class="btn btn-secondary m-1">Back</a>
        </div>
    </div>

    <style>
    thead input {
        width: 100%;
    }
    body {
        padding-right: 0px !important;
    }
    </style>
@endsection

@section('scripts')
@parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    <script>
    $( document ).ready(function() {
        $('#example thead tr').clone(true).appendTo( '#example thead' );
        $('#example thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
           
            if ( title !== 'Aksi' ) {
                $(this).html( '<input type="text" style="text-align:center; font-family:system-ui, FontAwesome;" placeholder="&#xF002;" class="placeholder-fix" />' );
    
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                } );
            } else {
                $(this).html('');
            }
        } );
    
        var table = $('#example').DataTable( {
            orderCellsTop: true,
            fixedHeader: true
        } );

        $('#example2 thead tr').clone(true).appendTo( '#example2 thead' );
        $('#example2 thead tr:eq(1) th').each( function (i) {
            var title2 = $(this).text();
            
            if ( title2 !== 'Aksi' ) {
                $(this).html( '<input type="text" style="text-align:center; font-family:system-ui, FontAwesome;" placeholder="&#xF002;" class="placeholder-fix" />' );
    
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table2.column(i).search() !== this.value ) {
                        table2
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                } );
            } else {
                $(this).html('');
            }
        } );
    
        var table2 = $('#example2').DataTable( {
            orderCellsTop: true,
            fixedHeader: true
        } );
        
        $('#example3 thead tr').clone(true).appendTo( '#example3 thead' );
        $('#example3 thead tr:eq(1) th').each( function (i) {
            var title3 = $(this).text();
            
            if ( title3 !== 'Aksi' ) {
                $(this).html( '<input type="text" style="text-align:center; font-family:system-ui, FontAwesome;" placeholder="&#xF002;" class="placeholder-fix" />' );
    
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table3.column(i).search() !== this.value ) {
                        table3
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                } );
            } else {
                $(this).html('');
            }
        } );
    
        var table3 = $('#example3').DataTable( {
            orderCellsTop: true,
            fixedHeader: true
        } );
    });
    </script>
@endsection