<table>
    <thead>
        @if($salesorder->type_req == 'Bundling')
        <tr>
            <th style="text-align:center;">No</th>
            <th style="text-align:center;">MSISDN</th>
            <th style="text-align:center;">ICCID</th>
            <th style="text-align:center;">Type Bundling</th>
            <th style="text-align:center;">Non M2M</th>
            <th style="text-align:center;">M2M</th>
            <th style="text-align:center;">Type Prepaid</th>
            <th style="text-align:center;">Type Package</th>
            <th style="text-align:center;">HLR</th>
            <th style="text-align:center;">SIM Specification</th>
            <th style="text-align:center;">Brand</th>
            <th style="text-align:center;">Basic Package</th>
            <th style="text-align:center;">Add On Package</th>
            <th style="text-align:center;">Type Payment</th>
            <th style="text-align:center;">Package MRC (Rp)</th>
            <th style="text-align:center;">Tenor</th>
            <th style="text-align:center;">Device Price</th>
            <th style="text-align:center;">Total Device Price</th>
        </tr>
        @else
        <tr>
            <th style="text-align:center;">No</th>
            <th style="text-align:center;">MSISDN</th>
            <th style="text-align:center;">ICCID</th>
            <th style="text-align:center;">Type Bundling</th>
            <th style="text-align:center;">Non M2M</th>
            <th style="text-align:center;">M2M</th>
            <th style="text-align:center;">Type Prepaid</th>
            <th style="text-align:center;">Type Package</th>
            <th style="text-align:center;">HLR</th>
            <th style="text-align:center;">SIM Specification</th>
            <th style="text-align:center;">Basic Package</th>
            <th style="text-align:center;">Add On Package</th>
            <th style="text-align:center;">Package MRC (Rp)</th>
        </tr>
        @endif
    </thead>
    <tbody>
    @if($salesorder->type_req == 'Bundling')
    @foreach($transaction as $tr)
        <tr>
            <td> {{$loop->iteration}} </td>
            <td> {{$tr->msisdn}} </td>
            <td> {{$tr->iccid}} </td>
            <td> {{$tr->type_bundling}} </td>
            <td> {{$tr->non_m2m}} </td>
            <td> {{$tr->m2m}} </td>
            <td> {{$tr->type_prepaid}} </td>
            <td> {{$tr->package_type}} </td>
            <td> {{$tr->hlr}} </td>
            <td> {{$tr->sim_specification}} </td>
            <td> {{$tr->brand}} </td>
            <td> {{$tr->package_basic}} </td>
            <td> {{$tr->addon}} </td>
            <td> {{$tr->type_payment}} </td>
            <td> {{$tr->device_mrc}} </td>
            <td> {{$tr->tenor}} bulan </td>
            <td> {{$tr->device_price}} </td>
            <td> {{$tr->price_total}} </td>
        </tr>
    @endforeach
    @else
    @foreach($transaction as $tr)
        <tr>
            <td> {{$loop->iteration}} </td>
            <td> {{$tr->msisdn}} </td>
            <td> {{$tr->iccid}} </td>
            <td> {{$tr->type_bundling}} </td>
            <td> {{$tr->non_m2m}} </td>
            <td> {{$tr->m2m}} </td>
            <td> {{$tr->type_prepaid}} </td>
            <td> {{$tr->package_type}} </td>
            <td> {{$tr->hlr}} </td>
            <td> {{$tr->sim_specification}} </td>
            <td> {{$tr->package_basic}} </td>
            <td> {{$tr->addon}} </td>
            <td> {{$tr->device_mrc}} </td>
        </tr>
    @endforeach
    @endif
    </tbody>
</table>