@extends('layouts.frontpage_layout')

@section('sidebar')
    @parent
@endsection

@section('content')
    @if($checkAttachment != null)
    	<h1 class="title">VIEW SALES ORDER<i class="fa fa-check" aria-hidden="true" style="color:green;"></i></h1>
    @else
        <h1 class="title">VIEW SALES ORDER</h1>
    @endif

    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
	<br/>
	@include('layouts.error')
        {!! Form::open(['class' => 'form-horizontal', 'id' => 'sales-form']) !!}
		<div class="row">
			<div class="col-md-6 col-sm-6">
                <!-- Nav tabs -->
                <ul id="gender" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="nav-item active">
                        <a class="nav-link" href="#gender" role="tab" data-toggle="tab">Customer Info</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="gender">
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                            {!! Form::label('corp_name', 'Corporate Name', ['class' => 'col-form-label']) !!}
                            </div>
                            <div class="col-md-8 col-sm-7">
                            <input type="text" name="corp_name" class="form-control" value="{{ $salesorder->corp_name }}" disabled/>
                            </div>
                        </div>
                    <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                            {!! Form::label('pic', 'PIC', ['class' => 'col-form-label']) !!}
                            </div>
                            <div class="col-md-8 col-sm-7">
                            <input type="text" name="pic" class="form-control" value="{{ $salesorder->pic }}" disabled/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                            {!! Form::label('pic_phone', 'No. Handphone', ['class' => 'col-form-label']) !!}
                            </div>
                            <div class="col-md-8 col-sm-7">
                            <input type="text" name="pic_phone" class="form-control" value="{{ $salesorder->pic_phone }}" disabled/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                            {!! Form::label('pic_email', 'Email', ['class' => 'col-form-label']) !!}
                            </div>
                            <div class="col-md-8 col-sm-7">
                            <input type="text" name="pic_email" class="form-control" value="{{ $salesorder->pic_email }}" disabled/>
                            </div>
                        </div>
                    </div>
                </div>
				<br/>
			</div>	
			<div class="col-md-6 col-sm-6">
                <!-- Nav tabs -->
                <ul id="gender" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="nav-item active">
                        <a class="nav-link" href="#gender" role="tab" data-toggle="tab">Pre-Order Info</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="gender">
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                                {!! Form::label('date_order', 'Request Date', ['class' => 'col-form-label']) !!}
                            </div>
                            <div class="col-md-8 col-sm-7">
                                <input type="date" name="date_order" class="form-control" value="{{ $salesorder->date_order }}" disabled/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                                {!! Form::label('quote_no', 'No. Quote', ['class' => 'col-form-label']) !!}
                            </div>
                            <div class="col-md-8 col-sm-7">
                                <input type="text" name="quote_no" class="form-control" value="{{ $salesorder->quote_no }}" disabled/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                                {!! Form::label('ba_no', 'No. BA', ['class' => 'col-form-label']) !!}
                            </div>
                            <div class="col-md-8 col-sm-7">
                            <input type="text" name="ba_no" class="form-control" value="{{ $salesorder->ba_no }}" disabled/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                                <label>Sales Agent</label>
                            </div>
                            <div class="col-md-8 col-sm-7">
                                <input type="text" name="sales_name" class="form-control" value="{{ $salesorder->sales['nama'] }}" disabled/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                                {!! Form::label('sales_group', 'Sales Dept.', ['class' => 'col-form-label']) !!}
                            </div>
                            <div class="col-md-8 col-sm-7">
                                <input type="text" name="sales_group" class="form-control" value="{{ $salesorder->sales_group }}" disabled/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                                {!! Form::label('type_req', 'Request Type', ['class' => 'col-form-label']) !!}
                            </div>
                            <div class="col-md-8 col-sm-7">
                                <input type="radio" name="type_req" {{ ($salesorder->type_req=="Bundling")? "checked" : "" }} disabled> Bundling</label>
                                <input type="radio" name="type_req" {{ ($salesorder->type_req=="SIM Only")? "checked" : "" }} disabled> SIM Only</label>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                                <label>Quantity</label>
                            </div>
                            <div class="col-md-8 col-sm-7">
                                <input type="text" name="quantity" class="form-control" value="{{ $salesorder->quantity }}" disabled/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                                <label>Status</label>
                            </div>
                            <div class="col-md-8 col-sm-7">
                                <input type="text" name="status" class="form-control" value="{{ $salesorder->status }}" disabled/>
                            </div>
                        </div>
                    </div>
                </div>
				<br/>
			</div>
		</div>
        {!! Form::close() !!}
        <br/>
        @if(Auth::user()->hasPermissionTo('create transaction'))
        @if($salesorder->status == 'Draft' || $salesorder->status == 'Rejected')
            @if($total_row < $salesorder->quantity)
            <div class="row">
                <div class="col-md-12">
                    <center>
                    <h4 class="title">FORM TRANSACTION</h4>
                    </center>
                </div>
            </div>
            <br/>
            <div class="container">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" role="tab" data-toggle="tab" href="#satuan" style="margin-left:40px;">Manual</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" role="tab" data-toggle="tab" href="#import">Import</a>
                    </li>
                </ul>
                <div id="myTabContent" class="container tab-content">
                    <!-- TAB MANUAL -->
                    <div class="tab-pane fade active show" id="satuan">
                    <br/>
                        {!! Form::open(['action' => ['SalesorderTransactionController@store', $salesorder->id], 'class' => 'form-horizontal', 'id' => 'sales-form']) !!}
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                @include('salesorder.form_transaction_info')
                                <br/>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                @if($salesorder->type_req == "Bundling")
                                @include('salesorder.form_device')
                                <br/>
                                @endif
                                @include('salesorder.form_package_info')
                                <br/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <strong><sup class="required">*</sup><span class="glyphicon glyphicon-asterisk"></span></sup> Required</strong><br/>
                                <center>
                                    <input type="submit" value="SAVE" class="btn btn-primary m-1">
                                </center>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- TAB IMPORT -->
                    <div class="tab-pane fade" id="import">
                        {!! Form::open(['action' => ['SalesorderTransactionController@import', $salesorder->id], 'class' => 'form-horizontal', 'id' => 'sales-form', 'enctype' => 'multipart/form-data']) !!}
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                @include('salesorder.form_import')
                                <br/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <center>
                                    <input type="submit" value="UPLOAD" class="btn btn-primary m-1">
                                </center>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <br/>
            <br/>
            @endif
        @endif
        @endif
        <div class="row">
            <div class="col-md-12">
                <center>
                <h4 class="title">TRANSACTION</h4>
                </center>
            </div>
        </div>
        <div class="activation-history-wrapper container" style="overflow-y: hidden; overflow-x: auto;">
            @if($salesorder->type_req == 'Bundling')
            <table id="example" class="table table-striped table-hover text-center" style="width:250%">
            @else
            <table id="example" class="table table-striped table-hover text-center" style="width:200%">
            @endif
                @if($salesorder->type_req == 'Bundling')
                <thead>
                    <tr>
                        <th style="text-align:center; width:2%">No</th>
                        <th style="text-align:center; width:6%">MSISDN</th>
                        <th style="text-align:center; width:6%">ICCID</th>
                        <th style="text-align:center; width:5%">Type Bundling</th>
                        <th style="text-align:center; width:5%">Non M2M</th>
                        <th style="text-align:center; width:5%">M2M</th>
                        <th style="text-align:center; width:5%">Type Prepaid</th>
                        <th style="text-align:center; width:5%">Type Package</th>
                        <th style="text-align:center; width:5%">HLR</th>
                        <th style="text-align:center; width:3%">SIM Specification</th>
                        <th style="text-align:center; width:6%">Brand</th>
                        <th style="text-align:center; width:7%">Basic Package</th>
                        <th style="text-align:center; width:7%">Add On Package</th>
                        <th style="text-align:center; width:5%">Type Payment</th>
                        <th style="text-align:center; width:5%">Package MRC (Rp)</th>
                        <th style="text-align:center; width:4%">Tenor</th>
                        <th style="text-align:center; width:5%">Device Price (Rp)</th>
                        <th style="text-align:center; width:5%">Total Device Price (Rp)</th>
                        <th style="text-align:center; width:5%">Status</th>
                        @if(Auth::user()->hasPermissionTo('manage transaction'))
                            @if($salesorder->status == 'Draft' || $salesorder->status == 'Rejected' || ($salesorder->status_helper != null && $salesorder->status_helper != 'Re-verification'))
                                <th style="text-align:center; width:5%">Aksi</th>
                            @endif
                        @endif
                    </tr>
                </thead>
                <tbody>
                @foreach($transaction as $tr)
                    <tr>
                        <td> {{$loop->iteration}} </td>
                        <td> {{$tr->msisdn}} </td>
                        <td> {{$tr->iccid}} </td>
                        <td> {{$tr->type_bundling}} </td>
                        <td> {{$tr->non_m2m}} </td>
                        <td> {{$tr->m2m}} </td>
                        <td> {{$tr->type_prepaid}} </td>
                        <td> {{$tr->package_type}} </td>
                        <td> {{$tr->hlr}} </td>
                        <td> {{$tr->sim_specification}} </td>
                        <td> {{$tr->brand}} </td>
                        <td> {{$tr->package_basic}} </td>
                        <td> {{$tr->addon}} </td>
                        <td> {{$tr->type_payment}} </td>
                        <td> {{$tr->device_mrc}} </td>
                        <td> {{$tr->tenor}} bulan </td>
                        <td> {{$tr->device_price}} </td>
                        <td> {{$tr->price_total}} </td>
                        <td> {{$tr->status}} </td>
                        @if(Auth::user()->hasPermissionTo('manage transaction'))
                            @if($salesorder->status == 'Draft' || $salesorder->status == 'Rejected' || ($salesorder->status_helper != null && $salesorder->status_helper != 'Re-verification'))
                            <td>
                                <a href="/web/salesorder/transaction/{{$tr->id}}/edit" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
                                @if($salesorder->status_helper == null && $salesorder->status_helper != 'Re-verification')
                                <a href="/web/salesorder/transaction/{{$tr->id}}/delete" onclick="return confirm('Are you sure you want to delete this transaction?');" class="btn btn-primary btn-sm"><i class="fa fa-trash"></i></a>
                                @endif
                            </td>
                            @endif
                        @endif
                    </tr>
                @endforeach
                </tbody>
                @else
                <thead>
                    <tr>
                        <th style="text-align:center; width:2%">No</th>
                        <th style="text-align:center; width:6%">MSISDN</th>
                        <th style="text-align:center; width:6%">ICCID</th>
                        <th style="text-align:center; width:5%">Type Bundling</th>
                        <th style="text-align:center; width:5%">Non M2M</th>
                        <th style="text-align:center; width:5%">M2M</th>
                        <th style="text-align:center; width:5%">Type Prepaid</th>
                        <th style="text-align:center; width:5%">Type Package</th>
                        <th style="text-align:center; width:5%">HLR</th>
                        <th style="text-align:center; width:3%">SIM Specification</th>
                        <th style="text-align:center; width:7%">Basic Package</th>
                        <th style="text-align:center; width:7%">Add On Package</th>
                        <th style="text-align:center; width:5%">Package MRC (Rp)</th>
                        @if(Auth::user()->hasPermissionTo('manage transaction'))
                            @if($salesorder->status == 'Draft' || $salesorder->status == 'Rejected' || ($salesorder->status_helper != null && $salesorder->status_helper != 'Re-verification'))
                                <th style="text-align:center; width:5%">Aksi</th>
                            @endif
                        @endif
                    </tr>
                </thead>
                <tbody>
                @foreach($transaction as $tr)
                    <tr>
                        <td> {{$loop->iteration}} </td>
                        <td> {{$tr->msisdn}} </td>
                        <td> {{$tr->iccid}} </td>
                        <td> {{$tr->type_bundling}} </td>
                        <td> {{$tr->non_m2m}} </td>
                        <td> {{$tr->m2m}} </td>
                        <td> {{$tr->type_prepaid}} </td>
                        <td> {{$tr->package_type}} </td>
                        <td> {{$tr->hlr}} </td>
                        <td> {{$tr->sim_specification}} </td>
                        <td> {{$tr->package_basic}} </td>
                        <td> {{$tr->addon}} </td>
                        <td> {{$tr->device_mrc}} </td>
                        @if(Auth::user()->hasPermissionTo('manage transaction'))
                            @if($salesorder->status == 'Draft' || $salesorder->status == 'Rejected' || ($salesorder->status_helper != null && $salesorder->status_helper != 'Re-verification'))
                            <td>
                                <a href="/web/salesorder/transaction/{{$tr->id}}/edit" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
                                @if($salesorder->status_helper == null && $salesorder->status_helper != 'Re-verification')
                                <a href="/web/salesorder/transaction/{{$tr->id}}/delete" onclick="return confirm('Are you sure you want to delete this transaction?');" class="btn btn-primary btn-sm"><i class="fa fa-trash"></i></a>
                                @endif
                            </td>
                            @endif
                        @endif
                    </tr>
                @endforeach
                </tbody>
                @endif
            </table>
            <a class="btn btn-warning btn-sm" href="/web/salesorder/transaction/{{$salesorder->id}}/export" style="float:right;"><i class="fa fa-file-excel-o"></i> Download</a>
        </div>
		<div class="row">
			<div class="col-md-12">
				<br/>
                <center>
    				<a href="/web/salesorder" class="btn btn-secondary m-1">BACK</a>
                    @if($salesorder->type_req == 'SIM Only' && count($transaction) == $salesorder->quantity)
                    <a href="" class="btn btn-warning m-1"><i class="fa fa-file-pdf-o"></i> FORM REQUEST</a>
                    @endif
                    <a href="/web/salesorder/{{$salesorder->salesorder_code}}/attachment" class="btn btn-primary m-1">ATTACHMENT</a>
                </center>
			</div>
		</div>

    <style>
    thead input {
        width: 100%;
    }
    </style>
@endsection

@section('scripts')
    @parent
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <script>
    $( document ).ready(function() {
        var postURL = "<?php echo url('addmore'); ?>";
        console.log(postURL);
        var i=1;  
        $('#add').click(function(){
            i++;
            $('#dynamic_field').append('<tr id="row'+i+'"><td width="35%" style="padding-left:3%"></td><td width="58%">{!! Form::select('package_add[]', $add_on, null, array('class'=>'form-control', 'placeholder' => '- Add On Package -', 'required' => 'required')) !!}</td><td width="7%"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
        });

        $(document).on('click', '.btn_remove', function(){  
            var button_id = $(this).attr("id");   
            $('#row'+button_id+'').remove();  
        });

        $('#example thead tr').clone(true).appendTo( '#example thead' );
        $('#example thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            $(this).html( '<input type="text" style="text-align:center; font-family:system-ui, FontAwesome;" placeholder="&#xF002;" class="placeholder-fix" />' );
    
            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );
    
        var table = $('#example').DataTable( {
            orderCellsTop: true,
            fixedHeader: true
        } );
    });
    </script>
    <script type="text/javascript">
        function tenorCheck() {
            if (document.getElementById('installment').checked) {
                document.getElementById('tenor').style.display = 'block';
            } else {
                document.getElementById('tenor').style.display = 'none';
            }
        }
    </script>
    <script type="text/javascript">
        function typeCheck() {
            if (document.getElementById('prepaid').checked) {
                document.getElementById('package_type').style.display = 'block';
                document.getElementById('prepaid_type').style.display = 'block';
                document.getElementById('hlr').style.display = 'block';
                document.getElementById('m2m').style.display = 'none';
            } else {
                document.getElementById('package_type').style.display = 'none';
                document.getElementById('prepaid_type').style.display = 'none';
                document.getElementById('hlr').style.display = 'none';
                document.getElementById('m2m').style.display = 'block';
            }
        }
    </script>
@endsection
