@extends('layouts.app')

@section('sidebar')
    @parent
@endsection

@section('content')
    <h1 class="title">Manage Quote</h1>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
    <div class="activation-history-wrapper container" style="overflow-y: hidden; overflow-x: auto;">
        <table id="example" class="table table-striped table-hover text-center" style="width:150%">
            <thead>
                <tr>
                    <th style="text-align:center; width:3%;">No</th>
                    <th style="text-align:center; width:7%;">Request Date</th>
                    <th style="text-align:center; width:7%;">No. Quote</th>
                    <th style="text-align:center; width:7%;">No. BA</th>
                    <th style="text-align:center; width:12%;">Corporate Name</th>
                    <th style="text-align:center; width:5%;">Qty</th>
                    <th style="text-align:center; width:14%;">Sales Name</th>
                    <th style="text-align:center; width:10%;">Price (Rp)</th>
                    <th style="text-align:center; width:6%;">Status</th>
                    <th style="text-align:center; width:13%;">Remarks</th>
                    <th style="text-align:center; width:9%;">PIC SD</th>
                    <th style="text-align:center; width:7%;">Aksi</th>
                </tr>
            </thead>
            <tbody>
            @foreach($salesorder as $so)
                <tr>
                    <td> {{$loop->iteration}} </td>
                    <td> {{$so->date_order}} </td>
                    <td> {{$so->quote_no}} </td>
                    <td> {{$so->ba_no}} </td>
                    <td style="text-align:left;"> {{$so->corp_name}} </td>
                    <td> {{$so->quantity}} </td>
                    <td style="text-align:left;"> {{$so->sales["nama"]}} ({{$so->sales_group}}) </td>
                    <td> {{$so->total_price}} </td>
                    <td>
                        @if($so->status_helper != null)
                            @if($so->status == 'Submitted')
                            <span class="badge badge-primary">{{$so->status}}</span>
                            <span class="badge badge-danger">{{$so->status_helper}}</span>
                            @elseif($so->status == 'Activation')
                            <span class="badge badge-warning">{{$so->status}}</span>
                            <span class="badge badge-danger">{{$so->status_helper}}</span>
                            @elseif($so->status == 'In Progress RM')
                            <span class="badge badge-warning">{{$so->status}}</span>
                            <span class="badge badge-danger">{{$so->status_helper}}</span>
                            @elseif($so->status == 'Delivered')
                            <span class="badge badge-info">{{$so->status}}</span>
                            <span class="badge badge-danger">{{$so->status_helper}}</span>
                            @elseif($so->status == 'Done')
                            <span class="badge badge-success">{{$so->status}}</span>
                            <span class="badge badge-danger">{{$so->status_helper}}</span>
                            @endif
                        @else
                            @if($so->status == 'Submitted')
                            <span class="badge badge-primary">{{$so->status}}</span>
                            @elseif($so->status == 'Activation')
                            <span class="badge badge-warning">{{$so->status}}</span>
                            @elseif($so->status == 'In Progress RM')
                            <span class="badge badge-warning">{{$so->status}}</span>
                            @elseif($so->status == 'Delivered')
                            <span class="badge badge-info">{{$so->status}}</span>
                            @elseif($so->status == 'Done')
                            <span class="badge badge-success">{{$so->status}}</span>
                            @endif
                        @endif
                    </td>
                    @if($so->last_remarks == null)
                        <td> - </td>
                    @else
                        <td style="text-align:left;">
                            ({{$so->last_remarks["created"]}}) {{$so->last_remarks["user"]}}: {{$so->last_remarks["remarks"]}}
                        </td>
                    @endif
                    @if($so->pic_quote == null)
                        <td> - </td>
                    @else
                        <td style="text-align:left;"> {{$so->pic_quote}} </td>
                    @endif
                    <td style="text-align:left;">
                        @if($so->pic_quote == null)
                            <a data-toggle="modal" data-target="#confirmation_{{$so->id}}" class="btn btn-success btn-sm m-1 text-white"><i class="fa fa-id-badge"></i></a>
                            @include('salesorder.modal_confirmation')
                        @elseif($so->status == 'In Progress RM' && Auth::user()->hasPermissionTo('check availability device'))
                            <a href="/salesorder/{{$so->id}}/activation" class="btn btn-warning btn-sm m-1 text-white"><i class="fa fa-arrow-circle-right"></i></a>
                            <a data-toggle="modal" data-target="#create_remarks_{{$so->id}}" class="btn btn-secondary btn-sm m-1"><i class="fa fa-comments"></i></a>
                            @include('salesorder.form_remarks')
                        @elseif($so->status_helper == 'In Progress RM' && Auth::user()->hasPermissionTo('check availability device'))
                            <a href="/salesorder/{{$so->id}}/check-device" class="btn btn-warning btn-sm m-1 text-white"><i class="fa fa-arrow-circle-right"></i></a>
                            <!-- <a data-toggle="modal" data-target="#create_remarks_{{$so->id}}" class="btn btn-secondary btn-sm m-1"><i class="fa fa-comments"></i></a>
                            @include('salesorder.form_remarks') -->
                        @elseif($so->status == 'Activation')
                            <a href="/salesorder/{{$so->id}}/activation" class="btn btn-info btn-sm m-1"><i class="fa fa-truck"></i></a>
                            <a data-toggle="modal" data-target="#create_remarks_{{$so->id}}" class="btn btn-secondary btn-sm m-1"><i class="fa fa-comments"></i></a>
                            @include('salesorder.form_remarks')
                        @elseif($so->status == 'Done')
                            <a href="/salesorder/{{$so->id}}" class="btn btn-primary btn-sm m-1"><i class="fa fa-folder-open-o"></i></a>
                        @endif 
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <a class="btn btn-warning btn-sm m-1" data-toggle="modal" data-target="#export" style="float:right;"><i class="fa fa-file-excel-o"></i> Download</a>
    </div>
    <br/>
    <small class="info-status">
        <strong>Info Status :</strong><br/>
        <table>
            <tr>
                <td>
                    <h6><span class="badge badge-primary">Submitted</span></h6>
                </td>
                <td style="padding:7px;"> : </td>
                <td>
                    Menunggu pick up oleh Tim SD
                </td>
            </tr>
            <tr>
                <td>
                    <h6><span class="badge badge-warning">In Progress RM</span></h6>
                </td>
                <td style="padding:7px;"> : </td>
                <td>
                    Mengecek ketersediaan device
                </td>
            </tr>
            <tr>
                <td>
                    <h6><span class="badge badge-warning">Activation</span></h6>
                </td>
                <td style="padding:7px;"> : </td>
                <td>
                    Menunggu aktivasi
                </td>
            </tr>
            <tr>
                <td>
                    <h6><span class="badge badge-info">Delivered</span></h6>
                </td>
                <td style="padding:7px;"> : </td>
                <td>
                    Pengiriman DR-BAST dan menunggu BAST ditandatangani
                </td>
            </tr>
            <tr>
                <td>
                    <h6><span class="badge badge-success">Done</span></h6>
                </td>
                <td style="padding:7px;"> : </td>
                <td>
                    Selesai
                </td>
            </tr>
        </table>
    </small>

    <!-- MODAL FORM -->
    @include('salesorder.form_manage_export')

    <style>
    thead input {
        width: 100%;
    }
    body {
        padding-right: 0px !important;
    }
    </style>
@endsection

@section('scripts')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#example thead tr').clone(true).appendTo( '#example thead' );
        $('#example thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();

            if(title === 'Aksi'){
                $(this).html('');
            } else {
                $(this).html( '<input type="text" style="text-align:center; font-family:system-ui, FontAwesome;" placeholder="&#xF002;" class="placeholder-fix" />' );
    
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                } );
            }
        } );
    
        var table = $('#example').DataTable( {
            orderCellsTop: true,
            fixedHeader: true,
        } );
	} );
    </script>
@endsection