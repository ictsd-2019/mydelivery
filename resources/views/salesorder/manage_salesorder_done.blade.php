@extends('layouts.app')

@section('content')
    <h1 class="title">Quotation #{{$salesorder->quote_no}}</h1>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
	@include('layouts.error')
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="active nav-link" role="tab" data-toggle="tab" href="#quotation">Quotation Details</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" role="tab" data-toggle="tab" href="#delivery_request">Delivery Request</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" role="tab" data-toggle="tab" href="#form_shipment">Form Shipment</a>
        </li>
    </ul>
    <div class="tab-content" style="background-color:#ffffff;">

        <!-- TAB QUOTATION -->
        <div class="tab-pane show active" id="quotation">
            <div class="row">
                <div class="col-md-6">
                    <div class="card" style="box-shadow: 0 2px 2px rgba(0,0,0,0.1);">
                        <div class="card-header">Customer Info</div>
                        <div class="card-body">
                            <table class="table table-sm">
                                <tr>
                                    <th><strong>Corporate Name</strong></th>
                                    <td>{{ $salesorder->corp_name }}</td>
                                </tr>
                                <tr>
                                    <th><strong>PIC</strong></th>
                                    <td>{{ $salesorder->pic }}</td>
                                </tr>
                                <tr>
                                    <th><strong>No. Handphone</strong></th>
                                    <td>{{ $salesorder->pic_phone }}</td>
                                </tr>
                                <tr>
                                    <th><strong>Email</strong></th>
                                    <td>{{ $salesorder->pic_email }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card" style="box-shadow: 0 2px 2px rgba(0,0,0,0.1);">
                        <div class="card-header">Pre-Order Info</div>
                        <div class="card-body">
                            <table class="table table-sm">
                                <tr>
                                    <th><strong>Request Date</strong></th>
                                    <td>{{ $salesorder->date_order }}</td>
                                </tr>
                                <tr>
                                    <th><strong>No. Quote</strong></th>
                                    <td>{{ $salesorder->quote_no }}</td>
                                </tr>
                                <tr>
                                    <th><strong>No. BA</strong></th>
                                    <td>{{ $salesorder->ba_no }}</td>
                                </tr>
                                <tr>
                                    <th><strong>Sales Agent</strong></th>
                                    <td>{{ $salesorder->sales['nama'] }}</td>
                                </tr>
                                <tr>
                                    <th><strong>Sales Dept.</strong></th>
                                    <td>{{ $salesorder->sales_group }}</td>
                                </tr>
                                <tr>
                                    <th><strong>Request Type</strong></th>
                                    <td>{{ $salesorder->type_req }}</td>
                                </tr>
                                <tr>
                                    <th><strong>Quantity</strong></th>
                                    <td>{{ $salesorder->quantity }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <center>
                        <h4 class="title">TRANSACTION</h4>
                    </center>
                </div>
            </div>
            <!-- <div class="row">
                <div class="col-md-12">
                    <div class="btn-group pull-right" role="group">
                        <a type="button" class="btn btn-success btn-sm m-1" href="/salesorder/transaction/{{$salesorder->id}}/create">Create Transaction</a>
                    </div>
                </div>
            </div> -->
            <div class="row">
                <div class="activation-history-wrapper container" style="overflow-y: hidden; overflow-x: auto;">
                @if($salesorder->type_req == 'Bundling')
                    <table id="example" class="table table-striped table-hover text-center" style="width:300%">
                @else
                    <table id="example" class="table table-striped table-hover text-center" style="width:200%">
                @endif
                    @if($salesorder->type_req == 'Bundling')
                        <thead>
                            <tr>
                                <th style="text-align:center; width:2%">No</th>
                                <th style="text-align:center; width:6%">MSISDN</th>
                                <th style="text-align:center; width:6%">ICCID</th>
                                <th style="text-align:center; width:5%">Type Bundling</th>
                                <th style="text-align:center; width:4%">Non M2M</th>
                                <th style="text-align:center; width:5%">M2M</th>
                                <th style="text-align:center; width:5%">Type Prepaid</th>
                                <th style="text-align:center; width:5%">Type Package</th>
                                <th style="text-align:center; width:5%">HLR</th>
                                <th style="text-align:center; width:3%">SIM Specification</th>
                                <th style="text-align:center; width:7%">Brand</th>
                                <th style="text-align:center; width:8%">Basic Package</th>
                                <th style="text-align:center; width:8%">Add On Package</th>
                                <th style="text-align:center; width:4%">Type Payment</th>
                                <th style="text-align:center; width:5%">Package MRC (Rp)</th>
                                <th style="text-align:center; width:4%">Tenor</th>
                                <th style="text-align:center; width:6%">Device Price (Rp)</th>
                                <th style="text-align:center; width:5%">Total Device Price (Rp)</th>
                                <th style="text-align:center; width:3%">Status</th>
                                <th style="text-align:center; width:4%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($transaction as $tr)
                            <tr>
                                <td> {{$loop->iteration}} </td>
                                <td> {{$tr->msisdn}} </td>
                                <td> {{$tr->iccid}} </td>
                                <td> {{$tr->type_bundling}} </td>
                                <td> {{$tr->non_m2m}} </td>
                                <td> {{$tr->m2m}} </td>
                                <td> {{$tr->type_prepaid}} </td>
                                <td> {{$tr->package_type}} </td>
                                <td> {{$tr->hlr}} </td>
                                <td> {{$tr->sim_specification}} </td>
                                <td> {{$tr->brand}} </td>
                                <td> {{$tr->package_basic}} </td>
                                <td> {{$tr->addon}} </td>
                                <td> {{$tr->type_payment}} </td>
                                <td> {{$tr->device_mrc}} </td>
                                <td> {{$tr->tenor}} bulan </td>
                                <td> {{$tr->device_price}} </td>
                                <td> {{$tr->price_total}} </td>
                                <td> {{$tr->status}} </td>
                                <td style="text-align:center;">
                                    @if($tr->status != 'On DR' && $tr->status != 'Delivered' && $tr->status != 'On Shipment')
                                    <a class="btn btn-success btn-sm m-1" href="/salesorder/transaction/{{$tr->id}}/edit"><i class="fa fa-edit"></i></a>
                                    <a href="/salesorder/transaction/{{$tr->id}}/delete" onclick="return confirm('Are you sure you want to delete this transaction?');" class="btn btn-primary btn-sm m-1"><i class="fa fa-trash"></i></a>
                                    @elseif($tr->status == 'On DR' || $tr->status == 'On Shipment')
                                    <a class="btn btn-success btn-sm m-1" href="/salesorder/transaction/{{$tr->id}}/edit"><i class="fa fa-edit"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    @else
                        <thead>
                            <tr>
                                <th style="text-align:center; width:3%">No</th>
                                <th style="text-align:center; width:7%">MSISDN</th>
                                <th style="text-align:center; width:7%">ICCID</th>
                                <th style="text-align:center; width:5%">Type Bundling</th>
                                <th style="text-align:center; width:5%">Non M2M</th>
                                <th style="text-align:center; width:5%">M2M</th>
                                <th style="text-align:center; width:7%">Type Prepaid</th>
                                <th style="text-align:center; width:8%">Type Package</th>
                                <th style="text-align:center; width:5%">HLR</th>
                                <th style="text-align:center; width:3%">SIM Specification</th>
                                <th style="text-align:center; width:10%">Basic Package</th>
                                <th style="text-align:center; width:10%">Add On Package</th>
                                <th style="text-align:center; width:8%">Package MRC (Rp)</th>
                                <th style="text-align:center; width:5%">Status</th>
                                <th style="text-align:center; width:5%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($transaction as $tr)
                            <tr>
                                <td> {{$loop->iteration}} </td>
                                <td> {{$tr->msisdn}} </td>
                                <td> {{$tr->iccid}} </td>
                                <td> {{$tr->type_bundling}} </td>
                                <td> {{$tr->non_m2m}} </td>
                                <td> {{$tr->m2m}} </td>
                                <td> {{$tr->type_prepaid}} </td>
                                <td> {{$tr->package_type}} </td>
                                <td> {{$tr->hlr}} </td>
                                <td> {{$tr->sim_specification}} </td>
                                <td> {{$tr->package_basic}} </td>
                                <td> {{$tr->addon}} </td>
                                <td> {{$tr->device_mrc}} </td>
                                <td> {{$tr->status}} </td>
                                <td style="text-align:center;">
                                    @if($tr->status != 'On DR' && $tr->status != 'Delivered')
                                    <a class="btn btn-success btn-sm m-1" href="/salesorder/transaction/{{$tr->id}}/edit"><i class="fa fa-edit"></i></a>
                                    <a href="/salesorder/transaction/{{$tr->id}}/delete" onclick="return confirm('Are you sure you want to delete this transaction?');" class="btn btn-primary btn-sm m-1"><i class="fa fa-trash"></i></a>
                                    @elseif($tr->status == 'On DR')
                                    <a class="btn btn-success btn-sm m-1" href="/salesorder/transaction/{{$tr->id}}/edit"><i class="fa fa-edit"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    @endif
                    </table>
                </div>
            </div>
        </div>

        <!-- TAB DELIVERY REQUEST -->
        <div class="tab-pane" id="delivery_request">
            <div class="activation-history-wrapper container" style="overflow-y: hidden; overflow-x: auto;">
                <table id="example2" class="table table-striped table-hover text-center" style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align:center; width:3%;">No</th>
                            <th style="text-align:center; width:14%;">No DR</th>
                            <th style="text-align:center; width:9%;">Request Date</th>
                            <th style="text-align:center; width:18%;">Corporate Name</th>
                            <th style="width:25%;">Destination</th>
                            <th style="text-align:center; width:5%;">Qty</th>
                            <th style="text-align:center; width:7%;">Status</th>
                            <th style="text-align:center; width:7%;">BAST Date</th>
                            <th style="text-align:center; width:11%;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($salesorder_delivery_request as $trans)
                        <tr>
                            <td> {{$loop->iteration}} </td>
                            <td> {{$trans->no_dr}} </td>
                            <td> {{$trans->request_date}} </td>
                            <td align="left"> {{$trans->corporate_name}} </td>
                            <td align="left"> {{$trans->address_1}} </td>
                            <td> {{$trans->quantity}} </td>
                            <td> {{$trans->status}} </td>
                            <td> {{$trans->bast_date}} </td>
                            <td style="text-align:center;">
                                @if($trans->status == 'Draft')
                                    <a href="/salesorder/delivery/{{$trans->id}}/edit/edit" class="btn btn-success btn-sm m-1"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-warning btn-sm m-1" data-toggle="modal" data-target="#export_pdf_{{$trans->id}}"><i class="fa fa-file-pdf-o"></i></a>
                                    @include('delivery_request.verification.send_verification')
                                @elseif($trans->status == 'Approved')
                                    <a href="/delivery/export/{{$trans->id}}" class="btn btn-warning btn-sm m-1 text-white"><i class="fa fa-download"></i></a>
                                    <a data-toggle="modal" data-target="#confirmation_{{$trans->id}}" class="btn btn-success btn-sm m-1 text-white"><i class="fa fa-arrow-circle-right"></i></a>
                                    @include('courier.modal_confirmation')
                                @elseif($trans->status == 'Signed')
                                    <a href="/bast/export/{{$trans->id}}-dr" class="btn btn-warning btn-sm m-1 text-white"><i class="fa fa-cloud-download"></i></a>
                                    <a data-toggle="modal" data-target="#modal_photo_{{$trans->id}}" class="btn btn-success btn-sm m-1 text-white"><i class="fa fa-file-photo-o"></i></a>
                                    @include('bast.modal_photo')
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <center>
                <a type="button" class="btn btn-primary m-1" href="/salesorder/delivery/{{$salesorder->id}}/create">Create DR</a>
            </center>
        </div>

        <!-- TAB FORM SHIPMENT -->
        <div class="tab-pane" id="form_shipment">
            <div class="activation-history-wrapper container" style="overflow-y: hidden; overflow-x: auto;">
                <table id="example3" class="table table-striped table-hover text-center" style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align:center; width:5%;">No</th>
                            <th style="text-align:center; width:10%;">Request Date</th>
                            <th style="width:19%;">Corporate Name</th>
                            <th style="width:24%;">Destination</th>
                            <th style="text-align:center; width:7%;">Quantity</th>
                            <th style="text-align:center; width:10%;">Status</th>
                            <th style="text-align:center; width:10%;">BAST Date</th>
                            <th style="text-align:center; width:10%;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($shipment as $trans)
                        <tr>
                            <td> {{$loop->iteration}} </td>
                            <td> {{$trans->request_date}} </td>
                            <td align="left"> {{$trans->corporate_name}} </td>
                            <td align="left"> {{$trans->address_1}} </td>
                            <td> {{$trans->quantity}} </td>
                            <td> {{$trans->status}} </td>
                            <td> {{$trans->bast_date}} </td>
                            <td style="text-align:center;">
                                @if($trans->status == 'Draft')
                                    <a class="btn btn-warning btn-sm m-1" href="/shipment/export/{{$trans->id}}"><i class="fa fa-file-pdf-o"></i></a>
                                    @include('shipment.form_export_pdf')
                                    <a data-toggle="modal" data-target="#confirmation_{{$trans->id}}" class="btn btn-success btn-sm m-1 text-white"><i class="fa fa-arrow-circle-right"></i></a>
                                    @include('courier.modal_confirmation')
                                @elseif($trans->status == 'Signed')
                                    <a href="/bast/export/{{$trans->id}}-shipment" class="btn btn-warning btn-sm m-1 text-white"><i class="fa fa-cloud-download"></i></a>
                                    <a data-toggle="modal" data-target="#modal_photo_{{$trans->id}}" class="btn btn-success btn-sm m-1 text-white"><i class="fa fa-file-photo-o"></i></a>
                                    @include('bast.modal_photo')
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <center>
                <a type="button" class="btn btn-primary m-1" href="/salesorder/shipment/{{$salesorder->id}}/create">Input Form</a>
            </center>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-12">
            <a href="/salesorder" class="btn btn-secondary m-1">Back</a>
            <a type="button" data-toggle="modal" data-target="#confirmation_{{$so->id}}" class="btn btn-primary m-1 text-white">Done</a>
            @include('salesorder.modal_confirmation')
        </div>
    </div>
    <style>
    thead input {
        width: 100%;
    }
    body {
        padding-right: 0px !important;
    }
    </style>
@endsection

@section('scripts')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    <script>
    $( document ).ready(function() {
        $('#example thead tr').clone(true).appendTo( '#example thead' );
        $('#example thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
           
            if ( title !== 'Aksi' ) {
                $(this).html( '<input type="text" style="text-align:center; font-family:system-ui, FontAwesome;" placeholder="&#xF002;" class="placeholder-fix" />' );
    
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                } );
            } else {
                $(this).html('');
            }
        } );
    
        var table = $('#example').DataTable( {
            orderCellsTop: true,
            fixedHeader: true
        } );

        $('#example2 thead tr').clone(true).appendTo( '#example2 thead' );
        $('#example2 thead tr:eq(1) th').each( function (i) {
            var title2 = $(this).text();
            
            if ( title2 !== 'Aksi' ) {
                $(this).html( '<input type="text" style="text-align:center; font-family:system-ui, FontAwesome;" placeholder="&#xF002;" class="placeholder-fix" />' );
    
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table2.column(i).search() !== this.value ) {
                        table2
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                } );
            } else {
                $(this).html('');
            }
        } );
    
        var table2 = $('#example2').DataTable( {
            orderCellsTop: true,
            fixedHeader: true
        } );
        
        $('#example3 thead tr').clone(true).appendTo( '#example3 thead' );
        $('#example3 thead tr:eq(1) th').each( function (i) {
            var title3 = $(this).text();
            
            if ( title3 !== 'Aksi' ) {
                $(this).html( '<input type="text" style="text-align:center; font-family:system-ui, FontAwesome;" placeholder="&#xF002;" class="placeholder-fix" />' );
    
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table3.column(i).search() !== this.value ) {
                        table3
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                } );
            } else {
                $(this).html('');
            }
        } );
    
        var table3 = $('#example3').DataTable( {
            orderCellsTop: true,
            fixedHeader: true
        } );
    });
    </script>
@endsection