<!-- Nav tabs -->
<ul id="gender" class="nav nav-tabs" role="tablist">
	<li role="presentation" class="nav-item active">
		<a class="nav-link" href="#gender" role="tab" data-toggle="tab">Pre-Order Info</a>
	</li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
	<div role="tabpanel" class="tab-pane active" id="gender">
        <div class="row form-group">
			<div class="col-md-4 col-sm-5">
		    	{!! Form::label('date_order', 'Request Date', ['class' => 'col-form-label']) !!}
			</div>
			<div class="col-md-8 col-sm-7">
		        {!! Form::date('date_order', $current_date, ['class' => 'form-control']) !!}
			</div>
		</div>
		<div class="row form-group">
			<div class="col-md-4 col-sm-5">
				{!! Form::label('quote_no', 'No. Quote', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
			</div>
			<div class="col-md-8 col-sm-7">
				{!! Form::text('quote_no', null, ['class' => 'form-control', 'placeholder' => 'No. Quote', 'required' => 'required']) !!}
			</div>
		</div>
        <div class="row form-group">
			<div class="col-md-4 col-sm-5">
				{!! Form::label('ba_no', 'No. BA', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
			</div>
			<div class="col-md-8 col-sm-7">
				{!! Form::text('ba_no', null, ['class' => 'form-control', 'placeholder' => 'No. BA', 'required' => 'required']) !!}
			</div>
		</div>
        <div class="row form-group">
            <div class="col-md-4 col-sm-5">
				<label>Sales Agent</label>
			</div>
			<div class="col-md-8 col-sm-7">
                {!! Form::select('sales_name', $sales_agent, null, array('class'=>'form-control', 'placeholder' => '- Sales Agent -', 'required' => 'required')) !!}
			</div>
		</div>
        <div class="row form-group">
			<div class="col-md-4 col-sm-5">
		    	{!! Form::label('type_req', 'Request Type', ['class' => 'col-form-label']) !!}
			</div>
			<div class="col-md-8 col-sm-7">
		        {!! Form::radio('type_req', 'Bundling', ['class' => 'form-control']) !!} Bundling
                {!! Form::radio('type_req', 'SIM Only', ['class' => 'form-control']) !!} SIM Only
			</div>
		</div>
        <div class="row form-group">
			<div class="col-md-4 col-sm-5">
				{!! Form::label('quantity', 'Quantity', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
			</div>
			<div class="col-md-8 col-sm-7">
				{!! Form::text('quantity', null, ['class' => 'form-control', 'placeholder' => 'Quantity', 'required' => 'required']) !!}
			</div>
		</div>
	</div>
</div>
