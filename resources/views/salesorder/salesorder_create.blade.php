@extends('layouts.frontpage_layout')

@section('sidebar')
    @parent
@endsection

@section('content')
	<h1 class="title">REQUEST SALES ORDER</h1>
	<hr/>
	@include('layouts.error')
    {!! Form::open(['action' => ['SalesorderController@store'], 'class' => 'form-horizontal', 'id' => 'sales-form']) !!}
		<div class="row">
			<div class="col-md-6 col-sm-6">
				@include('salesorder.form_customer_info')
				<br/>
			</div>
			<div class="col-md-6 col-sm-6">
                @include('salesorder.form_preorder_info')
				<br/>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<strong><sup class="required">*</sup><span class="glyphicon glyphicon-asterisk"></span></sup> Required</strong><br/>
				<br/>
                <center>
				<a href="/web/salesorder" onclick="return confirm('Are you sure you want to cancel?');" class="btn btn-secondary m-1">CANCEL</a>
				<input type="submit" value="SUBMIT" class="btn btn-primary m-1">
                </center>
			</div>
		</div>
	{!! Form::close() !!}
@endsection
