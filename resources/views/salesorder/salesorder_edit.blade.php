@extends('layouts.frontpage_layout')

@section('sidebar')
    @parent
@endsection

@section('content')
	<h1 class="title">UPDATE SALES ORDER</h1>
	@include('layouts.error')
    {!! Form::open(['method' => 'POST', 'action' => ['SalesorderController@update', $data_salesorder->id], 'class' => 'form-horizontal', 'id' => 'sales-form']) !!}
		<div class="row">
			<div class="col-md-6 col-sm-6">
                <!-- Nav tabs -->
                <ul id="gender" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="nav-item active">
                        <a class="nav-link" href="#gender" role="tab" data-toggle="tab">Customer Info</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="gender">
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                            {!! Form::label('corp_name', 'Corporate Name', ['class' => 'col-form-label']) !!}
                            </div>
                            <div class="col-md-8 col-sm-7">
                            <input type="text" name="corp_name" class="form-control" value="{{ $data_salesorder->corp_name }}" />
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                            {!! Form::label('pic', 'PIC', ['class' => 'col-form-label']) !!}
                            </div>
                            <div class="col-md-8 col-sm-7">
                            <input type="text" name="pic" class="form-control" value="{{ $data_salesorder->pic }}" />
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                            {!! Form::label('pic_phone', 'No. Handphone', ['class' => 'col-form-label']) !!}
                            </div>
                            <div class="col-md-8 col-sm-7">
                            <input type="text" name="pic_phone" class="form-control" value="{{ $data_salesorder->pic_phone }}" />
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                            {!! Form::label('pic_email', 'Email', ['class' => 'col-form-label']) !!}
                            </div>
                            <div class="col-md-8 col-sm-7">
                            <input type="text" name="pic_email" class="form-control" value="{{ $data_salesorder->pic_email }}" />
                            </div>
                        </div>
                    </div>
                </div>
				<br/>
			</div>
			<div class="col-md-6 col-sm-6">
                <!-- Nav tabs -->
                <ul id="gender" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="nav-item active">
                        <a class="nav-link" href="#gender" role="tab" data-toggle="tab">Pre-Order Info</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="gender">
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                                {!! Form::label('date_order', 'BAST Date', ['class' => 'col-form-label']) !!}
                            </div>
                            <div class="col-md-8 col-sm-7">
                                <input type="date" name="date_order" class="form-control" value="{{ $data_salesorder->date_order }}"/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                                {!! Form::label('quote_no', 'No. Quote', ['class' => 'col-form-label']) !!}
                            </div>
                            <div class="col-md-8 col-sm-7">
                                <input type="text" name="quote_no" class="form-control" value="{{ $data_salesorder->quote_no }}" />
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                                {!! Form::label('ba_no', 'No. BA', ['class' => 'col-form-label']) !!}
                            </div>
                            <div class="col-md-8 col-sm-7">
                            <input type="text" name="ba_no" class="form-control" value="{{ $data_salesorder->ba_no }}" />
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                                <label>Sales Agent</label>
                            </div>
                            <div class="col-md-8 col-sm-7">
                                {!! Form::select('id_sales', $sales_agent, $selected_sales_agent->id, array('class'=>'form-control', 'placeholder' => '- Sales Agent -')) !!}
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                                {!! Form::label('type_req', 'Request Type', ['class' => 'col-form-label']) !!}
                            </div>
                            <div class="col-md-8 col-sm-7">
                                <input type="radio" name="type_req" value="Bundling" {{ ($data_salesorder->type_req=="Bundling")? "checked" : "" }} > Bundling</label>
                                <input type="radio" name="type_req" value="SIM Only" {{ ($data_salesorder->type_req=="SIM Only")? "checked" : "" }} > SIM Only</label>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                                <label>Quantity</label>
                            </div>
                            <div class="col-md-8 col-sm-7">
                                <input type="text" name="quantity" class="form-control" value="{{ $data_salesorder->quantity }}" />
                            </div>
                        </div>
                    </div>
                </div>
				<br/>
			</div>
		</div>
        <br/>
		<div class="row">
            <div class="col-md-12">
                <center>
				<a href="/web/salesorder" onclick="return confirm('Are you sure you want to cancel?');" class="btn btn-secondary m-1">CANCEL</a>
				<input type="submit" value="UPDATE" class="btn btn-primary m-1">
                </center>
			</div>
		</div>
	{!! Form::close() !!}
@endsection
