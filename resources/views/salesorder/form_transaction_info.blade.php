<!-- Nav tabs -->
<ul id="gender" class="nav nav-tabs" role="tablist">
	<li role="presentation" class="nav-item active">
		<a class="nav-link" href="#gender" role="tab" data-toggle="tab">Pre-Order Info</a>
	</li>
</ul>

<!-- Nav tabs -->
<div class="tab-content">
	<div role="tabpanel" class="tab-pane active" id="gender">
        <div class="row form-group">
			<div class="col-md-4 col-sm-5">
				{!! Form::label('msisdn', 'MSISDN', ['class' => 'col-form-label']) !!}
			</div>
			<div class="col-md-8 col-sm-7">
				{!! Form::text('msisdn', null, ['class' => 'form-control', 'placeholder' => 'MSISDN']) !!}
			</div>
		</div>
		<div class="row form-group">
			<div class="col-md-4 col-sm-5">
				{!! Form::label('iccid', 'ICCID', ['class' => 'col-form-label']) !!}
			</div>
			<div class="col-md-8 col-sm-7">
				{!! Form::text('iccid', null, ['class' => 'form-control', 'placeholder' => 'ICCID']) !!}
			</div>
		</div>
        <div class="row form-group">
			<div class="col-md-4 col-sm-5">
		    	{!! Form::label('type_bundling', 'Bundling Type', ['class' => 'col-form-label']) !!}
			</div>
			<div class="col-md-8 col-sm-7">
				<input type="radio" onclick="javascript:typeCheck();" name="type_bundling" value="Prepaid" id="prepaid"> Pre-paid
				<input type="radio" onclick="javascript:typeCheck();" name="type_bundling" value="Postpaid" id="postpaid" checked> Post-paid
			</div>
		</div>
		<div class="row form-group">
			<div class="col-md-4 col-sm-5">
				{!! Form::label('non_m2m', 'Non M2M', ['class' => 'col-form-label']) !!}
			</div>
			<div class="col-md-8 col-sm-7">
				{!! Form::select('non_m2m', $list_non_m2m, null, array('class'=>'form-control', 'placeholder' => '- Non M2M -', 'required' => 'required')) !!}
			</div>
		</div>
		<div class="row form-group" id="prepaid_type" style="display:none">
			<table class="table table-borderless">
				<tr>
					<td width="35%" style="padding-left:3%; font-size:14px;">
						<label>Prepaid Type</label> <sup class="required">*</sup>
					</td>
					<td width="65%" style="padding-right:3%; font-size:14px;">
						{!! Form::radio('type_prepaid', 'Unmanaged', ['class' => 'form-control']) !!} Unmanaged
						{!! Form::radio('type_prepaid', 'Business Partner', ['class' => 'form-control']) !!} Business Partner
					</td>
				</tr>
			</table>
		</div>
		<div class="row form-group" id="package_type" style="display:none">
			<table class="table table-borderless">
				<tr>
					<td width="35%" style="padding-left:3%; font-size:14px;">
						<label>Package Type</label> <sup class="required">*</sup>
					</td>
					<td width="65%" style="padding-right:3%">
						{!! Form::select('package_type', $list_sim_pack, null, array('class'=>'form-control', 'placeholder' => '- Package Type -')) !!}
					</td>
				</tr>
			</table>
		</div>
		<div class="row form-group" id="hlr" style="display:none">
			<table class="table table-borderless">
				<tr>
					<td width="35%" style="padding-left:3%; font-size:14px;">
						<label>HLR</label> <sup class="required">*</sup>
					</td>
					<td width="65%" style="padding-right:3%">
					{!! Form::text('hlr', null, ['class' => 'form-control placeholder-fix', 'placeholder' => 'HLR']) !!}
					</td>
				</tr>
			</table>
		</div>
		<div class="row form-group" id="m2m" style="display:block">
			<table class="table table-borderless">
				<tr>
					<td width="35%" style="padding-left:3%; font-size:14px;">
						<label>M2M</label> <sup class="required">*</sup>
					</td>
					<td width="65%" style="padding-right:3%">
						{!! Form::select('m2m', $list_m2m, null, array('class'=>'form-control', 'placeholder' => '- M2M -')) !!}
					</td>
				</tr>
			</table>
		</div>
		<div class="row form-group">
			<div class="col-md-4 col-sm-5">
				{!! Form::label('sim_specification', 'SIM Specification', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
			</div>
			<div class="col-md-8 col-sm-7">
				{!! Form::select('sim_specification', $list_sim_spec, null, array('class'=>'form-control chosen-select', 'placeholder' => '- SIM Specification -', 'required' => 'required')) !!}
			</div>
		</div>
	</div>
</div>
