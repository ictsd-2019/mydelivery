<div id="export" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius:2%;">
            <div class="modal-header">
                <p class="modal-title" style="font-size:25px;"><b>Export Quotation</b></p>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form role="form" method="POST" action="/salesorder/export/param">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-1 col-sm-1">
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label">BAST Date From</label>
                        </div>
                        <div class="col-md-7 col-sm-7">
                            <input type="date" class="form-control" name="from" value={{$current_date}}>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-1 col-sm-1">
                        </div>
                        <div class="col-md-3 col-sm-3">
                        <label class="control-label">BAST Date To</label>
                        </div>
                        <div class="col-md-7 col-sm-7">
                            <input type="date" class="form-control" name="to" value={{$current_date}}>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4 col-sm-2">
                        </div>
                        <div class="col-md-8 col-sm-2">
                            <input type="checkbox" id="checkbox" name="check">
                            <label for="checkbox">All Quotation</label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12 col-sm-12">
                            <center>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary">Export</button>
                            </center>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>