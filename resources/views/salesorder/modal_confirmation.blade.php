<div id="confirmation_{{$so->id}}" class="modal fade">
    <div class="modal-dialog modal-sm" role="document" >
        <div class="modal-content" style="border-radius:2%;">
            <div class="modal-header">
                @if($so->status_helper == 'Re-verification')
                    <p class="modal-title" style="font-size:25px;"><b>Verify Quotation</b></p>
                @else
                    @if($so->status == 'Verification')
                        <p class="modal-title" style="font-size:25px;"><b>Verify Quotation</b></p>
                    @elseif($so->pic_quote == null)
                        <p class="modal-title" style="font-size:25px;"><b>Pick Up Quotation</b></p>
                    @elseif($so->status == 'In Progress RM')
                        <p class="modal-title" style="font-size:25px;"><b>Activate Quotation</b></p>
                    @elseif($so->status == 'Activation')
                        <p class="modal-title" style="font-size:25px;"><b>Finish Quotation</b></p>
                    @endif
                @endif
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                    <br/>
                    <center>
                    @if($so->status_helper == 'Re-verification')
                        <h5>Are you sure you want to verify this quotation?</h5>
                    @else
                        @if($so->status == 'Verification')
                            <h5>Are you sure you want to verify this quotation?</h5>
                        @elseif($so->pic_quote == null)
                            <h5>Are you sure you want to pick up this quotation?</h5>
                        @elseif($so->status == 'In Progress RM')
                            <h5>Are you sure you want to activate this quotation?</h5>
                        @elseif($so->status == 'Activation')
                            <h5>Are you sure you want to finish this quotation?</h5>
                        @endif
                    @endif
                    <center>
                    <br/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <center>
                        @if($so->status_helper == 'Re-verification')
                            <a type="button" class="btn btn-secondary m-1" href="/web/salesorder/{{$so->id}}/reject">Reject</a>                        
                            <a type="button" class="btn btn-primary m-1" href="/web/salesorder/{{$so->id}}/submit">Verify</a>
                        @else
                            @if($so->status == 'Verification')
                                <a type="button" class="btn btn-secondary m-1" href="/web/salesorder/{{$so->id}}/reject">Reject</a>                        
                                <a type="button" class="btn btn-primary m-1" href="/web/salesorder/{{$so->id}}/submit">Verify</a>
                            @elseif($so->pic_quote == null)
                                <a type="button" class="btn btn-primary m-1" href="/salesorder/{{$so->id}}/pick">Pick Up</a>
                            @elseif($so->status == 'In Progress RM')
                                <button type="submit" class="btn btn-primary m-1">Activate</button>
                                <!-- <a type="submit" class="btn btn-primary m-1">Activate</a> -->
                            @elseif($so->status == 'Activation')
                                <a type="button" class="btn btn-primary m-1" href="/salesorder/{{$so->id}}/done">Done</a>
                            @endif
                        @endif
                        <center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>