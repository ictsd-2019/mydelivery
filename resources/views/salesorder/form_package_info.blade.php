<!-- Nav tabs -->
<ul id="gender" class="nav nav-tabs" role="tablist">
	<li role="presentation" class="nav-item active">
		<a class="nav-link" href="#gender" role="tab" data-toggle="tab">Package Info</a>
	</li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
	<div role="tabpanel" class="tab-pane active" id="gender">
        <div class="row form-group">
            <div class="col-md-4 col-sm-5">
				<label>Basic Package</label> <sup class="required">*</sup>
			</div>
			<div class="col-md-8 col-sm-7">
                {!! Form::select('package_basic', $basic, null, array('class'=>'form-control', 'placeholder' => '- Basic Package -', 'required' => 'required')) !!}
			</div>
		</div>
        <div class="row form-group">
            <table class="table table-borderless" id="dynamic_field">
                <tr>
                    <td width="36%" style="padding-left:3%; font-size:14px;">
                        <label>Add On Package</label>
                    </td>
			        <td width="57%">
                        {!! Form::select('package_add[]', $add_on, null, array('class'=>'form-control', 'placeholder' => '- Add On Package -')) !!}
                    </td>
			        <td width="7%">
                        <button class="btn btn-secondary" type="button" name="add" id="add">+</button>
                    </td>
                </tr>
            </table>
		</div>
        @if($salesorder->type_req == "Bundling")
        <div class="row form-group">
			<div class="col-md-4 col-sm-5">
		    	{!! Form::label('type_payment', 'Payment Type', ['class' => 'col-form-label']) !!}
			</div>
			<div class="col-md-8 col-sm-7">
                <input type="radio" onclick="javascript:tenorCheck();" name="type_payment" value="Cash" id="cash" checked="checked"> Cash
                <input type="radio" onclick="javascript:tenorCheck();" name="type_payment" value="Installment" id="installment"> Installment
			</div>
		</div>
        <div class="row form-group" id="tenor" style="display:none">
            <table class="table table-borderless">
                <tr>
                    <td width="35%" style="padding-left:3%; font-size:14px;">
                        <label>Tenor Installment</label> <sup class="required">*</sup>
                    </td>
			        <td width="65%" style="padding-right:3%">
                    {!! Form::select('tenor', [
                    '' => '- Tenor -',
                    '3' => '3',
                    '6' => '6',
                    '12' => '12',
                    '24' => '24'
                    ], null, ['class' => 'form-control']) !!}
                    </td>
                </tr>
            </table>
		</div>
        <b><u>Pricing</u></b>
        <div class="row form-group">
			<div class="col-md-4 col-sm-5">
				{!! Form::label('device_price', 'Device Price/month', ['class' => 'col-form-label']) !!}
			</div>
            <div class="col-md-8 col-sm-7">
                <div class="input-group">
                    <div class="input-group-text">Rp</div>
                    {!! Form::text('device_price', null, ['class' => 'form-control', 'placeholder' => '(Optional)']) !!}
                </div>
            </div>
		</div>
        @endif
	</div>
</div>