@extends('layouts.frontpage_layout')

@section('sidebar')
    @parent
@endsection

@section('content')
    <h1 class="title">My Order</h1>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
    @if(Auth::user()->hasPermissionTo('create sales order'))
    <div class="btn-group pull-right" role="group">
        <a href="/web/salesorder/create" class="btn btn-primary">REQUEST NOW</a>
    </div>
    <br/>
    @endif
    <div class="activation-history-wrapper container" style="overflow-y: hidden; overflow-x: auto;">
        <table id="example2" class="table table-striped table-hover text-center" style="width:150%">
            <thead>
                <tr>
                    <th style="text-align:center; width:3%;">No</th>
                    <th style="text-align:center; width:8%;">Request Date</th>
                    <th style="text-align:center; width:7%;">No. Quote</th>
                    <th style="text-align:center; width:7%;">No. BA</th>
                    <th style="width:12%;">Corporate Name</th>
                    <th style="text-align:center; width:5%;">Qty</th>
                    <th style="text-align:center; width:12%;">Sales Name</th>
                    <th style="text-align:center; width:9%;">Price (Rp)</th>
                    <th style="text-align:center; width:6%;">Status</th>
                    <th style="width:13%;">Remarks</th>
                    <th style="width:11%;">PIC SD</th>
                    <th style="text-align:center; width:8%;">Aksi</th>
                </tr>
            </thead>
            <tbody>
            @foreach($salesorder as $so)
                <tr>
                    <td> {{$loop->iteration}} </td>
                    <td> {{$so->date_order}} </td>
                    <td> {{$so->quote_no}} </td>
                    <td> {{$so->ba_no}} </td>
                    <td style="text-align:left;"> {{$so->corp_name}} </td>
                    <td> {{$so->quantity}} </td>
                    <td style="text-align:left;"> {{$so->sales_name}} ({{$so->sales_group}}) </td>
                    <td> {{$so->total_price}} </td>
                    <td> 
                        @if($so->status_helper != null)
                            @if($so->status == 'Draft')
                            <span class="badge badge-dark">{{$so->status}}</span>
                            <span class="badge badge-danger">{{$so->status_helper}}</span>
                            @elseif($so->status == 'Verification')
                            <span class="badge badge-light">{{$so->status}}</span>
                            <span class="badge badge-danger">{{$so->status_helper}}</span>
                            @elseif($so->status == 'Rejected')
                            <span class="badge badge-danger">{{$so->status}}</span>
                            <span class="badge badge-danger">{{$so->status_helper}}</span>                            
                            @elseif($so->status == 'Submitted')
                            <span class="badge badge-primary">{{$so->status}}</span>
                            <span class="badge badge-danger">{{$so->status_helper}}</span>
                            @elseif($so->status == 'Activation')
                            <span class="badge badge-warning">{{$so->status}}</span>
                            <span class="badge badge-danger">{{$so->status_helper}}</span>
                            @elseif($so->status == 'In Progress RM')
                            <span class="badge badge-warning">{{$so->status}}</span>
                            <span class="badge badge-danger">{{$so->status_helper}}</span>
                            @elseif($so->status == 'Delivered')
                            <span class="badge badge-info">{{$so->status}}</span>
                            <span class="badge badge-danger">{{$so->status_helper}}</span>
                            @elseif($so->status == 'Done')
                            <span class="badge badge-success">{{$so->status}}</span>
                            <span class="badge badge-danger">{{$so->status_helper}}</span>
                            @endif
                        @else
                            @if($so->status == 'Draft')
                            <span class="badge badge-dark">{{$so->status}}</span>
                            @elseif($so->status == 'Verification')
                            <span class="badge badge-light">{{$so->status}}</span>
                            @elseif($so->status == 'Rejected')
                            <span class="badge badge-danger">{{$so->status}}</span>
                            @elseif($so->status == 'Submitted')
                            <span class="badge badge-primary">{{$so->status}}</span>
                            @elseif($so->status == 'Activation')
                            <span class="badge badge-warning">{{$so->status}}</span>
                            @elseif($so->status == 'In Progress RM')
                            <span class="badge badge-warning">{{$so->status}}</span>
                            @elseif($so->status == 'Delivered')
                            <span class="badge badge-info">{{$so->status}}</span>
                            @elseif($so->status == 'Done')
                            <span class="badge badge-success">{{$so->status}}</span>
                            @endif
                        @endif
                    </td>
                    @if($so->last_remarks == null)
                        <td> - </td>
                    @else
                        <td style="text-align:left;"> {{$so->last_remarks}} </td>
                    @endif
                    @if($so->pic_quote == null)
                        <td> - </td>
                    @else
                        <td style="text-align:left;"> {{$so->pic_quote}} </td>
                    @endif
                    <td style="text-align:left;">
                        @if(Auth::user()->hasPermissionTo('verificate sales order'))
                            <a data-toggle="modal" data-target="#confirmation_{{$so->id}}" class="btn btn-success btn-sm text-white"><i class="fa fa-arrow-circle-right"></i></a>
                            @include('salesorder.modal_confirmation')
                            <a href="/web/salesorder/{{$so->id}}" class="btn btn-warning btn-sm"><i class="fa fa-search"></i></a>
                            <a data-toggle="modal" data-target="#create_remarks_{{$so->id}}" class="btn btn-info btn-sm text-white"><i class="fa fa-sticky-note-o"></i></a>
                            @include('salesorder.form_remarks')
                        @else
                            @if($so->status == "Draft" || $so->status == "Rejected" || ($so->status_helper != null && $so->status_helper != "Re-verification"))
                            <a href="/web/salesorder/{{$so->id}}/edit" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
                            <a href="/web/salesorder/{{$so->id}}" class="btn btn-warning btn-sm"><i class="fa fa-search"></i></a>
                            <a href="/web/salesorder/{{$so->id}}/delete" onclick="return confirmation();" class="btn btn-primary btn-sm"><i class="fa fa-trash"></i></a>
                            @else
                            <a href="/web/salesorder/{{$so->id}}" class="btn btn-warning btn-sm"><i class="fa fa-search"></i></a>
                            <a href="/web/salesorder/{{$so->id}}/delete" onclick="return confirmation();" class="btn btn-primary btn-sm"><i class="fa fa-trash"></i></a>
                            @endif
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

        
        
        <br/>
        <center>
        <a href="/web" class="btn btn-secondary">BACK</a>
        @if(Auth::user()->hasPermissionTo('create sales order'))
        <a href="/web/salesorder/create" class="btn btn-primary">REQUEST NOW</a>
        @endif
        </center>

    <style>
    .left {
        text-align: left;
    }
    </style>
@endsection

@section('scripts')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script>
    $(function() {
        var table = $('#example').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/web/quotation/json',
            columns: [
                { data: null, orderable: false},
                { data: 'date_order', name: 'date_order' },
                { data: 'quote_no', name: 'quote_no' },
                { data: 'ba_no', name: 'ba_no' },
                { data: 'corp_name', name: 'corp_name', className: 'left' },
                { data: 'quantity', name: 'quantity' },
                { data: 'total_price', name: 'total_price' },
                { data: null, orderable: false },
                { data: 'sales_name', name: 'sales_name', className: 'left' },
                { data: 'last_remarks', name: 'last_remarks', className: 'left' },
                { data: 'pic_quote', name: 'pic_quote' },
                { data: 'aksi', name: 'aksi', className: 'left', orderable: false }
            ],
            columnDefs:[{
                targets: -1,
                render: function (data, type, row, meta) {
                    if(row.status == "Draft" || row.status == "Rejected" || (row.status_helper != null && row.status_helper != "Re-verification")){
                        return '<a href="/web/salesorder/'+ row.id +'/edit" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a> <a href="/web/salesorder/'+ row.id +'" class="btn btn-warning btn-sm"><i class="fa fa-search"></i></a> <a href="/web/salesorder/'+ row.id+'/delete" onclick="return confirmation();" class="btn btn-primary btn-sm"><i class="fa fa-trash"></i></a>'
                        // if(Auth::user()->hasPermissionTo('manage sales order')){
                        //     return '<a href="/web/salesorder/'+ row.id +'/edit" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a> <a href="/web/salesorder/'+ row.id +'" class="btn btn-warning btn-sm"><i class="fa fa-search"></i></a> <a href="/web/salesorder/'+ row.id+'/delete" onclick="return confirmation();" class="btn btn-primary btn-sm"><i class="fa fa-trash"></i></a>'
                        // } else {
                        //     return '<a href="/web/salesorder/'+ row.id +'" class="btn btn-warning btn-sm"><i class="fa fa-search"></i></a>'
                        // }
                    }
                    return '<a href="/web/salesorder/'+ row.id +'" class="btn btn-warning btn-sm"><i class="fa fa-search"></i></a> <a href="/web/salesorder/'+ row.id+'/delete" onclick="return confirmation();" class="btn btn-primary btn-sm"><i class="fa fa-trash"></i></a>'
                }
            }, {
                targets:-5,
                render: function (data, type, row, meta) {
                    if(row.status_helper != null){
                        if(row.status == "Draft"){
                            return '<span class="badge badge-dark">'+ row.status +'</span> <span class="badge badge-danger">'+ row.status_helper +'</span>'
                        } else if(row.status == "Rejected") {
                            return '<span class="badge badge-danger">'+ row.status +'</span> <span class="badge badge-danger">'+ row.status_helper +'</span>'
                        } else if(row.status == "Verification") {
                            return '<span class="badge badge-light">'+ row.status +'</span> <span class="badge badge-danger">'+ row.status_helper +'</span>'
                        } else if(row.status == "Submitted") {
                            return '<span class="badge badge-primary">'+ row.status +'</span> <span class="badge badge-danger">'+ row.status_helper +'</span>'
                        } else if(row.status == "Activation") {
                            return '<span class="badge badge-warning">'+ row.status +'</span> <span class="badge badge-danger">'+ row.status_helper +'</span>'
                        } else if(row.status == "In Progress RM") {
                            return '<span class="badge badge-warning">'+ row.status +'</span> <span class="badge badge-danger">'+ row.status_helper +'</span>'
                        } else if(row.status == "Delivered") {
                            return '<span class="badge badge-info">'+ row.status +'</span> <span class="badge badge-danger">'+ row.status_helper +'</span>'
                        } else if(row.status == "Done") {
                            return '<span class="badge badge-success">'+ row.status +'</span> <span class="badge badge-danger">'+ row.status_helper +'</span>'
                        }
                    } else {
                        if(row.status == "Draft"){
                            return '<span class="badge badge-dark">'+ row.status +'</span>'
                        } else if(row.status == "Rejected") {
                            return '<span class="badge badge-danger">'+ row.status +'</span>'
                        } else if(row.status == "Verification") {
                            return '<span class="badge badge-light">'+ row.status +'</span>'
                        } else if(row.status == "Submitted") {
                            return '<span class="badge badge-primary">'+ row.status +'</span>'
                        } else if(row.status == "Activation") {
                            return '<span class="badge badge-warning">'+ row.status +'</span>'
                        } else if(row.status == "In Progress RM") {
                            return '<span class="badge badge-warning">'+ row.status +'</span>'
                        } else if(row.status == "Delivered") {
                            return '<span class="badge badge-info">'+ row.status +'</span>'
                        } else if(row.status == "Done") {
                            return '<span class="badge badge-success">'+ row.status +'</span>'
                        }
                    }
                }
            }
            ],
        });

        $('#example thead tr').clone(true).appendTo( '#example thead' );
        $('#example thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            if(title === 'No' || title === 'Aksi' || title === 'Status'){
                $(this).html('');
            } else {
                $(this).html( '<input type="text" style="text-align:center; font-family:system-ui, FontAwesome;" placeholder="&#xF002;" class="placeholder-fix" />' );
    
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                } );
            }
        } );

        table.on('draw.dt', function () {
            var info = table.page.info();
            table.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1 + info.start;
            });
        });
    });
    </script>
    <script>
        function confirmation() {
            if(!confirm("Are you sure you want to delete this order?"))
            event.preventDefault();
        }
    </script>
    <script>
    $(document).ready(function() {
        $('#example2 thead tr').clone(true).appendTo( '#example2 thead' );
        $('#example2 thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();

            if(title === 'Aksi'){
                $(this).html('');
            } else {
                $(this).html( '<input type="text" style="text-align:center; font-family:system-ui, FontAwesome;" placeholder="&#xF002;" class="placeholder-fix" />' );
    
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                } );
            }
        } );
    
        var table = $('#example2').DataTable( {
            orderCellsTop: true,
            fixedHeader: true,
        } );
	} );
    </script>
@endsection