@extends('layouts.app')

@section('content')
	<h1 class="title">Update Transaction</h1>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
	<br/>
	@include('layouts.error')
    {!! Form::open(['method' => 'POST', 'action' => ['SalesorderTransactionController@update_manage', $transaction->id], 'class' => 'form-horizontal', 'id' => 'sales-form']) !!}
		<div class="row">
			<div class="col-md-6 col-sm-6">
                <!-- Nav tabs -->
                <ul id="gender" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="nav-item active">
                        <a class="nav-link" href="#gender" role="tab" data-toggle="tab">Pre-order Info</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="gender">
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                                {!! Form::label('msisdn', 'MSISDN', ['class' => 'col-form-label']) !!}
                            </div>
                            <div class="col-md-8 col-sm-7">
                                <input type="text" name="msisdn" class="form-control" placeholder="MSISDN" value="{{ $transaction->msisdn }}"/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                                {!! Form::label('iccid', 'ICCID', ['class' => 'col-form-label']) !!}
                            </div>
                            <div class="col-md-8 col-sm-7">
                                <input type="text" name="iccid" class="form-control" placeholder="ICCID" value="{{ $transaction->iccid }}"/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                                {!! Form::label('type_bundling', 'Bundling Type', ['class' => 'col-form-label']) !!}
                            </div>
                            <div class="col-md-8 col-sm-7">
                                <input type="radio" onclick="javascript:typeCheck();" name="type_bundling" value="Prepaid" id="prepaid" {{ ($transaction->type_bundling=="Prepaid")? "checked" : "" }}> Pre-paid</label>
                                <input type="radio" onclick="javascript:typeCheck();" name="type_bundling" value="Postpaid" id="postpaid" {{ ($transaction->type_bundling=="Postpaid")? "checked" : "" }}> Post-paid</label>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                                {!! Form::label('non_m2m', 'Non M2M', ['class' => 'col-form-label']) !!}
                            </div>
                            <div class="col-md-8 col-sm-7">
                                {!! Form::select('non_m2m', $list_non_m2m, $selected_non_m2m->id, array('class'=>'form-control', 'placeholder' => '- Non M2M -', 'required' => 'required')) !!}
                            </div>
                        </div>
                        @if($transaction->type_bundling == "Prepaid")
                        <div class="row form-group" id="prepaid_type" style="display:block">
                        @else
                        <div class="row form-group" id="prepaid_type" style="display:none">
                        @endif
                            <table class="table table-borderless">
                                <tr>
                                    <td width="35%" style="padding-left:3%; font-size:14px;">
                                        <label>Prepaid Type</label> <sup class="required">*</sup>
                                    </td>
                                    <td width="65%" style="padding-right:3%; font-size:14px;">
                                        <input type="radio" name="type_prepaid" value="Unmanaged" {{ ($transaction->type_prepaid=="Unmanaged")? "checked" : "" }} > Unmanaged</label>
                                        <input type="radio" name="type_prepaid" value="Business Partner" {{ ($transaction->type_prepaid=="Business Partner")? "checked" : "" }} > Business Partner</label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        @if($transaction->type_bundling == "Prepaid")
                        <div class="row form-group" id="package_type" style="display:block">
                        @else
                        <div class="row form-group" id="package_type" style="display:none">
                        @endif
                            <table class="table table-borderless">
                                <tr>
                                    <td width="35%" style="padding-left:3%; font-size:14px;">
                                        <label>Package Type</label> <sup class="required">*</sup>
                                    </td>
                                    <td width="65%" style="padding-right:3%">
                                        @if($transaction->type_bundling == 'Postpaid')
                                            {!! Form::select('package_type', $list_sim_pack, null, array('class'=>'form-control', 'placeholder' => '- Package Type -')) !!}
                                        @else
                                            {!! Form::select('package_type', $list_sim_pack, $selected_package_type->id, array('class'=>'form-control', 'placeholder' => '- Package Type -')) !!}
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </div>
                        @if($transaction->type_bundling == "Prepaid")
                        <div class="row form-group" id="hlr" style="display:block">
                        @else
                        <div class="row form-group" id="hlr" style="display:none">
                        @endif
                            <table class="table table-borderless">
                                <tr>
                                    <td width="35%" style="padding-left:3%; font-size:14px;">
                                        <label>HLR</label> <sup class="required">*</sup>
                                    </td>
                                    <td width="65%" style="padding-right:3%">
                                    {!! Form::text('hlr', $transaction->hlr, ['class' => 'form-control placeholder-fix', 'placeholder' => 'HLR']) !!}
                                    </td>
                                </tr>
                            </table>
                        </div>
                        @if($transaction->type_bundling == "Prepaid")
                        <div class="row form-group" id="m2m" style="display:none">
                        @else
                        <div class="row form-group" id="m2m" style="display:block">
                        @endif
                            <table class="table table-borderless">
                                <tr>
                                    <td width="35%" style="padding-left:3%; font-size:14px;">
                                        <label>M2M</label> <sup class="required">*</sup>
                                    </td>
                                    <td width="65%" style="padding-right:3%">
                                        @if($transaction->type_bundling == 'Postpaid')
                                            {!! Form::select('m2m', $list_m2m, $selected_m2m->id, array('class'=>'form-control', 'placeholder' => '- M2M -')) !!}
                                        @else
                                            {!! Form::select('m2m', $list_m2m, null, array('class'=>'form-control', 'placeholder' => '- M2M -')) !!}
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                                {!! Form::label('sim_specification', 'SIM Specification', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                            </div>
                            <div class="col-md-8 col-sm-7">
                                {!! Form::select('sim_specification', $list_sim_spec, $selected_sim_specification->id, array('class'=>'form-control chosen-select', 'placeholder' => '- SIM Specification -', 'required' => 'required')) !!}
                            </div>
                        </div>
                    </div>
                </div>
			</div>
			<div class="col-md-6 col-sm-6">
                @if($salesorder->type_req == 'Bundling')
                <!-- Nav tabs -->
                <ul id="gender" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="nav-item active">
                        <a class="nav-link" href="#gender" role="tab" data-toggle="tab">Device Select</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="gender">
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                                <label>Brand</label>
                            </div>
                            <div class="col-md-8 col-sm-7">
                                {!! Form::select('brand', $device, $selected_device_type->id, array('class'=>'form-control', 'placeholder' => '- Brand -', 'required' => 'required')) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                @endif
                <!-- Nav tabs -->
                <ul id="gender" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="nav-item active">
                        <a class="nav-link" href="#gender" role="tab" data-toggle="tab">Package Info</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="gender">
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                                <label>Basic Package</label>
                            </div>
                            <div class="col-md-8 col-sm-7">
                                {!! Form::select('package_basic', $basic, $selected_basic_package->id, array('class'=>'form-control', 'placeholder' => '- Basic Package -', 'required' => 'required')) !!}
                            </div>
                        </div>
                        <div class="row form-group">
                            <table class="table table-borderless" id="dynamic_field">
                                @if($total_selected_add_on != 0)
                                    @for ($i=0; $i<$total_selected_add_on; $i++)
                                        @if($i==0)
                                        <tr>
                                            <td width="36%" style="padding-left:3%; font-size:14px;">
                                                <label>Add On Package</label>
                                            </td>
                                            <td width="57%">
                                                {!! Form::select('package_add[]', $add_on, $selected_add_on[$i]->id, array('class'=>'form-control', 'placeholder' => '- Add On Package -')) !!}</div>
                                            </td>
                                            <td width="7%">
                                                <button class="btn btn-secondary" type="button" name="add" id="add">+</button>
                                            </td>
                                        </tr>
                                        @else
                                        <tr id="row{{$i}}">
                                            <td width="35%" style="padding-left:3%">
                                            </td>
                                            <td width="58%">
                                                {!! Form::select('package_add[]', $add_on, $selected_add_on[$i]->id, array('class'=>'form-control', 'placeholder' => '- Add On Package -', 'required' => 'required')) !!}</div>
                                            </td>
                                            <td width="7%">
                                                <button type="button" name="remove" id="{{$i}}" class="btn btn-danger btn_remove">X</button>
                                            </td>
                                        </tr>
                                        @endif
                                    @endfor
                                @else
                                    <tr>
                                        <td width="35%" style="padding-left:3%; font-size:14px;">
                                            <label>Add On Package</label>
                                        </td>
                                        <td width="58%">
                                            {!! Form::select('package_add[]', $add_on, null, array('class'=>'form-control', 'placeholder' => '- Add On Package -')) !!}</div>
                                        </td>
                                        <td width="7%">
                                            <button class="btn btn-secondary" type="button" name="add" id="add">+</button>
                                        </td>
                                    </tr>
                                @endif
                            </table>
                        </div>
                        @if($salesorder->type_req == 'Bundling')
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                                {!! Form::label('type_payment', 'Payment Type', ['class' => 'col-form-label']) !!}
                            </div>
                            <div class="col-md-8 col-sm-7">
                                <input type="radio" onclick="javascript:tenorCheck();" name="type_payment" value="Cash" id="cash" {{ ($transaction->type_payment=="Cash")? "checked" : "" }}> Cash
                                <input type="radio" onclick="javascript:tenorCheck();" name="type_payment" value="Installment" id="installment" {{ ($transaction->type_payment=="Installment")? "checked" : "" }}> Installment
                            </div>
                        </div>
                        @endif
                        @if($transaction->type_payment == "Installment")
                            <div class="row form-group" id="tenor" style="display:block">
                        @else
                            <div class="row form-group" id="tenor" style="display:none">
                        @endif
                            <table class="table table-borderless">
                                <tr>
                                    <td width="35%" style="padding-left:3%; font-size:14px;">
                                        <label>Tenor Installment</label>
                                    </td>
                                    <td width="65%" style="padding-right:3%">
                                    <select name="tenor" class="form-control" >
                                        <option value="">- Tenor -
                                        <option value=3 {{ ($transaction->tenor==3)? "selected" : "" }}>3
                                        <option value=6 {{ ($transaction->tenor==6)? "selected" : "" }}>6
                                        <option value=12 {{ ($transaction->tenor==12)? "selected" : "" }}>12
                                        <option value=24 {{ ($transaction->tenor==24)? "selected" : "" }}>24
                                    </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        
                    </div>
                </div>
				<br/>
                @if($salesorder->status == 'Activation')
                <!-- Nav tabs -->
                <ul id="gender" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="nav-item active">
                        <a class="nav-link" href="#gender" role="tab" data-toggle="tab">Availability</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="gender">
                        <div class="row form-group">
                            <div class="col-md-4 col-sm-5">
                                <label>Status</label>
                            </div>
                            <div class="col-md-8 col-sm-7">
                                <select name="status" class="form-control" >
                                    <option value="">- Status -
                                    <option value="Out of Stock" {{ ($transaction->status=="Out of Stock")? "selected" : "" }}>Out of Stock
                                    <option value="In Progress RM" {{ ($transaction->status=="In Progress RM")? "selected" : "" }}>In Progress RM
                                    <option value="Available" {{ ($transaction->status=="Available")? "selected" : "" }}>Available
                                    <option value="On DR" {{ ($transaction->status=="On DR")? "selected" : "" }}>On DR
                                    <option value="On Shipment" {{ ($transaction->status=="On Shipment")? "selected" : "" }}>On Shipment
                                    <option value="Delivered" {{ ($transaction->status=="Delivered")? "selected" : "" }}>Delivered
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
			</div>
		</div>
        <br/>
		<div class="row">
            <div class="col-md-12">
                <strong><sup class="required">*</sup> Required</strong>
                <br/><br/>
                <a href="/salesorder/{{$transaction->id_salesorder}}/activation" onclick="return confirm('Are you sure you want to cancel?');" class="btn btn-secondary m-1">Cancel</a>
                <input type="submit" value="Update" class="btn btn-primary m-1">
			</div>
		</div>
	{!! Form::close() !!}
@endsection

@section('scripts')
    @parent
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
        <script>
        $( document ).ready(function() {
            var postURL = "<?php echo url('addmore'); ?>";
            console.log(postURL);
            var i=101;
            $('#add').click(function(){
                i++;
                $('#dynamic_field').append('<tr id="row'+i+'"><td width="36%" style="padding-left:3%"></td><td width="57%">{!! Form::select('package_add[]', $add_on, null, array('class'=>'form-control', 'placeholder' => '- Add On Package -', 'required' => 'required')) !!}</td><td width="7%"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
            });
            $(document).on('click', '.btn_remove', function(){  
                var button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();  
            });
        });
        </script>
        <script type="text/javascript">
            function tenorCheck() {
                if (document.getElementById('installment').checked) {
                    document.getElementById('tenor').style.display = 'block';
                } else {
                    document.getElementById('tenor').style.display = 'none';
                }
            }
        </script>
        <script type="text/javascript">
            function typeCheck() {
                if (document.getElementById('prepaid').checked) {
                    document.getElementById('package_type').style.display = 'block';
                    document.getElementById('prepaid_type').style.display = 'block';
                    document.getElementById('hlr').style.display = 'block';
                    document.getElementById('m2m').style.display = 'none';
                } else {
                    document.getElementById('package_type').style.display = 'none';
                    document.getElementById('prepaid_type').style.display = 'none';
                    document.getElementById('hlr').style.display = 'none';
                    document.getElementById('m2m').style.display = 'block';
                }
            }
        </script>
@endsection