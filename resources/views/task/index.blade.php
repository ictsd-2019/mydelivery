@extends('layouts.app')
@section('content')
    <h1 class="title">Manage Task</h1>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
    <div class="btn-group pull-right" role="group">
		<a href="/task/create" class="btn btn-success m-1">Add Task</a>
	</div>
    <br/>
    <div class="activation-history-wrapper container" style="overflow-y: hidden; overflow-x: auto;">
        <table id="example" class="table table-striped table-hover text-center" style="width:140%">
            <thead>
                <tr>
                    <th style="text-align:center; width:3%;">No</th>
                    <th style="text-align:center; width:8%;">ID Task</th>
                    <th style="text-align:center; width:17%;">Name</th>
                    <th style="text-align:center; width:12%;">Start Date</th>
                    <th style="text-align:center; width:12%;">End Date</th>
                    <th style="text-align:center; width:7%;">Aksi</th>
                </tr>
            </thead>
            <tbody>
            
            @foreach($task as $row)
                <tr>
                    <td> {!! $loop->iteration !!} </td>
                    <td> {{$row->id_task}} </td>
                    <td align="left"> {!! $row->name !!} </td>
                    <td> {!! $row->start !!}</td>
                    <td> {!! $row->end !!}</td>
                    <td>
                        <a href="/task/{{$row->id}}/edit" class="btn btn-success btn-sm m-1"><i class="fa fa-edit"></i></a>
                        <a href="/task/{{$row->id}}/delete" onclick="return confirm('Are you sure you want to delete this data type scope?');" class="btn btn-primary btn-sm m-1"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <!--<a class="btn btn-warning btn-sm m-1" href="/employee/export" style="float:right;"><i class="fa fa-file-excel-o"></i> Download</a>-->
    </div>
    <style>
    thead input {
        width: 100%;
    }
    </style>
@endsection

@section('scripts')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script>
    $(document).ready(function() {
	    $('#example thead tr').clone(true).appendTo( '#example thead' );
        $('#example thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            if(title === 'Aksi'){
                $(this).html('');
            } else {
                $(this).html( '<input type="text" style="text-align:center; font-family:system-ui, FontAwesome;" placeholder="&#xF002;" class="placeholder-fix" />' );
    
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                } );
            }
        } );
    
        var table = $('#example').DataTable( {
            orderCellsTop: true,
            fixedHeader: true,
        } );
	} );
    </script>
@endsection