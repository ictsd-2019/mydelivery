@extends('layouts.app')

@section('content')
    <h1 class="title">Update Task</h1>
    @include('layouts.error')
    <br>
    {!! Form::open(['action' => ['TaskController@update', $coba->id], 'id' => 'edit-task', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
        @csrf
        @include('task.form_edit')
    {!! Form::close() !!}
@endsection
