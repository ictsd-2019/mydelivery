<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#" style="color:black;">Edit Info</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('id', 'ID Task', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('id', $coba->id_task, ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => 'ID Task', 'readonly' => 'true']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('name', 'Name Task', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('name', $coba->name, ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => 'Name Task']) !!}
                </div>
            </div>

            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('start', 'Start Date', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::date('start', date('d/m/Y', strtotime($coba->start)), ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => 'Start Date']) !!}
                </div>
            </div>

            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('end', 'End Date', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::date('end', date('d/m/Y', strtotime($coba->end)), ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => 'End Date']) !!}
                </div>
            </div>
          </div>
        <br>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <strong><sup class="required">*</sup> Required</strong>
        <br/><br/>
        <a href="/task" class="btn btn-secondary m-1">Cancel</a>
        <input type="submit" value="Update"  class="btn btn-primary m-1">
    </div>
</div>