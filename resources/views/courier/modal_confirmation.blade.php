<div id="confirmation_{{$trans->id}}" class="modal fade">
    <div class="modal-dialog modal-sm" role="document" >
        <div class="modal-content" style="border-radius:2%;">
            <div class="modal-header">
                <p class="modal-title" style="font-size:25px;"><b>In Courier</b></p>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                    <br/>
                    <center>
                    @if($trans->no_dr != null)
                    <h5>Are you sure you want to deliver {{$trans->no_dr}} ?</h5>
                    @else
                    <h5>Are you sure you want to deliver this quote?</h5>
                    @endif
                    <center>
                    <br/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <center>
                        @if($trans->no_dr != null)
                        <a type="button" class="btn btn-primary m-1" href="/delivery/courier/dr/{{$trans->id}}">Send to Courier</a>
                        @else
                        <a type="button" class="btn btn-primary m-1" href="/delivery/courier/shipment/{{$trans->id}}">Courier has Picked Up</a>
                        @endif
                        <center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>