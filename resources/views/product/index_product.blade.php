@extends('layouts.frontpage_layout')

@section('sidebar')
    @parent
@endsection

@section('content')
    <h1 class="title">Device Product</h1>

    <div class="row">
        @if($data_product->count() > 0)
            @foreach($data_product as $product)
                <div class="col-md-4">
                    <div class="card card-default">
                        <div class="card-header">{{ $product->title }}</div>
                        <div class="card-body">
                            Name : <strong>{{($product->name)}}</strong><br>
                            Storage : <strong>{{($product->type)}}</strong><br>
                            Color : <strong>{{($product->color)}}</strong><br>
                            Price : <strong>{{($product->price)}}</strong><br>
                            Open Date : <strong>{{ \Carbon\Carbon::parse($product->startdate)->format('Y-m-d h:i') }}</strong><br>
                            Close Date : <strong>{{ \Carbon\Carbon::parse($product->enddate)->format('Y-m-d h:i') }}</strong><br>
                            <br>
                            <a href="" class="btn btn-success">Opening Now</a>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <p>No Pre-Order seasons open.</p>
        @endif
    </div>
@endsection
