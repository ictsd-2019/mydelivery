@extends('layouts.frontpage_layout')

@section('content')
    <h1 class="title">Dashboard</h1>
    <div class="row">
        @foreach($menus as $menu)
            <div class="col-md-2 col-6 text-center">
                <a href="{{ $menu->path }}">
                    <div class="menu-item">
                        <h1><span class="{{ $menu->icon3 }}"></span></h1>
                        {{ $menu->title }}
                    </div>
                </a>
            </div>
        @endforeach
    </div>
@endsection
