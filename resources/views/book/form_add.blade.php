<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#" style="color:black;">Form PriceBook</a>
            </li>
        </ul>
        <div class="tab-content">
           
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('name', 'Name Task Scope', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('name', null, ['class' => 'form-control mb-2 mt-1 placeholder-fix', 'required' => 'required', 'placeholder' => 'Name Task Scope']) !!}
                </div>
            </div>
            
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('start', 'Start Date', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::date('start', null, ['class' => 'form-control mb-2 mt-1 placeholder-fix', 'required' => 'required', 'placeholder' => 'Start Date']) !!}
                </div>
            </div>

            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('end', 'End Date', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::date('end', null, ['class' => 'form-control mb-2 mt-1 placeholder-fix', 'required' => 'required', 'placeholder' => 'End Date']) !!}
                </div>
            </div>

          </div>
        <br>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <strong><sup class="required">*</sup> Required</strong>
        <br/><br/>
        <a href="/task" class="btn btn-secondary m-1">Cancel</a>
        <input type="submit" value="Save Data"  class="btn btn-primary m-1">
    </div>
</div>