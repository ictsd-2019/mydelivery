@extends('layouts.app')

@section('content')
    <h1 class="title">Update Type</h1>
    @include('layouts.error')
    <br>
    {!! Form::open(['action' => ['TypeController@update', $type->id], 'id' => 'edit-type', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
        @csrf
        @include('type_scope.form_manage_edit_type')
    {!! Form::close() !!}
@endsection