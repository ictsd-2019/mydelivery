@extends('layouts.app')

@section('content')
    <h1 class="title">Add Type</h1>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif

    @include('layouts.error')

    <br>
    {!! Form::open(['action' => ['TypeController@store'], 'id' => 'create-type', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
        @csrf
        @include('type_scope.form_manage_add_type')
    {!! Form::close() !!}

@endsection
