<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#" style="color:black;">Type Info</a>
            </li>
        </ul>
        <div class="tab-content">
            
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('type', 'Type Scope', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('type', null, ['class' => 'form-control mb-2 mt-1 placeholder-fix', 'required' => 'required', 'placeholder' => 'Type Scope']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('price', 'Price', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('price', null, ['class' => 'form-control mb-2 mt-1 placeholder-fix', 'required' => 'required', 'placeholder' => 'Price']) !!}
                </div>
            </div>
       
            
        </div>
        <br>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <strong><sup class="required">*</sup> Required</strong>
        <br/><br/>
        <a href="/type" class="btn btn-secondary m-1">Cancel</a>
        <input type="submit" value="Save Data"  class="btn btn-primary m-1">
    </div>
</div>