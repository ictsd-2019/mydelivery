<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#" style="color:black;">Edit Info</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('id', 'ID Type', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('id', $type->id_type, ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => 'ID Type', 'readonly' => 'true']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('type', 'Type Scope', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('type', $type->type_scope_work, ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => 'Type Scope']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('price', 'Price', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('price', $type->sub_type_price, ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => 'Price']) !!}
                </div>
            </div>
        </div>
        <br>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <strong><sup class="required">*</sup> Required</strong>
        <br/><br/>
        <a href="/type" class="btn btn-secondary m-1">Cancel</a>
        <input type="submit" value="Update"  class="btn btn-primary m-1">
    </div>
</div>