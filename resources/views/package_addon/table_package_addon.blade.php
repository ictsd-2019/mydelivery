<table id="example" class="table table-bordered table-hover dt-responsive text-center" style="width:100%">
    <thead>
        <tr>
            <th style="background:lightgrey; text-align:center; width:10%;">ID Package</th>
            <th style="background:lightgrey; text-align:center; width:40%;">Package</th>
            <th style="background:lightgrey; text-align:center; width:35%;">Price</th>
        </tr>
    </thead>
    <tbody>
    @foreach($addon as $ao)
        <tr>
            <td> {{$ao->id}} </td>
            <td> {{$ao->addon}} </td>
            <td> {{$ao->price}} </td>
        </tr>
    @endforeach
    </tbody>
</table>