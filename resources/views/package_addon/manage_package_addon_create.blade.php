@extends('layouts.app')

@section('content')
    <h1 class="title">Add Add On Package</h1>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
    @include('layouts.error')
    <br>
    {!! Form::open(['action' => ['PackageAddOnController@store'], 'id' => 'create-addon', 'class' => 'form-horizontal']) !!}
        @include('package_addon.form_manage_create_package_addon')
    {!! Form::close() !!}
@endsection
