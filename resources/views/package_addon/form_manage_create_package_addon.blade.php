<div class="row">
    <div class="col-md-6">
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#" style="color:black;">Add On Package Info</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('addon', 'Package', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    <div class="input-group">
						<div class="input-group-text">Rp</div>
                        {!! Form::text('addon', null, ['class' => 'form-control placeholder-fix', 'required' => 'required', 'placeholder' => 'Nama Package']) !!}
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('price', 'Price', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    <div class="input-group">
						<div class="input-group-text">Rp</div>
                        {!! Form::text('price', null, ['class' => 'form-control placeholder-fix', 'required' => 'required', 'placeholder' => '0']) !!}
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <strong><sup class="required">*</sup> Required</strong>
        <br/><br/>
        <a href="/addon" class="btn btn-secondary m-1">Cancel</a>
        <input type="submit" value="Submit" class="btn btn-primary m-1">
    </div>
</div>