@extends('layouts.app')

@section('content')
    <h1 class="title">Update Add On Package</h1>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
    @include('layouts.error')
    <br>
    {!! Form::open(['action' => ['PackageAddOnController@update', $addon->id], 'id' => 'edit-addon', 'class' => 'form-horizontal']) !!}
        @include('package_addon.form_manage_edit_package_addon')
    {!! Form::close() !!}
@endsection
