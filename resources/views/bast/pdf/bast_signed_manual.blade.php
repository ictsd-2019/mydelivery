<!DOCTYPE html>
<html>
<head>
    <title>BAST</title>

</head>
<body>
    <br/>
    <div class="row" style="white-space:nowrap">
        <div id="image" style="display:inline; float:left;">
            <img src="{{ asset('/storage/upload/Indosat_Ooredoo.png') }}" width="195" height="105">
        </div>
        <div id="texts" style="display:inline; white-space:nowrap;">
            <center>
            <p class="title"><b>BERITA ACARA SERAH TERIMA {{$jenis}}<b></p>
            </center>
        </div>
    <div>
    <br/>
    <br/>
    <br/>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <table class="table table-sm" id="table_so">
                        <tr>
                            <th width="7%"></th>
                            <th style="text-align:left;" width="15%"><strong>Requestor (Sales)</strong></th>
                            <td width="68%">: {{ $bast->sales }}</td>
                            <th width="7%"></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th style="text-align:left;"><strong>Segment</strong></th>
                            <td>: {{ $bast->sales_dept }}</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th style="text-align:left;"><strong>No. DR</strong></th>
                            <td>: {{ $bast->no_dr }}</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th style="text-align:left;"><strong>Quote ID</strong></th>
                            <td>: {{ $bast->quote_no }}</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th style="text-align:left;"><strong>Corporate Name</strong></th>
                            <td>: {{ $bast->corporate_name }}</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th style="text-align:left;"><strong>Address</strong></th>
                            <td>: {{ $bast->address_1 }}</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th style="text-align:left;"><strong>Address</strong></th>
                            <td>: {{ $bast->address_2 }}</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th style="text-align:left;"><strong>PIC Corporate 1</strong></th>
                            <td>: {{ $bast->pic_corporate_1 }}</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th style="text-align:left;"><strong>Telpon/HP</strong></th>
                            <td>: {{ $bast->pic_phone_1 }}</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th style="text-align:left;"><strong>PIC Corporate 2</strong></th>
                            <td>: {{ $bast->pic_corporate_2 }}</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th style="text-align:left;"><strong>Handphone</strong></th>
                            <td>: {{ $bast->pic_phone_2 }}</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th style="text-align:left;"><strong>Request Date</strong></th>
                            <td>: {{ $tanggal }}</td>
                            <th></th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <br/>
    <br/>
	<table class='table table-bordered' id="table_dr">
		<thead>
			<tr>
				<th style="width:5%;">No</th>
				<th style="width:11%;">MSISDN</th>
                <th style="width:15%;">ICCID</th>
				<th style="width:13%;">Brand</th>
				<th style="width:19%;">Device Type</th>
				<th style="width:14%;">IMEI</th>
                <th style="width:6%;">Qty</th>
                <th style="width:17%;">Note</th>
			</tr>
		</thead>
		<tbody>
			@foreach($transaction as $trans)
			<tr>
				<td style="text-align:center;">{{$loop->iteration}}</td>
				<td style="text-align:center;">{{$trans->msisdn}}</td>
                <td style="text-align:center;">{{$trans->iccid}}</td>
				<td style="text-align:center;">{{$trans->device_type}}</td>
				<td style="text-align:center;">{{$trans->brand}}</td>
				<td style="text-align:center;">{{$trans->imei}}</td>
                <td style="text-align:center;"> 1 </td>
				<td>{{$trans->note}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
    <br/>
    <br/>
    <br/>
    <div class="row">
        <p class="note">Dengan ini telah menyatakan telah menerima simcard sesuai data/kolom diatas, selanjutnya kami setuju setelah BAST diterima, Pihak Indosat melakukan aktivasi kartu</p>
        <p class="note">tersebut dengan paket yang telah disetujui dan BAST ini dapat dijadikan dasar PT Indosat melakukan penagihan dan ditandatangani oleh PIC corporate yang ditunjuk.</p>
    </div>
    <br/>
    <br/>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <table class="table table-sm" id="table_so">
                        <tr>
                            <th width="10%"></th>
                            <th style="text-align:center;" width="35%">{{$bast->address_2}}, {{$bulan}}</th>
                            <th width="10%"></th>
                            <td style="text-align:center;" width="35%">{{$bast->address_2}}, {{$bulan}}</td>
                            <th width="10%"></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th style="text-align:center;">Diterima oleh PIC Corporate</th>
                            <th></th>
                            <td> </td>
                            <th></th>
                        </tr>
                        <tr>
                            <th ></th>
                            <th style="text-align:center;"><img class="img-fluid" src="{{ asset($bast->sign_customer) }}" alt="" width="190" height="110"/></th>
                            <th ></th>
                            <th ></th>
                            <th></th>
                        </tr>
                        <tr>
                            <th ></th>
                            <th style="text-align:center;">({{$bast->name_customer}})</th>
                            <th ></th>
                            <td style="text-align:center;">(....................................)</td>
                            <th></th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>




	<style type="text/css">
        #table_dr, #table_so {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            table-layout:fixed;
            overflow-wrap: break-word;
            page-break-inside:auto
        }

        #table_dr td, #table_dr th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #table_dr tr:nth-child(even){background-color: #f2f2f2;}

        #table_dr th {
            padding-top: 12px;
            padding-bottom: 12px;
            font-size: 14pt;
            text-align: center;
            background-color: #4CAF50;
            color: white;
        }

        #table_dr td, .date, .note {
            font-size: 12pt;
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            overflow: hidden;
        }

        .title{
            font-size: 36pt;
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        }

        .page-break {
            page-break-after: always;
        }

        tr {
            page-break-inside:avoid; page-break-after:auto
        }

	</style>
</body>
</html>