<ul id="signature-tab" class="nav nav-tabs" role="tablist">
	<li role="presentation" class="nav-item active">
		<a class="nav-link" href="#disclaimer" role="tab" class="signature-used" data-toggle="tab">Tanda Tangan</a>
	</li>
</ul>

<!-- Tab panes -->
<div class="tab-content signature-tab">
	<div role="tabpanel" class="tab-pane active text-center" id="disclaimer">
		<button type="button" class="btn btn-warning btn-md signing" data-toggle="modal" data-target="#signature-modal" data-backdrop="static">
			<span class="glyphicon glyphicon-pencil"></span> Tandatangani
		</button>
		<div class="signature-wrapper"></div>
		<input type="button" id="resetBtn" class="btn btn-warning btn-sm re-sign pull-right" onclick="resetSignature();" value="TTD Ulang" style="margin-top: -10px; margin-right: -20px; display: none;">
	</div>
</div>

<!-- Button trigger modal -->

<!-- Modal -->
<div class="modal fade" id="signature-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
            <div class="modal-header">
                <p class="modal-title" style="font-size:25px;" id="myModalLabel"><b>Tanda Tangan Pelanggan</b></p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
            </div>

			<div class="modal-body">
				<!-- Nav tabs -->
				<ul id="signature-tab" class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a class="test" href="#disclaimer" role="tab" class="signature-used" data-toggle="tab">Tanda Tangan</a></li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content signature-tab" style="padding: 10px;">
					<div role="tabpanel" class="tab-pane active" id="disclaimer">
						<div class="form-group" style="max-width: 350px; margin: 0 auto;">
							<div class="js-signature" data-width="150" data-height="150" data-border="1px dotted black" data-line-color="#000000" data-auto-fit="true">
							</div>
							<input type="hidden" name="signature"/>
							<div class="signature-wrapper text-center"></div>
						</div>		
						<div class="form-group text-center">
							<input type="button" id="clearBtn" class="btn btn-secondary m-1" onclick="clearSignature();" value="Clear">
							<input type="button" id="resetBtn" class="btn btn-warning" onclick="resetSignature();" value="Reset" style="display: none;">
							<input type="button" id="saveBtn" class="btn btn-success" onclick="saveSignature();" value="Save">
						</div>
					</div>
				</div>
			</div>
			<div class="text-center">
				<button type="button" class="btn btn-primary m-1" data-dismiss="modal">Selesai</button>
			</div>
            <br/>
		</div>
	</div>
</div>