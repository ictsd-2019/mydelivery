<!-- Nav tabs -->
<ul id="gender" class="nav nav-tabs" role="tablist">
	<li role="presentation" class="nav-item active">
		<a class="nav-link" href="#gender" role="tab" data-toggle="tab">Data Penerima</a>
	</li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
	<div role="tabpanel" class="tab-pane active" id="gender">
		<div class="row form-group">
			<div class="col-md-4 col-sm-5 d-none d-md-block">
		    	{!! Form::label('name_customer', 'Nama Lengkap', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
			</div>
			<div class="col-md-8 col-sm-7">
		        {!! Form::text('name_customer', null, ['class' => 'form-control placeholder-fix', 'placeholder' => 'Nama Lengkap', 'required' => 'required']) !!}
			</div>
		</div>
        <div class="row form-group">
			<div class="col-md-4 col-sm-5 d-none d-md-block">
		    	{!! Form::label('photo_customer', 'Foto Pelanggan', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
			</div>
			<div class="col-md-8 col-sm-7">
                <input type="file" name="photo_customer" class="form-control" required/>
                <!-- <small>Ext: Jpeg/Jpg/PNG</small> -->
			</div>
		</div>
	</div>
</div>