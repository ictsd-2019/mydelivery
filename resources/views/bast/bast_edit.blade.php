@extends('layouts.app')

@section('content')
	<h1 class="title">BAST Info</h1>
	<hr/>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
	@include('layouts.error')
    {!! Form::open(['action' => ['CourierController@update', $id_code], 'class' => 'form-horizontal', 'id' => 'courier-form', 'enctype' => 'multipart/form-data']) !!}
		<div class="row">
			<div class="col-md-6 col-sm-6">
				@include('bast.form_customer')
				<br/>
			</div>	
			<div class="col-md-6 col-sm-6">
				@include('bast.form_signature')
				<br/>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<strong><sup class="required">*</sup> Required</strong><br/>
				<br/><br/>
				<a href="/courier/sign-bast" class="btn btn-secondary m-1">Back</a>
				<input type="submit" value="Submit"  class="btn btn-primary m-1">
			</div>
		</div>
	{!! Form::close() !!}
	<style>
    body {
        padding-right: 0px !important;
    }

    </style>
@endsection

@section('scripts')
	@parent
	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script src="/js/jqsignature/jq-signature.min.js"></script>
	<script src="/js/activation.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<script>
	$(document).ready(function() {
		$('.js-signature').jqSignature();

		var canvas = document.getElementsByTagName('canvas')[0];
		canvas.width  = 350;
		canvas.height = 150;
		canvas.style.width  = '100%';
		canvas.style.height = '100%';
	});
	</script>
	<script type="text/javascript">
		function clearSignature() {
			$('.js-signature').jqSignature('clearCanvas');
		}
    </script>
    <script type="text/javascript">
		function saveSignature() {
			var dataUrl = $('.js-signature').jqSignature('getDataURL');
			var img = $('<img>').attr('src', dataUrl);
			$('#disclaimer input[name="signature"]').val(dataUrl);

			$('#disclaimer .signature-wrapper').empty();
			$('#disclaimer .signature-wrapper').append('<span>TTD: </span><br/>')
			$('#disclaimer .signature-wrapper').append(img);
			$('#disclaimer .signing').hide();
			$('#disclaimer .re-sign').show();
			$('.js-signature').hide();

			$("#saveBtn").hide();
			$("#clearBtn").hide();
			$("#resetBtn").show();
		}
    </script>
    <script type="text/javascript">
		function resetSignature()
		{
			$('.js-signature').show();
			$('.js-signature').jqSignature('clearCanvas');
			$('#disclaimer .signature-wrapper').empty();
			$('#disclaimer input[name="signature"]').val("");

			$("#clearBtn").show();
			$("#saveBtn").show();
			$("#resetBtn").hide();
			$(".signing").show();
		}
	</script>

@endsection