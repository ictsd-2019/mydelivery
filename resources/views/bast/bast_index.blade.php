@extends('layouts.app')

@section('content')
    <h1 class="title">Daftar BAST</h1>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
    @include('layouts.error')
    <div class="row">
        <div class="col-md-12">
            <div class="activation-history-wrapper container" style="overflow-y: hidden; overflow-x: auto;">
                <table id="example" class="table table-striped table-hover text-center" style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align:center; width:3%;">No</th>
                            <th style="text-align:center; width:15%;">No DR</th>
                            <th style="text-align:center; width:10%;">Request Date</th>
                            <th style="text-align:center; width:20%;">Corporate Name</th>
                            <th style="width:28%;">Destination</th>
                            <th style="text-align:center; width:6%;">Quantity</th>
                            <th style="text-align:center; width:10%;">Status</th>
                            <th style="text-align:center; width:8%;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($bast as $ba)
                        <tr>
                            <td> {{$loop->iteration}} </td>
                            <td> {{$ba["no_dr"]}} </td>
                            <td> {{$ba["request_date"]}} </td>
                            <td align="left"> {{$ba["corporate_name"]}} </td>
                            <td align="left"> {{$ba["address_1"]}} </td>
                            <td> {{$ba["quantity"]}} </td>
                            <td> {{$ba["status"]}} </td>
                            <td style="text-align:center;">
                                @if($ba["no_dr"] != null)
                                    @if($ba["status"] == 'In Progress Courier')
                                    <a href="/courier/sign-bast/{{$ba['id']}}-dr/edit" class="btn btn-success btn-sm m-1"><i class="fa fa-edit"></i></a>
                                    @else
                                    <a href="/bast/export/{{$ba['id']}}-dr" class="btn btn-warning btn-sm m-1 text-white"><i class="fa fa-download"></i></a>
                                    @endif
                                @else
                                    @if($ba["status"] == 'In Progress Courier')
                                    <a href="/courier/sign-bast/{{$ba['id']}}-shipment/edit" class="btn btn-success btn-sm m-1"><i class="fa fa-edit"></i></a>
                                    @else
                                    <a href="/bast/export/{{$ba['id']}}-shipment" class="btn btn-warning btn-sm m-1 text-white"><i class="fa fa-download"></i></a>
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <style>
    thead input {
        width: 100%;
    }
    body {
        padding-right: 0px !important;
    }
    </style>
@endsection

@section('scripts')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    <script>
    $(document).ready(function() {
	    $('#example thead tr').clone(true).appendTo( '#example thead' );
        $('#example thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            $(this).html( '<input type="text" style="text-align:center;" placeholder="Search" class="placeholder-fix" />' );
    
            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );
    
        var table = $('#example').DataTable( {
            orderCellsTop: true,
            fixedHeader: true,
        } );
	} );
    </script>
@endsection