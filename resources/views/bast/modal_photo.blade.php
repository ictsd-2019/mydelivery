<div id="modal_photo_{{$trans->id}}" class="modal fade">
    <div class="modal-dialog modal-md" role="document" >
        <div class="modal-content" style="border-radius:2%;">
            <div class="modal-header">
                Photo Customer
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <img class="rounded" style="padding:5%; width:75%;" src="{{asset($trans->photo_customer)}}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>