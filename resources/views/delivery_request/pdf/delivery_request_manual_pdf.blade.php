<!DOCTYPE html>
<html>
<head>
	<title>DR {{$so_dr->courier}} {{$so_dr->nomor_dr}}-{{$so_dr->corporate_name}}-{{$so_dr->order_date}}</title>
</head>
<body>
    <br/>
    <div class="row" style="white-space:nowrap">
        <div id="image" style="display:inline; float:left;">
            <img src="{{ asset('/storage/upload/Indosat_Ooredoo.png') }}" width="195" height="105">
        </div>
        <div id="texts" style="display:inline; white-space:nowrap;">
            <center>
            <p class="title"><b>DELIVERY REQUEST<b></p>
            </center>
        </div>
    <div>
    <br/>
    <br/>
    <br/>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <table class="table table-sm" id="table_so">
                        <tr>
                            <th width="7%"></th>
                            <th style="text-align:left;" width="13%"><strong>No. DR</strong></th>
                            <td width="30%">: {{ $so_dr->no_dr }}</td>
                            <th style="text-align:left;" width="10%"><strong>Corporate Name</strong></th>
                            <td width="30%">: {{ $so_dr->corporate_name }}</td>
                            <th width="10%"></th>
                        </tr>
                        <tr>
                            <th width="7%"></th>
                            <th style="text-align:left;" width="13%"><strong>Request Date</strong></th>
                            <td width="30%">: {{$tanggal}}</td>
                            <th style="text-align:left;" width="13%"><strong>Address 1</strong></th>
                            <td width="30%">: {{ $so_dr->address_1 }}</td>
                            <th width="7%"></th>
                        </tr>
                        <tr>
                            <th width="7%"></th>
                            <th style="text-align:left;" width="13%"><strong>Quote ID</strong></th>
                            <td width="30%">: {{ $so_dr->quote_no }}</td>
                            <th style="text-align:left;" width="13%"><strong>RT/RW</strong></th>
                            <td width="30%">: {{ $so_dr->rt_rw }}</td>
                            <th width="7%"></th>
                        </tr>
                        <tr>
                            <th width="7%"></th>
                            <th style="text-align:left;" width="13%"><strong>Requestor (sales)</strong></th>
                            <td width="30%">: {{ $so_dr->sales }}</td>
                            <th style="text-align:left;" width="13%"><strong>Zip Post</strong></th>
                            <td width="30%">: {{ $so_dr->zip_post }}</td>
                            <th width="7%"></th>
                        </tr>
                        <tr>
                            <th width="7%"></th>
                            <th style="text-align:left;" width="13%"><strong>Segment</strong></th>
                            <td width="30%">: {{ $so_dr->sales_dept }}</td>
                            <th style="text-align:left;" width="13%"><strong>Address 2</strong></th>
                            <td width="30%">: {{ $so_dr->address_2 }}</td>
                            <th width="7%"></th>
                        </tr>
                        <tr>
                            <th width="7%"></th>
                            <th width="13%"></th>
                            <th width="30%"></th>
                            <th style="text-align:left;" width="13%"><strong>Address 3</strong></th>
                            <td width="30%">: {{ $so_dr->address_3 }}</td>
                            <th width="7%"></th>
                        </tr>
                        <tr>
                            <th width="7%"></th>
                            <th width="13%"></th>
                            <th width="30%"></th>
                            <th style="text-align:left;" width="13%"><strong>PIC Corporate 1</strong></th>
                            <td width="30%">: {{ $so_dr->pic_corporate_1 }}</td>
                            <th width="7%"></th>
                        </tr>
                        <tr>
                            <th width="7%"></th>
                            <th width="13%"></th>
                            <th width="30%"></th>
                            <th style="text-align:left;" width="13%"><strong>No. HP</strong></th>
                            <td width="30%">: {{ $so_dr->pic_phone_1 }}</td>
                            <th width="7%"></th>
                        </tr>
                        <tr>
                            <th width="7%"></th>
                            <th width="13%"></th>
                            <th width="30%"></th>
                            <th style="text-align:left;" width="13%"><strong>PIC Corporate 2</strong></th>
                            <td width="30%">: {{ $so_dr->pic_corporate_2 }}</td>
                            <th width="7%"></th>
                        </tr>
                        <tr>
                            <th width="7%"></th>
                            <th width="13%"></th>
                            <th width="30%"></th>
                            <th style="text-align:left;" width="13%"><strong>No. HP</strong></th>
                            <td width="30%">: {{ $so_dr->pic_phone_2 }}</td>
                            <th width="7%"></th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <br/>
    <br/>
    <table class='table table-bordered' id="table_dr">
        <thead>
            <tr>
                <th style="width:4%;">No</th>
                <th style="width:10%;">MSISDN</th>
                <th style="width:8%;">Brand</th>
                <th style="width:15%;">Device Type</th>
                <th style="width:10%;">Device Price<br>(Rp)</th>
                <th style="width:12%;">IMEI</th>
                <th style="width:6%;">Qty</th>
                <th style="width:6%;">UOM</th>
                <th style="width:23%;">Destination</th>
                <th style="width:6%;">Packing<br>Y/N</th>
            </tr>
        </thead>
        <tbody>
            @php $i=1 @endphp
            @foreach($delivery_request as $dr)
            <tr>
                <td style="text-align:center;">{{ $i++ }}</td>
                <td style="text-align:center;">{{$dr->msisdn}}</td>
                @if($salesorder->type_req == 'Bundling')
                <td style="text-align:center;">{{$dr->brand}}</td>
                <td style="text-align:center;">{{$dr->device_type}}</td>
                @else
                <td style="text-align:center;">{{$dr->brand}}</td>
                <td style="text-align:center;">{{$dr->device_type}}</td>
                @endif
                <td style="text-align:center;">{{$dr->device_price}}</td>
                @if($salesorder->type_req == 'Bundling')
                <td style="text-align:center;">{{$dr->imei}}</td>
                @else
                <td style="text-align:center;"></td>
                @endif
                <td style="text-align:center;">{{$dr->quantity}}</td>
                <td style="text-align:center;">{{$dr->uom}}</td>
                <td >{{$dr->destination}}</td>
                <td style="text-align:center;">{{$dr->packing}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <br/>
    <br/>
    <br/>
    <center>
        <p class="date">Jakarta, {{$tanggal}}</p>
    </center>
    <br/>
    <br/>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <table class="table table-sm" id="table_so">
                        <tr>
                            <th width="10%"></th>
                            <th style="text-align:center;" width="35%">Requester,</th>
                            <th width="10%"></th>
                            <td style="text-align:center;" width="35%">Approve by Manager</td>
                            <th width="10%"></th>
                        </tr>
                        <tr>
                            <th ></th>
                            <th style="text-align:center;"><img class="img-fluid" src="{{ asset($requestor->signature) }}" alt="" width="190" height="110"/></th>
                            <th ></th>
                            <td style="text-align:center;"><img class="img-fluid" src="{{ asset($manager->signature) }}" alt="" width="190" height="110"/></td>
                            <th></th>
                        </tr>
                        <tr>
                            <th ></th>
                            <th style="text-align:center;"><u>( {{$requestor->name}} )</u></th>
                            <th ></th>
                            <td style="text-align:center;"><u>( {{$manager->name}} )</u></td>
                            <th></th>
                        </tr>
                        <tr>
                            <th ></th>
                            <th style="text-align:center;">{{$requestor->nik}}</th>
                            <th ></th>
                            <td style="text-align:center;">{{$manager->nik}}</td>
                            <th ></th>
                        </tr>
                        <tr>
                            <th ></th>
                            <th style="text-align:center;" >Dept. {{$requestor->department}} Div. {{$requestor->division}}</th>
                            <th ></th>
                            <td style="text-align:center;">{{$manager->position}} {{$manager->department}}</td>
                            <th></th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="page-break"></div>

    <br/>
    <div class="row" style="white-space:nowrap">
        <div id="image" style="display:inline; float:left;">
            <img src="{{ asset('/storage/upload/Indosat_Ooredoo.png') }}" width="195" height="105">
        </div>
        <div id="texts" style="display:inline; white-space:nowrap;">
            <center>
            <p class="title"><b>BERITA ACARA SERAH TERIMA {{$jenis}}<b></p>
            </center>
        </div>
    <div>
    <br/>
    <br/>
    <br/>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <table class="table table-sm" id="table_so">
                        <tr>
                            <th width="7%"></th>
                            <th style="text-align:left;" width="15%"><strong>Requestor (Sales)</strong></th>
                            <td width="68%">: {{ $so_dr->sales }}</td>
                            <th width="7%"></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th style="text-align:left;"><strong>Segment</strong></th>
                            <td>: {{ $so_dr->sales_dept }}</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th style="text-align:left;"><strong>No. DR</strong></th>
                            <td>: {{ $so_dr->no_dr }}</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th style="text-align:left;"><strong>Quote ID</strong></th>
                            <td>: {{ $so_dr->quote_no }}</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th style="text-align:left;"><strong>Corporate Name</strong></th>
                            <td>: {{ $so_dr->corporate_name }}</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th style="text-align:left;"><strong>Address</strong></th>
                            <td>: {{ $so_dr->address_1 }}</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th style="text-align:left;"><strong>Address</strong></th>
                            <td>: {{ $so_dr->address_2 }}</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th style="text-align:left;"><strong>PIC Corporate 1</strong></th>
                            <td>: {{ $so_dr->pic_corporate_1 }}</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th style="text-align:left;"><strong>Telpon/HP</strong></th>
                            <td>: {{ $so_dr->pic_phone_1 }}</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th style="text-align:left;"><strong>PIC Corporate 2</strong></th>
                            <td>: {{ $so_dr->pic_corporate_2 }}</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th style="text-align:left;"><strong>Handphone</strong></th>
                            <td>: {{ $so_dr->pic_phone_2 }}</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th style="text-align:left;"><strong>Request Date</strong></th>
                            <td>: {{ $tanggal }}</td>
                            <th></th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <br/>
    <br/>
	<table class='table table-bordered' id="table_dr">
		<thead>
			<tr>
				<th style="width:7%;">No</th>
				<th style="width:12%;">MSISDN</th>
                <th style="width:16%;">ICCID</th>
				<th style="width:10%;">Brand</th>
				<th style="width:19%;">Device Type</th>
				<th style="width:13%;">IMEI</th>
                <th style="width:6%;">Qty</th>
                <th style="width:17%;">Note</th>
			</tr>
		</thead>
		<tbody>
			@foreach($delivery_request as $dr)
			<tr>
				<td style="text-align:center;">{{$loop->iteration}}</td>
				<td style="text-align:center;">{{$dr->msisdn}}</td>
                <td style="text-align:center;">{{$dr->iccid}}</td>
                @if($salesorder->type_req == 'Bundling')
				<td style="text-align:center;">{{$dr->brand}}</td>
				<td style="text-align:center;">{{$dr->device_type}}</td>
                @else
				<td style="text-align:center;">{{$dr->brand}}</td>
				<td style="text-align:center;">{{$dr->device_type}}</td>
                @endif
                @if($salesorder->type_req == 'Bundling')
				<td style="text-align:center;">{{$dr->imei}}</td>
                @else
				<td style="text-align:center;"></td>
                @endif
                <td style="text-align:center;">{{$dr->quantity}}</td>
				<td>{{$dr->note}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
    <br/>
    <br/>
    <br/>
    <div class="row">
        <p class="note">Dengan ini telah menyatakan telah menerima simcard sesuai data/kolom diatas, selanjutnya kami setuju setelah BAST diterima, Pihak Indosat melakukan aktivasi kartu</p>
        <p class="note">tersebut dengan paket yang telah disetujui dan BAST ini dapat dijadikan dasar PT Indosat melakukan penagihan dan ditandatangani oleh PIC corporate yang ditunjuk.</p>
    </div>
    <br/>
    <br/>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <table class="table table-sm" id="table_so">
                        <tr>
                            <th width="10%"></th>
                            <th style="text-align:center;" width="35%">{{$so_dr->address_2}}, &nbsp; &nbsp; &nbsp; {{$bulan}}</th>
                            <th width="10%"></th>
                            <td style="text-align:center;" width="35%">{{$so_dr->address_2}}, &nbsp; &nbsp; &nbsp; {{$bulan}}</td>
                            <th width="10%"></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th style="text-align:center;">Diterima oleh PIC Corporate</th>
                            <th></th>
                            <td> </td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>&nbsp;</th>
                            <th></th>
                            <td>&nbsp;</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>&nbsp;</th>
                            <th></th>
                            <td>&nbsp;</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>&nbsp;</th>
                            <th></th>
                            <td>&nbsp;</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>&nbsp;</th>
                            <th></th>
                            <td>&nbsp;</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>&nbsp;</th>
                            <th></th>
                            <td>&nbsp;</td>
                            <th></th>
                        </tr>
                        <tr>
                            <th ></th>
                            <th style="text-align:center;">(....................................)</th>
                            <th ></th>
                            <td style="text-align:center;">(....................................)</td>
                            <th></th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>




	<style type="text/css">
        #table_dr, #table_so {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            table-layout:fixed;
            overflow-wrap: break-word;
            page-break-inside:auto
        }

        #table_dr td, #table_dr th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #table_dr tr:nth-child(even){background-color: #f2f2f2;}

        #table_dr th {
            padding-top: 12px;
            padding-bottom: 12px;
            font-size: 14pt;
            text-align: center;
            background-color: #4CAF50;
            color: white;
        }

        #table_dr td, .date, .note {
            font-size: 12pt;
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            overflow: hidden;
        }

        .title{
            font-size: 36pt;
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        }

        .page-break {
            page-break-after: always;
        }

        tr {
            page-break-inside:avoid; page-break-after:auto
        }

	</style>
</body>
</html>