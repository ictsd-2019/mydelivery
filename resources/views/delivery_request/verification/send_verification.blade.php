<div id="export_pdf_{{$trans->id}}" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius:2%;">
            <div class="modal-header">
                <p class="modal-title" style="font-size:25px;"><b>Signature</b></p>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form role="form" method="POST" action="/salesorder/delivery/{{$trans->id}}/verification">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-2 col-sm-1">
                        </div>
                        <div class="col-md-2 col-sm-3">
                            <label class="control-label">Requester</label>
                        </div>
                        <div class="col-md-7 col-sm-7">
                            {!! Form::select('requester', $employee, null, array('class'=>'form-control', 'placeholder' => '- Choose Requester -', 'required' => 'required')) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2 col-sm-1">
                        </div>
                        <div class="col-md-2 col-sm-3">
                        <label class="control-label">Manager</label>
                        </div>
                        <div class="col-md-7 col-sm-7">
                            {!! Form::select('manager', $employee, null, array('class'=>'form-control', 'placeholder' => '- Choose Manager -', 'required' => 'required')) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12 col-sm-12">
                            <input type="checkbox" id="checkbox" name="check">
                            <label for="checkbox">Rekap DR</label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12 col-sm-12">
                            <center>
                                <button type="button" class="btn btn-secondary m-1" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary m-1">Send to Manager</button>
                            </center>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>