@extends('layouts.app')

@section('content')
    <h1 class="title">Delivery Request Approval</h1>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
    @include('layouts.error')
    <div class="row">
        <div class="col-md-12">
            <div class="activation-history-wrapper container" style="overflow-y: hidden; overflow-x: auto;">
                <table id="example" class="table table-striped table-hover text-center" style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align:center; width:3%;">No</th>
                            <th style="text-align:center; width:15%;">No DR</th>
                            <th style="text-align:center; width:10%;">Request Date</th>
                            <th style="text-align:center; width:20%;">Corporate Name</th>
                            <th style="width:28%;">Destination</th>
                            <th style="text-align:center; width:6%;">Quantity</th>
                            <th style="text-align:center; width:7%;">Status</th>
                            <th style="text-align:center; width:11%;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($salesorder_delivery_request as $so_dr)
                        <tr>
                            <td> {{$loop->iteration}} </td>
                            <td> {{$so_dr->no_dr}} </td>
                            <td> {{$so_dr->request_date}} </td>
                            <td align="left"> {{$so_dr->corporate_name}} </td>
                            <td align="left"> {{$so_dr->address_1}} </td>
                            <td> {{$so_dr->quantity}} </td>
                            <td> {{$so_dr->status}} </td>
                            <td style="text-align:center;">
                                <a href="/delivery/export/{{$so_dr->id}}" class="btn btn-warning btn-sm m-1 text-white"><i class="fa fa-download"></i></a>
                                <a data-toggle="modal" data-target="#confirmation_{{$so_dr->id}}" class="btn btn-success btn-sm m-1 text-white"><i class="fa fa-arrow-circle-right"></i></a>
                                @include('delivery_request.verification.modal_confirmation')
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <style>
    thead input {
        width: 100%;
    }
    body {
        padding-right: 0px !important;
    }
    </style>
@endsection

@section('scripts')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    <script>
    $(document).ready(function() {
	    $('#example thead tr').clone(true).appendTo( '#example thead' );
        $('#example thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            $(this).html( '<input type="text" style="text-align:center;" placeholder="Search" class="placeholder-fix" />' );
    
            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );
    
        var table = $('#example').DataTable( {
            orderCellsTop: true,
            fixedHeader: true,
        } );
	} );
    </script>
@endsection