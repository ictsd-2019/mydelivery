@extends('layouts.app')

@section('content')
    <h1 class="title">Delivery Request & BAST {{$so_dr->no_dr}}</h1>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
    @include('layouts.error')
    <div class="row">
        <div class="col-md-6">
            <ul class="nav nav-tabs">
                <li class="nav-item active">
                    <a class="nav-link" href="#" style="color:black;">Request Info</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="row form-group">
                    <div class="col-md-4">
                        {!! Form::label('no_dr', 'No. DR', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                    </div>
                    <div class="col-md-8">
                        {!! Form::text('no_dr', $so_dr->no_dr, ['class' => 'form-control placeholder-fix', 'placeholder' => 'Nomor DR', 'required' => 'required', 'disabled' => 'disabled']) !!}
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4">
                        {!! Form::label('request_date', 'Request Date', ['class' => 'col-form-label']) !!}
                    </div>
                    <div class="col-md-8">
                        {!! Form::date('request_date', $so_dr->request_date, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4">
                        {!! Form::label('sales', 'Sales Name', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                    </div>
                    <div class="col-md-8">
                        {!! Form::text('sales', $so_dr->sales, ['class' => 'form-control placeholder-fix', 'placeholder' => 'Sales Name', 'required' => 'required', 'disabled' => 'disabled']) !!}
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4">
                        {!! Form::label('sales_dept', 'Sales Department', ['class' => 'col-form-label']) !!}<sup class="required">*</sup>
                    </div>
                    <div class="col-md-8">
                        {!! Form::text('sales_dept', $so_dr->sales_dept, ['class' => 'form-control placeholder-fix', 'placeholder' => 'Sales Department', 'required' => 'required', 'disabled' => 'disabled']) !!}
                    </div>
                </div>
            </div>
            <br>
        </div>
        <div class="col-md-6">
            <ul class="nav nav-tabs">
                <li class="nav-item active">
                    <a class="nav-link" href="#" style="color:black;">Corporate Info</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="row form-group">
                    <div class="col-md-4">
                        {!! Form::label('corporate_name', 'Corporate Name', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                    </div>
                    <div class="col-md-8">
                        {!! Form::text('corporate_name', $so_dr->corporate_name, ['class' => 'form-control placeholder-fix', 'placeholder' => 'Corporate Name', 'required' => 'required', 'disabled' => 'disabled']) !!}
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4">
                        {!! Form::label('address_1', 'Address 1', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                    </div>
                    <div class="col-md-8">
                        {!! Form::textarea('address_1', $so_dr->address_1, ['rows' => 3,'class' => 'form-control placeholder-fix', 'placeholder' => 'Address', 'required' => 'required', 'disabled' => 'disabled']) !!}
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4">
                        {!! Form::label('rt_rw', 'RT/RW', ['class' => 'col-form-label']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::text('rt_rw', $so_dr->rt_rw, ['class' => 'form-control placeholder-fix', 'placeholder' => 'No. RT/RW', 'disabled' => 'disabled']) !!}
                    </div>
                    <div class="col-md-1">
                        {!! Form::label('zip_post', 'Zip', ['class' => 'col-form-label']) !!}
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('zip_post', $so_dr->zip_post, ['class' => 'form-control placeholder-fix', 'placeholder' => 'Zip Code', 'disabled' => 'disabled', 'disabled' => 'disabled']) !!}
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4">
                        {!! Form::label('address_2', 'Address 2', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                    </div>
                    <div class="col-md-8">
                        {!! Form::text('address_2', $so_dr->address_2, ['class' => 'form-control placeholder-fix', 'placeholder' => 'Address', 'required' => 'required', 'disabled' => 'disabled']) !!}
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4">
                        {!! Form::label('address_3', 'Address 3', ['class' => 'col-form-label']) !!}
                    </div>
                    <div class="col-md-8">
                        {!! Form::text('address_3', $so_dr->address_3, ['class' => 'form-control placeholder-fix', 'placeholder' => 'Address', 'disabled' => 'disabled']) !!}
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4">
                        {!! Form::label('pic_corporate_1', 'PIC Corporate 1', ['class' => 'col-form-label']) !!} <sup class=" required">*</sup>
                    </div>
                    <div class="col-md-8">
                        {!! Form::text('pic_corporate_1', $so_dr->pic_corporate_1, ['class' => 'form-control placeholder-fix', 'placeholder' => 'PIC Name', 'required' => 'required', 'disabled' => 'disabled']) !!}
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4">
                        {!! Form::label('pic_phone_1', 'No. HP', ['class' => 'col-form-label']) !!} <sup class=" required">*</sup>
                    </div>
                    <div class="col-md-8">
                        {!! Form::tel('pic_phone_1', $so_dr->pic_phone_1, ['class' => 'form-control placeholder-fix', 'placeholder' => 'No. Handphone', 'required' => 'required', 'maxlength' => '16', 'disabled' => 'disabled']) !!}
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4">
                        {!! Form::label('pic_corporate_2', 'PIC Corporate 2', ['class' => 'col-form-label']) !!}
                    </div>
                    <div class="col-md-8">
                        {!! Form::text('pic_corporate_2', $so_dr->pic_corporate_2, ['class' => 'form-control placeholder-fix', 'placeholder' => 'PIC Name', 'disabled' => 'disabled']) !!}
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4">
                        {!! Form::label('pic_phone_2', 'No. HP', ['class' => 'col-form-label']) !!}
                    </div>
                    <div class="col-md-8">
                        {!! Form::tel('pic_phone_2', $so_dr->pic_phone_2, ['class' => 'form-control placeholder-fix', 'placeholder' => 'No. Handphone', 'maxlength' => '16', 'disabled' => 'disabled']) !!}
                    </div>
                </div>
            </div>
            <br>
        </div>
    </div>
    <br>
    <div class="btn-group pull-right" role="group">
        <a type="button" class="btn btn-success btn-sm text-white m-1" data-toggle="modal" data-target="#importDR">Import DR</a>
    </div>
    <div class="activation-history-wrapper container" style="overflow-y: hidden; overflow-x: auto;">
        <table id="example" class="table table-striped table-hover text-center" style="width:200%">
            <thead>
                <tr>
                    <th style="text-align:center; width:2%;">No</th>
                    <th style="text-align:center; width:8%;">MSISDN</th>
                    <th style="text-align:center; width:10%;">ICCID</th>
                    <th style="text-align:center; width:8%;">Brand</th>
                    <th style="text-align:center; width:9%;">Device Type</th>
                    <th style="text-align:center; width:8%;">Device Price</th>
                    <th style="text-align:center; width:8%;">IMEI</th>
                    <th style="text-align:center; width:5%;">Quantity</th>
                    <th style="text-align:center; width:8%;">UOM</th>
                    <th style="text-align:center; width:15%;">Destination</th>
                    <th style="text-align:center; width:4%;">Packing Y/N</th>
                    <th style="text-align:center; width:10%;">Notes</th>
                    <th style="text-align:center; width:6%;">Aksi</th>
                </tr>
            </thead>
            <tbody>
            @foreach($delivery_request as $dr)
                <tr>
                    <td> {{$loop->iteration}} </td>
                    <td> {{$dr->msisdn}} </td>
                    <td> {{$dr->iccid}} </td>
                    <td> {{$dr->brand}} </td>
                    <td> {{$dr->device_type}} </td>
                    <td> {{$dr->device_price}} </td>
                    <td> {{$dr->imei}} </td>
                    <td> {{$dr->quantity}} </td>
                    <td> {{$dr->UOM}} </td>
                    <td> {{$dr->destination}} </td>
                    <td> {{$dr->packing}} </td>
                    <td> {{$dr->notes}} </td>
                    <td>
                        <a data-toggle="modal" data-target="#edit-modal-{{ $dr->id }}" class="btn btn-success btn-sm text-white m-1"><i class="fa fa-edit"></i></a>
                        <a href="/delivery/{{$dr->id}}/delete" class="btn btn-danger btn-sm m-1" onclick="return confirm('Are you sure you want to delete this info?');"><i class="fa fa-trash"></i></a>
                        @include('delivery_request.form_edit_delivery_request')
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-12">
            @if($salesorder->status == "Activation")
                <a href="/salesorder/{{$salesorder->id}}/activation" class="btn btn-secondary m-1">Back</a>
            @else
                <a href="/salesorder/{{$so_dr->id_salesorder}}" class="btn btn-secondary m-1">Back</a>
            @endif
        </div>
    </div>
    <!-- MODAL -->
    @include('delivery_request.form_generate_delivery_request')
    @include('delivery_request.form_import')

    <style>
    thead input {
        width: 100%;
    }
    body {
        padding-right: 0px !important;
    }
    </style>
@endsection

@section('scripts')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    <script>
    $(document).ready(function() {
	    $('#example thead tr').clone(true).appendTo( '#example thead' );
        $('#example thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            $(this).html( '<input type="text" style="text-align:center;" placeholder="Search" class="placeholder-fix" />' );
    
            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );
    
        var table = $('#example').DataTable( {
            orderCellsTop: true,
            fixedHeader: true,
        } );
	} );
    </script>
@endsection