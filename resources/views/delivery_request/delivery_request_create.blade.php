@extends('layouts.app')

@section('content')
    <h1 class="title">Create Delivery Request</h1>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
    @include('layouts.error')
    <br>
    {!! Form::open(['action' => ['SalesorderDeliveryRequestController@store', $salesorder->id], 'id' => 'create-so-dr', 'class' => 'form-horizontal']) !!}
        @include('delivery_request.form_create_so_dr')
    {!! Form::close() !!}
@endsection
