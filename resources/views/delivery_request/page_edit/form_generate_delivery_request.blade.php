<div id="generateDR" class="modal fade">
    <div class="modal-dialog modal-lg" role="document" >
        <div class="modal-content" style="border-radius:2%;">
            <div class="modal-header">
                <p class="modal-title" style="font-size:25px;"><b>Generate Delivery Request & BAST</b></p>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form role="form" method="POST" action="/delivery/{{$so_dr->id}}/store/edit">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-1 col-sm-1">
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label">MSISDN</label>
                        </div>
                        <div class="col-md-7 col-sm-7">
                            <input type="text" class="form-control placeholder-fix" name="msisdn" placeholder="MSISDN">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-1 col-sm-1">
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label">ICCID</label>
                        </div>
                        <div class="col-md-7 col-sm-7">
                            <input type="text" class="form-control placeholder-fix" name="iccid" placeholder="ICCID">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-1 col-sm-1">
                        </div>
                        <div class="col-md-3 col-sm-3">
                        <label class="control-label">Brand</label>
                        </div>
                        <div class="col-md-7 col-sm-7">
                            <input type="text" class="form-control placeholder-fix" name="brand" placeholder="Brand">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-1 col-sm-1">
                        </div>
                        <div class="col-md-3 col-sm-3">
                        <label class="control-label">Device Type</label> <sup class="required">*</sup>
                        </div>
                        <div class="col-md-7 col-sm-7">
                            <input type="text" class="form-control placeholder-fix" name="device_type" placeholder="Device Type" required="required">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-1 col-sm-1">
                        </div>
                        <div class="col-md-3 col-sm-3">
                        <label class="control-label">Device Price</label> <sup class="required">*</sup>
                        </div>
                        <div class="col-md-7 col-sm-7">
                            <div class="input-group">
                                <div class="input-group-text">Rp</div>
                                <input type="text" class="form-control placeholder-fix" name="device_price" placeholder="0" required="required">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-1 col-sm-1">
                        </div>
                        <div class="col-md-3 col-sm-3">
                        <label class="control-label">IMEI</label>
                        </div>
                        <div class="col-md-7 col-sm-7">
                            <input type="text" class="form-control placeholder-fix" name="imei" placeholder="IMEI">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-1 col-sm-1">
                        </div>
                        <div class="col-md-3 col-sm-3">
                        <label class="control-label">Quantity</label> <sup class="required">*</sup>
                        </div>
                        <div class="col-md-7 col-sm-7">
                            <input type="text" class="form-control placeholder-fix" name="quantity" placeholder="0" required="required">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-1 col-sm-1">
                        </div>
                        <div class="col-md-3 col-sm-3">
                        <label class="control-label">UOM</label>
                        </div>
                        <div class="col-md-7 col-sm-7">
                            <input type="text" class="form-control placeholder-fix" name="uom" placeholder="UOM">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-1 col-sm-1">
                        </div>
                        <div class="col-md-3 col-sm-3">
                        <label class="control-label">Destination</label> <sup class="required">*</sup>
                        </div>
                        <div class="col-md-7 col-sm-7">
                            <textarea name="destination" class="form-control placeholder-fix" rows=2 placeholder="Enter Destination" required="required">{{$so_dr->address_1}}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-1 col-sm-1">
                        </div>
                        <div class="col-md-3 col-sm-3">
                        <label class="control-label">Packing Y/N</label>
                        </div>
                        <div class="col-md-7 col-sm-7">
                            <input type="text" class="form-control placeholder-fix" name="packing" placeholder="Packing">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-1 col-sm-1">
                        </div>
                        <div class="col-md-3 col-sm-3">
                        <label class="control-label">Notes</label>
                        </div>
                        <div class="col-md-7 col-sm-7">
                            <textarea rows="2" class="form-control placeholder-fix" name="notes" placeholder="Add notes here"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-1 col-sm-1">
                        </div>
                        <div class="col-md-11 col-sm-11">
                            <strong><sup class="required">*</sup> Required</strong>
                            <br/>
                            <center>
                                <button type="button" class="btn btn-secondary m-1" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary m-1">Submit</button>
                            </center>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>