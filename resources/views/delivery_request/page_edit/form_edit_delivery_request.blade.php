@foreach($delivery_request as $dr)
    <div id="edit-modal-{{ $dr->id }}" class="modal fade">
        <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content" style="border-radius:2%;">
                <div class="modal-header">
                    <p class="modal-title" style="font-size:25px;"><b>Edit Delivery Request & BAST</b></p>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" align="left">
                    <form role="form" method="POST" action="/delivery/{{$dr->id}}/edit/update">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <div class="col-md-1 col-sm-1">
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <label class="control-label">MSISDN</label>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <input type="text" class="form-control placeholder-fix" name="msisdn" placeholder="MSISDN" value="{{$dr->msisdn}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-1 col-sm-1">
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <label class="control-label">ICCID</label>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <input type="text" class="form-control placeholder-fix" name="iccid" placeholder="ICCID" value="{{$dr->iccid}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-1 col-sm-1">
                            </div>
                            <div class="col-md-3 col-sm-3">
                            <label class="control-label">Brand</label>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <input type="text" class="form-control placeholder-fix" name="brand" placeholder="Brand" value="{{$dr->brand}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-1 col-sm-1">
                            </div>
                            <div class="col-md-3 col-sm-3">
                            <label class="control-label">Device Type</label> <sup class="required">*</sup>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <input type="text" class="form-control placeholder-fix" name="device_type" placeholder="Device Type" required="required" value="{{$dr->device_type}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-1 col-sm-1">
                            </div>
                            <div class="col-md-3 col-sm-3">
                            <label class="control-label">Device Price</label> <sup class="required">*</sup>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <div class="input-group">
                                    <div class="input-group-text">Rp</div>
                                    <input type="text" class="form-control placeholder-fix" name="device_price" placeholder="Device Price" required="required" value="{{$dr->device_price}}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-1 col-sm-1">
                            </div>
                            <div class="col-md-3 col-sm-3">
                            <label class="control-label">IMEI</label>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <input type="text" class="form-control placeholder-fix" name="imei" placeholder="IMEI" value="{{$dr->imei}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-1 col-sm-1">
                            </div>
                            <div class="col-md-3 col-sm-3">
                            <label class="control-label">Quantity</label> <sup class="required">*</sup>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <input type="text" class="form-control placeholder-fix" name="quantity" placeholder="Quantity" required="required" value="{{$dr->quantity}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-1 col-sm-1">
                            </div>
                            <div class="col-md-3 col-sm-3">
                            <label class="control-label">UOM</label>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <input type="text" class="form-control placeholder-fix" name="uom" placeholder="UOM" value="{{$dr->uom}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-1 col-sm-1">
                            </div>
                            <div class="col-md-3 col-sm-3">
                            <label class="control-label">Destination</label>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <textarea name="destination" class="form-control placeholder-fix" rows=2 placeholder="Enter Destination">{{$dr->destination}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-1 col-sm-1">
                            </div>
                            <div class="col-md-3 col-sm-3">
                            <label class="control-label">Packing Y/N</label>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <input type="text" class="form-control placeholder-fix" name="packing" placeholder="Packing" value="{{$dr->packing}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-1 col-sm-1">
                            </div>
                            <div class="col-md-3 col-sm-3">
                            <label class="control-label">Notes</label>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <textarea name="notes" class="form-control placeholder-fix" rows=2 placeholder="Add notes here">{{$dr->notes}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-1 col-sm-1">
                            </div>
                            <div class="col-md-10 col-sm-10">
                                <strong><sup class="required" >*</sup> Required</strong>
                                <br/>
                                <center>
                                    <button type="button" class="btn btn-secondary m-1" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-primary m-1">Submit</button>
                                </center>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endforeach