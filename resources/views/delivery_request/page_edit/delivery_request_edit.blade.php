@extends('layouts.app')

@section('content')
    <h1 class="title">Edit Delivery Request</h1>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
    @include('layouts.error')
    {!! Form::open(['action' => ['SalesorderDeliveryRequestController@update_edit', $so_dr->id], 'id' => 'edit-so-dr', 'class' => 'form-horizontal']) !!}
        @include('delivery_request.page_edit.form_edit_so_dr')
    {!! Form::close() !!}

    <!-- MODAL -->
    @include('delivery_request.page_edit.form_edit_delivery_request')
    @include('delivery_request.page_edit.form_import')
    
    <style>
    thead input {
        width: 100%;
    }
    body {
        padding-right: 0px !important;
    }
    </style>
@endsection

@section('scripts')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    <script>
    $(document).ready(function() {
	    $('#example thead tr').clone(true).appendTo( '#example thead' );
        $('#example thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            $(this).html( '<input type="text" style="text-align:center; font-family:system-ui, FontAwesome;" placeholder="&#xF002;" class="placeholder-fix" />' );
    
            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );
    
        var table = $('#example').DataTable( {
            orderCellsTop: true,
            fixedHeader: true,
        } );
	} );
    </script>
@endsection