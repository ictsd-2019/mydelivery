<div class="row">
    <div class="col-md-6">
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#" style="color:black;">Request Info</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('no_dr', 'No. DR', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('no_dr', $so_dr->no_dr, ['class' => 'form-control placeholder-fix', 'placeholder' => 'Nomor DR', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('request_date', 'Request Date', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    {!! Form::date('request_date', $so_dr->request_date, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('sales', 'Sales Name', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('sales', $so_dr->sales, ['class' => 'form-control placeholder-fix', 'placeholder' => 'Sales Name', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('sales_dept', 'Sales Department', ['class' => 'col-form-label']) !!}<sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('sales_dept', $so_dr->sales_dept, ['class' => 'form-control placeholder-fix', 'placeholder' => 'Sales Department', 'required' => 'required']) !!}
                </div>
            </div>
        </div>
        <br>
    </div>
    <div class="col-md-6">
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#" style="color:black;">Corporate Info</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('corporate_name', 'Corporate Name', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('corporate_name', $so_dr->corporate_name, ['class' => 'form-control placeholder-fix', 'placeholder' => 'Corporate Name', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('address_1', 'Address 1', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::textarea('address_1', $so_dr->address_1, ['rows' => 3,'class' => 'form-control placeholder-fix', 'placeholder' => 'Address', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('rt_rw', 'RT/RW', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-3">
                    {!! Form::text('rt_rw',  $so_dr->rt_rw, ['class' => 'form-control placeholder-fix', 'placeholder' => 'No. RT/RW']) !!}
                </div>
                <div class="col-md-1">
                    {!! Form::label('zip_post', 'Zip', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-4">
                    {!! Form::text('zip_post',  $so_dr->zip_post, ['class' => 'form-control placeholder-fix', 'placeholder' => 'Zip Code']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('address_2', 'Address 2', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('address_2',  $so_dr->address_2, ['class' => 'form-control placeholder-fix', 'placeholder' => 'Address']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('address_3', 'Address 3', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    {!! Form::text('address_3',  $so_dr->address_3, ['class' => 'form-control placeholder-fix', 'placeholder' => 'Address']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('pic_corporate_1', 'PIC Corporate 1', ['class' => 'col-form-label']) !!} <sup class=" required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('pic_corporate_1',  $so_dr->pic_corporate_1, ['class' => 'form-control placeholder-fix', 'placeholder' => 'PIC Name', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('pic_phone_1', 'No. HP', ['class' => 'col-form-label']) !!} <sup class=" required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::tel('pic_phone_1', $so_dr->pic_phone_1, ['class' => 'form-control placeholder-fix', 'placeholder' => 'No. Handphone', 'required' => 'required', 'maxlength' => '16']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('pic_corporate_2', 'PIC Corporate 2', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    {!! Form::text('pic_corporate_2', $so_dr->pic_corporate_2, ['class' => 'form-control placeholder-fix', 'placeholder' => 'PIC Name']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('pic_phone_2', 'No. HP', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    {!! Form::tel('pic_phone_2', $so_dr->pic_phone_2, ['class' => 'form-control placeholder-fix', 'placeholder' => 'No. Handphone', 'maxlength' => '16']) !!}
                </div>
            </div>
        </div>
        <br>
    </div>
</div>
<br/>
<div class="btn-group pull-right" role="group">
    <a type="button" class="btn btn-success btn-sm text-white m-1" data-toggle="modal" data-target="#importDR">Import DR</a>
</div>
<div class="activation-history-wrapper container" style="overflow-y: hidden; overflow-x: auto;">
    <table id="example" class="table table-striped table-hover text-center" style="width:200%">
        <thead>
            <tr>
                <th style="text-align:center; width:2%;">No</th>
                <th style="text-align:center; width:8%;">MSISDN</th>
                <th style="text-align:center; width:10%;">ICCID</th>
                <th style="text-align:center; width:8%;">Brand</th>
                <th style="text-align:center; width:9%;">Device Type</th>
                <th style="text-align:center; width:7%;">Device Price (Rp)</th>
                <th style="text-align:center; width:8%;">IMEI</th>
                <th style="text-align:center; width:5%;">Quantity</th>
                <th style="text-align:center; width:8%;">UOM</th>
                <th style="text-align:center; width:15%;">Destination</th>
                <th style="text-align:center; width:5%;">Packing Y/N</th>
                <th style="text-align:center; width:10%;">Notes</th>
                <th style="text-align:center; width:6%;">Aksi</th>
            </tr>
        </thead>
        <tbody>
        @foreach($delivery_request as $dr)
            <tr>
                <td> {{$loop->iteration}} </td>
                <td> {{$dr->msisdn}} </td>
                <td> {{$dr->iccid}} </td>
                <td> {{$dr->brand}} </td>
                <td> {{$dr->device_type}} </td>
                <td> {{$dr->device_price}} </td>
                <td> {{$dr->imei}} </td>
                <td> {{$dr->quantity}} </td>
                <td> {{$dr->UOM}} </td>
                <td> {{$dr->destination}} </td>
                <td> {{$dr->packing}} </td>
                <td> {{$dr->notes}} </td>
                <td>
                    <a data-toggle="modal" data-target="#edit-modal-{{ $dr->id }}" class="btn btn-success btn-sm text-white m-1"><i class="fa fa-edit"></i></a>
                    <a href="/delivery/{{$dr->id}}/edit/delete" onclick="return confirm('Are you sure you want to delete this info?');" class="btn btn-danger btn-sm m-1"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<div class="row">
    <div class="col-md-12">
        <strong><sup class="required">*</sup> Required</strong>
        <br/><br/>
        @if($salesorder->status == "Activation")
            <a href="/salesorder/{{$salesorder->id}}/activation" onclick="return confirm('Are you sure you want to cancel?');" class="btn btn-secondary m-1">Cancel</a>
        @else
            <a href="/salesorder/{{$so_dr->id_salesorder}}" onclick="return confirm('Are you sure you want to cancel?');" class="btn btn-secondary m-1">Cancel</a>
        @endif
        <input type="submit" value="Save" class="btn btn-primary m-1">
    </div>
</div>
