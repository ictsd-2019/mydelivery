<div id="importDR" class="modal fade">
    <div class="modal-dialog modal-lg" role="document" >
        <div class="modal-content" style="border-radius:2%;">
            <div class="modal-header">
                <p class="modal-title" style="font-size:25px;"><b>Import Delivery Request & BAST</b></p>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                {!! Form::open(['action' => ['SalesorderDeliveryRequestController@import', $so_dr->id], 'class' => 'form-horizontal', 'id' => 'dr-form', 'enctype' => 'multipart/form-data']) !!}
                <div class="row form-group">
                    <div class="col-md-1 col-sm-1">
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <label>Select File for Upload</label>
                    </div>
                    <div class="col-md-7 col-sm-7">
                        <input type="file" name="select_file" required/>
                        <p><small>.xls, .xslx</small></p>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4 col-sm-5">
                    </div>
                    <div class="col-md-8 col-sm-7">
                        <a href={{ asset('/storage/upload/(no_quote)TemplateGR-BAST.xlsx') }}>Download Template</a>
                        <br>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-12">
                        <center>
                            <button type="button" class="btn btn-secondary m-1" data-dismiss="modal">Cancel</button>
                            <input type="submit" value="Upload" class="btn btn-primary m-1">
                        </center>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>