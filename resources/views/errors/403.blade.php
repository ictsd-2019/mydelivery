@extends('layouts.general')

@section('content')

<h1 class="title"><i class="fa fa-lock" aria-hidden="true"></i> Access Denied</h1>
<hr />
<p>You dont have permission to access this page.</p>
<br/>
<script>
	window.setTimeout(function(){

        // Move to a new location or you can do something else
        @if(Auth::user())
			window.location.href = "{{ Auth::user()->roles[0]->landing_page }}";
        @else
			window.location.href = "/login";
        @endif

    }, 5000);
</script>
@endsection