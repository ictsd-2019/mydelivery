@extends('layouts.general')

@section('content')

<h1 class="title"><i class="fa fa-lock" aria-hidden="true"></i>Account Suspended</h1>
<hr />
<p>Your account has been suspended, please contact System Administrator.</p>
<br/>
@endsection