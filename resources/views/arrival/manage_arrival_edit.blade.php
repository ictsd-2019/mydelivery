@extends('layouts.app')

@section('content')
    <h1 class="title">Manage Update Arrival</h1>
    @include('layouts.error')
    <br>
    {!! Form::open(['action' => ['ArrivalController@update', $arrival->id], 'id' => 'edit-arrival', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
        @csrf
        @include('arrival.form_manage_edit_arrival')
    {!! Form::close() !!}
@endsection
