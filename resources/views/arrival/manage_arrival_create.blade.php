@extends('layouts.app')
@section('content')
    <h1 class="title">Add Data Arrival</h1>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
    @include('layouts.error')
    <br>
    {!! Form::open(['action' => ['ArrivalController@store'], 'id' => 'create-arrival', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
        @csrf
        @include('arrival.form_manage_add_arrival')
    {!! Form::close() !!}
@endsection