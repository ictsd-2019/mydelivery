<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#" style="color:black;">Form Update Arrival</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('id', 'ID Arrival', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-4">
                    {!! Form::text('id', $arrival->id_arrival, ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => 'NIK','readonly' => 'true']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('day', 'Day Work', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-4">
                {!! Form::select('day', array('D' => '- Select Option -', 'Hari Kerja' => 'Hari Kerja', 'Hari Libur' => 'Hari Libur'), 'Hari Kerja', ['class' => 'form-control mb-2 mt-1 placeholder-fix']) !!}
                </div>
            </div>

            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('vendor', 'Vendor', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-4">
                {!! Form::select('vendor', $selected_vendor, $arrival->id_outtask, array('class' => 'form-control mb-2 mt-1 placeholder-fix', 'placeholder' => '- Select Vendor -', 'required' => 'required')) !!}
                </div>
            </div>
         
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('price', 'Price', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-4">
                    {!! Form::number('price', $arrival->arrival_price, ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => ' Price']) !!}
                </div>
            </div>

            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('fee', 'Discriptiom', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::textarea('fee', $arrival->arrival_fee, ['class' => 'form-control mb-2 mt-1', 'required' => 'required', 'placeholder' => 'Discriptiom']) !!}
                </div>
            </div>
        </div>
        <br>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <strong><sup class="required">*</sup> Required</strong>
        <br/><br/>
        <a href="/arrival" class="btn btn-secondary m-1">Cancel</a>
        <input type="submit" value="Update Data"  class="btn btn-primary m-1">
    </div>
</div>