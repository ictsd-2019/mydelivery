<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#" style="color:black;">Arrival Info</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('day', 'Work Day', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-4">
                    {!! Form::select('day', array('D' => '- Select Option -', 'Hari Kerja' => 'Hari Kerja', 'Hari Libur' => 'Hari Libur'), 'D', ['class' => 'form-control mb-2 mt-1 placeholder-fix']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('vendor', 'Vendor', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-4">
                    {!! Form::select('vendor', $selected_vendor, array('placeholder' => '- Select Vendor -', 'required' => 'required'), ['class' => 'form-control mb-2 mt-1 placeholder-fix']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('price', 'Price Fee', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-4">
                    {!! Form::number('price', null, ['class' => 'form-control mb-2 mt-1 placeholder-fix', 'required' => 'required', 'placeholder' => 'Price Fee']) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('fee', 'Discription', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::textarea('fee', null, ['class' => 'form-control mb-2 mt-1 placeholder-fix', 'required' => 'required', 'placeholder' => 'Discription']) !!}
                </div>
            </div>
          </div>
        <br>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <strong><sup class="required">*</sup> Required</strong>
        <br/><br/>
        <a href="/arrival" class="btn btn-secondary m-1">Cancel</a>
        <input type="submit" value="Save Data"  class="btn btn-primary m-1">
    </div>
</div>