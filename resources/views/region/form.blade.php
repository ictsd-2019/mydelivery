<div class="row">
    <div class="col-md-6">
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#" style="color:black;">{{$title}}</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row{{ $errors->has('name') ? ' has-error' : '' }} form-group">
                <div class="col-md-4">
                    {!! Form::label('name', 'Region Name', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    {!! Form::text('name', null, ['class' => 'form-control placeholder-fix', 'placeholder' => 'Category Name', 'autocomplete' => 'off', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="row{{ $errors->has('name') ? ' has-error' : '' }} form-group">
                <div class="col-md-4">
                    {!! Form::label('category', 'Category', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    {!! Form::select('category', [	
                                'online' 	=> 'Online',
                                'offline' 	=> 'Offline'
                            ], null, ['class' => 'form-control', 'placeholder' => '- Choose Category -', 'required' => 'required'])
                    !!}
                </div>
            </div>
        </div>
        <br>
    </div>
</div>
