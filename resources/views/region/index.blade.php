@extends('layouts.app')

@section('content')
	<div class="row mb-3">
		<div class="col-md-9">
			<h1 class="title">
				Region
			</h1>
		</div>
		<div class="col-md-3">
			<a href="{{ route('region.create') }}" class="btn btn-success pull-right btn-sm m-1">
				Add Region
			</a>
		</div>
	</div>
	<div clsas="mb-3">
		@if (session('message'))
		<div class="alert alert-success">
			<a href="#" class="close" data-dismiss="alert"
			   aria-label="close">&times;</a> {{ session('message') }}
		</div>
	    @endif
	</div>

    <div class="table-wrapper container" style="overflow-y: hidden; overflow-x: auto;">
        <table id="example" class="table table-striped table-hover" style="width:100%">
            <thead>
                <tr>
                    <th style="text-align:center; width:8%;">No</th>
                    <th style="text-align:center; width:62%;">Region Name</th>
                    <th style="text-align:center; width:25%;">Category</th>
                    <th style="text-align:center; width:5%;">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($regions as $region)
                <tr>
                    <td style="text-align:center;">{{$loop->iteration}}</td>
                    <td>{{ $region->name }}</td>
                    <td style="text-align:center;">{{ $region->category }}</td>
                    <td style="text-align:center;">
                        <a href="{{ route('region.edit', $region->id) }}" data-toggle="tooltip" data-placement="top" title="Tooltip on top"><span class="fa fa-edit"></span></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <style>
    thead input {
        width: 100%;
    }
    th, td { 
        font-size: 12px; 
    }
    </style>
@endsection

@section('scripts')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script>
    $(document).ready(function() {
	    $('#example thead tr').clone(true).appendTo( '#example thead' );
        $('#example thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            if(title === 'Aksi'){
                $(this).html('');
            } else {
                $(this).html( '<input type="text" style="text-align:center;" placeholder="Search" class="placeholder-fix" />' );
    
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                } );
            }
        } );
    
        var table = $('#example').DataTable( {
            orderCellsTop: true,
            fixedHeader: true,
        } );
	} );
    </script>
@endsection