@extends('layouts.app')

@section('content')
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
    @include('layouts.error')
    {!! Form::open(['method' => 'POST', 'route' => 'region.store']) !!}
    @include('region.form')
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('region.index') }}" class="btn btn-secondary btn-sm m-1">Cancel</a>
            <button name="submit" type="submit" class="btn btn-primary btn-sm m-1">Add Region</button>
        </div>
    </div>
    {!! Form::close() !!}
@endsection