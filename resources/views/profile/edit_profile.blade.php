@extends('layouts.app')

@section('sidebar')
    @parent
@endsection

@section('content')
<div id="user-profile-page">
	<h1 class="title">Edit Profile</h1>
	<hr/>
	@include('layouts.error')

	<div class="row">
		<div class="col-md-8">
		{!! Form::model($user, ['method' => 'PATCH', 'action' => ['UserProfileController@update', $user->id]]) !!}
			<div class="row">
				<div class="col-md-2">{!! Form::label('username', 'Username') !!}</div>
				<div class="col-md-9">{!! Form::text('username',null ,['class' => 'form-control m-1', 'disabled' => 'disabled']) !!}</div>
			</div>
			<div class="row">
				<div class="col-md-2">{!! Form::label('fullname', 'Fullname') !!}</div>
				<div class="col-md-9">{!! Form::text('fullname', null,['class' => 'form-control m-1']) !!}</div>
			</div>
			<div class="row">
				<div class="col-md-2">{!! Form::label('nik', 'NIK') !!}</div>
				<div class="col-md-9">{!! Form::text('nik', null,['class' => 'form-control m-1']) !!}</div>
			</div>
			<div class="row">
				<div class="col-md-2">{!! Form::label('email', 'Email') !!}</div>
				<div class="col-md-9">{!! Form::text('email', null,['class' => 'form-control m-1']) !!}</div>
			</div>
			<div class="row">
				<div class="col-md-2">{!! Form::label('phone', 'Phone') !!}</div>
				<div class="col-md-9">{!! Form::text('phone', null,['class' => 'form-control m-1']) !!}</div>
			</div>
			<br/>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-9">
		            <a href="/profile" class="btn btn-secondary m-1">Cancel</a>
		            {!! Form::submit( 'Update' , ['class'  => 'btn btn-primary  ']) !!}
				</div>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
