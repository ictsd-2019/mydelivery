@extends('layouts.frontpage_layout')

@section('sidebar')
    @parent
@endsection

@section('content')
<div id="user-change-password-page">
	<h1 class="title">Reset Password</h1>
	<hr/>
	@if (session('message'))
	<div class="alert alert-{{ session('type') }}">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('message') }}
	</div>
	@endif
		
	@include('layouts.error')

	<div class="row">
		<div class="col-md-8">
		{!! Form::open(['method' => 'POST', 'route' => 'web.profile.reset_password_submit']) !!}
			<div class="row">
				<div class="col-md-3">{!! Form::label('old_password', 'Old Password') !!}</div>
				<div class="col-md-8">{!! Form::password('old_password', ['class' => 'form-control m-1']) !!}</div>
			</div>
			<div class="row">
				<div class="col-md-3">{!! Form::label('new_password', 'New Password') !!}</div>
				<div class="col-md-8">{!! Form::password('new_password',['class' => 'form-control m-1']) !!}</div>
			</div>
			<div class="row">
				<div class="col-md-3">{!! Form::label('new_password_confirmation', 'Confirm New Password') !!}</div>
				<div class="col-md-8">{!! Form::password('new_password_confirmation',['class' => 'form-control m-1']) !!}</div>
			</div>
			<br/>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-8">
		            <a href="{{ route('web.profile') }}" class="btn btn-secondary m-1">Cancel</a>
		            {!! Form::submit( 'Update' , ['class'  => 'btn btn-primary']) !!}
				</div>
			</div>
		{!! Form::close() !!}
		</div>
	</div>		
</div>
@endsection