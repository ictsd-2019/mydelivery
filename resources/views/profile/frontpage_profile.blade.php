@extends('layouts.frontpage_layout')

@section('sidebar')
    @parent
@endsection

@section('content')
	<div id="user-profile-page">
	<h1 class="title">User Profile</h1>
	<div class="btn-group mb-3" role="group">    
		<a href="{{ action('UserProfileController@edit', $user['id']) }}" class="btn btn-success">Edit Profile</a>
		<a href="{{ route('web.profile.reset_password') }}" class="btn btn-success">Change Password</a>
	</div>

	@if (session('message'))
	<div class="alert alert-{{ session('type') }}">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('message') }}
	</div>
	@endif
	
	<br><br>
	<div class="row">
		<div class="col-md-6">
			<div class="card" style="box-shadow: 0 2px 2px rgba(0,0,0,0.1);">
				<div class="card-header">User Info</div>
				<div class="card-body">
					<table class="table table-sm">
						<tr>
							<th><strong>Username</strong></th>
							<td>{{ $user['username'] }}</td>
						</tr>
						<tr>
							<th><strong>Fullname</strong></th>
							<td>{{ $user['fullname'] }}</td>
						</tr>
						<tr>
							<th><strong>NIK</strong></th>
							<td>{{ $user['nik'] }}</td>
						</tr>
						<tr>
							<th><strong>Email</strong></th>
							<td>{{ $user['email'] }}</td>
						</tr>
						<tr>
							<th><strong>Phone</strong></th>
							<td>{{ $user['phone'] }}</td>
						</tr>
						<tr>
							<th><strong>Region</strong></th>
							<td>{{ $user->region['name'] }}</td>
						</tr>
						<tr>
							<th><strong>Role</strong></th>
							<td>{{ $user->roles[0]->name }}</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="card" style="box-shadow: 0 2px 2px rgba(0,0,0,0.1);">
				<div class="card-header">Last Activity</div>
				<div class="card-body">
					<table class="table table-sm">
						<tr><th width="150">Time</th><th>Activity</th></tr>
						@foreach($activities as $activity)
							<tr><td>{{ $activity->created_at }}</td><td>{{ $activity->log }}</td></tr>
						@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection