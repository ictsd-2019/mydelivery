@extends('layouts.app')
@section('content')
    <h1 class="title">Manage Device</h1>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a>
               {{ session('message') }}
        </div>
    @endif
    <div class="btn-group pull-right" role="group">
		<a href="/device/create" class="btn btn-success m-1">Add Device</a>
	</div>
    <br/>
    <div class="activation-history-wrapper container" style="overflow-y: hidden; overflow-x: auto;">
        <table id="example" class="table table-striped table-hover text-center" style="width:230%">
            <thead>
                <tr>
                    <th style="text-align:center; width:2%;">No</th>
                    <th style="text-align:center; width:15%;">Product</th>
                    <th style="text-align:center; width:6%;">Cash 1-100 (Rp)</th>
                    <th style="text-align:center; width:6%;">Cash 101-500 (Rp)</th>
                    <th style="text-align:center; width:6%;">Cash >500 (Rp)</th>
                    <th style="text-align:center; width:5%;">3 Bulan 1-100 (Rp)</th>
                    <th style="text-align:center; width:5%;">3 Bulan 101-500 (Rp)</th>
                    <th style="text-align:center; width:5%;">3 Bulan >500 (Rp)</th>
                    <th style="text-align:center; width:5%;">6 Bulan 1-100 (Rp)</th>
                    <th style="text-align:center; width:5%;">6 Bulan 101-500 (Rp)</th>
                    <th style="text-align:center; width:5%;">6 Bulan >500 (Rp)</th>
                    <th style="text-align:center; width:5%;">12 Bulan 1-100 (Rp)</th>
                    <th style="text-align:center; width:5%;">12 Bulan 101-500 (Rp)</th>
                    <th style="text-align:center; width:5%;">12 Bulan >500 (Rp)</th>
                    <th style="text-align:center; width:5%;">24 Bulan 1-100 (Rp)</th>
                    <th style="text-align:center; width:5%;">24 Bulan 101-500 (Rp)</th>
                    <th style="text-align:center; width:5%;">24 Bulan >500 (Rp)</th>
                    <th style="text-align:center; width:5%;">Aksi</th>
                </tr>
            </thead>
            <!-- <tfoot>
                <tr>
                    <td style="background:lightgrey; text-align:center; width:2%;"></td>
                    <th style="background:lightgrey; text-align:center; width:15%;">Product</th>
                    <th style="background:lightgrey; text-align:center; width:6%;">Cash 1-100</th>
                    <th style="background:lightgrey; text-align:center; width:6%;">Cash 101-500</th>
                    <th style="background:lightgrey; text-align:center; width:6%;">Cash >500</th>
                    <th style="background:lightgrey; text-align:center; width:5%;">3 Bulan 1-100</th>
                    <th style="background:lightgrey; text-align:center; width:5%;">3 Bulan 101-500</th>
                    <th style="background:lightgrey; text-align:center; width:5%;">3 Bulan >500</th>
                    <th style="background:lightgrey; text-align:center; width:5%;">6 Bulan 1-100</th>
                    <th style="background:lightgrey; text-align:center; width:5%;">6 Bulan 101-500</th>
                    <th style="background:lightgrey; text-align:center; width:5%;">6 Bulan >500</th>
                    <th style="background:lightgrey; text-align:center; width:5%;">12 Bulan 1-100</th>
                    <th style="background:lightgrey; text-align:center; width:5%;">12 Bulan 101-500</th>
                    <th style="background:lightgrey; text-align:center; width:5%;">12 Bulan >500</th>
                    <th style="background:lightgrey; text-align:center; width:5%;">24 Bulan 1-100</th>
                    <th style="background:lightgrey; text-align:center; width:5%;">24 Bulan 101-500</th>
                    <th style="background:lightgrey; text-align:center; width:5%;">24 Bulan >500</th>
                    <td style="background:lightgrey; text-align:center; width:5%;"></td>
                </tr>
            </tfoot> -->
        </table>
        <a class="btn btn-warning btn-sm m-1" href="/device/export" style="float:right;"><i class="fa fa-file-excel-o"></i> Download</a>
    </div>
    <style>
    thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    .left {
        text-align: left;
    }
    </style>
@endsection

@section('scripts')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script>
    $(function() {
        var table = $('#example').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/device/json',
            columns: [
                { data: null, orderable: false},
                { data: 'product', className: 'left', name: 'product' },
                { data: 'cash_1', render: $.fn.dataTable.render.number(',', '.', '', '') },
                { data: 'cash_2', render: $.fn.dataTable.render.number(',', '.', '', '') },
                { data: 'cash_3', render: $.fn.dataTable.render.number(',', '.', '', '') },
                { data: 'months_3_1', render: $.fn.dataTable.render.number(',', '.', '', '') },
                { data: 'months_3_2', render: $.fn.dataTable.render.number(',', '.', '', '') },
                { data: 'months_3_3', render: $.fn.dataTable.render.number(',', '.', '', '') },
                { data: 'months_6_1', render: $.fn.dataTable.render.number(',', '.', '', '') },
                { data: 'months_6_2', render: $.fn.dataTable.render.number(',', '.', '', '') },
                { data: 'months_6_3', render: $.fn.dataTable.render.number(',', '.', '', '') },
                { data: 'months_12_1', render: $.fn.dataTable.render.number(',', '.', '', '') },
                { data: 'months_12_2', render: $.fn.dataTable.render.number(',', '.', '', '') },
                { data: 'months_12_3', render: $.fn.dataTable.render.number(',', '.', '', '') },
                { data: 'months_24_1', render: $.fn.dataTable.render.number(',', '.', '', '') },
                { data: 'months_24_2', render: $.fn.dataTable.render.number(',', '.', '', '') },
                { data: 'months_24_3', render: $.fn.dataTable.render.number(',', '.', '', '') },
                { data: 'aksi', name: 'aksi', orderable: false }
            ],
            columnDefs:[{
                targets: -1,
                render: function (data, type, row, meta) {
                    return '<a href="/device/'+ row.id+'/edit" class="btn btn-success btn-sm m-1"><i class="fa fa-edit"></i></a> <a href="/device/'+ row.id+'/delete" onclick="return confirmation();" class="btn btn-primary btn-sm m-1"><i class="fa fa-trash"></i></a>';
                }
            }],
            order: [[1, 'asc']],
        });

        $('#example thead tr').clone(true).appendTo( '#example thead' );
        $('#example thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            if(title === 'No' || title === 'Aksi'){
                $(this).html('');
            } else {
                $(this).html( '<input type="text" style="text-align:center; font-family:system-ui, FontAwesome;" placeholder="&#xF002;" class="placeholder-fix" />' );
    
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                } );
            }
        } );

        table.on('draw.dt', function () {
            var info = table.page.info();
            table.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1 + info.start;
            });
        });

        // $('#example tfoot th').each( function () {
        //     var title = $(this).text();
        //     $(this).html( '<input type="text" style="text-align:center;" placeholder="Search" class="placeholder-fix" />' );
        // } );

        // table.columns().every( function () {
        //     var that = this;
    
        //     $( 'input', this.footer() ).on( 'keyup change', function () {
        //         if ( that.search() !== this.value ) {
        //             that
        //                 .search( this.value )
        //                 .draw();
        //         }
        //     } );
        // } );

        // table.on('draw.dt', function () {
        //     var info = table.page.info();
        //     table.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {
        //         cell.innerHTML = i + 1 + info.start;
        //     });
        // });

        
    });
    </script>
    <script>
        function confirmation() {
            if(!confirm("Are you sure you want to delete this device?"))
            event.preventDefault();
        }
    </script>
@endsection
