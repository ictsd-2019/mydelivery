@extends('layouts.app')

@section('content')
    <h1 class="title">Add Device</h1>
    @if (session('message'))
        <div class="alert alert-{{ session('status') }}">
            <a class="close" onclick="this.parentElement.style.display='none';" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('message') }}
        </div>
    @endif
    @include('layouts.error')
    <br>
    {!! Form::open(['action' => ['DeviceController@store'], 'id' => 'add-device', 'class' => 'form-horizontal']) !!}
        @include('device.form_manage_add_device')
    {!! Form::close() !!}
@endsection
