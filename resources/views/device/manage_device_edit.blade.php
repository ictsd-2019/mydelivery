@extends('layouts.app')

@section('content')
    <h1 class="title">Update Device</h1>
    @include('layouts.error')
    <br>
    {!! Form::open(['action' => ['DeviceController@update', $device->id], 'id' => 'edit-device', 'class' => 'form-horizontal']) !!}
        @include('device.form_manage_edit_device')
    {!! Form::close() !!}
@endsection
