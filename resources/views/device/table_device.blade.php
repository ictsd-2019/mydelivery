<table id="example" class="table table-bordered table-hover dt-responsive text-center" style="width:100%">
    <thead>
        <tr>
            <th style="background:lightgrey; text-align:center; width:1%;">ID Device</th>
            <th style="background:lightgrey; text-align:center; width:14%;">Product</th>
            <th style="background:lightgrey; text-align:center; width:8%;">Cash 1-100</th>
            <th style="background:lightgrey; text-align:center; width:8%;">Cash 101-500</th>
            <th style="background:lightgrey; text-align:center; width:8%;">Cash >500</th>
            <th style="background:lightgrey; text-align:center; width:7%;">3 Bulan 1-100</th>
            <th style="background:lightgrey; text-align:center; width:7%;">3 Bulan 101-500</th>
            <th style="background:lightgrey; text-align:center; width:7%;">3 Bulan >500</th>
            <th style="background:lightgrey; text-align:center; width:7%;">6 Bulan 1-100</th>
            <th style="background:lightgrey; text-align:center; width:7%;">6 Bulan 101-500</th>
            <th style="background:lightgrey; text-align:center; width:7%;">6 Bulan >500</th>
            <th style="background:lightgrey; text-align:center; width:7%;">12 Bulan 1-100</th>
            <th style="background:lightgrey; text-align:center; width:7%;">12 Bulan 101-500</th>
            <th style="background:lightgrey; text-align:center; width:7%;">12 Bulan >500</th>
            <th style="background:lightgrey; text-align:center; width:7%;">24 Bulan 1-100</th>
            <th style="background:lightgrey; text-align:center; width:7%;">24 Bulan 101-500</th>
            <th style="background:lightgrey; text-align:center; width:7%;">24 Bulan >500</th>
        </tr>
    </thead>
    <tbody>
    @foreach($device as $dv)
        <tr>
            <td> {{$dv->id}} </td>
            <td> {{$dv->product}} </td>
            <td> {{$dv->cash_1}} </td>
            <td> {{$dv->cash_2}} </td>
            <td> {{$dv->cash_3}} </td>
            <td> {{$dv->months_3_1}} </td>
            <td> {{$dv->months_3_2}} </td>
            <td> {{$dv->months_3_3}} </td>
            <td> {{$dv->months_6_1}} </td>
            <td> {{$dv->months_6_2}} </td>
            <td> {{$dv->months_6_3}} </td>
            <td> {{$dv->months_12_1}} </td>
            <td> {{$dv->months_12_2}} </td>
            <td> {{$dv->months_12_3}} </td>
            <td> {{$dv->months_24_1}} </td>
            <td> {{$dv->months_24_2}} </td>
            <td> {{$dv->months_24_3}} </td>
        </tr>
    @endforeach
    </tbody>
</table>