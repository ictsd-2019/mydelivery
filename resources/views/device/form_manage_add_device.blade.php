<div class="row">
    <div class="col-md-6">
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#" style="color:black;">Device Info</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('product', 'Device Name', ['class' => 'col-form-label']) !!} <sup class="required">*</sup>
                </div>
                <div class="col-md-8">
                    {!! Form::text('product', null, ['class' => 'form-control mb-2 mt-1 placeholder-fix', 'required' => 'required', 'placeholder' => 'Nama Device']) !!}
                </div>
            </div>
        </div>
        <br>
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#" style="color:black;">Cash Info</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('cash_1', 'Batas 1', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    <div class="input-group">
						<div class="input-group-text">Rp</div>
                        {!! Form::text('cash_1', null, ['class' => 'form-control placeholder-fix', 'placeholder' => '0']) !!}
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('cash_2', 'Batas 2', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    <div class="input-group">
						<div class="input-group-text">Rp</div>
                    {!! Form::text('cash_2', null, ['class' => 'form-control placeholder-fix', 'placeholder' => '0']) !!}
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('cash_3', 'Batas 3', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    <div class="input-group">
						<div class="input-group-text">Rp</div>
                    {!! Form::text('cash_3', null, ['class' => 'form-control placeholder-fix', 'placeholder' => '0']) !!}
                    </div>
                </div>
            </div>
        </div>
        <br>
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#" style="color:black;">Tenor 3 Bulan Info</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('months_3_1', 'Batas 1', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    <div class="input-group">
						<div class="input-group-text">Rp</div>
                       {!! Form::text('months_3_1', null, ['class' => 'form-control placeholder-fix', 'placeholder' => '0']) !!}
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('months_3_2', 'Batas 2', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    <div class="input-group">
						<div class="input-group-text">Rp</div>
                        {!! Form::text('months_3_2', null, ['class' => 'form-control placeholder-fix', 'placeholder' => '0']) !!}
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('months_3_3', 'Batas 3', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    <div class="input-group">
						<div class="input-group-text">Rp</div>
                        {!! Form::text('months_3_3', null, ['class' => 'form-control placeholder-fix', 'placeholder' => '0']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#" style="color:black;">Tenor 6 Bulan Info</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('months_6_1', 'Batas 1', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    <div class="input-group">
						<div class="input-group-text">Rp</div>
                        {!! Form::text('months_6_1', null, ['class' => 'form-control placeholder-fix', 'placeholder' => '0']) !!}
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('months_6_2', 'Batas 2', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    <div class="input-group">
						<div class="input-group-text">Rp</div>
                        {!! Form::text('months_6_2', null, ['class' => 'form-control placeholder-fix', 'placeholder' => '0']) !!}
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('months_6_3', 'Batas 3', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    <div class="input-group">
						<div class="input-group-text">Rp</div>
                        {!! Form::text('months_6_3', null, ['class' => 'form-control placeholder-fix', 'placeholder' => '0']) !!}
                    </div>
                </div>
            </div>
        </div>
        <br>
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#" style="color:black;">Tenor 12 Bulan Info</a>
            </li>
        </ul>
        <div class="tab-content">
        <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('months_12_1', 'Batas 1', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    <div class="input-group">
						<div class="input-group-text">Rp</div>
                       {!! Form::text('months_12_1', null, ['class' => 'form-control placeholder-fix', 'placeholder' => '0']) !!}
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('months_12_2', 'Batas 2', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    <div class="input-group">
						<div class="input-group-text">Rp</div>
                        {!! Form::text('months_12_2', null, ['class' => 'form-control placeholder-fix', 'placeholder' => '0']) !!}
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('months_12_3', 'Batas 3', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    <div class="input-group">
						<div class="input-group-text">Rp</div>
                        {!! Form::text('months_12_3', null, ['class' => 'form-control placeholder-fix', 'placeholder' => '0']) !!}
                    </div>
                </div>
            </div>
        </div>
        <br>
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#" style="color:black;">Tenor 24 Bulan Info</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('months_24_1', 'Batas 1', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    <div class="input-group">
						<div class="input-group-text">Rp</div>
                        {!! Form::text('months_24_1', null, ['class' => 'form-control placeholder-fix', 'placeholder' => '0']) !!}
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('months_24_2', 'Batas 2', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    <div class="input-group">
						<div class="input-group-text">Rp</div>
                        {!! Form::text('months_24_2', null, ['class' => 'form-control placeholder-fix', 'placeholder' => '0']) !!}
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('months_24_3', 'Batas 3', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-md-8">
                    <div class="input-group">
						<div class="input-group-text">Rp</div>
                        {!! Form::text('months_24_3', null, ['class' => 'form-control placeholder-fix', 'placeholder' => '0']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <strong><sup class="required">*</sup> Required</strong>
        <br/><br/>
        <a href="/device" class="btn btn-secondary m-1">Cancel</a>
        <input type="submit" value="Submit"  class="btn btn-primary m-1">
    </div>
</div>