@extends('layouts.app')

@section('sidebar')
    @parent
@endsection

@section('content')
<div class="homepage">
    <div id="summary-info">
        <div class="row">
            <div class="col-md-6">
                <div class="well">
                    <p>Transaction Today</p>
                    <h3><strong><div id="transaction-today">0</div></strong></h3>
                </div>
            </div>
            <div class="col-md-6">
                <div class="well">
                    <p>Device Sales Today</p>
                    <h3><strong><div id="device-sales-today">0</div></strong></h3>
                </div>
            </div>
        </div>
    </div>     
    <div class="card">
        <div class="card-header">
            Recent Transaction
            <a href="/sales/create" class="btn btn-sm btn-primary pull-right add-sales">Add sales</a>
            <a href="/sales" class="btn btn-sm btn-secondary pull-right add-sales">View all</a>
        </div>
        <div class="panel-body">
            <table id="recent-transaction" class="table table-bordered-table-sm">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>MSISDN</th>
                        <th width="30%">Item</th>
                        <th>Package</th>
                        <th>Total</th>
                        <th width="12%">Time</th>
                    </tr>
                </thead>
                <tbody>
                                                           
                </tbody>
            </table>
        </div>
    </div>   
</div>

@endsection

@section('scripts')
    @parent

    <script type="text/javascript">
        $(function(){

            $.ajax({
                url: '/api/sales/today',
                success: function(data)
                {
                    $('#transaction-today').html(data.total);
                    $('#sales-amount-today').html(data.total_amount.formatMoney(0, ',', '.'));
                    $('#device-sales-today').html(data.total);
                }
            });

            $.ajax({
                url: '/api/sales/recent/10',
                success: function(data)
                {
                    var arrayLength = data.items.length;
                    var htmlList = "";

                    for (var i = 0; i < arrayLength; i++) 
                    {
                        htmlList += "<tr>";
                        htmlList += "<td>" + (i+1) +"</td>";
                        htmlList += "<td>" + data.items[i].msisdn + "</td>";
                        htmlList += "<td>" + data.items[i].device_name + "</td>";
                        htmlList += "<td>" + data.items[i].product_name + "</td>";
                        htmlList += "<td>" + data.items[i].payment_total + "</td>";
                        htmlList += "<td>" + data.items[i].transaction_date + "</td>";
                        htmlList += "</tr>";
                    }


                    $("#recent-transaction tbody").html(htmlList);
                }
            });                 
        })
    </script>    
@endsection
