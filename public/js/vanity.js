
$(function()
{
	$("#submit-search ").click(function(){
		var fullNum = $("#search-number").val();

		if(fullNum.length > 2)
		{
			check_number(fullNum);

			setTimeout(function(){
				$("#search-result").fadeIn();
			}, 500);
		}

		if(fullNum.length == 0)
		{
			check_number(fullNum);

			setTimeout(function(){
				$("#search-result").fadeIn();
			}, 500);
		}
	});

	$(".number-delete").click(function(){
		var fullNum = $("#search-number").val().slice(0, -1);

		$('#search-number').val(fullNum);

		if(fullNum.length > 2)
		{		
			check_number(fullNum);
			bind_click_number();
		}

		if(fullNum.length == 0)
		{
			$("#search-result").hide();
			$("#number-slideshow").fadeIn();
		}

	});

	$('.number-clear').click(function(){
		$('#search-number').val("");
	});

	$('.checkout-button').click(function(){

		var cartTotal = parseInt($('.cart-total').val());

		console.log(cartTotal)

		if(cartTotal == 0)
		{
			$('#empty-cart-warning').modal('show');
		}
		else 
		{
			$('#show-cart').modal('show');
		}

	});

	$('.proceed-checkout').click(function(){
		$("#checkout").modal('show');

		$.ajax({
			url: "/api/get/reg_code"
		})
		.done(function(data){
			$('#reg-code').val(data);
			$('#reg-url').html('<a href="'+ window.location.origin +'/reg/'+ data +'">'+ window.location.origin +'/reg/'+ data +'<a/>');
			$('#reg-img').attr('src', 'https://api.qrserver.com/v1/create-qr-code/?size=100x100&data=' + window.location.origin +'/reg/' + data);

			$.ajax({
				url: "/api/process",
				method: "POST",
				data: $('#book-number').serialize()
			})
			.done(function(data){
				console.log(data);
			})
			.fail(function(error){
				console.log(error.responseText);
			});
		});
	});

	bind_click_number();

});

function bind_click_number()
{
	$('a.number-item').click(function(e)
	{
		e.preventDefault();
		var number = $(this).attr('href');

		selectNumber(number);
	});
}

function check_number(num) 
{
	$.ajax({
		url: "/api/number/" + num
	})
	.done(function(data){

		if(data.length > 0)
		{
			var numList = '';

			var i,j,temparray,chunk = 6;
			for (i=0,j=data.length; i<j; i+=chunk) 
			{
			    colNumber = data.slice(i,i+chunk);
			    numList += '';

				for (a = 0; a < colNumber.length; a++)
				{
				    numList += '<li><a href="'+ colNumber[a].number +'" class="number-item">'+ colNumber[a].number +'</a></li>';
				}
			}

			$("#search-result ul").html(numList);

			bind_click_number();
		}
		else
		{	
			$("#search-result ul").html('<li><h4 style="text-align:center">No data found.</h4></li>');
			bind_click_number();
		}
	});	
}

function selectNumber(number)
{
	var currentTotal = parseInt($('.cart-total').val());

	$('.cart-total').val(currentTotal + 1);
	//$('#select-confirm').modal('hide');
	$("a[href="+ number +"]").parent('li').remove();

	$("#book-number").append('<input type="hidden" name="number[]" class="number" value="'+ number +'" />');
	$('#show-cart .modal-body').append('<a class="number-item"><i class="fa fa-phone-square" aria-hidden="true"></i> ' + number + '</a>');
	$('#checkout .modal-body .shopping-items').append('<a class="number-item"><i class="fa fa-phone-square" aria-hidden="true"></i> ' + number + '</a>');
	$('#cart-content').find('span').remove();
	$('#cart-content').find('ul').append('<li><a class="number-item">' + number + '</a></li>');
}
