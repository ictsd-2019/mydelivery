/*
 * Materialize property
*/

$(document).ready(function(){
    $('.sidenav').sidenav();
});

$(".dropdown-trigger").dropdown();

$(document).ready(function(){
    $('.tabs').tabs();
});

$(document).ready(function(){
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });
});

$(document).ready(function(){
    $('select').formSelect();
});

$(document).ready(function(){
    $('.modal').modal();
});

/*
 * ==============================================
*/

/*
 * Resize Image
*/

$("input[type=file]").change(function(){

    var targetDiv = $(this).attr('id');
    readURL(this, '#display-' + targetDiv);
    $('#display-' + targetDiv).show();
});

function handleFiles(f, div, sz = 400)
{
    var o=[];
    for(var i =0;i<f.length;i++)
    {
    var reader = new FileReader();

    reader.onload = (function(theFile) {
        return function(e) {
            var image = new Image();
            image.onload=function(){
                maxWidth = sz;
                maxHeight = sz;
                //manage resizing
                if (image.width >= image.height) {
                    var ratio = 1 / (image.width / maxWidth);
                }
                else {
                    var ratio = 1 / (image.height / maxHeight);
                }
                var scale = 0.15;
                var canvas = document.createElement('canvas');
                canvas.width = image.width * ratio;
                canvas.height = image.height * ratio;
                var context = canvas.getContext('2d');
                context.drawImage(image, 0, 0,image.width, image.height, 0,0,canvas.width,canvas.height);
                var dt = canvas.toDataURL('image/jpeg');
                $(div).val(dt);
            }
            image.src=e.target.result;
        };
    })(f[i]);

    reader.readAsDataURL(f[i]);
    }
}

function readURL(input, target) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(target).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

/*
 * ==================================================
*/
