setInterval(function()
{
    if(navigator.onLine)
    {
        $('#online-status').html('<span class="badge badge-success">Online</span>');
        $('input[type=submit]').attr('disabled', false);
    }
    else
    {
        $('#online-status').html('<span class="badge badge-danger">Offline</span>');
        $('input[type=submit]').attr('disabled', true);
    }
}, 1000);

$(function(){

    $('.chosen-select').chosen();

    getLocation();

});

var date = new Date();
var hour = date.getHours();
var minute = date.getMinutes();
var second = date.getSeconds();

$('#report-time').val(hour +":"+ minute +":"+ second);

$('.datepicker').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
});

function getLocation()
{
    if ("geolocation" in navigator)
    {
        console.log("Map refreshed.");
        navigator.geolocation.getCurrentPosition(function(position){
            $('#audit-longitude').val(position.coords.longitude);
            $('#audit-latitude').val(position.coords.latitude);

            var storeLoc = {lat: position.coords.latitude, lng: position.coords.longitude};
            var map = new google.maps.Map(document.getElementById('location'), {
                zoom: 15,
                center: storeLoc
            });
            var marker = new google.maps.Marker({
                position: storeLoc,
                map: map
            });
        });
    }
    else
    {
        console.log("Browser doesn't support geolocation!");
    }
}

function readURL(input, target) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(target).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("input[type=file]").change(function(){

    var targetDiv = $(this).attr('id');
    readURL(this, '#display-' + targetDiv);
    $('#display-' + targetDiv).show();
});

$(function(){

    $('input[type=hidden]').each(function(){

        if($(this).val() == "1")
        {
            $(this).next().addClass('active');
            $(this).next().addClass('btn-primary');
            $(this).next().removeClass('btn-secondary');
        }

        if($(this).val() == "0")
        {
            $(this).next().next().addClass('active');
            $(this).next().next().addClass('btn-primary');
            $(this).next().next().removeClass('btn-secondary');
        }
    });

    $('button')
        .click(function(e){
            e.preventDefault();
            var pos = $(this).val();
            var target = $(this).data('target');
            $('input[name="'+ target +'"]').val(pos);

            $(this)
                .siblings()
                .removeClass('btn-primary')
                .removeClass('active')
                .addClass('btn-secondary');
            $(this)
                .removeClass('btn-secondary')
                .addClass('btn-primary')
                .addClass('active');
        });

    $('button.pos-material').click(function(e){

        var pos = $(this).val();

        if(pos == "1")
        {
            $('.pos-material-item').show();
        }
        else
        {
            $('.pos-material-item').hide();
        }
    });

    $('button.sis-agent').click(function(e){

        var sis = $(this).val();

        if(sis == "1")
        {
            $('.sis-agent-item').show();
        }
        else
        {
            $('.sis-agent-item').hide();
            $('.sis-agent-item button').removeClass('btn-primary active').addClass('btn-secondary');
            $('input[name=sis_agent_as_schedule]').val("");
            $('#sis_agent_count').val(0);
            $('.sis-agent-item .checkbox input').attr('checked', false);
        }
    });


    $('button.agent').click(function(e){

        var agent = $(this).val();

        if(agent == "1")
        {
            $('.agent-item').show();
        }
        else
        {
            $('.agent-item').hide();
            $('.agent-item button').removeClass('btn-primary active').addClass('btn-secondary');
            $('input[name=agent_as_schedule]').val("");
            $('#agent_count').val(0);
            $('.agent-item .checkbox input').attr('checked', false);
        }
    });

    $('button.competitor-product').click(function(){

        var competitor = $(this).val();

        if(competitor == "1")
        {
            $('.competitor-item').show();
        }
        else
        {
            $('.competitor-item').hide();
            $('.competitor-item button').removeClass('btn-primary active').addClass('btn-secondary');
        }
    });

    $('button.sis-mobilecrm') .click(function(){
        var mobilecrm = $(this).val();

        if(mobilecrm == "1")
        {
            $('.sis-mobilecrm-item').show();
        }
        else
        {
            $('.sis-mobilecrm-item').hide();
            $('.sis-mobilecrm-item button').removeClass('btn-primary active').addClass('btn-secondary');
        }
    });

    $('button.sis-mobilecrm-problem') .click(function(){
        var mobilecrm = $(this).val();

        if(mobilecrm == "0")
        {
            $('.sis-mobilecrm-problem-item').show();
        }
        else
        {
            $('.sis-mobilecrm-problem-item').hide();
            $('.sis-mobilecrm-problem-item button').removeClass('btn-primary active').addClass('btn-secondary');
        }
    });

    $('.show-more-pos-photo').click(function(e){
        e.preventDefault();

        if($(".pos-material-item-additional:hidden").length != 1)
        {
            $(".pos-material-item-additional:hidden").eq(0).show();
        }
        else
        {
            $(".pos-material-item-additional:hidden").eq(0).show();
            $(this).hide();
        }
    });

    $('.show-more-store-photo').click(function(e){
        e.preventDefault();

        if($(".store-photo-additional:hidden").length != 1)
        {
            $(".store-photo-additional:hidden").eq(0).show();
        }
        else
        {
            $(".store-photo-additional:hidden").eq(0).show();
            $(this).hide();
        }
    });

    $('#pjp-form').submit(function(e){

        /* Disabled localStorage
        var obj = $(':input').serializeArray();
        localStorage.setItem('dataArray', JSON.stringify(obj));
        */

        if($('select[name="store"]').val() == "-")
        {
            alert('Silahkan pilih store dahulu');
            return false;
        }

        // Hindari multiple submit
        $(this).submit(function() {
            return false;
        });

        $(this).find('input[type=submit]')
            .removeClass('btn-primary')
            .addClass('btn-warning active')
            .val('Mengirim data...');

        return true;
    });

    /* Submit Activation Status Update */
    $('#activation-status').submit(function(e){
        e.preventDefault();

        $.ajaxSetup({
            headers:{
                'X-XSRF-Token': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type:"POST",
            url:"/api/activation/message",
            data:$(this).serialize(),

            success:function(data){
                $("ul#activation-status-item").empty();
                document.getElementById("activation-status").reset();

                $(".activation-status-label").html('<span class="label label'+ data[data.length -1].status.css_class +'">'+ data[data.length - 1].status.name +'</span>');

                for (var key in data) {
                    var message = (data[key].message != null) ? data[key].message : "-";
                    var type = data[key].status.name;
                    var created = data[key].created_at;
                    var statusItem = "";

                    statusItem += '<li>';
                    statusItem += '<span class="date">' + created +'</span><br/>';
                    statusItem += '<span class="type">'+ type +'</span> : '+ message;

                    if(data[key].attachment)
                    {
                        statusItem += '<div class="status-attachment"><img src="'+ data[key].attachment +'" alt="" class="img-responsive"></div>';
                    }

                    statusItem += '</li>';

                    $("ul#activation-status-item").append(statusItem);
                }

                $("#display-attachment").attr('src', '').css('display', 'none');
                $("#value-attachment").val("");
            },
            error:function(data)
            {
            }
        });
    });

});

function handleFiles(f, div, sz = 600)
{
    var o=[];
    for(var i =0;i<f.length;i++)
    {
        var reader = new FileReader();

        reader.onload = (function(theFile) {
            return function(e) {
                var image = new Image();
                image.onload=function(){
                    maxWidth = sz;
                    maxHeight = sz;
                    //manage resizing
                    if (image.width >= image.height) {
                        var ratio = 1 / (image.width / maxWidth);
                    }
                    else {
                        var ratio = 1 / (image.height / maxHeight);
                    }
                    var scale = 0.15;
                    var canvas = document.createElement('canvas');
                    canvas.width = image.width * ratio;
                    canvas.height = image.height * ratio;
                    var context = canvas.getContext('2d');
                    context.drawImage(image, 0, 0,image.width, image.height, 0,0,canvas.width,canvas.height);
                    var dt = canvas.toDataURL('image/jpeg');
                    $(div).val(dt);
                }
                image.src=e.target.result;
            };
        })(f[i]);

        reader.readAsDataURL(f[i]);
    }
}