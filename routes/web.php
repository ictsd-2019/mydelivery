<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/product','ProductController@index');
Route::get('/preorder','PreorderController@index');


Route::get('signin', 'SigninController@form')->name('signin.form');
Route::post('attempt', 'SigninController@attempt')->name('signin.attempt');

Auth::routes();
/*
Route::group(['middleware' => 'auth'], function ()
{
*/
	Route::group(['prefix' => 'dashboard'], function ()
	{
		Route::get('/webtools','DashboardWebtoolsController@index');
		Route::get('/','DashboardActivationController@index');
		Route::get('stores/{store_name}', ['uses' => 'DashboardWebtoolsController@store_detail']);
		Route::get('stores','DashboardActivationController@get_store_sales_daily');
	});

	Route::get('/', 'HomeController@index')->name('home');
	Route::get('/home', 'HomeController@index');
/*
});
*/
Route::group(['middleware' => 'auth'], function ()
{
	Route::get('/web','FrontPageController@index');
	Route::get('/home', 'HomeController@index')->name('home');

	Route::resource('region', 'RegionController');

	/**
	 * Config
	*/
	Route::get('config','ConfigController@index')->name('config');
	Route::resource('config/user','UserController');
	Route::resource('config/role', 'RoleController');
	Route::resource('config/permission', 'PermissionController');
	Route::resource('config/menu', 'MenuController');

	/**
	 * User Profile
	*/
	Route::get('web/profile', ['as' => 'web.profile', 'uses' => 'UserProfileController@frontpage_profile']);
	Route::get('web/profile/reset_password', ['as' => 'web.profile.reset_password', 'uses' => 'ChangePasswordFrontPageController@reset_password']);
	Route::post('web/profile/riset_password_submit', ['as' => 'web.profile.reset_password_submit', 'uses' => 'ChangePasswordFrontPageController@reset_password_submit']);
	Route::get('profile', 'UserProfileController@index')->name('profile.view');
	Route::get('profile/change_password', 'UserProfileController@change_password')->name('profile.change_password');
	Route::get('profile/{id}/edit', 'UserProfileController@edit');
	Route::patch('profile/{id}', 'UserProfileController@update');
	Route::post('profile/change_password', 'UserProfileController@change_password_submit');

	/**
	 * Product
	*/
	Route::get('/product','ProductController@index');
	
	/**
	 * Pre-Order
	 */
	Route::resource('preorder/manage', 'PreOrderController', ['as' => 'preorder']);
	Route::get('preorder/manage/{preorder_id}/transactions','PreOrderController@transactions')->name('preorder.manage.transactions');
	Route::get('preorder/manage/{preorder_id}/store-report','PreOrderController@store_report')->name('preorder.manage.store_report');
	Route::post('preorder/confirm-payment', 'PreOrderTransactionController@confirm_payment');
	Route::post('preorder/restore-transaction', 'PreOrderTransactionController@restore_trans');
	Route::resource('preorder','PreOrderTransactionController');

	/**
	 * Sales Order
	 */
	Route::get('/web/salesorder','SalesorderController@index');
	Route::get('/web/salesorder/all','SalesorderController@index_all');
	Route::get('/web/salesorder/export', ['uses' => 'SalesorderController@export']);
	Route::get('/web/salesorder/create','SalesorderController@create');
	Route::post('/web/salesorder','SalesorderController@store');
	Route::get('/web/salesorder/{salesorder_id}', ['uses' => 'SalesorderController@show']);
	Route::get('/web/salesorder/{salesorder_id}/edit', ['uses' => 'SalesorderController@edit']);
	Route::post('/web/salesorder/{salesorder_id}/update', ['uses' => 'SalesorderController@update']);
	Route::get('/web/salesorder/{salesorder_id}/delete', ['uses' => 'SalesorderController@destroy']);
	Route::get('/web/salesorder/{salesorder_code}/attachment', ['uses' => 'SalesorderController@add_attachment']);
	Route::post('/web/salesorder/{salesorder_code}/store-attachment', 'SalesorderController@store_attachment');
	Route::get('/web/salesorder/{attachment_id}/delete-attachment', ['uses' => 'SalesorderController@destroy_attachment']);
	Route::get('/web/salesorder/{salesorder_code}/verification', ['uses' => 'SalesorderController@verification']);
	Route::get('/web/salesorder/{salesorder_id}/revision', ['uses' => 'SalesorderController@revision']);
	Route::get('/web/salesorder/{salesorder_id}/submit', ['uses' => 'SalesorderController@submit']);
	Route::get('/web/salesorder/{salesorder_id}/reject', ['uses' => 'SalesorderController@reject']);
	Route::get('/web/salesorder/{salesorder_id}/reverification', ['uses' => 'SalesorderController@reverification']);

	Route::get('/web/quotation/json', ['uses' => 'SalesorderController@json']);
	Route::post('/data/salesorder','SalesorderController@getQuotation')->name('dataSalesorder');

	Route::get('/salesorder',['uses' => 'SalesorderController@index_manage']);
	Route::get('/salesorder/{salesorder_id}', ['uses' => 'SalesorderController@show_manage']);
	Route::get('/salesorder/{salesorder_id}/pick', ['uses' => 'SalesorderController@pick_salesorder']);
	Route::get('/salesorder/{salesorder_id}/activation', ['uses' => 'SalesorderController@activation_salesorder']);
	Route::get('/salesorder/{salesorder_id}/activated', ['uses' => 'SalesorderController@activated_salesorder']);
	// Route::post('/salesorder/{salesorder_id}/change-update', ['uses' => 'SalesorderController@save_change_salesorder']);
	// hatihati ini
	// Route::get('/salesorder/{salesorder_id}/check', ['uses' => 'SalesorderController@check_salesorder']);
	Route::get('/salesorder/{salesorder_id}/done', ['uses' => 'SalesorderController@done_salesorder']);

	Route::post('/salesorder/{salesorder_id}/remarks', ['uses' => 'SalesorderRemarksController@store_remarks']);
	Route::get('/salesorder/{salesorder_id}/check-device', ['uses' => 'SalesorderController@check_device']);
	Route::post('/salesorder/{salesorder_id}/update-device', ['uses' => 'SalesorderController@update_device']);
	Route::post('/salesorder/export/param', ['uses' => 'SalesorderController@export_param']);

	/**
	 * Sales Order Transaction
	 */
	Route::post('/web/salesorder/{salesorder_id}', 'SalesorderTransactionController@store');
	Route::get('/web/salesorder/transaction/{transaction_id}/edit', ['uses' => 'SalesorderTransactionController@edit']);
	Route::post('/web/salesorder/transaction/{transaction_id}/update', ['uses' => 'SalesorderTransactionController@update']);
	Route::get('/web/salesorder/transaction/{transaction_id}/delete', ['uses' => 'SalesorderTransactionController@destroy']);
	Route::post('/web/salesorder/transaction/{salesorder_id}/import', ['uses' => 'SalesorderTransactionController@import']);
	Route::get('/web/salesorder/transaction/{salesorder_id}/export', ['uses' => 'SalesorderTransactionController@export']);
	Route::get('/web/salesorder/transaction/{salesorder_id}/request-sim-card', ['uses' => 'SalesorderTransactionController@request_sim']);	

	Route::get('/salesorder/transaction/{salesorder_id}/create', ['uses' => 'SalesorderTransactionController@create_manage']);
	Route::post('/salesorder/transaction/{salesorder_id}/store', ['uses' => 'SalesorderTransactionController@store_manage']);
	Route::get('/salesorder/transaction/{transaction_id}/edit', ['uses' => 'SalesorderTransactionController@edit_manage']);
	Route::post('/salesorder/transaction/{transaction_id}/update', ['uses' => 'SalesorderTransactionController@update_manage']);
	Route::get('/salesorder/transaction/{transaction_id}/delete', ['uses' => 'SalesorderTransactionController@destroy_manage']);

	/**
	 * Sales Order Shipment
	 */
	Route::get('/salesorder/shipment/{salesorder_id}/create', ['uses' => 'SalesorderShipmentController@create']);
	Route::post('/salesorder/shipment/{salesorder_id}/store', ['uses' => 'SalesorderShipmentController@store']);
	Route::get('/salesorder/shipment/{shipment_id}/edit', ['uses' => 'SalesorderShipmentController@edit']);
	Route::post('/salesorder/shipment/{shipment_id}/update', ['uses' => 'SalesorderShipmentController@update']);
	Route::get('/delivery/courier/shipment/{shipment_id}', 'SalesorderShipmentController@courier');
	Route::get('/shipment/export/{ship_id}', 'SalesorderShipmentController@export');
	
	/**
	 * Sales Order DR
	 */
	Route::get('/salesorder/delivery/{salesorder_id}/create', ['uses' => 'SalesorderDeliveryRequestController@create']);
	Route::post('/salesorder/delivery/{salesorder_id}/store', ['uses' => 'SalesorderDeliveryRequestController@store']);
	Route::get('/salesorder/delivery/{so_dr_id}/edit', ['uses' => 'SalesorderDeliveryRequestController@edit']);
	Route::post('/salesorder/delivery/{so_dr_id}/update', ['uses' => 'SalesorderDeliveryRequestController@update']);

// paling atas ini bisa diapus nanti
	Route::get('/delivery/export/{so_dr_id}', 'SalesorderDeliveryRequestController@export');
	Route::post('/salesorder/delivery/{so_dr_id}/verification', ['uses' => 'SalesorderDeliveryRequestController@verification']);
	// Route::post('/salesorder/delivery/{so_dr_id}/signed', ['uses' => 'SalesorderDeliveryRequestController@signed']);
	Route::get('/delivery/courier/dr/{so_dr_id}', 'SalesorderDeliveryRequestController@courier');


	Route::get('/salesorder/delivery/{so_dr_id}/edit/edit', ['uses' => 'SalesorderDeliveryRequestController@edit_edit']);
	Route::post('/salesorder/delivery/{so_dr_id}/update/edit', ['uses' => 'SalesorderDeliveryRequestController@update_edit']);


	/**
	 * Delivery Request
	 */
	Route::get('/delivery/{so_dr_id}', ['uses' => 'DeliveryRequestController@index']);
	Route::post('/delivery/{salesorder_id}/store', ['uses' => 'DeliveryRequestController@store']);
	Route::post('/delivery/{dr_id}/update', ['uses' => 'DeliveryRequestController@update']);
	Route::get('/delivery/{dr_id}/delete', ['uses' => 'DeliveryRequestController@destroy']);
	Route::post('/delivery/import/{so_dr_id}', 'SalesorderDeliveryRequestController@import');

	Route::post('/delivery/{salesorder_id}/store/edit', ['uses' => 'DeliveryRequestController@store_edit']);
	Route::post('/delivery/{dr_id}/edit/update', ['uses' => 'DeliveryRequestController@update_edit']);
	Route::get('/delivery/{dr_id}/edit/delete', ['uses' => 'DeliveryRequestController@destroy_edit']);
	Route::post('/delivery/import/{so_dr_id}/edit', 'SalesorderDeliveryRequestController@import_edit');

	/**
	 * Manage Device
	 */
	
	Route::get('/device', ['uses' => 'DeviceController@index']);
	Route::get('/device/create', ['uses' => 'DeviceController@create']);
	Route::post('/device', ['uses' => 'DeviceController@store']);
	Route::get('/device/{device_id}/edit', ['uses' => 'DeviceController@edit']);
	Route::post('/device/{device_id}/update', ['uses' => 'DeviceController@update']);
	Route::get('/device/{device_id}/delete', ['uses' => 'DeviceController@destroy']);
	Route::get('/device/export', ['uses' => 'DeviceController@export']);

	Route::get('/device/json','DeviceController@json');
	Route::post('/data/device','DeviceController@getDevice')->name('dataDevice');

	/**
	 * Manage Sales Agent
	 */
	Route::get('/sales', ['uses' => 'SalesAgentController@index']);
	Route::get('/sales/create', ['uses' => 'SalesAgentController@create']);
	Route::post('/sales', ['uses' => 'SalesAgentController@store']);
	Route::get('/sales/{sales_id}/edit', ['uses' => 'SalesAgentController@edit']);
	Route::post('/sales/{sales_id}/update', ['uses' => 'SalesAgentController@update']);
	Route::get('/sales/{sales_id}/delete', ['uses' => 'SalesAgentController@destroy']);
	Route::get('/sales/export', ['uses' => 'SalesAgentController@export']);

	/**
	 * Manage Basic Package
	 */
	Route::get('/package', ['uses' => 'PackageBasicController@index']);
	Route::get('/package/create', ['uses' => 'PackageBasicController@create']);
	Route::post('/package', ['uses' => 'PackageBasicController@store']);
	Route::get('/package/{package_id}/edit', ['uses' => 'PackageBasicController@edit']);
	Route::post('/package/{package_id}/update', ['uses' => 'PackageBasicController@update']);
	Route::get('/package/{package_id}/delete', ['uses' => 'PackageBasicController@destroy']);
	Route::get('/package/export', ['uses' => 'PackageBasicController@export']);

	/**
	 * Manage Add On Package
	 */
	Route::get('/addon', ['uses' => 'PackageAddOnController@index']);
	Route::get('/addon/create', ['uses' => 'PackageAddOnController@create']);
	Route::post('/addon', ['uses' => 'PackageAddOnController@store']);
	Route::get('/addon/{addon_id}/edit', ['uses' => 'PackageAddOnController@edit']);
	Route::post('/addon/{addon_id}/update', ['uses' => 'PackageAddOnController@update']);
	Route::get('/addon/{addon_id}/delete', ['uses' => 'PackageAddOnController@destroy']);
	Route::get('/addon/export', ['uses' => 'PackageAddOnController@export']);

	/**
	 * Manage Employee
	 */
	Route::get('/employee', ['uses' => 'EmployeeController@index']);
	Route::get('/employee/create', ['uses' => 'EmployeeController@create']);
	Route::post('/employee', ['uses' => 'EmployeeController@store']);
	Route::get('/employee/{employee_id}/edit', ['uses' => 'EmployeeController@edit']);
	Route::post('/employee/{employee_id}/update', ['uses' => 'EmployeeController@update']);
	Route::get('/employee/{employee_id}/delete', ['uses' => 'EmployeeController@destroy']);
	Route::get('/employee/export', ['uses' => 'EmployeeController@export']);

	/**
	 * Manage M2M
	 */


	/**
	 * Approval DR
	*/
	Route::get('/approval-dr', ['uses' => 'VerificationController@index_delivery_request']);
	Route::get('/approval-dr/{so_dr_id}/reject', ['uses' => 'VerificationController@reject_dr']);
	Route::get('/approval-dr/{so_dr_id}/approve', ['uses' => 'VerificationController@approve_dr']);

	/**
	 * Courier Sign
	*/
	Route::get('/courier/sign-bast', ['uses' => 'CourierController@index']);
	Route::get('/courier/sign-bast/{code_id}/edit', ['uses' => 'CourierController@edit']);
	Route::post('/courier/sign-bast/{code_id}/update', ['uses' => 'CourierController@update']);

	/**
	 * BAST Signed
	 */
	Route::get('/bast/export/{id_code}', ['uses' => 'CourierController@export']);

	/**
	 * Action PriceBook
	 */
	Route::get('/employee', ['uses' => 'EmployeeController@index']);
	Route::get('/employee/create', ['uses' => 'EmployeeController@create']);
	Route::post('/employee', ['uses' => 'EmployeeController@store']);
	Route::get('/employee/{employee_id}/edit', ['uses' => 'EmployeeController@edit']);
	Route::post('/employee/{employee_id}/update', ['uses' => 'EmployeeController@update']);
	Route::get('/employee/{employee_id}/delete', ['uses' => 'EmployeeController@destroy']);
	//Route::get('/employee/export', ['uses' => 'EmployeeController@export']);


	 /**
	 * Action type scope
	 */
	Route::get('/type', ['uses' => 'TypeController@index']);
	Route::get('/type/create', ['uses' => 'TypeController@create']);
	Route::post('/type', ['uses' => 'TypeController@store']);
	Route::get('/type/{type_id}/edit', ['uses' => 'TypeController@edit']);
	Route::post('/type/{type_id}/update', ['uses' => 'TypeController@update']);
	Route::get('/type/{type_id}/delete', ['uses' => 'TypeController@destroy']);


	 /**
	 * Action Arrival Fee
	 */
	Route::get('/arrival', ['uses' => 'ArrivalController@index']);
	Route::get('/arrival/create', ['uses' => 'ArrivalController@create']);
	Route::post('/arrival', ['uses' => 'ArrivalController@store']);
	Route::get('/arrival/{id}/edit', ['uses' => 'ArrivalController@edit']);
	Route::post('/arrival/{id}/update', ['uses' => 'ArrivalController@update']);
	Route::get('/arrival/{id}/delete', ['uses' => 'ArrivalController@destroy']);

	 /**
	 * Action Main Activity
	 */
	Route::get('/main', ['uses' => 'MainActivityController@index']);
	Route::get('/main/create', ['uses' => 'MainActivityController@create']);
	Route::post('/main', ['uses' => 'MainActivityController@store']);
	Route::get('/main/{id}/edit', ['uses' => 'MainActivityController@edit']);
	Route::post('/main/{id}/update', ['uses' => 'MainActivityController@update']);
	Route::get('/main/{id}/delete', ['uses' => 'MainActivityController@destroy']);


	 /**
	 * Action Detail Activity
	 */
	Route::get('/detail', ['uses' => 'DetailActivityController@index']);
	Route::get('/detail/create', ['uses' => 'DetailActivityController@create']);
	Route::post('/detail', ['uses' => 'DetailActivityController@store']);
	Route::get('/detail/{id}/edit', ['uses' => 'DetailActivityController@edit']);
	Route::post('/detail/{id}/update', ['uses' => 'DetailActivityController@update']);
	Route::get('/detail/{id}/delete', ['uses' => 'DetailActivityController@destroy']);


	 /**
	 * Action Task
	 */
	Route::get('/task', ['uses' => 'TaskController@index']);
	Route::get('/task/create', ['uses' => 'TaskController@create']);
	Route::post('/task', ['uses' => 'TaskController@store']);
	Route::get('/task/{task_id}/edit', ['uses' => 'TaskController@edit']);
	Route::post('/task/{task_id}/update', ['uses' => 'TaskController@update']);
	Route::get('/task/{task_id}/delete', ['uses' => 'TaskController@destroy']);

	/**
	 * Action Price Book
	 */

	Route::get('/book', ['uses' => 'BookController@index']);
	Route::get('/book/create', ['uses' => 'BookController@create']);
	Route::post('/book', ['uses' => 'BookController@store']);
	Route::get('/book/{book_id}/edit', ['uses' => 'BookController@edit']);
	Route::post('/book/{book_id}/update', ['uses' => 'BookController@update']);
	Route::get('/book/{book_id}/delete', ['uses' => 'BookController@destroy']);

});

// URL::forceScheme('https');